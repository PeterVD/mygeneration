namespace Dnp.Utils
{
    /// <summary>
    /// This class can be used to display progress messages
    /// and/or a ProgressBar in Template Code while a template
    /// executes.
    /// </summary>
    /// <example>
    /// Windows Forms elements can be used in the Template Code
    /// portion of a template to display during template execution.
    /// This class contains a Dialog with a ProgressBar.
    /// It can be used to display progress messages and update the
    /// ProgressBar during execution.
    /// See the ProgressBar Example templates in the
    /// DNP.PluginExamples namespace for more details.
    /// The top line of the Template Code section with
    /// REFERENCE and NAMESPACE must be on one line with
    /// the trailing '&lt;%'
    /// <para>
    /// The following code loops the tables in a database and
    /// updates the ProgressBar for each table. The ProgressBar
    /// title is changed to display the current table alias.
    /// </para>
    /// <code>
    /// Dnp.Utils.ProgressDialog pd = new Dnp.Utils.ProgressDialog();
    /// 
    /// pd.ProgressBar.Minimum = 1;
    /// pd.ProgressBar.Maximum = Tables.Count;
    /// pd.ProgressBar.Value = 1;
    /// pd.Show();
    /// 
    /// foreach(string tableName in Tables)
    /// {
    /// 	tableMeta = MyMeta.Databases[DatabaseName].Tables[tableName];
    /// 	
    /// 	// Change the progress message in the caption.
    /// 	pd.Text = "Processing " + tableMeta.Alias + "...";
    /// 
    /// 	// Do something useful here.
    /// 
    /// 	// Update the ProgressBar
    /// 	if(pd.ProgressBar.Value &lt; Tables.Count)
    /// 	{
    /// 		pd.ProgressBar.Value += 1;
    /// 	}
    /// }
    /// pd.Close();
    /// </code>
    /// </example>
    public class ProgressDialog : System.Windows.Forms.Form
    {
        /// <summary>
        /// Used to display a ProgressBar in Template Code
        /// while a template executes.
        /// </summary>
        public System.Windows.Forms.ProgressBar ProgressBar;

        /// <summary>
        /// This class can be used to display progress messages
        /// and/or a ProgressBar in Template Code while a template
        /// executes.
        /// </summary>
        /// <example>
        /// See the ProgressBar Example templates in the
        /// DNP.PluginExamples namespace for more details.
        /// The top line of the Template Code section with
        /// REFERENCE and NAMESPACE must be on one line with
        /// the trailing '&lt;%'
        /// <code>
        /// Dnp.Utils.ProgressDialog pd = new Dnp.Utils.ProgressDialog();
        /// 
        /// pd.ProgressBar.Minimum = 1;
        /// pd.ProgressBar.Maximum = Tables.Count;
        /// pd.ProgressBar.Value = 1;
        /// pd.Show();
        /// 
        /// foreach(string tableName in Tables)
        /// {
        /// 	tableMeta = MyMeta.Databases[DatabaseName].Tables[tableName];
        /// 	
        /// 	// Change the progress message in the caption.
        /// 	pd.Text = "Processing " + tableMeta.Alias + "...";
        /// 
        /// 	// Do something useful here.
        /// 
        /// 	// Update the ProgressBar
        /// 	if(pd.ProgressBar.Value &lt; Tables.Count)
        /// 	{
        /// 		pd.ProgressBar.Value += 1;
        /// 	}
        /// }
        /// pd.Close();
        /// </code>
        /// </example>
        public ProgressDialog()
        {
            InitializeComponent();
        }

        private void InitializeComponent() {
            this.ProgressBar = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // ProgressBar
            // 
            this.ProgressBar.Location = new System.Drawing.Point(16, 8);
            this.ProgressBar.Name = "ProgressBar";
            this.ProgressBar.Size = new System.Drawing.Size(320, 23);
            this.ProgressBar.TabIndex = 0;
            // 
            // ProgressDialog
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(344, 50);
            this.Controls.Add(this.ProgressBar);
            this.ControlBox = false;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ProgressDialog";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Progress Dialog";
            this.ResumeLayout(false);
        }
    }
}