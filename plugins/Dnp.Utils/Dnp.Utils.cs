/*
 * Created by David Parsons.
 * Date: 11/4/2005
 * Dnp.Utils.dll is a MyGeneration 1.1 plugin which
 * provides easy access to commonly used template functions.
 *
 * Revised - 11/5/2005
 * Mike Griffin contributed a template cache.
 * 
 * Revised - 11/12/2005
 * Added ReadUserMap()
 * 
 * Revised - 11/20/2005
 * Angelo Hulshout contributed the GentleRelation class
 * from his Gentle.NET template (renamed to TableRelation)
 * which was derived from Justin Greenwood's Hierarchical template.
 * 
 * Revised - 12/1/2005
 * jcfigueiredo suggested some additional properties which
 * were added to the TableRelation class.
 * 
 * Revised - 12/17/2005
 * Added the ProgressDialog class.
 * 
 * Revised - 12/18/2005
 * Added overloads to TrimSpaces, SetCamelCase, and SetPascalCase
 * to trim addtional characters.
 * 
 * Revised - 1/5/2006
 * Greatly expanded the XML comments for the compiled help.
 * 
 * Revised - 1/6/2006
 * Added RelType Enum and RelationType Property.
 * 
 * Revised - 1/8/2006
 * Added VB.NET example code to the compiled help.
 * 
 * Revised - 5/8/2006
 * Added GetOrderedTableList.
 * 
 * Revised - 6/18/2006
 * Added Experimental method CreateTableRelation
 * to support templates in VBScript and JScript.
 * Works for JScript, but not for VBScript.
 */

using System;
using System.IO;
using System.Xml;
using System.Collections;
using Zeus;
using MyMeta;
using System.Runtime.InteropServices;

namespace Dnp.Utils
{
	/// <summary>
	/// MyGeneration helper functions. Starting with MyGeneration 1.1.5
	/// the plugin is included with the download and installed for you.
	/// </summary>
	/// <remarks>
	/// The latest version of MyGeneration can be found at
	/// <code>http://www.mygenerationsoftware.com.</code>
	/// The latest version of the plugin is in this Template Archive:
	/// <code>http://www.mygenerationsoftware.com/TemplateLibrary/Archive/?guid=4a285a9a-4dd2-4655-b7ca-996b5516a5a1</code>
	/// The archive also contains additional explanation of the functions included,
	/// source code, and many example templates demonstrating how to use the plugin.
	/// </remarks>
	/// <example>
	/// <para>The following instructions only apply to MyGeneration 1.1.4:</para>
	/// Copy Dnp.Utils.dll to your MyGeneration program folder.
	/// <para>
	/// Add this line to ZeusConfig.xml:
	/// </para>
	/// <code>
	/// &lt;IntrinsicObject assembly="%ZEUSHOME%\Dnp.Utils.dll" classpath="Dnp.Utils.Utils" varname="DnpUtils" /&gt;
	/// </code>
	/// </example>
    [ComVisible(true)]
	public class Utils
	{	
		#region Naming Functions
		/// <summary>
		/// Trim spaces from a string.
		/// </summary>
		public string TrimSpaces(string name)
		{
			return name.Replace(" ", "");
		}
	
		/// <summary>
		/// Trim spaces from a string.
		/// Pass in a char[] containing a list of additional characters
		/// to remove. Spaces are always trimmed.
		/// </summary>
		/// <example>
		/// <code>
		/// char[] c = new char[5] {'\\', '@', ',', '-', ':'};
		///	string str = DnpUtils.TrimSpaces(Table.Alias, c);
		/// </code>
		/// </example>
		/// <param name="name">The string that needs to be adjusted.</param>
		/// <param name="trimList">char[] of characters to be trimmed.</param>
		/// <returns>A string with all listed characters trimmed. </returns>
		public string TrimSpaces(string name, char[] trimList)
		{
			for (int i = 0; i < trimList.GetLength(0); i++)
			{
				name = name.Replace(trimList[i], ' ');
			}
			
			return this.TrimSpaces(name);
		}
	
		/// <summary>
		/// SetPascalCase sets the first character to upper case,
		/// trims spaces, underscores, and periods, and sets next to upper.
		/// (PascalCase is sometimes referred to as UpperCamelCase.)
		/// </summary>
		/// <example>
		/// <code>
		/// string tableName = DnpUtils.SetPascalCase("MY_TABLE_NAME");
		/// </code>
		/// The result is "MyTableName"
		/// </example>
		public string SetPascalCase(string name)
		{
			string buff = "";
			bool next2upper = true;
			bool allUpper = true;
			
			// checks for names in all CAPS
			foreach(char c in name)
			{
				if(Char.IsLower(c))
				{
					allUpper = false;
					break;
				}
			}
			
			foreach(char c in name)
			{
				switch(c)
				{
					// Trim spaces
					case ' ':
						next2upper = true;
						break;
					// Trim underscores
					case '_':
						next2upper = true;
						break;
					// Trim periods
					case '.':
						next2upper = true;
						break;
					default:
						if(next2upper)
						{
							buff += c.ToString().ToUpper();
							next2upper= false;
						}
						else if(allUpper)
						{
							buff += c.ToString().ToLower();
						}
						else
						{
							buff += c.ToString();
						}
						break;
				}
			}
		
			return buff;
		}

		/// <summary>
		/// SetPascalCase sets the first character to upper case,
		/// trims spaces, underscores, and periods, and sets next to upper.
		/// (PascalCase is sometimes referred to as UpperCamelCase.)
		/// Pass in a char[] containing a list of additional characters
		/// to remove. Spaces, underscores, and periods are always trimmed.
		/// </summary>
		/// <example>
		/// <code>
		/// char[] c = new char[5] {'\\', '@', ',', '-', ':'};
		///	string str = DnpUtils.SetPascalCase(Table.Alias, c);
		/// </code>
		/// </example>
		/// <param name="name">The string that needs to be adjusted.</param>
		/// <param name="trimList">char[] of characters to be trimmed.</param>
		/// <returns>A string with all listed characters trimmed. </returns>
		public string SetPascalCase(string name, char[] trimList)
		{
			for (int i = 0; i < trimList.GetLength(0); i++)
			{
				name = name.Replace(trimList[i], ' ');
			}
			
			return this.SetPascalCase(name);
		}
	
		/// <summary>
		/// SetCamelCase sets the first character to lower case,
		/// trims spaces, underscores, and periods, and sets next to upper.
		/// (CamelCase in this context refers to the lowerCamelCase convention.)
		/// </summary>
		/// <example>
		/// <code>
		/// string tableName = DnpUtils.SetCamelCase("MY_TABLE_NAME");
		/// </code>
		/// The result is "myTableName"
		/// </example>
		public string SetCamelCase(string name)
		{
			string buff = "";
			bool next2upper = false;
			bool firstLetter = true;
			bool allUpper = true;
			
			// checks for names in all CAPS
			foreach(char c in name)
			{
				if(Char.IsLower(c))
				{
					allUpper = false;
					break;
				}
			}
			
			foreach(char c in name)
			{
				switch(c)
				{
					// Trim spaces
					case ' ':
						if(!firstLetter)
						{
							next2upper = true;
						}
						break;
					// Trim underscores
					case '_':
						if(!firstLetter)
						{
							next2upper = true;
						}
						break;
					// Trim periods
					case '.':
						if(!firstLetter)
						{
							next2upper = true;
						}
						break;
					default:
						if(next2upper)
						{
							buff += c.ToString().ToUpper();
							next2upper= false;
						}
						else
						{
							if(firstLetter)
							{
								buff += c.ToString().ToLower();
								firstLetter = false;
							}
							else if(allUpper)
							{
								buff += c.ToString().ToLower();
							}
							else
							{
								buff += c.ToString();
							}
						}
						
						break;
				}
			}
		
			return buff;
		}
		
		/// <summary>
		/// SetCamelCase sets the first character to lower case,
		/// trims spaces, underscores, and periods, and sets next to upper.
		/// (CamelCase in this context refers to the lowerCamelCase convention.)
		/// Pass in a char[] containing a list of additional characters
		/// to remove. Spaces, underscores, and periods are always trimmed.
		/// </summary>
		/// <example>
		/// <code>
		/// char[] c = new char[5] {'\\', '@', ',', '-', ':'};
		///	string str = DnpUtils.SetCamelCase(Table.Alias, c);
		/// </code>
		/// </example>
		/// <param name="name">The string that needs to be adjusted.</param>
		/// <param name="trimList">char[] of characters to be trimmed.</param>
		/// <returns>A string with all listed characters trimmed. </returns>
		public string SetCamelCase(string name, char[] trimList)
		{
			for (int i = 0; i < trimList.GetLength(0); i++)
			{
				name = name.Replace(trimList[i], ' ');
			}
			
			return this.SetCamelCase(name);
		}
	
		/// <summary>
		/// Used to construct an output file name.
		/// </summary>
		/// <example>
		/// <code>
		/// string outputPath = "C:\\Output";
		/// string fileName = "Order Details";
		/// bool prefix = true;
		/// bool trim = true;
		/// string fullFileName = 
		///     DnpUtils.GetOutputFile(outputPath, fileName, ".cs", prefix, trim);
		/// </code>
		/// The result is "C:\Output\_OrderDetails.cs"
		/// </example>
		/// <param name="path">The path (with or without the trailing '\')</param>
		/// <param name="fileName">The name of the file (without the extension)</param>
		/// <param name="suffix">the file extension (with the dot), e.g. '.cs'</param>
		/// <param name="prefix">Set to true if you want a preceding underscore</param>
		/// <param name="trimName">Set to true to TrimSpaces() from the fileName</param>
		/// <returns>A string containing the concatenated path, prefix, filename, and suffix</returns>
		public string GetOutputFile(string path, string fileName, string suffix, bool prefix, bool trimName)
		{
			string buff = path;
			
			if (!buff.EndsWith("\\") )
			{
				buff += "\\";
			}

			if(prefix)
			{
				if(trimName)
				{
					buff += "_" + TrimSpaces(fileName);
				}
				else
				{
					buff += "_" + fileName;
				}
			}
			else 
			{
				if(trimName)
				{
					buff += TrimSpaces(fileName);
				}
				else
				{
					buff += fileName;
				}
			}
			
			buff += suffix;
			
			return buff;
		}
		#endregion		

		#region Caching Functions
		static Hashtable cache = new Hashtable();

		/// <summary>
		/// This caches template input to be retrieved with <see cref="ReadInputFromCache"/>.
		/// Thanks to Mike Griffin for contributing these.
		/// </summary>
		/// <example>
		/// At the top of your ui code you add this:
		/// <code>
		///	Sub setup()
		///	   DnpUtils.ReadInputFromCache(context);
		///	   ....
		///	End Sub
		/// </code>
		/// Then at the top of your template body you add:
		/// <code>
		/// 	DnpUtils.SaveInputToCache(context);
		/// </code>
		/// After you do that try running your template,
		/// it will come up with the last settings you had (no project files required)
		/// as long as you haven't closed down MyGeneration.
		/// You can even alternate between two templates and it will remember
		/// the input for each.
		/// </example>
		public void SaveInputToCache(IZeusContext theContext)
		{
			Hashtable h;
		
			if(cache.ContainsKey(theContext.ExecutingTemplate.UniqueID.ToString()))
			{
				h = cache[theContext.ExecutingTemplate.UniqueID.ToString()] as Hashtable;
				h.Clear();
			}
			else
			{
				h = new Hashtable();
				cache[theContext.ExecutingTemplate.UniqueID.ToString()] = h;			
			}	
			
			foreach(string key in theContext.Input.Keys)
			{
				switch(key)
				{
					// Eliminate the default input variables
					case "__dbDriver":
					case "dbDriver":
					case "__language":
					case "__domainOverride":
					case "__dbConnectionString":
					case "defaultTemplatePath":
					case "__defaultOutputPath":
					case "__dbTarget":
					case "__dbTargetMappingFileName":
					case "__dbLanguageMappingFileName":
					case "defaultOutputPath":
					case "__userMetaDataFileName":
					case "__defaultTemplatePath":
					case "__version":
					case "dbConnectionString":
						break;
					
					default:
			
						// This is truly a user setting
						// theContext.Output.writeln(key);
						h[key] = theContext.Input[key];
						break;
				}
			}
		}
	
		/// <summary>
		/// This retrieves cached template input. See <see cref="SaveInputToCache"/>.
		/// Thanks to Mike Griffin for contributing these.
		/// </summary>
		/// <example>
		/// At the top of your ui code you add this:
		/// <code>
		///	Sub setup()
		///	   DnpUtils.ReadInputFromCache(context);
		///	   ....
		///	End Sub
		/// </code>
		/// Then at the top of your template body you add:
		/// <code>
		/// 	DnpUtils.SaveInputToCache(context);
		/// </code>
		/// After you do that try running your template,
		/// it will come up with the last settings you had (no project files required)
		/// as long as you haven't closed down MyGeneration.
		/// You can even alternate between two templates and it will remember
		/// the input for each.
		/// </example>
		public void ReadInputFromCache(IZeusContext theContext)
		{
			if(cache.ContainsKey(theContext.ExecutingTemplate.UniqueID.ToString()))
			{
				Hashtable h = cache[theContext.ExecutingTemplate.UniqueID.ToString()] as Hashtable;	
			
				foreach(string key in h.Keys)
				{		
					theContext.Gui.Defaults[key] = h[key];
				}
			}
		}
		#endregion
		
		#region User Mappping
		/// <summary>
		/// This function lets developers define their own mapping
		/// system similar to DbTargets.xml or Languanges.xml to
		/// use in templates. The xml mapping file
		/// must be located in the same folder as DbTargets.xml
		/// (as specified in the Default Settings.)
		/// It returns a Hashtable of key/value pairs with the
		/// "From" attribute the key and the "To" attribute
		/// the value.
		/// </summary>
		/// <example>
		/// Excerpt from FunctionalTypes.xml:
		/// <code>
		/// &lt;?xml version="1.0" encoding="utf-8"?&gt;
		/// &lt;mapTypes&gt;
		///     &lt;mapType From="SQL" To="SqlType"&gt;
		///         &lt;Type From="bigint" To="NumericInteger" /&gt;
    	/// 		&lt;Type From="bit" To="Logical" /&gt;
		///         &lt;Type From="text" To="TextLarge" /&gt;
	    /// 		&lt;Type From="varchar" To="TextSmall" /&gt;
		///     &lt;/mapType&gt;
		///     &lt;mapType From="ACCESS" To="AccessType"&gt;
		///         &lt;Type From="Text" To="TextSmall" /&gt;
		///         &lt;Type From="Memo" To="TextLarge" /&gt;
		///     &lt;/mapType&gt;
		///     &lt;mapType From="MYSQL2" To="MySqlType"&gt;
		///         &lt;Type From="TINYINT UNSIGNED" To="Logical" /&gt;
		///         &lt;Type From="VARCHAR" To="TextSmall" /&gt;
		///         &lt;Type From="TIMESTAMP" To="TimeStamp" /&gt;
		///     &lt;/mapType&gt;
		/// &lt;/mapTypes&gt;
		/// </code>
		/// The following code looks up the DataTypeName (e.g., bit) using
		/// the DriverString (e.g., SQL) as the mapGroupType and returns the
		/// corresponding Functional Type (in this case - Logical).
		/// <code>
		/// Hashtable functionalTypes = new Hashtable();
		/// functionalTypes = DnpUtils.ReadUserMap("FunctionalTypes.xml", MyMeta.DriverString, MyMeta);
		///	if(functionalTypes[column.DataTypeName].ToString() == "Logical")
		/// {
		/// 	// add code for a CheckBox
		/// }
		///	if(functionalTypes[column.DataTypeName].ToString() == "TextSmall")
		/// {
		/// 	// add code for a TextBox
		/// }
		/// </code>
		/// See the template archive for further explanation and example
		/// templates.
		/// <code>http://www.mygenerationsoftware.com/TemplateLibrary/Archive/?guid=4a285a9a-4dd2-4655-b7ca-996b5516a5a1</code>
		/// </example>
        [ComVisible(false)]
		public Hashtable ReadUserMap(string mapFileName, string mapGroupType, MyMeta.dbRoot myMeta)
		{
			Hashtable h = new Hashtable();
			XmlTextReader reader = null;
			string sPath = myMeta.DbTargetMappingFileName;
			sPath = sPath.Substring(0, sPath.LastIndexOf("\\"));
			
			try
			{
				reader = new XmlTextReader(sPath + "\\" + mapFileName);
				reader.WhitespaceHandling = WhitespaceHandling.None;

				while(reader.Read())
				{
					if(reader.GetAttribute("From") == mapGroupType)
					{
						break;
					}
				}
				while(reader.Read())
				{
			      	if(reader.NodeType == XmlNodeType.EndElement)
			      	{
			      		break;
			      	}
			      	else
			      	{
			      		h.Add(reader.GetAttribute("From"), reader.GetAttribute("To"));
			      	}
				}
			}
			finally
			{
				if (reader != null)
				{
					reader.Close();
				}
			}
			
			return h;
		}
		#endregion
		
		#region Miscellaneous
		
		/// <summary>
		/// EXPERIMENTAL!
		/// This creates an instance of the TableRelation Class
		/// so that it can be accessed in templates using
		/// Microsoft script (VBScript or JScript.)
		/// </summary>
		/// <remarks>
		/// THIS IS EXPERIMENTAL!
		/// As yet, it does not work for VBScript.
		/// </remarks>
		/// <example>
		/// In JScript:
		/// <code>
		/// var tableRel = DnpUtils.CreateTableRelation(tableMeta, foreignKey);
		/// </code>
		/// In VBScript:
		/// <code>
		/// Dim tableRel
		/// Set tableRel = DnpUtils.CreateTableRelation(tableMeta, foreignKey)
		/// </code>
		/// </example>
        [ComVisible(false)]
		public TableRelation CreateTableRelation(ITable tableMeta, IForeignKey foreignKey)
		{
			TableRelation tr = new TableRelation(tableMeta, foreignKey);
			return tr;
		}
		
		/// <summary>
		/// Helper function that retrieves an ordered list of tables in a database.
		/// If reverse is false, the list is an insert order list, so that
		/// the parent tables are listed before the child tables.
		/// If reverse is true, the list is a delete order list, so that
		/// the child tables are listed before the parent tables.
		/// Most of this is converted from Justin Greenwood's sql_library.js.
		/// </summary>
		/// <example>
		/// <code>
		/// ArrayList orderedList = DnpUtils.GetOrderedTableList(MyMeta.Databases[DatabaseName], false);
		/// </code>
		/// </example>
		public ArrayList GetOrderedTableList(IDatabase database, bool reverse)
		{
			ArrayList orderedList = new ArrayList();
			int numberAdded = -1;
	
			while(numberAdded != 0)
			{
				numberAdded = 0;
				
				for(int i = 0; i < database.Tables.Count; i++)
				{
					string tableName = database.Tables[i].Name;
					IForeignKeys fKeys = database.Tables[i].ForeignKeys;
					
					// If there are no foreign keys add it to the list.
					if(fKeys.Count == 0 && !orderedList.Contains(tableName))
					{
						orderedList.Add(tableName);
						numberAdded++;
					}
					else
					{
						// If there are foreign keys, loop through them and see if
						// all of the referencing tables are already in the collection.
						// If they are all in there, add the table.
						bool allExist = true;
						for(int x = 0; x < fKeys.Count; x++)
						{
							IForeignKey fKey = fKeys[x];
							
							// only look at indirect foreign keys.
							if(fKey.ForeignTable.Name == tableName)
							{
								// Check to see if it is in the list or is self-referencing.
								if(!orderedList.Contains(fKey.PrimaryTable.Name) && (fKey.PrimaryTable.Name != tableName))
								{
									allExist = false;
									break;
								}
							}
						}
			
						if(allExist) 
						{
							if(!orderedList.Contains(tableName))
							{
								orderedList.Add(tableName);
								numberAdded++;
							}
						}
					}
				} // End table loop
			} // End while
			
			// Any tables left over get added to the end of the list
			for (int i = 0; i < database.Tables.Count; i++)
			{
				string tableName = database.Tables[i].Name;
				if(!orderedList.Contains(tableName))
				{
					orderedList.Add(tableName);
				}
			}
		
			if(reverse)
			{
				orderedList.Reverse();
			}
			
			return orderedList;
		} // End GetOrderedTableList
		
		#endregion	
	}
}
