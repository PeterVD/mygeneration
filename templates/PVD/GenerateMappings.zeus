﻿##|TYPE Template
##|UNIQUEID 5f6a5765-90f2-46a9-b9de-aaaedfd82f30
##|TITLE EF Mapping files
##|NAMESPACE 
##|SOURCE_TYPE Source
##|OUTPUT_LANGUAGE C#
##|GUI_ENGINE .Net Script
##|GUI_LANGUAGE C#
##|GUI_BEGIN
<%#REFERENCE System.Windows.Forms.dll %>
<%#NAMESPACE System, System.Text, System.Collections, Zeus, Zeus.UserInterface, Zeus.DotNetScript %>

public class GeneratedGui : DotNetScriptGui
{
	public GeneratedGui(ZeusGuiContext context) : base(context) {}

	public override void Setup()
	{
		if ( !input.Contains("lstTables") || !input.Contains("txtPath") )
		{
			ui.Title = "Generate EF Mapping files";
			ui.Width = 370;
			ui.Height = 500;
	
			// Grab default output path
			string sOutputPath = "";
			if (input.Contains("defaultOutputPath")) 
			{
				sOutputPath = input["defaultOutputPath"].ToString();
			}
	
			// Display and errors here
			GuiLabel lblError = ui.AddLabel("lblError", "", "");
			lblError.ForeColor = "Red";
	
			// Setup Folder selection input control.
			GuiLabel lblPath = ui.AddLabel("lblPath", "Select the output path:", "Select the output path in the field below.");
			GuiTextBox outpath = ui.AddTextBox("txtPath", sOutputPath, "Select the Output Path.");
			GuiFilePicker btnSelectPath = ui.AddFilePicker("btnPath", "Select Path", "Select the Output Path.", "txtPath", true);
	
			// Setup Database selection combobox.
			GuiLabel label_d = ui.AddLabel("lblDatabases", "Select a database:", "Select a database in the dropdown below.");
			GuiComboBox cmbDatabases = ui.AddComboBox("cmbDatabase", "Select a database.");
	
			// Setup Tables selection multi-select listbox.
			GuiLabel label_t = ui.AddLabel("lblTables", "Select tables:", "Select tables from the list below.");
			GuiListBox lstTables = ui.AddListBox("lstTables", "Select tables.");
			lstTables.Height = 200;
	
			setupDatabaseDropdown(cmbDatabases, lblError);
			cmbDatabases.AttachEvent("onchange", "cmbDatabases_onchange");
			
			ui.ShowGui = true;
		}
		else 
		{
			ui.ShowGui = false;
		}
	}
	
	public void setupDatabaseDropdown(GuiComboBox cmbDatabases, GuiLabel lblError)
	{
		try 
		{	
			if (MyMeta.IsConnected) 
			{
				cmbDatabases.BindData(MyMeta.Databases);
				if (MyMeta.DefaultDatabase != null) 
				{
					cmbDatabases.SelectedValue = MyMeta.DefaultDatabase.Name;
					bindTables(cmbDatabases.SelectedValue);
				}
	
				lblError.Text = "";
			}
			else
			{
				lblError.Text = "Please set up your database connection in the Default Settings dialog.";
			}
		}
		catch (Exception ex)
		{
			lblError.Text = ex.Message;
		}
	}
	
	public void bindTables(string sDatabase)
	{
		GuiLabel lblError = ui["lblError"] as GuiLabel;
		int count = 0;
	
		GuiListBox lstTables = ui["lstTables"] as GuiListBox;
		
		try 
		{	
			IDatabase db = MyMeta.Databases[sDatabase];
			lstTables.BindData(db.Tables);
	
			lblError.Text = "";
		}
		catch (Exception ex)
		{
			lblError.Text = ex.Message;
		}
	}
	
	public void cmbDatabases_onchange(GuiComboBox control)
	{
		GuiLabel lblError = ui["lblError"] as GuiLabel;
		int count = 0;
	
		GuiComboBox cmbDatabases = ui["cmbDatabase"] as GuiComboBox;
	
		bindTables(cmbDatabases.SelectedText);
	}
}
##|GUI_END
##|BODY_MODE Markup
##|BODY_ENGINE .Net Script
##|BODY_LANGUAGE C#
##|BODY_TAG_START <%
##|BODY_TAG_END %>
##|BODY_BEGIN
<%#NAMESPACE System.IO, System, System.Text, System.Collections, System.Linq, Zeus, Zeus.DotNetScript %><%
public class GeneratedTemplate : DotNetScriptTemplate
{
	public GeneratedTemplate(ZeusContext context) : base(context) {}

	public override void Render()
	{
		string databaseName = input["cmbDatabase"].ToString(); 		
		IDatabase database = MyMeta.Databases[databaseName];
		ArrayList tables = input["lstTables"] as ArrayList;	
		
		// Filename info
		string filepath = input["txtPath"].ToString();
		if (!filepath.EndsWith("\\") ) filepath += "\\";
			
		foreach (string tableName in tables)
		{
			ITable table = database.Tables[tableName];
			var entityName = RenderEntity(table);
			output.save(filepath + entityName + "Map.cs", "o");
			output.clear();
		}
	}
	
	public string RenderEntity(ITable table)
	{
		var entityName = GetEntityName(table.Name);
		var pkColumns = table.Columns.Where(x => x.IsInPrimaryKey).ToList();
		
		%>
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Lansweeper.Service.Domain;

namespace Lansweeper.Service.DL.Mappings
{
	// <%= table.Name%>
    public class <%= entityName%>Map : EntityTypeConfiguration<<%= entityName%>>
    {
        public <%= entityName%>Map()
            : this("<%= table.Schema%>")
        {
        }
		
		public <%= entityName%>Map(string schema)
        {
            ToTable("<%= table.Name%>", schema);
            HasKey(x => x.<%= pkColumns.Count == 1 ? pkColumns[0].Name : string.Join(", ", pkColumns.Select(x => string.Format("x.{0}", x.Name)))%>);
			
<%
			foreach (IColumn col in table.Columns)
			{
				output.write(string.Format("\t\t\tProperty(x => x.{0}).HasColumnName(@\"{1}\").HasColumnType(\"{2}\")", 
											ToUpperFirstLetter(col.Name), col.Name, col.DataTypeName));
				if (col.IsNullable)
				{
					output.write(".IsOptional()");
				}
				else
				{
					output.write(".IsRequired()");
				}
				if (col.IsAutoKey)
				{
					output.write(".HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)");
				}
				if (col.DataTypeName.ToLowerInvariant() == "nvarchar" || col.DataTypeName.ToLowerInvariant() == "nchar"
					|| col.DataTypeName.ToLowerInvariant() == "varchar" || col.DataTypeName.ToLowerInvariant() == "char")
				{
					output.write(string.Format(".HasMaxLength({0})", col.CharacterMaxLength));
				}
				if (col.DataTypeName.ToLowerInvariant() == "numeric")
				{
					output.write(string.Format(".HasPrecision({0},{1})", col.NumericPrecision, col.NumericScale));
				}
				output.writeln(";");
			}
			
			output.writeln("");
			
			foreach (var fk in table.ForeignKeys.Where(x => x.ForeignTable.Schema != table.Schema || x.ForeignTable.Name != table.Name))
			{
				output.writeln(string.Format("\t\t\tHasMany(x => x.{0}s);", GetEntityName(fk.ForeignTable.Name)));
			}
			
			foreach (var fk in table.ForeignKeys.Where(x => x.ForeignTable.Schema == table.Schema || x.ForeignTable.Name == table.Name))
			{
				if (fk.ForeignColumns[0].IsNullable)
				{
					output.writeln(string.Format("\t\t\tHasOptional(x => x.{0});", GetEntityName(fk.PrimaryTable.Name)));
				}
				else
				{
					output.writeln(string.Format("\t\t\tHasRequired(x => x.{0});", GetEntityName(fk.PrimaryTable.Name)));
				}
			}
%>		}
	}
}
<%
		return entityName;
	}

	private static string GetEntityName(string input)
	{
		var prefixes = new[] {"tsys","tbl", "htbl"};
		foreach (var prefix in prefixes)
		{
			if (input.StartsWith(prefix, StringComparison.InvariantCultureIgnoreCase))
			{
				input = input.Substring(prefix.Length, input.Length - prefix.Length);
			}
		}	
		return ToUpperFirstLetter(input);
	}
	
	public static string ToUpperFirstLetter(string source)
	{
		if (string.IsNullOrEmpty(source))
			return string.Empty;
		// convert to char array of the string
		char[] letters = source.ToCharArray();
		// upper case the first char
		letters[0] = char.ToUpper(letters[0]);
		// return the array made of the new char array
		return new string(letters);
	}
}
%>
##|BODY_END
