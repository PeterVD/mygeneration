
namespace MyGeneration
{
    public interface IScintillaEditControl : IMyGenDocument
    {
        IScintillaNet ScintillaEditor { get; }
    }
}
