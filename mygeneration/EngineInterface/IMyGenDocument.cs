
namespace MyGeneration
{
    public interface IMyGenDocument : IMyGenContent
    {
        string DocumentIndentity { get; }
        string TextContent { get; }
    }
}
