using WeifenLuo.WinFormsUI.Docking;
using System.Windows.Forms;

namespace MyGeneration
{
    public interface IMyGenContent
    {
        ToolStrip ToolStrip { get; }
        void ProcessAlert(IMyGenContent sender, string command, params object[] args);
        bool CanClose(bool allowPrevent);
        DockContent DockContent { get; }
    }
}
