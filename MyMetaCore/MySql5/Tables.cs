using System;
using System.Data;
using System.Runtime.InteropServices;

namespace MyMeta.MySql5;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(ITables))]
#endif
public class MySql5Tables : Tables
{
    internal override void LoadAll()
    {
        try
        {
            var db = Database.Databases as MySql5Databases;

            var query = "SHOW TABLES";
            if (db.Version.StartsWith("5")) query = @"SHOW FULL TABLES WHERE Table_type = 'BASE TABLE'";

            var metaData = new DataTable();
            var adapter = MySql5Databases.CreateAdapter(query, DbRoot.ConnectionString);

            adapter.Fill(metaData);

            metaData.Columns[0].ColumnName = "TABLE_NAME";

            PopulateArray(metaData);

            if (db.Version.StartsWith("5")) LoadTableDescriptions();
        }
        catch
        {
        }
    }

    private void LoadTableDescriptions()
    {
        try
        {
            var query = @"SELECT TABLE_NAME, TABLE_COMMENT, CREATE_TIME, UPDATE_TIME FROM information_schema.TABLES WHERE TABLE_SCHEMA = '" +
                        Database.Name + "'";

            var metaData = new DataTable();
            var adapter = MySql5Databases.CreateAdapter(query, DbRoot.ConnectionString);

            adapter.Fill(metaData);

            if (Database.Tables.Count > 0)
            {
                var t = Database.Tables[0] as Table;

                if (!t._row.Table.Columns.Contains("DESCRIPTION"))
                {
                    t._row.Table.Columns.Add("DESCRIPTION", Types.StringType);
                    f_Description = t._row.Table.Columns["DESCRIPTION"];
                }

                if (!t._row.Table.Columns.Contains("TABLE_SCHEMA"))
                {
                    t._row.Table.Columns.Add("TABLE_SCHEMA", Types.StringType);
                    f_Schema = t._row.Table.Columns["TABLE_SCHEMA"];
                }

                if (!t._row.Table.Columns.Contains("DATE_CREATED"))
                {
                    t._row.Table.Columns.Add("DATE_CREATED", Types.DateTimeType);
                    f_DateCreated = t._row.Table.Columns["DATE_CREATED"];
                }

                if (!t._row.Table.Columns.Contains("DATE_MODIFIED"))
                {
                    t._row.Table.Columns.Add("DATE_MODIFIED", Types.DateTimeType);
                    f_DateModified = t._row.Table.Columns["DATE_MODIFIED"];
                }
            }

            if (metaData.Rows.Count > 0)
                foreach (DataRow row in metaData.Rows)
                {
                    var t = this[row["TABLE_NAME"] as string] as Table;

                    t._row["DESCRIPTION"] = row["TABLE_COMMENT"] as string;
                    t._row["TABLE_SCHEMA"] = Database.Name;

                    if (row["CREATE_TIME"] != DBNull.Value) t._row["DATE_CREATED"] = (DateTime)row["CREATE_TIME"];

                    if (row["UPDATE_TIME"] != DBNull.Value) t._row["DATE_MODIFIED"] = (DateTime)row["UPDATE_TIME"];
                }
        }
        catch
        {
        }
    }
}