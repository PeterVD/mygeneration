using System.Data;
using System.Runtime.InteropServices;

namespace MyMeta.MySql5;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IIndexes))]
#endif
public class MySql5Indexes : Indexes
{
    internal override void LoadAll()
    {
        try
        {
            var query = @"SHOW INDEX FROM `" + Table.Name + "`";

            var metaData = new DataTable();
            var adapter = MySql5Databases.CreateAdapter(query, DbRoot.ConnectionString);

            adapter.Fill(metaData);

            metaData.Columns["Key_name"].ColumnName = "INDEX_NAME";
            metaData.Columns["Index_type"].ColumnName = "TYPE";
            metaData.Columns["Non_unique"].ColumnName = "UNIQUE";

            PopulateArray(metaData);
        }
        catch
        {
        }
    }
}