using System.Data;
using System.Runtime.InteropServices;

namespace MyMeta.MySql5;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IForeignKeys))]
#endif
public class MySql5ForeignKeys : ForeignKeys
{
    internal override void LoadAll()
    {
        try
        {
            var db = (MySql5Database)Table.Database;

            if (!db._FKsInLoad)
            {
                db._FKsInLoad = true;

                MySql5ForeignKeys fks = null;

                foreach (Table table in Table.Tables) fks = table.ForeignKeys as MySql5ForeignKeys;

                AddMyHalf();

                foreach (Table table in Table.Tables)
                {
                    fks = table.ForeignKeys as MySql5ForeignKeys;
                    fks.AddTheOtherHalf();
                }

                AddTheOtherHalf();

                db._FKsInLoad = false;
            }
            else
            {
                AddMyHalf();
            }
        }
        catch
        {
        }
    }

    internal void AddMyHalf()
    {
        var query = @"SHOW CREATE TABLE `" + Table.Name + "`";

        var dt = new DataTable();
        var adapter = MySql5Databases.CreateAdapter(query, DbRoot.ConnectionString);

        adapter.Fill(dt);

        var text = dt.Rows[0][1] as string;

        // CONSTRAINT `FK_mastertypes_3` FOREIGN KEY (`TheINT1`, `TheINT2`) REFERENCES `employee` (`TheInt1`, `TheInt2`),
        // CONSTRAINT `FK_mastertypes_1` FOREIGN KEY (`MyPK`) REFERENCES `employee` (`EmployeeID`),
        // CONSTRAINT `FK_mastertypes_2` FOREIGN KEY (`TheVARCHAR`) REFERENCES `employee` (`LastName`)
        // CONSTRAINT `ShippersOrders` FOREIGN KEY (`ShipVia`) REFERENCES `shippers` (`ShipperID`) ON DELETE NO ACTION ON UPDATE NO ACTION\n)

        var metaData = new DataTable();
        metaData.Columns.Add("PK_TABLE_CATALOG");
        metaData.Columns.Add("PK_TABLE_SCHEMA");
        metaData.Columns.Add("PK_TABLE_NAME");
        metaData.Columns.Add("FK_TABLE_CATALOG");
        metaData.Columns.Add("FK_TABLE_SCHEMA");
        metaData.Columns.Add("FK_TABLE_NAME");
        metaData.Columns.Add("ORDINAL");
        metaData.Columns.Add("UPDATE_RULE");
        metaData.Columns.Add("DELETE_RULE");
        metaData.Columns.Add("PK_NAME");
        metaData.Columns.Add("FK_NAME");
        metaData.Columns.Add("DEFERRABILITY");
        metaData.Columns.Add("PK_COLUMN_NAME");
        metaData.Columns.Add("FK_COLUMN_NAME");

        var s = "";
        string[] fkRec = null;
        string[] pkColumns = null;
        string[] fkColumns = null;

        var iStart = 0;

        while (true)
        {
            iStart = text.IndexOf("CONSTRAINT", iStart);
            if (iStart == -1) break;
            var iEnd = text.IndexOf('\n', iStart);

            var fk = text.Substring(iStart, iEnd - iStart);

            iStart = iEnd + 2;

            if (-1 != fk.IndexOf("FOREIGN KEY"))
            {
                // MySQL 5.0 trick !!
                var index = fk.IndexOf(")");
                index = fk.IndexOf(")", index + 1);
                s = fk.Substring(0, index);
                //


                // Munge it down it a record I can split with a ',' seperator
                s = s.Replace("`", "");
                s = s.Replace(" ", "");
                s = s.Replace("),", "");
                s = s.Replace(",", "|");
                s = s.Replace("CONSTRAINT", "");
                s = s.Replace("FOREIGNKEY", "");
                s = s.Replace("REFERENCES", ",");
                s = s.Replace("(", ",");
                s = s.Replace(")", "");

                fkRec = s.Split(',');

                fkColumns = fkRec[1].Split('|');
                pkColumns = fkRec[3].Split('|');

                for (var i = 0; i < pkColumns.Length; i++)
                {
                    var row = metaData.NewRow();
                    metaData.Rows.Add(row);

                    row["PK_TABLE_CATALOG"] = Table.Database.Name;
                    row["FK_TABLE_CATALOG"] = Table.Database.Name;

                    row["PK_TABLE_NAME"] = fkRec[2];
                    row["FK_TABLE_NAME"] = Table.Name;
                    row["FK_NAME"] = fkRec[0];
                    row["PK_COLUMN_NAME"] = pkColumns[i];
                    row["FK_COLUMN_NAME"] = fkColumns[i];

                    row["ORDINAL"] = i;

                    //  ON DELETE NO ACTION ON UPDATE NO ACTION\n)
                    try
                    {
                        row["DELETE_RULE"] = "RESTRICT";
                        var ond = fk.IndexOf("ON DELETE");
                        if (-1 != ond)
                        {
                            var c = fk[ond + 10];

                            switch (c)
                            {
                                case 'R':
                                case 'r':

                                    row["DELETE_RULE"] = "RESTRICT";
                                    break;

                                case 'C':
                                case 'c':

                                    row["DELETE_RULE"] = "CASCADE";
                                    break;

                                case 'S':
                                case 's':

                                    row["DELETE_RULE"] = "SET NULL";
                                    break;

                                case 'N':
                                case 'n':

                                    row["DELETE_RULE"] = "NO ACTION";
                                    break;
                            }
                        }


                        row["UPDATE_RULE"] = "RESTRICT";
                        var onu = fk.IndexOf("ON UPDATE");
                        if (-1 != onu)
                        {
                            var c = fk[onu + 10];

                            switch (c)
                            {
                                case 'R':
                                case 'r':

                                    row["UPDATE_RULE"] = "RESTRICT";
                                    break;

                                case 'C':
                                case 'c':

                                    row["UPDATE_RULE"] = "CASCADE";
                                    break;

                                case 'S':
                                case 's':

                                    row["UPDATE_RULE"] = "SET NULL";
                                    break;

                                case 'N':
                                case 'n':

                                    row["UPDATE_RULE"] = "NO ACTION";
                                    break;
                            }
                        }
                    }
                    catch
                    {
                    }
                }
            }
        }

        PopulateArray(metaData);
    }

    internal void AddTheOtherHalf()
    {
        var myName = Table.Name;

        foreach (Table table in Table.Tables)
            if (table.Name != myName)
                foreach (MySql5ForeignKey fkey in table.ForeignKeys)
                    if (fkey.ForeignTable.Name == myName || fkey.PrimaryTable.Name == myName)
                        AddForeignKey(fkey);
    }
}