using System;
using System.Runtime.InteropServices;

namespace MyMeta.MySql5;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IColumn))]
#endif
public class MySql5Column : Column
{
    private static char[] chars = new[] { ' ', '(' };
    private int characterLength;
    private string dataType = "";

    private int numericScale;
    private int precision;

    public override bool IsNullable
    {
        get
        {
            var cols = Columns as MySql5Columns;
            var s = GetString(cols.f_IsNullable);
            return s == "YES" ? true : false;
        }
    }

    public override bool HasDefault => Default == "" ? false : true;

    public override bool IsAutoKey
    {
        get
        {
            var isAutoKey = false;

            var cols = Columns as MySql5Columns;
            var s = GetString(cols.f_Extra);

            if (-1 != s.IndexOf("auto_increment")) isAutoKey = true;

            return isAutoKey;
        }
    }

    public override string DataTypeName
    {
        get
        {
            if (dataType == "")
            {
                var cols = Columns as MySql5Columns;
                var type = GetString(cols.f_DataType).ToUpper();

                var data = type.Split(new[] { ' ' });
                var typeandsize = data[0].Split('(', ')', ',');

                dataType = typeandsize[0];

                if (dataType != "ENUM")
                {
                    if (-1 != type.IndexOf("UNSIGNED")) dataType += " UNSIGNED";

                    var parts = typeandsize.Length;

                    if (parts >= 2)
                    {
                        if (dataType == "VARCHAR" || dataType == "CHAR")
                            characterLength = Convert.ToInt32(typeandsize[1]);
                        else
                            precision = Convert.ToInt32(typeandsize[1]);
                    }

                    if (parts >= 3)
                        if (typeandsize[2].Length > 0)
                            numericScale = Convert.ToInt32(typeandsize[2]);
                }
            }

            return dataType;
        }
    }

    public override string DataTypeNameComplete
    {
        get
        {
            try
            {
                var cols = Columns as MySql5Columns;
                var origType = GetString(cols.f_DataType);
                var type = origType.ToUpper();

                var data = type.Split(new[] { ' ' });

                if (data[0].StartsWith("ENUM")) return "ENUM" + origType.Substring(4, origType.Length - 4);

                if (-1 != type.IndexOf("UNSIGNED")) return data[0] + " UNSIGNED";

                return data[0];
            }
            catch
            {
                return "ERROR";
            }
        }
    }

    public override int NumericPrecision => precision;

    public override int NumericScale => numericScale;

    public override int CharacterMaxLength => characterLength;

    internal override Column Clone()
    {
        var c = base.Clone();

        return c;
    }
}