using System.Runtime.InteropServices;

namespace MyMeta.MySql5;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IIndex))]
#endif
public class MySql5Index : Index
{
    public override string Type => GetString(Indexes.f_Type);

    public override bool Unique =>
        // We have to reverse the meaning
        base.Unique ? false : true;

    public override string Collation
    {
        get
        {
            var s = GetString(Indexes.f_Collation);

            switch (s)
            {
                case "A":
                    return "ASCENDING";
                case "D":
                    return "DECENDING";
                default:
                    return "UNKNOWN";
            }
        }
    }
}