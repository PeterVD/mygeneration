using System.Data;
using System.Runtime.InteropServices;

namespace MyMeta.MySql5;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IViews))]
#endif
public class MySql5Views : Views
{
    internal override void LoadAll()
    {
        try
        {
            var db = Database.Databases as MySql5Databases;
            if (db.Version.StartsWith("5"))
            {
                var query = @"SHOW FULL TABLES WHERE Table_type = 'VIEW'";

                var metaData = new DataTable();
                var adapter = MySql5Databases.CreateAdapter(query, DbRoot.ConnectionString);

                adapter.Fill(metaData);

                metaData.Columns[0].ColumnName = "TABLE_NAME";

                PopulateArray(metaData);
            }
        }
        catch
        {
        }
    }
}