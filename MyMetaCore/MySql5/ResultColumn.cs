using System.Runtime.InteropServices;

namespace MyMeta.MySql5;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IResultColumn))]
#endif
public class MySql5ResultColumn : ResultColumn
{
}