using System;
using System.Data;
using System.Data.Common;
using System.Reflection;
using System.Runtime.InteropServices;

namespace MyMeta.MySql5;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IDatabases))]
#endif
public class MySql5Databases : Databases
{
    internal static string nameSpace = "MySql.Data.MySqlClient.";
    internal static Assembly asm;
    internal static Module mod;

    internal static ConstructorInfo IDbConnectionCtor;
    internal static ConstructorInfo IDbDataAdapterCtor;

    internal string Version = "";

    static MySql5Databases()
    {
        LoadAssembly();
    }

    public MySql5Databases()
    {
        LoadAssembly();
    }

    public static void LoadAssembly()
    {
        try
        {
            if (asm is null)
                try
                {
                    asm = Assembly.Load("MySql.Data");
                    var mods = asm.GetModules(false);
                    mod = mods[0];
                }
                catch
                {
                    throw new Exception("Make sure the MySql.Data.dll is registered in the Gac or is located in the MyGeneration folder.");
                }
        }
        catch
        {
        }
    }

    internal override void LoadAll()
    {
        try
        {
            var name = "";

            // We add our one and only Database
            var conn = CreateConnection(DbRoot.ConnectionString);
            conn.Open();
            name = conn.Database;
            conn.Close();
            conn.Dispose();

            var database = (MySql5Database)DbRoot.ClassFactory.CreateDatabase();
            database._name = name;
            database.DbRoot = DbRoot;
            database.Databases = this;
            _array.Add(database);

            try
            {
                var metaData = new DataTable();
                var adapter = CreateAdapter("SELECT VERSION()", DbRoot.ConnectionString);

                adapter.Fill(metaData);

                Version = metaData.Rows[0][0] as string;
            }
            catch
            {
            }
        }
        catch
        {
        }
    }

    internal static IDbConnection CreateConnection()
    {
        var type = mod.GetType(nameSpace + "MySqlConnection");
        var con = type.Assembly.CreateInstance(type.Name) as IDbConnection;
        return con;
    }

    internal static IDbConnection CreateConnection(string connStr)
    {
        var con = CreateConnection();
        con.ConnectionString = connStr;
        con.Open();
        return con;
    }

    internal static DbDataAdapter CreateAdapter(string query, string connStr)
    {
        if (IDbDataAdapterCtor is null)
        {
            var type = mod.GetType(nameSpace + "MySqlDataAdapter");

            IDbDataAdapterCtor = type.GetConstructor(new[] { typeof(string), typeof(string) });
        }

        var obj = IDbDataAdapterCtor.Invoke
        (BindingFlags.CreateInstance | BindingFlags.OptionalParamBinding, null,
            new object[] { query, connStr }, null);

        return obj as DbDataAdapter;
    }
}