using System.Data;
using System.Runtime.InteropServices;

namespace MyMeta.MySql5;

#if ENTERPRISE

[ComVisible(false)]
#endif
public class ClassFactory : IClassFactory
{
    public ITables CreateTables()
    {
        return new MySql5Tables();
    }

    public ITable CreateTable()
    {
        return new MySql5Table();
    }

    public IColumn CreateColumn()
    {
        return new MySql5Column();
    }

    public IColumns CreateColumns()
    {
        return new MySql5Columns();
    }

    public IDatabase CreateDatabase()
    {
        return new MySql5Database();
    }

    public IDatabases CreateDatabases()
    {
        return new MySql5Databases();
    }

    public IProcedure CreateProcedure()
    {
        return new MySql5Procedure();
    }

    public IProcedures CreateProcedures()
    {
        return new MySql5Procedures();
    }

    public IView CreateView()
    {
        return new MySql5View();
    }

    public IViews CreateViews()
    {
        return new MySql5Views();
    }

    public IParameter CreateParameter()
    {
        return new MySql5Parameter();
    }

    public IParameters CreateParameters()
    {
        return new MySql5Parameters();
    }

    public IForeignKey CreateForeignKey()
    {
        return new MySql5ForeignKey();
    }

    public IForeignKeys CreateForeignKeys()
    {
        return new MySql5ForeignKeys();
    }

    public IIndex CreateIndex()
    {
        return new MySql5Index();
    }

    public IIndexes CreateIndexes()
    {
        return new MySql5Indexes();
    }

    public IResultColumn CreateResultColumn()
    {
        return new MySql5ResultColumn();
    }

    public IResultColumns CreateResultColumns()
    {
        return new MySql5ResultColumns();
    }

    public IDomain CreateDomain()
    {
        return new MySql5Domain();
    }

    public IDomains CreateDomains()
    {
        return new MySql5Domains();
    }

    public IProviderType CreateProviderType()
    {
        return new ProviderType();
    }

    public IProviderTypes CreateProviderTypes()
    {
        return new ProviderTypes();
    }

    #region IClassFactory Members

    public IDbConnection CreateConnection()
    {
        MySql5Databases.LoadAssembly();
        var conn = MySql5Databases.CreateConnection();

        return conn;
    }

    #endregion

    public static void Register()
    {
        var drv = new InternalDriver
        (typeof(ClassFactory)
            , "Database=Test;Data Source=Griffo;User Id=anonymous;Password=;"
            , false);
        drv.StripTrailingNulls = true;
        drv.RequiredDatabaseName = true;

        InternalDriver.Register("MYSQL2", drv);
    }
}