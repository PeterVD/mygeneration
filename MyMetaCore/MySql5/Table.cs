using System.Data;
using System.Runtime.InteropServices;

namespace MyMeta.MySql5;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(ITable))]
#endif
public class MySql5Table : Table
{
    public override IColumns PrimaryKeys
    {
        get
        {
            if (null == _primaryKeys)
            {
                _primaryKeys = (Columns)DbRoot.ClassFactory.CreateColumns();
                _primaryKeys.Table = this;
                _primaryKeys.DbRoot = DbRoot;

                var query = @"SHOW INDEX FROM `" + Name + "`";

                var metaData = new DataTable();
                var adapter = MySql5Databases.CreateAdapter(query, DbRoot.ConnectionString);

                adapter.Fill(metaData);

                var rows = metaData.Rows;

                var s = "";
                for (var i = 0; i < rows.Count; i++)
                {
                    s = rows[i]["Key_Name"] as string;

                    if (s == "PRIMARY")
                    {
                        s = metaData.Rows[i]["Column_name"] as string;
                        _primaryKeys.AddColumn((Column)Columns[s]);
                    }
                }
            }

            return _primaryKeys;
        }
    }

//		public override IColumns PrimaryKeys
//		{
//			get
//			{
//				if(null == _primaryKeys)
//				{
//					OleDbConnection cn = new OleDbConnection(this.dbRoot.ConnectionString); 
//					cn.Open(); 
//					DataTable metaData = cn.GetOleDbSchemaTable(OleDbSchemaGuid.Primary_Keys, 
//						new Object[] {null, this.Tables.Database.Name, this.Name});
//					cn.Close();
//
//					_primaryKeys = (Columns)this.dbRoot.ClassFactory.CreateColumns();
//
//					Columns cols = (Columns)this.Columns;
//
//					string colName = "";
//
//					int count = metaData.Rows.Count;
//					for(int i = 0; i < count; i++)
//					{
//						colName = metaData.Rows[i]["COLUMN_NAME"] as string;
//						_primaryKeys.AddColumn((Column)cols[colName]);
//					}
//				}
//
//				return _primaryKeys;
//			}
//		}
}