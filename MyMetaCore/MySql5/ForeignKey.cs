using System.Runtime.InteropServices;

namespace MyMeta.MySql5;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IForeignKey))]
#endif
public class MySql5ForeignKey : ForeignKey
{
    public override ITable ForeignTable
    {
        get
        {
            var catalog = ForeignKeys.Table.Database.Name;
            var schema = GetString(ForeignKeys.f_FKTableSchema);

            return DbRoot.Databases[catalog].Tables[GetString(ForeignKeys.f_FKTableName)];
        }
    }

    public override string PrimaryKeyName
    {
        get
        {
            if (PrimaryTable.Indexes["PRIMARY"]  is not null)
                return "PRIMARY";
            return base.PrimaryKeyName;
        }
    }
}