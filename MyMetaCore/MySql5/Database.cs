using System.Runtime.InteropServices;

namespace MyMeta.MySql5;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IDatabase))]
#endif
public class MySql5Database : Database
{
    internal string _desc = "";

    internal bool _FKsInLoad;

    internal string _name = "";
    public override string Alias => _name;

    public override string Name => _name;

    public override string Description => _desc;
}