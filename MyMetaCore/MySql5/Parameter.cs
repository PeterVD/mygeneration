using System.Runtime.InteropServices;

namespace MyMeta.MySql5;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IParameter))]
#endif
public class MySql5Parameter : Parameter
{
}