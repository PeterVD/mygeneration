using System;
using System.Data;
using System.Runtime.InteropServices;

namespace MyMeta.MySql5;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IColumns))]
#endif
public class MySql5Columns : Columns
{
    internal DataColumn f_Extra;

    internal override void LoadForTable()
    {
        var query = @"SHOW COLUMNS FROM `" + Table.Name + "`";

        var metaData = new DataTable();
        var adapter = MySql5Databases.CreateAdapter(query, DbRoot.ConnectionString);

        adapter.Fill(metaData);

        if (metaData.Columns.Contains("Extra")) f_Extra = metaData.Columns["Extra"];

        metaData.Columns["Field"].ColumnName = "COLUMN_NAME";
        metaData.Columns["Type"].ColumnName = "DATA_TYPE";
        metaData.Columns["Null"].ColumnName = "IS_NULLABLE";
        metaData.Columns["Default"].ColumnName = "COLUMN_DEFAULT";

        PopulateArray(metaData);

        LoadTableColumnDescriptions();
    }

    internal override void LoadForView()
    {
        var db = View.Database as MySql5Database;
        var dbs = db.Databases as MySql5Databases;
        if (dbs.Version.StartsWith("5"))
        {
            var query = @"SHOW COLUMNS FROM `" + View.Name + "`";

            var metaData = new DataTable();
            var adapter = MySql5Databases.CreateAdapter(query, DbRoot.ConnectionString);

            adapter.Fill(metaData);

            if (metaData.Columns.Contains("Extra")) f_Extra = metaData.Columns["Extra"];

            metaData.Columns["Field"].ColumnName = "COLUMN_NAME";
            metaData.Columns["Type"].ColumnName = "DATA_TYPE";
            metaData.Columns["Null"].ColumnName = "IS_NULLABLE";
            metaData.Columns["Default"].ColumnName = "COLUMN_DEFAULT";

            PopulateArray(metaData);
        }
    }

    private void LoadTableColumnDescriptions()
    {
        try
        {
            var query = @"SELECT TABLE_NAME, COLUMN_NAME, COLUMN_COMMENT FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = '" +
                        Table.Database.Name + "' AND TABLE_NAME ='" + Table.Name + "'";

            var metaData = new DataTable();
            var adapter = MySql5Databases.CreateAdapter(query, DbRoot.ConnectionString);

            adapter.Fill(metaData);

            if (metaData.Rows.Count > 0)
                foreach (DataRow row in metaData.Rows)
                {
                    var c = this[row["COLUMN_NAME"] as string] as Column;

                    if (!c._row.Table.Columns.Contains("DESCRIPTION"))
                    {
                        c._row.Table.Columns.Add("DESCRIPTION", Types.StringType);
                        f_Description = c._row.Table.Columns["DESCRIPTION"];
                    }

                    c._row["DESCRIPTION"] = row["COLUMN_COMMENT"] as string;
                }
        }
        catch
        {
        }
    }
}