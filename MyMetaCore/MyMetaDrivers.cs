namespace MyMeta;

public static class MyMetaDrivers
{
    public const string Access = "ACCESS";
    public const string Advantage = "ADVANTAGE";
    public const string DB2 = "DB2";
    public const string Firebird = "FIREBIRD";
    public const string Interbase = "INTERBASE";
    public const string ISeries = "ISERIES";
    public const string MySql = "MYSQL";
    public const string MySql2 = "MYSQL2";
    public const string None = "NONE";
    public const string Oracle = "ORACLE";
    public const string Pervasive = "PERVASIVE";
    public const string PostgreSQL = "POSTGRESQL";
    public const string PostgreSQL8 = "POSTGRESQL8";
    public const string SQLite = "SQLITE";
    public const string SQL = "SQL";
#if !IGNORE_VISTA
    public const string VistaDB = "VISTADB";
#endif
    public static dbDriver GetDbDriverFromName(string name)
    {
        switch (name)
        {
            case SQL:
                return dbDriver.SQL;
            case Oracle:
                return dbDriver.Oracle;
            case Access:
                return dbDriver.Access;
            case MySql:
                return dbDriver.MySql;
            case MySql2:
                return dbDriver.MySql2;
            case DB2:
                return dbDriver.DB2;
            case ISeries:
                return dbDriver.ISeries;
            case Pervasive:
                return dbDriver.Pervasive;
            case PostgreSQL:
                return dbDriver.PostgreSQL;
            case PostgreSQL8:
                return dbDriver.PostgreSQL8;
            case Firebird:
                return dbDriver.Firebird;
            case Interbase:
                return dbDriver.Interbase;
            case SQLite:
                return dbDriver.SQLite;
#if !IGNORE_VISTA
            case VistaDB:
                return dbDriver.VistaDB;
#endif
            case Advantage:
                return dbDriver.Advantage;
            case None:
                return dbDriver.None;
            default:
                return dbDriver.Plugin;
        }
    }
}