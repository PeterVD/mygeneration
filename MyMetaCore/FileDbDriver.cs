using System;

namespace MyMeta;

internal class FileDbDriver : InternalDriver
{
    private readonly string _connPostfix;
    private readonly string _connPrefix;
    private readonly string _fileMask;
    private string _lastFileName;

    internal FileDbDriver(Type factory, string connPrefix, string connFileName, string connPostfix, string fileMask)
        : base(factory, "", true)
    {
        _connPrefix = connPrefix;
        _connPostfix = connPostfix;
        _fileMask = fileMask;
        LastFileName = connFileName;
    }

    public string LastFileName
    {
        get => _lastFileName;
        set
        {
            _lastFileName = value;
            ConnectString = $"{_connPrefix}{LastFileName}{_connPostfix}";
        }
    }

    public override string BrowseConnectionString(string connstr)
    {
        return BrowseFileConnectionString(connstr);
    }

    protected string BrowseFileConnectionString(string connstr)
    {
        //using var dlg = new OpenFileDialog();
        //if (connstr.StartsWith(_connPrefix) && connstr.EndsWith(_connPostfix))
        //{
        //    var filename = connstr.Substring(_connPrefix.Length, connstr.Length - _connPrefix.Length - _connPostfix.Length);
        //    dlg.FileName = filename;
        //}
        //else
        //{
        //    dlg.FileName = LastFileName;
        //}

        //dlg.Filter = _fileMask;
        //dlg.ValidateNames = true;
        //dlg.AddExtension = true;
        //if (dlg.ShowDialog() == DialogResult.OK)
        //{
        //    LastFileName = dlg.FileName;
        //    return ConnectString;
        //}

        return null;
    }
}