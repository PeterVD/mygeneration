using System.Runtime.InteropServices;

namespace MyMeta.MySql;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IParameters))]
#endif
public class MySqlParameters : Parameters
{
    internal override void LoadAll()
    {
        try
        {
            var metaData = LoadData(OleDbSchemaGuid.Procedure_Parameters,
                new object[] { Procedure.Database.Name, null, Procedure.Name });

            PopulateArray(metaData);
        }
        catch
        {
        }
    }
}