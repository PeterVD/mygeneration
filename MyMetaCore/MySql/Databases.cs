using System.Runtime.InteropServices;

namespace MyMeta.MySql;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IDatabases))]
#endif
public class MySqlDatabases : Databases
{
    internal override void LoadAll()
    {
        try
        {
            OleDbConnection cn = new OleDbConnection(DbRoot.ConnectionString);

            // We add our one and only Database
            var database = (MySqlDatabase)DbRoot.ClassFactory.CreateDatabase();
            database._name = cn.DataSource;
            database.DbRoot = DbRoot;
            database.Databases = this;
            _array.Add(database);
        }
        catch
        {
        }
    }
}