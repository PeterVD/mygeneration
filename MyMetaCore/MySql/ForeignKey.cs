using System.Runtime.InteropServices;

namespace MyMeta.MySql;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IForeignKey))]
#endif
public class MySqlForeignKey : ForeignKey
{
    public override ITable ForeignTable
    {
        get
        {
            var catalog = ForeignKeys.Table.Database.Name;
            var schema = GetString(ForeignKeys.f_FKTableSchema);

            return DbRoot.Databases[catalog].Tables[GetString(ForeignKeys.f_FKTableName)];
        }
    }
}