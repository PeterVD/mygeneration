using System.Runtime.InteropServices;

namespace MyMeta.MySql;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IIndexes))]
#endif
public class MySqlIndexes : Indexes
{
    internal override void LoadAll()
    {
        try
        {
            var metaData = LoadData(OleDbSchemaGuid.Indexes,
                new object[] { Table.Database.Name, null, null, null, Table.Name });

            PopulateArray(metaData);
        }
        catch
        {
        }
    }
}