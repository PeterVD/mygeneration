using System.Runtime.InteropServices;

namespace MyMeta.MySql;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(ITables))]
#endif
public class MySqlTables : Tables
{
    internal override void LoadAll()
    {
        try
        {
            var type = DbRoot.ShowSystemData ? "SYSTEM TABLE" : "TABLE";
            var metaData = LoadData(OleDbSchemaGuid.Tables, new object[] { Database.Name, null, null, type });

            PopulateArray(metaData);
        }
        catch
        {
        }
    }
}