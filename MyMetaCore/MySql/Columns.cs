using System;
using System.Text;

namespace MyMeta.MySql;

public class MySqlColumns : Columns
{
    internal DataColumn f_AutoKey;
    internal DataColumn f_InPrimaryKey;
    internal DataColumn f_TypeName;

    internal override void LoadForTable()
    {
        var metaData = LoadData(OleDbSchemaGuid.Columns, [Table.Database.Name, null, Table.Name]);

        PopulateArray(metaData);

        LoadExtraData(Table.Name);
    }

    internal override void LoadForView()
    {
        var metaData = LoadData(OleDbSchemaGuid.Columns, [View.Database.Name, null, View.Name]);

        PopulateArray(metaData);

        LoadExtraData(View.Name);
    }

    private void LoadExtraData(string name)
    {
        try
        {
            var myConString = GetConnectionString();
            
            var select = $"show fields from {name}";
            using var adapter = new OdbcDataAdapter(select, myConString.ToString());
            using var dataTable = new DataTable();

            adapter.Fill(dataTable);

            if (_array.Count <= 0) return;

            var col = _array[0] as Column;

            f_TypeName = new DataColumn("TYPE_NAME", Types.StringType);
            col._row.Table.Columns.Add(f_TypeName);

            f_AutoKey = new DataColumn("AUTO_INCREMENT", Types.BoolType);
            col._row.Table.Columns.Add(f_AutoKey);

            f_InPrimaryKey = new DataColumn("IN_PRIMARY_KEY", Types.BoolType);
            col._row.Table.Columns.Add(f_InPrimaryKey);

            var rows = dataTable.Rows;

            var count = _array.Count;

            for (var index = 0; index < count; index++)
            {
                var c = (Column)this[index];

                // TypeName
                c._row["TYPE_NAME"] = rows[index]["Type"] as string;

                // IsAutoKey
                var extra = rows[index]["Extra"] as string;

                c._row["AUTO_INCREMENT"] = extra.StartsWith("auto_increment", StringComparison.InvariantCultureIgnoreCase);

                // IsInPrimaryKey
                var key = rows[index]["Key"] as string;

                c._row["IN_PRIMARY_KEY"] = key.StartsWith("PRI", StringComparison.InvariantCultureIgnoreCase);
            }
        }
        catch
        {
        }
    }

    private StringBuilder GetConnectionString()
    {
        var conn = DbRoot.ParsedConnectionString;

        var myConString = new StringBuilder("DRIVER={MySQL ODBC 3.51 Driver}");

        myConString.Append(";SERVER=");
        if (conn.TryGetValue("LOCATION", out var location))
        {
            myConString.Append(string.IsNullOrWhiteSpace(location) ? "localhost" : location);
        }
        else
        {
            if (conn.TryGetValue("SERVER", out var server) && !string.IsNullOrWhiteSpace(server))
            {
                myConString.Append(server);
            }
            else
            {
                myConString.Append("localhost");
            }
        }

        myConString.Append(";DATABASE=");
        if (conn.TryGetValue("DB", out var db))
        {
            myConString.Append(db);
        }
        else
        {
            if (conn.TryGetValue("DATA SOURCE", out var dataSource))
            {
                myConString.Append(dataSource);
            }
        }

        myConString.Append(";UID=");
        if (conn.TryGetValue("UID", out var uid))
        {
            myConString.Append(uid);
        }
        else
        {
            if (conn.TryGetValue("USER", out var user))
            {
                myConString.Append(user);
            }
        }

        myConString.Append(";PWD=");
        if (conn.TryGetValue("PWD", out var pwd))
        {
            myConString.Append(pwd);
        }
        else
        {
            if (conn.TryGetValue("PASSWORD", out var password))
            {
                myConString.Append(password);
            }
        }

        myConString.Append(";OPTION=3");
        return myConString;
    }
}