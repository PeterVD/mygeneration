using System.Runtime.InteropServices;

namespace MyMeta.MySql;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IViews))]
#endif
public class MySqlViews : Views
{
    internal override void LoadAll()
    {
        try
        {
            var type = DbRoot.ShowSystemData ? "SYSTEM VIEW" : "VIEW";
            var metaData = LoadData(OleDbSchemaGuid.Tables, new object[] { Database.Name, null, null, type });

            PopulateArray(metaData);
        }
        catch
        {
        }
    }
}