using System.Runtime.InteropServices;

namespace MyMeta.MySql;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IColumn))]
#endif
public class MySqlColumn : Column
{
    public override bool IsInPrimaryKey
    {
        get
        {
            var cols = Columns as MySqlColumns;
            return GetBool(cols.f_InPrimaryKey);
        }
    }

    public override bool IsAutoKey
    {
        get
        {
            var cols = Columns as MySqlColumns;
            return GetBool(cols.f_AutoKey);
        }
    }

    public override string DataTypeName
    {
        get
        {
            var cols = Columns as MySqlColumns;
            var type = GetString(cols.f_TypeName);
            var startIndex = type.IndexOf("(");
            if (-1 != startIndex)
            {
                var endIndex = type.IndexOf(")");
                type = type.Remove(startIndex, endIndex - startIndex + 1);
            }

            return type;
        }
    }

    public override string DataTypeNameComplete
    {
        get
        {
            var cols = Columns as MySqlColumns;
            return GetString(cols.f_TypeName);
        }
    }

    internal override Column Clone()
    {
        var c = base.Clone();

        return c;
    }
}