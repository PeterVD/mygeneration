using System.Runtime.InteropServices;

namespace MyMeta.MySql;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(ITable))]
#endif
public class MySqlTable : Table
{
    public override IColumns PrimaryKeys
    {
        get
        {
            if (null == _primaryKeys)
            {
                var metaData = LoadData(OleDbSchemaGuid.Primary_Keys,
                    new object[] { Tables.Database.Name, null, Name });

                _primaryKeys = (Columns)DbRoot.ClassFactory.CreateColumns();
                _primaryKeys.Table = this;
                _primaryKeys.DbRoot = DbRoot;

                var colName = "";

                var count = metaData.Rows.Count;
                for (var i = 0; i < count; i++)
                {
                    colName = metaData.Rows[i]["COLUMN_NAME"] as string;
                    _primaryKeys.AddColumn((Column)Columns[colName]);
                }
            }

            return _primaryKeys;
        }
    }

//		public override IColumns PrimaryKeys
//		{
//			get
//			{
//				if(null == _primaryKeys)
//				{
//					OleDbConnection cn = new OleDbConnection(this.dbRoot.ConnectionString); 
//					cn.Open(); 
//					DataTable metaData = cn.GetOleDbSchemaTable(OleDbSchemaGuid.Primary_Keys, 
//						new Object[] {null, this.Tables.Database.Name, this.Name});
//					cn.Close();
//
//					_primaryKeys = (Columns)this.dbRoot.ClassFactory.CreateColumns();
//
//					Columns cols = (Columns)this.Columns;
//
//					string colName = "";
//
//					int count = metaData.Rows.Count;
//					for(int i = 0; i < count; i++)
//					{
//						colName = metaData.Rows[i]["COLUMN_NAME"] as string;
//						_primaryKeys.AddColumn((Column)cols[colName]);
//					}
//				}
//
//				return _primaryKeys;
//			}
//		}
}