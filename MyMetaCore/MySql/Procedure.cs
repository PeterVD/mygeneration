using System.Runtime.InteropServices;

namespace MyMeta.MySql;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IProcedure))]
#endif
public class MySqlProcedure : Procedure
{
}