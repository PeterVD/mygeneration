using System.Runtime.InteropServices;

namespace MyMeta.VistaDB;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IDomain))]
#endif
public class VistaDBDomain : Domain
{
}