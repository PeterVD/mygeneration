using System.Runtime.InteropServices;

namespace MyMeta.VistaDB;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IIndexes))]
#endif
public class VistaDBIndexes : Indexes
{
    internal override void LoadAll()
    {
        try
        {
            var db = (VistaDBDatabase)Table.Database;

            var metaData = db._mh.LoadIndexes(DbRoot.ConnectionString, Table.Database.Name, Table.Name);

            PopulateArray(metaData);
        }
        catch
        {
        }
    }
}