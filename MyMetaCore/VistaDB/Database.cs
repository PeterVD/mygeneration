using System.Runtime.InteropServices;

namespace MyMeta.VistaDB;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IDatabase))]
#endif
public class VistaDBDatabase : Database
{
    internal string _desc = "";

    internal bool _FKsInLoad;
    internal object _metaHelper;
    internal MetaHelper _mh = new();

    internal string _name = "";
    public override string Alias => _name;

    public override string Name => _name;

    public override string Description => _desc;
}