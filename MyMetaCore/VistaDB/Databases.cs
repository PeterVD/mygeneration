using System.Runtime.InteropServices;

namespace MyMeta.VistaDB;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IDatabases))]
#endif
public class VistaDBDatabases : Databases
{
    internal override void LoadAll()
    {
        try
        {
            var mh = new MetaHelper();
            var dbName = mh.LoadDatabases(DbRoot.ConnectionString);

            var database = (VistaDBDatabase)DbRoot.ClassFactory.CreateDatabase();

            database._name = dbName;
            database.DbRoot = DbRoot;
            database.Databases = this;

            _array.Add(database);
        }
        catch
        {
        }
    }
}