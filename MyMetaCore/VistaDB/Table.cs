using System.Runtime.InteropServices;

namespace MyMeta.VistaDB;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(ITable))]
#endif
public class VistaDBTable : Table
{
    public override IColumns PrimaryKeys
    {
        get
        {
            if (null == _primaryKeys)
            {
                _primaryKeys = (Columns)DbRoot.ClassFactory.CreateColumns();
                _primaryKeys.Table = this;
                _primaryKeys.DbRoot = DbRoot;

                foreach (IColumn col in Columns)
                    if (col.IsInPrimaryKey)
                        _primaryKeys.AddColumn((Column)Columns[col.Name]);
            }

            return _primaryKeys;
        }
    }
}