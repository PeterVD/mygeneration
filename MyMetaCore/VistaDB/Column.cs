using System.Runtime.InteropServices;

namespace MyMeta.VistaDB;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IColumn))]
#endif
public class VistaDBColumn : Column
{
    public override string DataTypeName
    {
        get
        {
            var cols = Columns as VistaDBColumns;
            return GetString(cols.f_TypeName);
        }
    }

    public override bool IsInPrimaryKey
    {
        get
        {
            var cols = Columns as VistaDBColumns;
            return GetBool(cols.f_InPrimaryKey);
        }
    }


    public override string DataTypeNameComplete => "Unknown";

    internal override Column Clone()
    {
        var c = base.Clone();

        return c;
    }
    //				VistaDBColumns cols = Columns as VistaDBColumns;
    //				return this.GetString(cols.f_TypeName);
}