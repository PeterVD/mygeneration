using System;
//using System.Windows.Forms;

namespace Provider.VistaDB;

/// <summary>
///     VistaDBSQL class for managing V-SQL query statements.
/// </summary>
internal abstract class VistaDBSQLConnection : IDisposable
{
    protected bool caseSensitivity;
    protected int clusterSize;
    protected int cultureID;
    protected CypherType cypher;
    protected string database;
    protected string databaseDescription;
    protected string dataSource;
    protected bool exclusive;
    protected bool opened;
    protected string password;
    protected VistaDBSQLQuery[] queries;
    protected bool readOnly;
    protected object syncRoot = new();

    /// <summary>
    ///     Constructor.
    /// </summary>
    public VistaDBSQLConnection()
    {
        dataSource = "";
        database = "";
        cypher = CypherType.None;
        password = "";
        exclusive = false;
        readOnly = false;
        opened = false;
        queries = new VistaDBSQLQuery[0];
        cultureID = 0;
        clusterSize = 0;
        caseSensitivity = false;
        databaseDescription = "";
    }

    /// <summary>
    ///     Gets or sets the data source.
    /// </summary>
    public virtual string DataSource
    {
        get => dataSource;
        set => dataSource = value;
    }

    /// <summary>
    ///     Gets or sets the database name
    /// </summary>
    public string Database
    {
        get => database;
        set => database = value;
    }

    /// <summary>
    ///     Gets or sets the database password.
    /// </summary>
    public string Password
    {
        get => password;

        set => password = value;
    }

    /// <summary>
    ///     Gets or sets the database encryption type, or Cypher type.
    /// </summary>
    public CypherType Cypher
    {
        get => cypher;

        set => cypher = value;
    }

    /// <summary>
    ///     Gets or sets if a database is to be opened in exclusive mode. Required for altering the database schema.
    /// </summary>
    public bool Exclusive
    {
        get => exclusive;
        set => exclusive = value;
    }

    /// <summary>
    ///     Gets or sets if a database is to be opened in readonly mode.
    /// </summary>
    public bool ReadOnly
    {
        get => readOnly;
        set => readOnly = value;
    }

    public int CultureID => cultureID;

    public int ClusterSize => clusterSize;

    public bool CaseSensitivity => caseSensitivity;

    public string DatabaseDescription => databaseDescription;

    public abstract void Dispose();

    /// <summary>
    ///     Open a database connection to a VistaDB database.
    /// </summary>
    /// <returns></returns>
    public abstract void OpenDatabaseConnection();

    /// <summary>
    ///     Close an active database connection.
    /// </summary>
    public virtual void CloseDatabaseConnection()
    {
        lock (syncRoot)
        {
            for (var i = 0; i < queries.Length; i++)
            {
                queries[i].Close();
                queries[i].FreeQuery();
            }
        }
    }

    protected abstract VistaDBSQLQuery CreateSQLQuery();

    /// <summary>
    ///     Create new query for this connection.
    /// </summary>
    /// <returns></returns>
    public VistaDBSQLQuery NewSQLQuery()
    {
        VistaDBSQLQuery query;
        VistaDBSQLQuery[] newQueries;

        query = CreateSQLQuery();

        lock (syncRoot)
        {
            newQueries = new VistaDBSQLQuery[queries.Length + 1];

            for (var i = 0; i < queries.Length; i++) newQueries[i] = queries[i];

            queries = newQueries;

            queries[queries.GetUpperBound(0)] = query;
        }

        return query;
    }

    public bool DropQuery(VistaDBSQLQuery query)
    {
        var indexQuery = -1;
        VistaDBSQLQuery[] newQueries;

        lock (syncRoot)
        {
            for (var i = 0; i < queries.Length; i++)
                if (queries[i] == query)
                {
                    indexQuery = i;
                    break;
                }

            if (indexQuery < 0)
                return false;

            queries[indexQuery].Close();
            queries[indexQuery].FreeQuery();

            newQueries = new VistaDBSQLQuery[queries.Length - 1];

            for (var i = 0; i < newQueries.Length; i++)
                if (i < indexQuery)
                    newQueries[i] = queries[i];
                else
                    newQueries[i] = queries[i + 1];

            queries = newQueries;
        }

        return true;
    }

    /// <summary>
    ///     Begin a transaction. Transactions may be nested.
    /// </summary>
    public abstract bool BeginTransaction();

    /// <summary>
    ///     Commit an active transaction. Transactions may be nested.
    /// </summary>
    public abstract bool CommitTransaction();

    /// <summary>
    ///     Rollback the active transaction. Transactions may be nested.
    /// </summary>
    public abstract void RollbackTransaction();
}