using System;
using System.Globalization;
using System.Text;

namespace Provider.VistaDB;

/// <summary>
///     Summary description for VistaDBRemoteQuery.
/// </summary>
internal class VistaDBRemoteQuery : VistaDBSQLQuery
{
    private const byte FLD_NULL = 0;
    private const byte FLD_NOTNULL = 1;
    private readonly int fetchCount;
    private readonly RemoteParameterCollection parameterCollection;
    private int currentRow;
    private int lastRowNo, firstRowNo;

    private int queryID;
    private int recordSize;
    private object[,] values;

    public VistaDBRemoteQuery(VistaDBSQLConnection parent) : base(parent)
    {
        fetchCount = 100;
        parameterCollection = new RemoteParameterCollection();
        queryID = 0; //srv_Prepare();
    }

    private VistaDBRemoteConnection Parent => (VistaDBRemoteConnection)parent;

    #region Remote access function

    private string[] srv_GetStructure()
    {
        string answer;
        string[] result;
        int linesCount;
        int index, endIndex;
        int len;

        //Send request
        Parent.SendMessage(ActionList.sc_sql_GETSTRUCTURE, queryID.ToString("X8"));

        //Receive answer
        Parent.ReceiveMessage(out answer);

        //Calculate line count
        linesCount = 1;
        index = 0;
        while (true)
        {
            index = answer.IndexOf(Environment.NewLine, index);
            if (index >= 0)
                linesCount++;
            else
                break;
            index += Environment.NewLine.Length;
        }

        //Break line
        result = new string[linesCount];
        index = 0;
        for (var i = 0; i < linesCount; i++)
        {
            endIndex = answer.IndexOf(Environment.NewLine, index);

            if (endIndex < 0)
                len = answer.Length - index;
            else
                len = endIndex - index;

            result[i] = answer.Substring(index, len);
            index = endIndex + Environment.NewLine.Length;
        }

        return result;
    }

    private int srv_Prepare()
    {
        var size = 0;
        byte[] buffer;
        int queryID;
        int errorCode;
        string answer;
        var encoding = Encoding.Default;

        //Calc buffer size and create buffer
        size = 255 + parameterCollection.GetParameters(null, 0, true);
        buffer = new byte[size];

        //Put info into the buffer
        encoding.GetBytes(Parent.Database.PadRight(255)).CopyTo(buffer, 0);
        parameterCollection.GetParameters(buffer, 255, false);

        //Send request
        Parent.SendMessage(ActionList.sc_sql_PREPARE, buffer);

        //Get answer
        Parent.ReceiveMessage(out answer);

        //Parse answer
        queryID = int.Parse(answer.Substring(0, 8), NumberStyles.AllowHexSpecifier);
        errorCode = int.Parse(answer.Substring(8, 8), NumberStyles.AllowHexSpecifier);

        if (errorCode != 0)
        {
            var message = answer.Substring(16, answer.Length - 16);
            throw new VistaDBException(message, null, true, null);
        }

        return queryID;
    }

    private void srv_OpenSQL()
    {
        int errorCode;
        var encoding = Encoding.Default;
        string answer;

        //Send messege
        Parent.SendMessage(ActionList.sc_sql_OPENSQL, queryID.ToString("X8") + commandText.Trim());

        //Receive answer
        Parent.ReceiveMessage(out answer);

        //Get error code
        errorCode = int.Parse(answer.Substring(24, 8), NumberStyles.AllowHexSpecifier);

        if (errorCode != 0)
        {
            var message = answer.Substring(32, answer.Length - 32);
            throw new VistaDBException(message, null, true, null);
        }

        recordCount = int.Parse(answer.Substring(0, 8), NumberStyles.AllowHexSpecifier);
        columnCount = int.Parse(answer.Substring(8, 8), NumberStyles.AllowHexSpecifier);
        recordSize = int.Parse(answer.Substring(16, 8), NumberStyles.AllowHexSpecifier);
    }

    private void srv_UnPrepare(int sqlID)
    {
        byte[] buffer;

        //Send request
        Parent.SendMessage(ActionList.sc_sql_UNPREPARE, sqlID.ToString("X8"));

        //Receive answer
        Parent.ReceiveMessage(out buffer);
    }

    private byte[] srv_GetRecord(int recNo)
    {
        int bufferSize;
        int curFetchCount;
        var buffer = new StringBuilder(8 + 8 + 8 + 8);
        byte[] answer;

        if (fetchCount > recordCount - recNo + 1)
            curFetchCount = recordCount - recNo + 1;
        else
            curFetchCount = fetchCount;

        bufferSize = (recordSize + 8) * curFetchCount + 8;

        //Put info into the buffer
        buffer.Insert(0, queryID.ToString("X8"));
        buffer.Insert(8, recNo.ToString("X8"));
        buffer.Insert(16, curFetchCount.ToString("X8"));
        buffer.Insert(24, bufferSize.ToString("X8"));

        //Send request
        Parent.SendMessage(ActionList.sc_sql_READRECORD, buffer.ToString());

        //Receive answer
        Parent.ReceiveMessage(out answer);

        return answer;
    }

    private byte[] srv_ReadBlobData(int recNo, int fieldIndex)
    {
        byte[] answer;
        var buffer = new StringBuilder(8 + 8 + 8);

        //Put info into the buffer
        buffer.Insert(0, queryID.ToString("X8"));
        buffer.Insert(8, recNo.ToString("X8"));
        buffer.Insert(16, fieldIndex.ToString("X8"));

        //Send message
        Parent.SendMessage(ActionList.sc_sql_READBLOBDATA, buffer.ToString());

        //Receive answer
        Parent.ReceiveMessage(out answer);

        return answer;
    }

    private void srv_ExecSQL()
    {
        var len = commandText.Length;
        byte[] buffer;
        int size;
        var encoding = Encoding.Default;
        string answer;
        int errorCode;

        //Create buffer
        size = 16 + len + parameterCollection.GetParameters(null, 0, true);
        buffer = new byte[size];

        //Put data into the buffer
        encoding.GetBytes(queryID.ToString("X8")).CopyTo(buffer, 0);
        encoding.GetBytes(len.ToString("X8")).CopyTo(buffer, 8);
        encoding.GetBytes(commandText).CopyTo(buffer, 16);
        parameterCollection.GetParameters(buffer, 16 + len, false);

        //Send message
        Parent.SendMessage(ActionList.sc_sql_EXECSQL, buffer);

        //Receive answer
        Parent.ReceiveMessage(out answer);

        //Check if here error occured
        errorCode = int.Parse(answer.Substring(0, 8), NumberStyles.AllowHexSpecifier);
        if (errorCode != 0)
        {
            var message = answer.Substring(8, answer.Length - 8);
            throw new VistaDBException(message, null, true, null);
        }
    }

    private void srv_Close()
    {
        byte[] answer;

        //Send request
        Parent.SendMessage(ActionList.sc_sql_CLOSESQL, queryID.ToString("X8"));

        //Receive answer
        Parent.ReceiveMessage(out answer);
    }

    #endregion Remote access function

    #region Overriden functions

    public override void CreateQuery()
    {
    }

    public override void FreeQuery()
    {
    }

    public override void Open()
    {
        if (queryID != 0)
            return;

        lock (this)
        {
            queryID = srv_Prepare();
            srv_OpenSQL();
            InternalInitFieldDefs();
            First();
            opened = true;
            values = null;
            lastRowNo = -1;
            firstRowNo = -1;
        }
    }

    public override void Close()
    {
        if (opened)
            lock (this)
            {
                srv_Close();
                srv_UnPrepare(queryID);
                columns = null;
                values = null;
                opened = false;
            }

        if (queryID != 0)
            try
            {
                srv_UnPrepare(queryID);
            }
            finally
            {
                queryID = 0;
            }
    }

    public override void ExecSQL()
    {
        if (opened)
            throw new InvalidOperationException("Query opened");

        lock (this)
        {
            if (queryID == 0)
                queryID = srv_Prepare();

            srv_ExecSQL();

            rowsAffected = 1;
        }
    }

    public override void SetParameter(string paramName, VistaDBType dataType, object value)
    {
        lock (this)
        {
            parameterCollection.SetParameter(paramName, dataType, value);
        }
    }

    public override bool ParamIsNull(string pName)
    {
        return false;
    }

    public override void SetParamNull(string pName, VistaDBType type)
    {
        lock (this)
        {
            parameterCollection.SetParameter(pName, type, null);
        }
    }

    public override bool First()
    {
        lock (this)
        {
            currentRow = 0; // set before prior position
        }

        return true;
    }

    public override bool Next()
    {
        lock (this)
        {
            if (currentRow < recordCount)
                currentRow++;
        }

        return true;
    }

    public override bool Eof => currentRow == recordCount;

    public override object GetValue(int fieldNo)
    {
        lock (this)
        {
            if (FetchRow(currentRow))
                return values[currentRow - firstRowNo, fieldNo];
            return null;
        }
    }

    public override bool IsNull(int columnNumber)
    {
        return GetValue(columnNumber) is null;
    }

    #endregion Overriden functions

    #region Private Methods

    private void InternalInitFieldDefs()
    {
        if (queryID == 0)
            return;

        string[] buffer;
        string columnName, columnCaption;
        VistaDBType columnType;
        bool allowNull, readOnly, autoIncrement, primaryKey, unique, reservedWord;
        int dataSize, columnWidth;
        int index;

        lock (this)
        {
            columns = new VistaDBColumn[columnCount];
            buffer = srv_GetStructure();
            index = 0;

            for (var i = 0; i < columnCount; i++)
            {
                columnName = buffer[index];
                columnType = VistaDBAPI.NetDataType(buffer[index + 1]);
                columnWidth = int.Parse(buffer[index + 2], NumberStyles.AllowHexSpecifier);
                columnCaption = buffer[index + 3];
                allowNull = buffer[index + 4] != "Y";
                readOnly = buffer[index + 5] == "Y";
                autoIncrement = buffer[index + 6] == "Y";
                primaryKey = buffer[index + 7] == "Y";
                unique = buffer[index + 8] == "Y";
                reservedWord = buffer[index + 9] == "Y";

                dataSize = 0; // default

                switch (columnType)
                {
                    case VistaDBType.Character:
                        dataSize = columnWidth;
                        break;
                    case VistaDBType.Varchar:
                        dataSize = columnWidth;
                        break;
                    case VistaDBType.Date:
                        dataSize = 8;
                        break;
                    case VistaDBType.DateTime:
                        dataSize = 8;
                        break;
                    case VistaDBType.Boolean:
                        dataSize = 2;
                        break;
                    case VistaDBType.Int32:
                        dataSize = 4;
                        break;
                    case VistaDBType.Int64:
                        dataSize = 8;
                        break;
                    case VistaDBType.Currency:
                        dataSize = 8;
                        break;
                    case VistaDBType.Double:
                        dataSize = 8;
                        break;
                    case VistaDBType.Memo:
                    case VistaDBType.Blob:
                    case VistaDBType.Picture:
                        dataSize = 2147483647;
                        break;
                    case VistaDBType.Guid:
                        dataSize = 16;
                        break;
                }

                columns[i] = new VistaDBColumn(columnName, columnType, dataSize, (short)columnWidth, 0, allowNull, readOnly, primaryKey, unique,
                    autoIncrement, 0, "", columnCaption, "", reservedWord, false, false, false, false);

                index += 10;
            }
        }
    }

    private bool FetchRow(int rowNo)
    {
        if (rowNo < 0 || rowNo >= recordCount)
            return false;

        //Create data buffer
        if (values is null)
        {
            if (recordCount < fetchCount)
                values = new object[recordCount, columnCount];
            else
                values = new object[fetchCount, columnCount];
        }
        else if (rowNo <= lastRowNo && rowNo >= firstRowNo)
        {
            return true;
        }

        byte[] buffer;
        var encoding = Encoding.Default;
        int rowToRead;
        int ptr;

        //Get rows from the server
        buffer = srv_GetRecord(rowNo + 1);

        //Parse result

        //Get read row
        rowToRead = int.Parse(encoding.GetString(buffer, 0, 8), NumberStyles.AllowHexSpecifier);

        if (rowToRead == 0)
            throw new VistaDBException("Server send wrong row count", VistaDBErrorCodes.ServerError);

        //Parse rows
        ptr = 8;
        for (var i = 0; i < rowToRead; i++)
        {
            if (i == rowToRead - 1)
                lastRowNo = int.Parse(encoding.GetString(buffer, ptr, 8), NumberStyles.AllowHexSpecifier) - 1;
            GetRowFromRawData(i, buffer, ref ptr);
        }

        firstRowNo = int.Parse(encoding.GetString(buffer, 8, 8), NumberStyles.AllowHexSpecifier) - 1;

        return true;
    }

    private void GetRowFromRawData(int index, byte[] buffer, ref int ptr)
    {
        var encoding = Encoding.Default;
        long i64;
        int rowNo;
        bool isNull;

        rowNo = int.Parse(encoding.GetString(buffer, ptr, 8), NumberStyles.AllowHexSpecifier);
        ptr += 8;

        for (var i = 0; i < columnCount; i++)
        {
            isNull = buffer[ptr] == FLD_NULL;

            if (isNull)
                values[index, i] = null;

            ptr++;

            switch (columns[i].VistaDBType)
            {
                case VistaDBType.Blob:
                case VistaDBType.Picture:
                    if (!isNull)
                        values[index, i] = srv_ReadBlobData(rowNo, i + 1);
                    break;
                case VistaDBType.Memo:
                    if (!isNull)
                        values[index, i] = encoding.GetString(srv_ReadBlobData(rowNo, i + 1));
                    break;
                case VistaDBType.Boolean:
                    if (!isNull)
                        values[index, i] = buffer[ptr] != 0 || buffer[ptr + 1] != 0;
                    ptr += 2;
                    break;
                case VistaDBType.Character:
                case VistaDBType.Varchar:
                    if (!isNull)
                        values[index, i] = encoding.GetString(buffer, ptr, columns[i].ColumnWidth).Trim();
                    ptr += columns[i].ColumnWidth + 1;
                    break;
                case VistaDBType.Currency:
                    if (!isNull)
                        values[index, i] = BcdToCurr(buffer, ptr);
                    ptr += 34;
                    break;
                case VistaDBType.Date:
                    if (!isNull)
                    {
                        i64 = (BitConverter.ToInt32(buffer, ptr) - 1) * VistaDBAPI.MSecsPerDay * 10000;
                        values[index, i] = new DateTime(i64);
                    }

                    ptr += 4;
                    break;
                case VistaDBType.DateTime:
                    if (!isNull)
                    {
                        i64 = (long)(BitConverter.ToDouble(buffer, ptr) * 10000);
                        values[index, i] = new DateTime(i64);
                    }

                    ptr += 8;
                    break;
                case VistaDBType.Double:
                    if (!isNull)
                        values[index, i] = BitConverter.ToDouble(buffer, ptr);
                    ptr += 8;
                    break;
                case VistaDBType.Guid:
                    if (!isNull)
                        values[index, i] = new Guid(encoding.GetString(buffer, ptr, 38).Substring(1, 36));
                    ptr += 39;
                    break;
                case VistaDBType.Int32:
                    if (!isNull)
                        values[index, i] = BitConverter.ToInt32(buffer, ptr);
                    ptr += 4;
                    break;
                case VistaDBType.Int64:
                    if (!isNull)
                        values[index, i] = BitConverter.ToInt64(buffer, ptr);
                    ptr += 8;
                    break;
            }
        }
    }

    private decimal BcdToCurr(byte[] buffer, int ptr)
    {
        //Convert TBcd type to decimal
        long power = 1;
        var first = buffer[ptr] / 2;
        long i64 = 0;
        byte nibble;

        for (var i = first - 1; i >= 0; i--)
        {
            nibble = (byte)((buffer[i + ptr + 2] & 0x0F) + 10 * ((buffer[i + ptr + 2] >> 4) & 0x0F));
            i64 += power * nibble;
            power = power * 100;
        }

        if ((buffer[ptr + 1] & 0x80) == 0x80)
            i64 = -i64;

        return i64 / (decimal)10000;
    }

    #endregion Private Methods
}