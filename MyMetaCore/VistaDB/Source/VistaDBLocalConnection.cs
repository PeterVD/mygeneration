namespace Provider.VistaDB;

/// <summary>
///     VistaDBSQL class for managing V-SQL query statements.
/// </summary>
internal class VistaDBLocalConnection : VistaDBSQLConnection
{
    public VistaDBLocalConnection()
    {
        ConnectionID = VistaDBAPI.ivsql_CreateDatabaseConnection();
    }

    public VistaDBLocalConnection(int cultureID, bool caseSensitive) : this()
    {
        var dbID = VistaDBAPI.ivdb_CreateDatabase(null, null, false, false, (uint)cultureID,
            (uint)VDBDatabaseParam.InMemory, null, 0, caseSensitive);

        try
        {
            if (!VistaDBAPI.ivsql_AssignDatabaseConnection(ConnectionID, dbID, null, true, false, 0, null, caseSensitive))
                throw new VistaDBException(VistaDBErrorCodes.DatabaseNotOpened);

            this.cultureID = cultureID;
            caseSensitivity = caseSensitivity;
            opened = true;
        }
        catch
        {
            VistaDBAPI.ivdb_SelectDb(dbID);
            VistaDBAPI.ivdb_CloseDatabase();
            throw;
        }
    }

    public VistaDBLocalConnection(VistaDBDatabase db) : this()
    {
        if (!VistaDBAPI.ivsql_AssignDatabaseConnection(ConnectionID, db.DatabaseId, null, true, false, 0, null, db.CaseSensitive))
            throw new VistaDBException(VistaDBErrorCodes.DatabaseNotOpened);

        db.FreeDatabase();

        cultureID = db.Locale;
        caseSensitivity = db.CaseSensitive;
        opened = true;
    }

    /// <summary>
    ///     Return a unique connection ID to the opened database.
    /// </summary>
    public int ConnectionID { get; private set; }

    /// <summary>
    ///     Gets or sets the data source.
    /// </summary>
    public override string DataSource
    {
        set
        {
            dataSource = value;
            Database = value;
        }
    }

    ~VistaDBLocalConnection()
    {
        Dispose();
    }

    /// <summary>
    ///     Open a database connection to a VistaDB database.
    /// </summary>
    public override void OpenDatabaseConnection()
    {
        if (opened)
            return;

        var success = false;
        CypherType _cypher;
        string _password;
        int dbID;

        lock (syncRoot)
        {
            try
            {
                if (cypher == CypherType.None)
                {
                    _cypher = CypherType.Blowfish;
                    _password = "";
                }
                else
                {
                    _cypher = cypher;
                    _password = password;
                }

                success = VistaDBAPI.ivsql_OpenDatabaseConnection(ConnectionID, dataSource, exclusive, readOnly, _cypher, _password, false);
            }
            catch (VistaDBException e)
            {
                if (!e.Critical)
                {
                    for (var i = 0; i < queries.Length; i++)
                        queries[i].CreateQuery();

                    opened = true;
                }

                throw;
            }

            if (!success) throw new VistaDBException(VistaDBErrorCodes.SQLDatabaseCouldNotBeFound);

            opened = true;

            for (var i = 0; i < queries.Length; i++)
                queries[i].CreateQuery();

            dbID = VistaDBAPI.ivsql_GetCurrentDatabaseID(ConnectionID);

            cultureID = (int)VistaDBAPI.ivdb_GetDatabaseCultureId();
            clusterSize = VistaDBAPI.ivdb_GetClusterLength();
            caseSensitivity = VistaDBAPI.ivdb_GetCaseSensitive();
            databaseDescription = VistaDBAPI.ivdb_GetDatabaseDescription();
        }
    }

    /// <summary>
    ///     Close an active database connection.
    /// </summary>
    public override void CloseDatabaseConnection()
    {
        if (!opened)
            return;

        base.CloseDatabaseConnection();

        lock (syncRoot)
        {
            VistaDBAPI.ivsql_CloseDatabaseConnection(ConnectionID);
            opened = false;
        }
    }

    protected override VistaDBSQLQuery CreateSQLQuery()
    {
        return new VistaDBLocalQuery(this);
    }

    /// <summary>
    ///     Begin a transaction. Transactions may be nested.
    /// </summary>
    public override bool BeginTransaction()
    {
        bool res;

        res = VistaDBAPI.ivsql_BeginTransaction(ConnectionID);

        return res;
    }

    /// <summary>
    ///     Commit an active transaction. Transactions may be nested.
    /// </summary>
    public override bool CommitTransaction()
    {
        bool res;

        res = VistaDBAPI.ivsql_CommitTransaction(ConnectionID);

        return res;
    }

    /// <summary>
    ///     Rollback the active transaction. Transactions may be nested.
    /// </summary>
    public override void RollbackTransaction()
    {
        VistaDBAPI.ivsql_RollbackTransaction(ConnectionID);
    }

    public override void Dispose()
    {
        if (ConnectionID == 0)
            return;

        try
        {
            CloseDatabaseConnection();
        }
        finally
        {
            VistaDBAPI.ivsql_FreeDatabaseConnection(ConnectionID);
            ConnectionID = 0;
        }
    }
}