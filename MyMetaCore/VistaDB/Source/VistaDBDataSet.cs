using System;
using System.Collections;
using System.ComponentModel;

namespace Provider.VistaDB;

/// <summary>
///     Summary description for VistaDBDataSet.
/// </summary>
public class VistaDBDataSet : Component, IBindingList, ITypedList, ISupportInitialize, IVistaDBDataSet
{
    private readonly int CacheSize = 100;
    private int cacheMinRowIndex, cacheMaxRowIndex, cacheIndex;
    private VistaDBDataRow dataRow;
    private VistaDBDataRow[] dataRowCache;
    private bool firstRun, secondRun;
    private string indexName;
    private bool initStarted;
    private bool needOpen;
    private int rowIndex;

    internal VistaDBDataSetTable table;

    /// <summary>
    ///     Constructor
    /// </summary>
    public VistaDBDataSet()
    {
        table = null;
        SortProperty = null;
        SortDirection = ListSortDirection.Ascending;
        cacheMinRowIndex = -1;
        cacheMaxRowIndex = -1;
        rowIndex = -1;

        dataRowCache = new VistaDBDataRow[CacheSize];
        cacheIndex = -1;

        firstRun = true;
        secondRun = false;
        Count = 0;

        table = new VistaDBDataSetTable(this);

        dataRow = null;

        Inserting = false;
    }


    //////////////////////////////////////////////////////////////
    //////////////// P R O P E R T I E S /////////////////////////
    //////////////////////////////////////////////////////////////

    /// <summary>
    ///     Active table index
    /// </summary>
    public string ActiveIndex
    {
        get => table.ActiveIndex;
        set
        {
            if (initStarted)
            {
                indexName = value;
            }
            else
            {
                if (value != table.ActiveIndex)
                {
                    table.ActiveIndex = value;

                    if (table.ActiveIndex == value) RefreshDataSet(false);
                }
            }
        }
    }

    /// <summary>
    ///     Data set columns
    /// </summary>
    public VistaDBTable.ColumnCollection Columns => table.Columns;

    /// <summary>
    ///     VistaDBDatabase object
    /// </summary>
    public VistaDBDatabase Database
    {
        get => table.Database;
        set
        {
            table.Database = value;

            if (initStarted && table.Database is not null)
                table.Database.AddToInitList(this);
        }
    }

    internal bool Inserting { get; set; }

    /// <summary>
    ///     Show if data set opened
    /// </summary>
    public bool IsOpened
    {
        get => table.Opened;
        set
        {
            if (initStarted)
            {
                needOpen = value;
            }
            else
            {
                if (value != table.Opened)
                {
                    if (value)
                    {
                        table.Open();
                        dataRow = new VistaDBDataRow(this);
                    }
                    else
                    {
                        table.InternalClose();
                        dataRow = null;
                    }

                    RefreshCount();
                    ClearCache();
                    SortProperty = null;

                    if (ListChanged is not null)
                    {
                        if (value)
                        {
                            firstRun = true;
                            ListChanged(this, new ListChangedEventArgs(ListChangedType.PropertyDescriptorAdded, 0));
                            firstRun = true;
                            secondRun = true;
                            ListChanged(this, new ListChangedEventArgs(ListChangedType.ItemAdded, 0));
                        }
                        else
                        {
                            ListChanged(this, new ListChangedEventArgs(ListChangedType.PropertyDescriptorDeleted, 0));
                        }
                    }
                    else
                    {
                        firstRun = true;
                        secondRun = false;
                    }
                }
            }
        }
    }

    /// <summary>
    ///     Return VistaDBDataRow
    /// </summary>
    public VistaDBDataRow this[int index]
    {
        get
        {
            if (SyncRecordSet(index))
                return GetDataRow();
            return null;
        }
    }

    /// <summary>
    ///     Table name
    /// </summary>
    public string TableName
    {
        get => table.TableName;
        set => table.TableName = value;
    }

    /// <summary>
    ///     Not used. Always return -1.
    /// </summary>
    public int Add(object value)
    {
        return -1;
    }

    /// <summary>
    ///     Adds the PropertyDescriptor to the indexes used for searching.
    /// </summary>
    /// <param name="property">The PropertyDescriptor to add to the indexes used for searching.</param>
    public void AddIndex(PropertyDescriptor property)
    {
    }

    /// <summary>
    ///     Adds a new item to the list.
    /// </summary>
    /// <returns>The item added to the list.</returns>
    public object AddNew()
    {
        VistaDBDataRow dataRow;

        if (!AllowNew) throw new InvalidOperationException("Adding a new row is not allowed");

        dataRow = Insert();

        if (ListChanged is not null) ListChanged(this, new ListChangedEventArgs(ListChangedType.ItemAdded, Count - 1));
        return dataRow;
    }

    /// <summary>
    ///     Sorts the list based on a PropertyDescriptor and a ListSortDirection.
    /// </summary>
    /// <param name="property">The PropertyDescriptor to sort by.</param>
    /// <param name="direction">One of the ListSortDirection values.</param>
    public void ApplySort(PropertyDescriptor property, ListSortDirection direction)
    {
        var columnName = property.Name.ToUpper();
        bool active, unique, primary, desc, fts;
        string keyExpr;
        int orderIndex;
        string indexName;
        string[] list;

        CheckOpened(true);

        table.EnumIndexes(out list);

        for (var i = 0; i < list.Length; i++)
        {
            indexName = list[i];

            table.GetIndex(indexName, out active, out orderIndex, out unique, out primary, out desc, out keyExpr, out fts);

            if (!fts && keyExpr.Trim().ToUpper() == columnName)
            {
                table.ActiveIndex = "";
                table.ActiveIndex = indexName;

                if (direction == ListSortDirection.Ascending)
                    table.IndexAscending();
                else if (direction == ListSortDirection.Descending) table.IndexDescending();

                SortProperty = property;
                SortDirection = direction;

                RefreshDataSet(false);

                return;
            }
        }
    }

    /// <summary>
    ///     Not used.
    /// </summary>
    public void Clear()
    {
    }

    /// <summary>
    ///     Determines whether the VistaDBDataSet object contains a specific VistaDBDataRow object.
    /// </summary>
    /// <param name="value">The VistaDBDataRow object to locate.</param>
    /// <returns>true if the VistaDBDataRow object is found; otherwise, false.</returns>
    public bool Contains(object value)
    {
        return !(IndexOf(value) < 0);
    }

    /// <summary>
    ///     Copies the VistaDBDataRow objects of to an Array, starting at a particular Array index
    /// </summary>
    /// <param name="array">
    ///     The one-dimensional Array that is the destination of the elements copied from VistaDBDataSet. The
    ///     Array must have zero-based indexing.
    /// </param>
    /// <param name="index">A 32-bit integer that represents the position of the Array element to set.</param>
    public void CopyTo(Array array, int index)
    {
        for (var i = 0; i < Count; i++)
            array.SetValue(this[i], index + i);
    }

    /// <summary>
    ///     Returns the index of the row that has the given PropertyDescriptor.
    /// </summary>
    /// <param name="property">The PropertyDescriptor to search on.</param>
    /// <param name="key">The value of the property parameter to search for.</param>
    /// <returns>The index of the row that has the given PropertyDescriptor.</returns>
    public int Find(PropertyDescriptor property, object key)
    {
        throw new NotSupportedException("Method is not supported");
    }

    /// <summary>
    ///     Returns an enumerator that can iterate through a rows.
    /// </summary>
    /// <returns>An IEnumerator that can be used to iterate through the rows.</returns>
    public IEnumerator GetEnumerator()
    {
        return new Enumerator(this);
    }

    /// <summary>
    ///     Determines the index of a specific VistaDBDataRow object.
    /// </summary>
    /// <param name="value">The VistaDBDataRow object to locate.</param>
    /// <returns>The index of value if found in the list; otherwise, -1.</returns>
    public int IndexOf(object value)
    {
        for (var i = 0; i < Count; i++)
            if (this[i] == value)
                return i;

        return -1;
    }

    /// <summary>
    ///     Not supported.
    /// </summary>
    public void Insert(int index, object value)
    {
    }

    /// <summary>
    ///     Not supported.
    /// </summary>
    public void Remove(object value)
    {
    }

    /// <summary>
    ///     Removes the row at the specified index.
    /// </summary>
    /// <param name="index">The zero-based index of the item to remove.</param>
    public void RemoveAt(int index)
    {
        if (!AllowRemove) throw new InvalidOperationException("Removing a row is not allowed");

        if (SyncRecordSet(index, true))
        {
            table.DeleteCurrentRow();
            if (cacheMaxRowIndex - index >= 1)
            {
                Array.Copy(dataRowCache, cacheIndex + 1, dataRowCache, cacheIndex, cacheMaxRowIndex - cacheMinRowIndex);
            }
            else
            {
                cacheIndex--;
                rowIndex--;
            }

            cacheMaxRowIndex--;
            if (cacheMaxRowIndex < 0)
                cacheMinRowIndex = -1;

            SetCount(Count - 1);
        }
    }

    /// <summary>
    ///     Removes the PropertyDescriptor from the indexes used for searching.
    /// </summary>
    /// <param name="property">The PropertyDescriptor to remove from the indexes used for searching.</param>
    public void RemoveIndex(PropertyDescriptor property)
    {
    }

    /// <summary>
    ///     Removes any sort applied using ApplySort.
    /// </summary>
    public void RemoveSort()
    {
        CheckOpened(true);
        table.ActiveIndex = "";
    }

    /// <summary>
    ///     Gets whether you can update items in the list.
    /// </summary>
    [Browsable(false)]
    public bool AllowEdit => CheckOpened(false) && !table.Database.ReadOnly;

    /// <summary>
    ///     Gets whether you can add items to the list using AddNew.
    /// </summary>
    [Browsable(false)]
    public bool AllowNew => CheckOpened(false) && !table.Database.ReadOnly;

    /// <summary>
    ///     Gets whether you can remove items from the list, using Remove or RemoveAt.
    /// </summary>
    [Browsable(false)]
    public bool AllowRemove => CheckOpened(false) && !table.Database.ReadOnly;

    /// <summary>
    ///     Gets the row count
    /// </summary>
    [Browsable(false)]
    public int Count { get; private set; }

    /// <summary>
    ///     Gets or sets the VistaDBDataRow object at the specified index.
    /// </summary>
    object IList.this[int index]
    {
        get
        {
            if (secondRun && !firstRun)
            {
                secondRun = false;
                firstRun = true;
            }

            if (firstRun)
            {
                firstRun = index != Count - 1;
                return dataRow;
            }

            if (SyncRecordSet(index))
                return GetDataRow();
            return null;
        }
        set { }
    }

    /// <summary>
    /// </summary>
    [Browsable(false)]
    public bool IsFixedSize => true;

    /// <summary>
    /// </summary>
    [Browsable(false)]
    public bool IsReadOnly => CheckOpened(false) && table.Database.ReadOnly;

    /// <summary>
    ///     Gets whether the items in the list are sorted.
    /// </summary>
    [Browsable(false)]
    public bool IsSorted => table.ActiveIndex != "" && SortProperty is not null;

    /// <summary>
    ///     Always return false
    /// </summary>
    [Browsable(false)]
    public bool IsSynchronized => false;

    /// <summary>
    ///     Gets the direction of the sort.
    /// </summary>
    [Browsable(false)]
    public ListSortDirection SortDirection { get; private set; }

    /// <summary>
    ///     Gets the PropertyDescriptor that is being used for sorting.
    /// </summary>
    [Browsable(false)]
    public PropertyDescriptor SortProperty { get; private set; }

    /// <summary>
    ///     Gets whether a ListChanged event is raised when the list changes or an item in the list changes.
    /// </summary>
    [Browsable(false)]
    public bool SupportsChangeNotification => true;

    /// <summary>
    ///     Gets whether the list supports searching using the Find method.
    /// </summary>
    [Browsable(false)]
    public bool SupportsSearching => true;

    /// <summary>
    ///     Gets whether the list supports sorting.
    /// </summary>
    [Browsable(false)]
    public bool SupportsSorting => true;

    /// <summary>
    ///     Gets an object that can be used to synchronize access to the VistaDBDataSet
    /// </summary>
    [Browsable(false)]
    public object SyncRoot => this;


    //////////////////////////////////////////////////////////////////
    ///////////////// E V E N T S ////////////////////////////////////
    //////////////////////////////////////////////////////////////////

    /// <summary>
    ///     Occurs when the list changes or an item in the list changes.
    /// </summary>
    public event ListChangedEventHandler ListChanged;

    void ISupportInitialize.BeginInit()
    {
        initStarted = true;
        needOpen = false;
        indexName = null;
    }

    void ISupportInitialize.EndInit()
    {
        initStarted = false;
        ((IVistaDBDataSet)this).OpenAfterInit();
    }

    /// <summary>
    ///     Returns the PropertyDescriptorCollection that represents the properties on each item used to bind data.
    /// </summary>
    /// <param name="listAccessors">
    ///     An array of PropertyDescriptor objects to find in the collection as bindable. This can be a
    ///     null reference (Nothing in Visual Basic).
    /// </param>
    /// <returns>The PropertyDescriptorCollection that represents the properties on each item used to bind data.</returns>
    public PropertyDescriptorCollection GetItemProperties(PropertyDescriptor[] listAccessors)
    {
        PropertyDescriptorCollection collection;
        collection = new PropertyDescriptorCollection(null);

        if (CheckOpened(false))
            for (var i = 0; i < table.ColumnCount(); i++)
                collection.Add(new VistaDBPropertyDescriptor(table.Columns[i].Name, table.Columns[i].Type, i + 1, table.Columns[i].VistaDBType,
                    table.Columns[i].ReadOnly));

        return collection;
    }

    /// <summary>
    ///     Returns the name of the list.
    /// </summary>
    /// <param name="listAccessors">
    ///     An array of PropertyDescriptor objects, the list name for which is returned. This can be a
    ///     null reference (Nothing in Visual Basic).
    /// </param>
    /// <returns>The name of the list.</returns>
    public string GetListName(PropertyDescriptor[] listAccessors)
    {
        return "List";
    }

    void IVistaDBDataSet.OpenAfterInit()
    {
        if (needOpen)
        {
            Open();
            ActiveIndex = indexName;
            needOpen = false;
        }
    }

    /// <summary>
    ///     Destructor
    /// </summary>
    ~VistaDBDataSet()
    {
        table.InternalClose();
        SortProperty = null;
        dataRowCache = null;
        dataRow = null;
    }

    internal void CancelInsert()
    {
        cacheMaxRowIndex--;
        if (cacheMaxRowIndex < 0)
            cacheMinRowIndex = -1;
        SetCount(Count - 1);
        Inserting = false;
    }

    private bool CheckOpened(bool raiseException)
    {
        if (!IsOpened && raiseException)
            throw new InvalidOperationException("Table is not opened");

        return IsOpened;
    }

    private void ClearCache()
    {
        rowIndex = -1;
        cacheMinRowIndex = -1;
        cacheMaxRowIndex = -1;

        for (var i = 0; i < CacheSize; i++)
            SetDataRow(i, null);

        cacheIndex = -1;

        RefreshCount();
    }

    /// <summary>
    ///     Clear data set filter
    /// </summary>
    public void ClearFilter()
    {
        SetFilter("");
    }

    /// <summary>
    ///     Clear data set scope
    /// </summary>
    public void ClearScope()
    {
        SetScope("");
    }

    /// <summary>
    ///     Close data set
    /// </summary>
    public void Close()
    {
        IsOpened = false;
    }

    private VistaDBDataRow GetDataRow(int index)
    {
        return dataRowCache[index];
    }

    private VistaDBDataRow GetDataRow()
    {
        if (cacheIndex >= 0)
            return GetDataRow(cacheIndex);
        return null;
    }

    private VistaDBDataRow Insert()
    {
        //Synchronize record set
        SyncRecordSet(Count - 1, true);

        //Prepare space for new row
        SetCount(Count + 1);
        SynchronizeCache(Count - 1);
        cacheIndex++;
        rowIndex = Count - 1;

        SetDataRow(new VistaDBDataRow(this, -1, Count - 1));
        firstRun = true;
        Inserting = true;

        return GetDataRow();
    }

    /// <summary>
    ///     Opens data set
    /// </summary>
    public void Open()
    {
        IsOpened = true;
    }

    /// <summary>
    ///     Refresh data set
    /// </summary>
    public void RefreshDataSet()
    {
        RefreshDataSet(false);
    }

    private void RefreshDataSet(bool requiredSecondRun)
    {
        if (IsOpened)
        {
            ClearCache();
            firstRun = true;
            secondRun = requiredSecondRun;
            ListChanged(this, new ListChangedEventArgs(ListChangedType.Reset, 0));
        }
    }

    private void RefreshCount()
    {
        if (IsOpened)
            SetCount((int)table.RowCount());
        else
            SetCount(0);

        Inserting = false;
    }

    private void SetCount(int value)
    {
        Count = value;
    }

    private void SetDataRow(int index, VistaDBDataRow dataRow)
    {
        dataRowCache[index] = dataRow;
    }

    private void SetDataRow(VistaDBDataRow dataRow)
    {
        if (cacheIndex >= 0)
            dataRowCache[cacheIndex] = dataRow;
    }

    /// <summary>
    ///     Set filter on data set
    /// </summary>
    /// <param name="expression">Filter expression</param>
    /// <returns>True if success</returns>
    public bool SetFilter(string expression)
    {
        return SetFilter(expression, true);
    }

    /// <summary>
    ///     Set filter on data set
    /// </summary>
    /// <param name="expression">Filter expression</param>
    /// <param name="useOptimization">True for using optimization</param>
    /// <returns>True if success</returns>
    public bool SetFilter(string expression, bool useOptimization)
    {
        if (IsOpened)
        {
            table.SetFilter(expression, useOptimization);
            RefreshDataSet(false);
            return true;
        }

        return false;
    }

    /// <summary>
    ///     Set scope
    /// </summary>
    /// <param name="val">Scope expression</param>
    /// <returns>True if success</returns>
    public bool SetScope(string val)
    {
        return SetScope(val, val);
    }

    /// <summary>
    ///     Set scope
    /// </summary>
    /// <param name="lowVal">Scope low value</param>
    /// <param name="highVal">Scoppe high value</param>
    /// <returns>Tru if success</returns>
    public bool SetScope(string lowVal, string highVal)
    {
        if (IsOpened)
        {
            table.SetScope(lowVal, highVal);
            RefreshDataSet(false);
            return true;
        }

        return false;
    }

    private void SynchronizeCache(int index)
    {
        //Synchronize cache
        if (cacheMinRowIndex >= 0)
        {
            if (cacheMinRowIndex - index + 1 > CacheSize || index - cacheMaxRowIndex + 1 > CacheSize)
            {
                ClearCache();
            }
            else
            {
                if (index < cacheMinRowIndex)
                {
                    var addLen = cacheMinRowIndex - index;
                    var copyLen = cacheMaxRowIndex - cacheMinRowIndex + 1;

                    if (copyLen + addLen > CacheSize)
                    {
                        copyLen = CacheSize - addLen;
                        cacheMaxRowIndex -= CacheSize - copyLen;
                    }

                    Array.Copy(dataRowCache, 0, dataRowCache, addLen, copyLen);
                    cacheMinRowIndex = index;
                    cacheIndex = addLen;
                }
                else
                {
                    var addLen = index - cacheMaxRowIndex;
                    var copyLen = cacheMaxRowIndex - cacheMinRowIndex + 1;

                    cacheIndex = copyLen - 1;

                    if (copyLen + addLen > CacheSize)
                    {
                        copyLen = CacheSize - addLen;
                        Array.Copy(dataRowCache, cacheIndex - copyLen + 1, dataRowCache, 0, copyLen);
                        cacheMinRowIndex += cacheIndex - copyLen + 1;
                        cacheIndex = copyLen - 1;
                    }

                    cacheMaxRowIndex += addLen;
                }
            }
        }
        else
        {
            cacheMinRowIndex = 0;
            cacheMaxRowIndex = 0;
            cacheIndex = -1;
        }
    }

    private bool SyncRecordSet(int index, bool hard)
    {
        if (!SyncRecordSet(index)) return false;

        if (hard && rowIndex != index)
        {
            if (rowIndex < index)
                for (var i = rowIndex; i < index; i++)
                {
                    rowIndex++;
                    table.Next();
                }
            else
                for (var i = rowIndex; i > index; i--)
                {
                    rowIndex--;
                    table.Prior();
                }
        }

        return true;
    }

    private bool SyncRecordSet(int index)
    {
        int cacheRowIndex;

        if (index < 0 || index >= Count)
            return false;

        //Check if data present in the cache, then use this data, else refresh
        if (cacheMinRowIndex > index || cacheMaxRowIndex < index)
        {
            CheckOpened(true);

            if (rowIndex < 0)
            {
                ClearCache();

                if (index < Count - index)
                {
                    table.First();

                    if (table.EndOfSet())
                        return false;

                    rowIndex = 0;
                }
                else
                {
                    table.Last();

                    if (table.BeginOfSet())
                        return false;

                    rowIndex = Count - 1;
                }
            }
            else
            {
                //If this required go to last or first row
                if (!Inserting)
                {
                    if (index == 0)
                    {
                        table.First();
                        ClearCache();
                        rowIndex = 0;
                    }
                    else if (index == Count - 1)
                    {
                        table.Last();
                        ClearCache();
                        rowIndex = Count - 1;
                    }
                }
            }

            //Synchronize cache
            if (rowIndex < index)
                cacheRowIndex = cacheMaxRowIndex;
            else
                cacheRowIndex = cacheMinRowIndex;
            SynchronizeCache(index);

            //Find requested row
            if (rowIndex < index)
                for (var i = rowIndex; i < index; i++)
                {
                    rowIndex++;
                    table.Next();

                    if (table.EndOfSet())
                        break;

                    if (cacheMinRowIndex >= 0 && cacheRowIndex >= 0 && rowIndex > cacheRowIndex)
                    {
                        cacheIndex++;
                        dataRowCache[cacheIndex] = new VistaDBDataRow(this, table.CurrentRowID(), rowIndex);
                    }
                }
            else
                for (var i = rowIndex; i > index; i--)
                {
                    rowIndex--;
                    table.Prior();

                    if (table.BeginOfSet())
                        break;

                    if (cacheMinRowIndex >= 0 && cacheRowIndex >= 0 && rowIndex < cacheRowIndex)
                    {
                        cacheIndex--;
                        dataRowCache[cacheIndex] = new VistaDBDataRow(this, table.CurrentRowID(), rowIndex);
                    }
                }

            //Synchronize
            if (table.EndOfSet())
            {
                RefreshCount();
                rowIndex = Count - 1;
                cacheIndex = -1;
            }
            else if (table.BeginOfSet())
            {
                RefreshCount();
                rowIndex = 0;
                cacheIndex = -1;
            }

            //Create new VistaDBDataRow object
            if (cacheRowIndex < 0 && Count > 0)
            {
                cacheIndex = 0;
                cacheMinRowIndex = rowIndex;
                cacheMaxRowIndex = rowIndex;
                dataRowCache[cacheIndex] = new VistaDBDataRow(this, table.CurrentRowID(), rowIndex);
            }
        }
        else
        {
            //Synchronize cacheIndex
            cacheIndex = index - cacheMinRowIndex;
        }

        return true;
    }


    //////////////////////////////////////////////////////////////////
    ///////////////////// C L A S S E S //////////////////////////////
    //////////////////////////////////////////////////////////////////

    /// <summary>
    ///     VistaDB property descriptor class
    /// </summary>
    public class VistaDBPropertyDescriptor : PropertyDescriptor
    {
        private readonly int columnNo;

        private readonly Type propertyType;
        private readonly bool readOnly;
        private VistaDBType dataType;

        internal VistaDBPropertyDescriptor(string name, Type propertyType, int columnNo, VistaDBType dataType, bool readOnly) : base(name, null)
        {
            this.propertyType = propertyType;
            this.columnNo = columnNo;
            this.dataType = dataType;
            this.readOnly = readOnly;
        }

        /// <summary>
        ///     Type of VistaDBDataRow
        /// </summary>
        public override Type ComponentType => typeof(VistaDBDataRow);

        /// <summary>
        ///     Return if data set is read only (true) or no (fase)
        /// </summary>
        public override bool IsReadOnly => readOnly;

        /// <summary>
        /// </summary>
        public override Type PropertyType => propertyType;

        /// <summary>
        /// </summary>
        public override bool CanResetValue(object component)
        {
            return false;
        }

        /// <summary>
        ///     Return column value
        /// </summary>
        /// <param name="component">VistaDBDataRow object</param>
        /// <returns></returns>
        public override object GetValue(object component)
        {
            return ((VistaDBDataRow)component).GetValue(columnNo);
        }

        /// <summary>
        /// </summary>
        /// <param name="component"></param>
        public override void ResetValue(object component)
        {
        }

        /// <summary>
        ///     Set column value
        /// </summary>
        /// <param name="component"></param>
        /// <param name="value"></param>
        public override void SetValue(object component, object value)
        {
            ((VistaDBDataRow)component).SetValue(columnNo, value);
        }

        /// <summary>
        /// </summary>
        /// <param name="component"></param>
        /// <returns></returns>
        public override bool ShouldSerializeValue(object component)
        {
            return true;
        }
    }


    private class Enumerator : IEnumerator
    {
        private readonly VistaDBDataSet parent;
        private int position;

        public Enumerator(VistaDBDataSet parent)
        {
            parent.CheckOpened(true);
            this.parent = parent;
            Reset();
        }

        /// <summary>
        ///     Advances the enumerator to the next element of the collection.
        /// </summary>
        /// <returns>
        ///     true if the enumerator was successfully advanced to the next element; false if the enumerator has passed the
        ///     end of the collection
        /// </returns>
        public bool MoveNext()
        {
            if (position < parent.Count - 2)
            {
                position++;
                return true;
            }

            return false;
        }

        /// <summary>
        ///     Sets the enumerator to its initial position, which is before the first element in the collection.
        /// </summary>
        public void Reset()
        {
            position = -1;
            parent.table.First();
        }

        /// <summary>
        ///     Gets the current element in the collection.
        /// </summary>
        public object Current => parent[position];
    }


    internal class VistaDBDataSetTable : VistaDBTable
    {
        private readonly VistaDBDataSet parent;

        internal VistaDBDataSetTable(VistaDBDataSet parent)
        {
            this.parent = parent;
        }

        public override void Close()
        {
            parent.IsOpened = false;
        }

        internal void InternalClose()
        {
            base.Close();
        }
    }
}