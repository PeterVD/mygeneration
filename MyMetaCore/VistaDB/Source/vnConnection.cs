using System;
using System.ComponentModel;
using System.Data;

namespace Provider.VistaDB;

/// <summary>
///     Connection object.
/// </summary>
#if EVAL
	[LicenseProviderAttribute(typeof(VistaDBLicenseProvider))]
#endif
public class VistaDBConnection : Component, IDbConnection
{
#if EVAL
		private License license = null;
#endif

    /// <summary>
    ///     Constructor.
    /// </summary>
    public VistaDBConnection()
    {
        VistaDBErrorMsgs.SetErrorFunc();
        State = ConnectionState.Closed;
        AccessMode = AccessMode.Local;
        VistaDBSQL = null;

        DataSource = null;
        Database = null;
        Cypher = CypherType.None;
        Password = null;
        Exclusive = false;
        ReadOnly = false;
        LoginUser = "";
        LoginPassword = "";
    }

    /// <summary>
    ///     Constructor with a connection string.
    /// </summary>
    public VistaDBConnection(string sConnString)
    {
        ConnectionString = sConnString;
    }

    internal VistaDBConnection(int cultureID, bool caseSensitive) : this()
    {
        VistaDBSQL = new VistaDBLocalConnection(cultureID, caseSensitive);
        Exclusive = true;
        State = ConnectionState.Open;
    }

    internal VistaDBConnection(VistaDBDatabase db) : this()
    {
        if (db.Parameters != VDBDatabaseParam.InMemory)
            throw new VistaDBException(VistaDBErrorCodes.DatabaseMustBeTemporary);

        VistaDBSQL = new VistaDBLocalConnection(db);
        Exclusive = true;
        State = ConnectionState.Open;
    }

    /// <summary>
    ///     VistaDBConnection destructor
    /// </summary>
    ~VistaDBConnection()
    {
        Dispose(false);
    }

    /// <summary>
    ///     Overloaded. Releases the resources used by the component.
    /// </summary>
    /// <param name="disposing"></param>
    protected override void Dispose(bool disposing)
    {
#if EVAL
			if (license is not null) 
			{
				license.Dispose();
				license = null;
			}
#endif
        try
        {
            Close();
        }
        finally
        {
            base.Dispose(disposing);
        }
    }

    /// <summary>
    ///     Returns remote service object
    /// </summary>
    /// <returns>Remote service object</returns>
    public IVistaDBRemoteService GetRemoteService()
    {
        if (State != ConnectionState.Open || !(VistaDBSQL is VistaDBRemoteConnection))
            return null;

        return (IVistaDBRemoteService)VistaDBSQL;
    }

    private bool GetValueForProp(string expr, string propName, out string value)
    {
        var len = propName.Length;
        var limit = expr.Length - len;
        propName = propName.ToUpper();
        int i, eqNo, previous;
        string s;
        string curPropName;

        value = "";

        //Find property position
        previous = 0;
        i = expr.IndexOf(';', 0);

        while ((i >= 0 && i <= limit) || previous <= limit)
        {
            if (i < 0)
                i = expr.Length;

            s = expr.Substring(previous, i - previous).TrimStart();

            eqNo = s.IndexOf('=');

            if (eqNo < 0)
                curPropName = "";
            else
                curPropName = s.Substring(0, eqNo).Trim().ToUpper();

            if (curPropName == propName)
            {
                value = s.Substring(eqNo + 1, s.Length - eqNo - 1).TrimStart();
                return true;
            }

            previous = i + 1;

            if (i < expr.Length)
                i = expr.IndexOf(';', i + 1);
        }

        return false;
    }

    /// <summary>
    ///     Gets the current state of the connection.
    /// </summary>
    public ConnectionState State { get; private set; }

    IDbTransaction IDbConnection.BeginTransaction()
    {
        return BeginTransaction();
    }

    /// <summary>
    ///     Overloaded. Begins a database transaction. Transactions may be nested.
    /// </summary>
    /// <remarks>
    ///     VistaDB always uses snapshot isolation, where each new transaction receives
    ///     its own view of the database at the moment the transaction is started.
    ///     Uncommitted and committed changes by other connections and transactions are
    ///     not seen until this transaction is committed or rolled back.
    /// </remarks>
    /// <returns>Transaction ID</returns>
    public VistaDBTransaction BeginTransaction()
    {
        return new VistaDBTransaction(this);
    }

    /// <summary>
    ///     Not supported.
    /// </summary>
    /// <param name="level"></param>
    /// <returns></returns>
    public IDbTransaction BeginTransaction(IsolationLevel level)
    {
        throw new NotSupportedException();
    }

    /// <summary>
    ///     Not implemented.
    /// </summary>
    public void ChangeDatabase(string dbName) //IDbConnection.ChangeDatabase
    {
        //		' Change the database setting on the back-end. Note that it is a method
        //		' and not a property because the operation requires an expensive
        //		' round trip.
    }

    /// <summary>
    ///     Open the connection.
    /// </summary>
    public void Open() //IDbConnection.Open
    {
#if EVAL
			if(license is null)
				license = LicenseManager.Validate(typeof(VistaDBConnection), this);
#endif

        if (State == ConnectionState.Open)
            return;

        if (DataSource == "")
            throw new VistaDBException(VistaDBErrorCodes.ConnectionDataSourceIsEmpty);

        switch (AccessMode)
        {
            case AccessMode.Local:
                VistaDBSQL = new VistaDBLocalConnection();
                break;
            case AccessMode.Remote:
                VistaDBSQL = new VistaDBRemoteConnection();
                ((VistaDBRemoteConnection)VistaDBSQL).User = LoginUser;
                ((VistaDBRemoteConnection)VistaDBSQL).LoginPassword = LoginPassword;
                ((VistaDBRemoteConnection)VistaDBSQL).Timeout = ConnectionTimeout;
                break;
            default:
                throw new VistaDBException(VistaDBErrorCodes.UnusableAccessMode);
        }

        VistaDBSQL.DataSource = DataSource;
        VistaDBSQL.Database = Database;
        VistaDBSQL.Cypher = Cypher;
        VistaDBSQL.Password = Password;
        VistaDBSQL.Exclusive = Exclusive;
        VistaDBSQL.ReadOnly = ReadOnly;

        VistaDBSQL.OpenDatabaseConnection();

        State = ConnectionState.Open;
    }

    /// <summary>
    ///     Close the connection.
    /// </summary>
    public void Close() //IDbConnection.Close
    {
        if (State == ConnectionState.Open)
            try
            {
                VistaDBSQL.Dispose();
            }
            finally
            {
                VistaDBSQL = null;
                State = ConnectionState.Closed;
            }
    }

    IDbCommand IDbConnection.CreateCommand() //IDbConnection.CreateCommand
    {
        return CreateCommand();
    }

    /// <summary>
    ///     Creates and returns a VistaDBCommand object associated with the VistaDBConnection.
    /// </summary>
    public VistaDBCommand CreateCommand()
    {
        //' Return a new instance of a command object.
        var comm = new VistaDBCommand();
        comm.Connection = this;

        return comm;
    }

    /// <summary>
    ///     Creates temporary database and connects to it
    /// </summary>
    /// <param name="cultureID">Database culture ID</param>
    /// <param name="caseSensitive">True if database is case sensitive</param>
    /// <returns>VistaDBConnection object</returns>
    public static VistaDBConnection CreateTemporaryDatabase(int cultureID, bool caseSensitive)
    {
        return new VistaDBConnection(cultureID, caseSensitive);
    }

    /// <summary>
    ///     Connects to temporary database.
    ///     After connection created VistaDBDatabase object lose control to database.
    /// </summary>
    /// <param name="db">VistaDBDatabase object</param>
    /// <returns>VistaDBConnection object</returns>
    public static VistaDBConnection CreateTemporaryDatabase(VistaDBDatabase db)
    {
        return new VistaDBConnection(db);
    }

    internal VistaDBSQLConnection VistaDBSQL { get; private set; }

    /// <summary>
    ///     Returns the connection time-out value set in the connection
    ///     string. Zero indicates an indefinite time-out period.
    /// </summary>
    int IDbConnection.ConnectionTimeout => ConnectionTimeout;

    /// <summary>
    ///     Gets or sets the connection string.
    /// </summary>
    /// <remarks>
    ///     The ConnectionString is similar to an OLE DB connection string, but is not identical. Unlike OLE DB or ADO,
    ///     the connection string that is returned is the same as the user-set ConnectionString.
    ///     You can use the ConnectionString property to connect to a database.
    /// </remarks>
    [Browsable(true)]
    [Editor("VistaDB.Designer.VistaDBDatabasePathEditor, VistaDB.Designer.VS2003", "System.Drawing.Design.UITypeEditor")]
    public string ConnectionString //IDbConnection.ConnectionString
    {
        get
        {
            var tmp = "";

            tmp += "AccessMode=" + AccessMode + ";";

            if (DataSource != "")
                tmp += "DataSource=" + DataSource + ";";

            if (Database != "")
                tmp += "Database=" + Database + ";";

            if (LoginUser != "")
                tmp += "LoginUser=" + LoginUser + ";";

            if (LoginPassword != "")
                tmp += "LoginPassword=" + LoginPassword + ";";

            tmp += "Cypher=" + Cypher + ";";

            if (Password != "")
                tmp += "Password=" + Password + ";";

            tmp += "Exclusive=" + Exclusive + ";";

            tmp += "ReadOnly=" + ReadOnly;

            return tmp;
        }

        set
        {
            if (State == ConnectionState.Open)
                throw new VistaDBException(VistaDBErrorCodes.ConnectionCannotBeChanged);

            string s;

            value = value.TrimStart(' ');

            if (GetValueForProp(value, "AccessMode", out s))
            {
                s = s.ToUpper();

                switch (s)
                {
                    case "LOCAL":
                        AccessMode = AccessMode.Local;
                        break;
                    case "REMOTE":
                        AccessMode = AccessMode.Remote;
                        break;
                }
            }
            else
            {
                AccessMode = AccessMode.Local;
            }

            if (GetValueForProp(value, "DataSource", out s))
                DataSource = s;
            else
                DataSource = string.Empty;

            if (GetValueForProp(value, "Database", out s))
                Database = s;
            else
                Database = DataSource;

            //Find login user
            if (GetValueForProp(value, "LoginUser", out s))
                LoginUser = s;
            else
                LoginUser = "";

            //Find login password
            if (GetValueForProp(value, "LoginPassword", out s))
                LoginPassword = s;
            else
                LoginPassword = "";


            //Find Cypher
            if (GetValueForProp(value, "Cypher", out s))
            {
                s = s.ToUpper();

                switch (s)
                {
                    case "NONE":
                        Cypher = CypherType.None;
                        break;
                    case "BLOWFISH":
                        Cypher = CypherType.Blowfish;
                        break;
                    case "DES":
                        Cypher = CypherType.DES;
                        break;
                }
            }
            else
            {
                Cypher = CypherType.None;
            }

            //Find Password
            if (GetValueForProp(value, "Password", out s))
                Password = s;
            else
                Password = string.Empty;


            //Find Exclusive
            if (GetValueForProp(value, "Exclusive", out s))
                Exclusive = Convert.ToBoolean(s);
            else
                Exclusive = false;


            //Find ReadOnly
            if (GetValueForProp(value, "ReadOnly", out s))
                ReadOnly = Convert.ToBoolean(s);
            else
                ReadOnly = false;
        }
    }

    /// <summary>
    ///     Returns the connection time-out value set in the connection
    ///     string. Zero indicates an indefinite time-out period.
    /// </summary>
    public int ConnectionTimeout { get; set; }

    /// <summary>
    ///     Gets or sets the name of the instance of the VistaDB database to connect to.
    /// </summary>
    //[Browsable(true), Editor("VistaDB.Designer.VistaDBDatabasePathEditor, VistaDB.Designer.VS2003", "System.Drawing.Design.UITypeEditor")]
    public string DataSource { get; set; }

    string IDbConnection.Database => Database;

    /// <summary>
    ///     Gets or sets the name of the current database or the database to be used after a connection is opened.
    /// </summary>
    public string Database { get; set; }

    /// <summary>
    ///     Gets or sets the database password.
    /// </summary>
    public string Password { get; set; }

    /// <summary>
    ///     Gets or sets the database encryption type, or Cypher type.
    /// </summary>
    public CypherType Cypher { get; set; }

    /// <summary>
    ///     Gets or sets if a database is to be opened in exclusive mode. Required for altering the database schema.
    /// </summary>
    public bool Exclusive { get; set; }

    /// <summary>
    ///     Gets or sets if a database is to be opened in readonly mode.
    /// </summary>
    public bool ReadOnly { get; set; }

    /// <summary>
    ///     User login name for server connect
    /// </summary>
    public string LoginUser { get; set; }

    /// <summary>
    ///     Login password for server connect
    /// </summary>
    public string LoginPassword { get; set; }

    /// <summary>
    ///     Connection access mode
    /// </summary>
    public AccessMode AccessMode { get; set; }

    /// <summary>
    ///     Opened database culture id
    /// </summary>
    [Browsable(false)]
    public int CultureID => VistaDBSQL is null ? -1 : VistaDBSQL.CultureID;

    /// <summary>
    ///     Opened database cluster size
    /// </summary>
    [Browsable(false)]
    public int ClusterSize => VistaDBSQL is null ? -1 : VistaDBSQL.ClusterSize;

    /// <summary>
    ///     Returns True if opened database is case sensitive
    /// </summary>
    [Browsable(false)]
    public bool CaseSensitivity => VistaDBSQL is null ? false : VistaDBSQL.CaseSensitivity;

    /// <summary>
    ///     Opened database description
    /// </summary>
    [Browsable(false)]
    public string DatabaseDescription => VistaDBSQL is null ? "" : VistaDBSQL.DatabaseDescription;
}