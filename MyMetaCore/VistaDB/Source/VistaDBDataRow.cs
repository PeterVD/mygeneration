using System;
using System.ComponentModel;

namespace Provider.VistaDB;

/// <summary>
///     Summary description for VistaDBDataRow.
/// </summary>
public class VistaDBDataRow : IEditableObject
{
    private readonly VistaDBDataSet parent;
    private readonly int rowIndex;
    private readonly object[] values;
    private bool editing;
    private long rowID;

    internal VistaDBDataRow(VistaDBDataSet parent, long rowID, int rowIndex)
    {
        editing = false; //rowID == -1;
        this.parent = parent;
        this.rowID = rowID;
        this.rowIndex = rowIndex;
        values = new object[this.parent.table.ColumnCount()];

        if (this.rowID > 0)
            for (var i = 0; i < this.parent.table.ColumnCount(); i++)
                values[i] = this.parent.table.GetObject(i);
        else
            for (var i = 0; i < this.parent.table.ColumnCount(); i++)
                values[i] = DBNull.Value;
    }

    internal VistaDBDataRow(VistaDBDataSet parent)
    {
        editing = false;
        this.parent = parent;
        rowID = -1;
        rowIndex = -1;
        values = new object[this.parent.table.ColumnCount()];
        for (var i = 0; i < this.parent.table.ColumnCount(); i++)
            values[i] = DBNull.Value;
    }

    ////////////////////////////////////////////////////////////////////
    ///////////////// P R O P E R T I E S //////////////////////////////
    ////////////////////////////////////////////////////////////////////

    /// <summary>
    ///     Physical row index
    /// </summary>
    public long RowIndex => rowIndex;

    /// <summary>
    ///     Begins an edit on an object.
    /// </summary>
    public void BeginEdit()
    {
    }

    /// <summary>
    ///     Discards changes since the last BeginEdit call.
    /// </summary>
    public void CancelEdit()
    {
        if (rowID < 0)
            parent.CancelInsert();
        else
            editing = false;
    }

    /// <summary>
    ///     Pushes changes since the last BeginEdit or IBindingList.AddNew call into the underlying object.
    /// </summary>
    public void EndEdit()
    {
        if (editing)
        {
            if (rowID > 0)
            {
                parent.table.MoveTo(rowID);
            }
            else
            {
                parent.table.Insert();
                parent.Inserting = false;
            }

            for (var i = 0; i < values.Length; i++)
                parent.table.PutObject(i, values[i]);

            parent.table.Post();

            if (rowID < 0) rowID = parent.table.CurrentRowID();

            editing = false;
        }
    }

    /// <summary>
    ///     Return column value
    /// </summary>
    /// <param name="columnNo">Column number (1-based)</param>
    /// <returns>Column value</returns>
    public object GetValue(int columnNo)
    {
        return values[columnNo - 1];
    }

    /// <summary>
    ///     Set column value
    /// </summary>
    /// <param name="columnNo">Column number (1-based)</param>
    /// <param name="value"></param>
    public void SetValue(int columnNo, object value)
    {
        values[columnNo - 1] = value;
        editing = true;
    }
}