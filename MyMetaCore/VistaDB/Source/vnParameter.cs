using System;
using System.ComponentModel;
using System.ComponentModel.Design.Serialization;
using System.Data;
using System.Globalization;
using System.Reflection;

namespace Provider.VistaDB;

/// <summary>
///     Represents a parameter to a VistaDBCommand
/// </summary>
[TypeConverter(typeof(VDBParameterConverter))]
[Serializable]
public class VistaDBParameter : IDataParameter, IDbDataParameter, ICloneable
{
    private static DbType[] vdbTypes =
    {
        DbType.StringFixedLength, //Character
        DbType.Date, //Date
        DbType.DateTime, //DateTime
        DbType.Boolean, //Boolean
        DbType.Binary, //Memo
        DbType.Binary, //Picture
        DbType.Binary, //Blob
        DbType.Currency, //Currency
        DbType.Int32, //Integer
        DbType.Int64, //Int64
        DbType.Double, //Double
        DbType.String //Varchar
    };

    private VistaDBType dbType = VistaDBType.Character;
    private ParameterDirection m_direction = ParameterDirection.Input;

    private DataRowVersion m_sourceVersion = DataRowVersion.Current;
    private bool nullable;
    private string paramName;
    private byte precision;
    private byte scale;
    private int size;
    private string sourceColumn;
    private bool typeDefined;

    private object val;

    /// <summary>
    ///     Constructor
    /// </summary>
    public VistaDBParameter()
    {
    }

    /// <summary>
    ///     Constructor
    /// </summary>
    /// <param name="parameterName">Parameter name</param>
    /// <param name="type">Parameter data type</param>
    public VistaDBParameter(string parameterName, VistaDBType type)
    {
        paramName = parameterName;
        dbType = type;
        typeDefined = true;
    }

    /// <summary>
    ///     Constructor
    /// </summary>
    /// <param name="parameterName">Parameter name</param>
    /// <param name="value">Parameter value</param>
    public VistaDBParameter(string parameterName, object value)
    {
        paramName = parameterName;
        Value = value;
        //' Setting the value also infers the type.
    }

    /// <summary>
    ///     Constructor
    /// </summary>
    /// <param name="parameterName">Parameter name</param>
    /// <param name="type">Parameter data type</param>
    /// <param name="sourceColumn">The name of the source column.</param>
    public VistaDBParameter(string parameterName, VistaDBType type, string sourceColumn)
    {
        paramName = parameterName;
        dbType = type;
        this.sourceColumn = sourceColumn;
        typeDefined = true;
    }

    /// <summary>
    ///     Constructor
    /// </summary>
    /// <param name="parameterName">Parameter name.</param>
    /// <param name="dbType">VistaDBType type of the parameter.</param>
    /// <param name="size">maximum size, in bytes, of the data within the column.</param>
    /// <param name="direction">ParameterDirection.</param>
    /// <param name="isNullable">Indicates whether the parameter accepts null values.</param>
    /// <param name="precision">Maximum number of digits used to represent the Value property.</param>
    /// <param name="scale">Number of decimal places to which Value is resolved.</param>
    /// <param name="sourceColumn">
    ///     Name of the source column that is mapped to the DataSet and used for loading or returning
    ///     the Value.
    /// </param>
    /// <param name="sourceVersion">DataRowVersion to use when loading Value.</param>
    /// <param name="value">Value of the parameter.</param>
    public VistaDBParameter(string parameterName, VistaDBType dbType, int size, ParameterDirection direction, bool isNullable, byte precision,
        byte scale, string sourceColumn, DataRowVersion sourceVersion, object value)
    {
        this.dbType = dbType;
        m_direction = direction;
        nullable = isNullable;
        paramName = parameterName;
        this.sourceColumn = sourceColumn;

        m_sourceVersion = sourceVersion;

        val = value;
        this.size = size;
        this.scale = scale;
        this.precision = precision;

        typeDefined = true;
    }

    /// <summary>
    ///     Gets or sets the VistaDBType parameter.
    /// </summary>
    public VistaDBType VistaDBType
    {
        get => dbType;
        set
        {
            dbType = value;
            typeDefined = true;
        }
    }

    /// <summary>
    ///     Gets the parameter name.
    /// </summary>
    [Browsable(false)]
    public string Name => ParameterName;

    /// <summary>
    ///     Creates a new VistaDBParameter object.
    /// </summary>
    /// <returns>New VistaDBParameter object</returns>
    public object Clone()
    {
        return new VistaDBParameter();
    }

    /// <summary>
    ///     Gets or sets the parameter type.
    /// </summary>
    /// <remarks>Provided for compatibility with Sql's DbType. Use VistaDBType</remarks>
    public DbType DbType
    {
        get => VistaDBTypeToDbType(dbType);
        set
        {
            dbType = DbTypeToVistaDBType(value);
            typeDefined = true;
        }
    }

    /// <summary>
    ///     Gets or sets a value indicating whether the parameter is input-only, output-only or bidirectional return value
    ///     parameter.
    /// </summary>
    public ParameterDirection Direction //IDataParameter.Direction
    {
        get => m_direction;
        set => m_direction = value;
    }

    /// <summary>
    ///     Gets or sets a value indicating whether the parameter accepts null values.
    /// </summary>
    public bool IsNullable => true; //m_fNullable;

    /// <summary>
    ///     Gets or sets the parameter name.
    /// </summary>
    public string ParameterName
    {
        get => paramName;
        set => paramName = value;
    }

    /// <summary>
    ///     Gets or sets the name of the source column that is mapped to the DataSet and used for loading or returning the
    ///     Value.
    /// </summary>
    public string SourceColumn
    {
        get => sourceColumn;
        set => sourceColumn = value;
    }

    /// <summary>
    ///     Gets or sets the DataRowVersion to use when loading Value.
    /// </summary>
    public DataRowVersion SourceVersion
    {
        get => m_sourceVersion;
        set => m_sourceVersion = value;
    }

    /// <summary>
    ///     Gets or sets the value of the parameter.
    /// </summary>
    public object Value
    {
        get => val;
        set
        {
            if (value is not null && !typeDefined)
            {
                dbType = FitSystemType(value);
                typeDefined = true;
            }

            if (value is null || value == DBNull.Value)
                val = null;
            else
                switch (dbType)
                {
                    case VistaDBType.Blob:
                    case VistaDBType.Picture:
                        val = value;
                        break;

                    case VistaDBType.Boolean:
                        val = (bool)value;
                        break;

                    case VistaDBType.Character:
                    case VistaDBType.Memo:
                    case VistaDBType.Varchar:
                        val = value.ToString();
                        break;

                    case VistaDBType.Currency:
                        val = (decimal)value;
                        break;

                    case VistaDBType.Double:
                        val = (double)value;
                        break;

                    case VistaDBType.Date:
                    case VistaDBType.DateTime:
                        val = (DateTime)value;
                        break;

                    case VistaDBType.Int32:
                        val = (int)value;
                        break;

                    case VistaDBType.Int64:
                        val = (long)value;
                        break;

                    case VistaDBType.Guid:
                        val = (Guid)value;
                        break;
                }
        }
    }

    /// <summary>
    ///     Gets or sets the maximum number of digits used to represent the Value property.
    /// </summary>
    public byte Precision
    {
        get => precision;
        set => precision = value;
    }

    /// <summary>
    ///     Gets or sets the number of decimal places to which Value is resolved.
    /// </summary>
    public byte Scale
    {
        get => scale;
        set => scale = value;
    }

    /// <summary>
    ///     Gets or sets the maximum size, in bytes, of the data within the column.
    /// </summary>
    public int Size
    {
        get => size;
        set => size = value;
    }

    private VistaDBType FitSystemType(object value)
    {
        var typeCode = Type.GetTypeCode(value.GetType());

        switch (typeCode)
        {
            case TypeCode.Empty:
            case TypeCode.DBNull:
            case TypeCode.Byte:
            case TypeCode.UInt16:
            case TypeCode.UInt32:
            case TypeCode.UInt64:
            case TypeCode.SByte:
            case TypeCode.Int16:
            case TypeCode.Single:
            case TypeCode.Char:
                //' Throw a SystemException for unsupported data types.
                return dbType;

            case TypeCode.Boolean:
                return VistaDBType.Boolean;
            case TypeCode.Int32:
                return VistaDBType.Int32;
            case TypeCode.Int64:
                return VistaDBType.Int64;
            case TypeCode.Double:
                return VistaDBType.Double;
            case TypeCode.DateTime:
                return VistaDBType.DateTime;
            case TypeCode.String:
                return VistaDBType.Character;
            case TypeCode.Decimal:
                return VistaDBType.Currency;
            case TypeCode.Object:
                if (value.GetType() == typeof(Guid))
                    return VistaDBType.Guid;
                if (value.GetType() == typeof(byte[]))
                    return VistaDBType.Blob;
                throw new SystemException("Value is of unknown data type");
            default:
                throw new SystemException("Value is of unknown data type");
        }
    }

    private DbType VistaDBTypeToDbType(VistaDBType type)
    {
        return vdbTypes[(int)type];
    }

    private VistaDBType DbTypeToVistaDBType(DbType type)
    {
        return (VistaDBType)Array.IndexOf(vdbTypes, type);
    }

    /// <summary>
    ///     Provides a unified way of converting types of values to other types, as well as for accessing standard values and
    ///     subproperties.
    /// </summary>
    /// <remarks>
    ///     VistaDB type converter is used to convert values between data types, and to assist property configuration
    ///     at design time by providing text-to-value conversion or a drop-down list of values to select from.
    /// </remarks>
    public class VDBParameterConverter : TypeConverter
    {
        /// <summary>
        ///     Overloaded. Returns whether this converter can convert the object to the specified type.
        /// </summary>
        /// <param name="context">An ITypeDescriptorContext that provides a format context. </param>
        /// <param name="destinationType">The Type to convert the value parameter to.</param>
        /// <returns></returns>
        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            if (destinationType == typeof(InstanceDescriptor)) return true;

            return base.CanConvertTo(context, destinationType);
        }

        /// <summary>
        ///     Overloaded. Converts the given value object to the specified type.
        /// </summary>
        /// <param name="context">An ITypeDescriptorContext that provides a format context. </param>
        /// <param name="culture">
        ///     A CultureInfo object. If a null reference (Nothing in Visual Basic) is passed, the current
        ///     culture is assumed.
        /// </param>
        /// <param name="value">The Object to convert.</param>
        /// <param name="destinationType">The Type to convert the value parameter to.</param>
        /// <returns></returns>
        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            ConstructorInfo info1;
            VistaDBParameter parameter1;
            Type[] array1;
            object[] array2;

            if (destinationType == typeof(InstanceDescriptor))
            {
                array1 = new Type[10];
                array1[0] = typeof(string);
                array1[1] = typeof(VistaDBType);
                array1[2] = typeof(int);
                array1[3] = typeof(ParameterDirection);
                array1[4] = typeof(bool);
                array1[5] = typeof(byte);
                array1[6] = typeof(byte);
                array1[7] = typeof(string);
                array1[8] = typeof(DataRowVersion);
                array1[9] = typeof(object);

                info1 = typeof(VistaDBParameter).GetConstructor(array1);
                parameter1 = (VistaDBParameter)value;
                array2 = new object[10];
                array2[0] = parameter1.ParameterName;
                array2[1] = parameter1.VistaDBType;
                array2[2] = parameter1.Size;
                array2[3] = parameter1.Direction;
                array2[4] = parameter1.IsNullable;
                array2[5] = parameter1.Precision;
                array2[6] = parameter1.Scale;
                array2[7] = parameter1.SourceColumn;
                array2[8] = parameter1.SourceVersion;
                array2[9] = parameter1.Value;
                return new InstanceDescriptor(info1, array2);
            }

            return base.ConvertTo(context, culture, value, destinationType);
        }
    }
}