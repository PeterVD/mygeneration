using System.Runtime.InteropServices;

namespace MyMeta.VistaDB;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IProcedure))]
#endif
public class VistaDBProcedure : Procedure
{
}