using System.Runtime.InteropServices;

namespace MyMeta.VistaDB;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(ITables))]
#endif
public class VistaDBTables : Tables
{
    internal override void LoadAll()
    {
        try
        {
            var db = (VistaDBDatabase)Database;

            var metaData = db._mh.LoadTables(DbRoot.ConnectionString);

            PopulateArray(metaData);
        }
        catch
        {
        }
    }
}