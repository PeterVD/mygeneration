using System;
using System.Data;
using System.Runtime.InteropServices;

namespace MyMeta.VistaDB;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IColumns))]
#endif
public class VistaDBColumns : Columns
{
    internal DataColumn f_InPrimaryKey;
    internal DataColumn f_TypeName;

    internal override void LoadForTable()
    {
        try
        {
            var db = (VistaDBDatabase)Table.Database;

            var metaData = db._mh.LoadColumns(DbRoot.ConnectionString, Table.Name);

            f_TypeName = metaData.Columns["DATA_TYPE_NAME"];
            f_InPrimaryKey = metaData.Columns["IS_PRIMARY_KEY"];

            PopulateArray(metaData);
        }
        catch (Exception ex)
        {
            var s = ex.Message;
        }
    }

    internal override void LoadForView()
    {
    }
}