using System;
using System.Data;
using Provider.VistaDB;

namespace MyMeta.VistaDB;

/// <summary>
///     Summary description for MetaHelper.
/// </summary>
public class MetaHelper
{
    public DataTable LoadColumns(string cn, string tableName)
    {
        var metaData = new DataTable();
        metaData.Columns.Add("TABLE_NAME", Types.StringType);
        metaData.Columns.Add("COLUMN_NAME", Types.StringType);
        metaData.Columns.Add("ORDINAL_POSITION", Types.Int32Type);
        metaData.Columns.Add("IS_NULLABLE", Types.BoolType);
        metaData.Columns.Add("COLUMN_HASDEFAULT", Types.BoolType);
        metaData.Columns.Add("COLUMN_DEFAULT", Types.StringType);
        metaData.Columns.Add("IS_AUTO_KEY", Types.BoolType);
        metaData.Columns.Add("AUTO_KEY_SEED", Types.Int32Type);
        metaData.Columns.Add("AUTO_KEY_INCREMENT", Types.Int32Type);
        metaData.Columns.Add("DATA_TYPE_NAME", Types.StringType);
        metaData.Columns.Add("NUMERIC_PRECISION", Types.Int32Type);
        metaData.Columns.Add("NUMERIC_SCALE", Types.Int32Type);
        metaData.Columns.Add("CHARACTER_MAXIMUM_LENGTH", Types.Int32Type);
        metaData.Columns.Add("CHARACTER_OCTET_LENGTH", Types.Int32Type);
        metaData.Columns.Add("DESCRIPTION", Types.StringType);
        metaData.Columns.Add("IS_PRIMARY_KEY", Types.BoolType);

        try
        {
            var db = OpenDatabase(cn);
            db.Connect();

            var table = new Provider.VistaDB.VistaDBTable(db, tableName);
            table.Open();

            for (var ordinal = 0; ordinal < table.ColumnCount(); ordinal++)
            {
                var c = table.Columns[ordinal];

                var b = false;
                var colName = c.Name;

                var def = table.GetDefaultValue(colName, out b);
                int width = c.ColumnWidth;
                int dec = c.ColumnDecimals;
                var length = 0;
                var octLength = width;

                if (c.Identity)
                    // While I'll see their point this is not typically how it is done
                    def = "";

                var type = c.VistaDBType.ToString();

                switch (type)
                {
                    case "Character":
                    case "Varchar":
                        length = width;
                        width = 0;
                        dec = 0;
                        break;

                    case "Currency":
                    case "Double":
                        break;

                    default:
                        width = 0;
                        dec = 0;
                        break;
                }

                metaData.Rows.Add(table.TableName, c.Name, ordinal, c.AllowNull, def == string.Empty ? false : true, def, c.Identity, 1,
                    (int)c.IdentityStep, c.VistaDBType.ToString(), width, dec, length, octLength, c.ColumnDescription, c.PrimaryKey);
            }

            table.Close();
            db.Close();
        }
        catch
        {
        }

        return metaData;
    }

    public DataTable LoadTables(string cn)
    {
        var metaData = new DataTable();
        metaData.Columns.Add("TABLE_NAME", Types.StringType);
        metaData.Columns.Add("DESCRIPTION", Types.StringType);

        try
        {
            var db = OpenDatabase(cn);
            db.Connect();

            var list = new string[0];
            db.EnumTables(ref list);

            if (list is not null)
                foreach (var tableName in list)
                {
                    var table = new Provider.VistaDB.VistaDBTable(db, tableName);
                    table.Open();
                    metaData.Rows.Add(table.TableName, "");
                    table.Close();
                }

            db.Close();
        }
        catch
        {
        }

        return metaData;
    }

    public DataTable LoadForeignKeys(string cn, string databaseName, string tableName)
    {
        var metaData = new DataTable();
        metaData.Columns.Add("PK_TABLE_CATALOG", Types.StringType);
        metaData.Columns.Add("PK_TABLE_SCHEMA", Types.StringType);
        metaData.Columns.Add("FK_TABLE_CATALOG", Types.StringType);
        metaData.Columns.Add("FK_TABLE_SCHEMA", Types.StringType);
        metaData.Columns.Add("FK_TABLE_NAME", Types.StringType);
        metaData.Columns.Add("PK_TABLE_NAME", Types.StringType);
        metaData.Columns.Add("ORDINAL", Types.Int32Type);
        metaData.Columns.Add("FK_NAME", Types.StringType);
        metaData.Columns.Add("PK_NAME", Types.StringType);
        metaData.Columns.Add("PK_COLUMN_NAME", Types.StringType);
        metaData.Columns.Add("FK_COLUMN_NAME", Types.StringType);

        try
        {
            var db = OpenDatabase(cn);
            db.Connect();

            var table = new Provider.VistaDB.VistaDBTable(db, tableName);
            table.Open();

            var fkeys = new string[0];
            table.EnumForeignKeys(out fkeys);

            var foreignKey = "";
            var primaryTable = "";
            var primaryKey = "";

            if (fkeys is not null)
            {
                foreach (var fkey in fkeys)
                {
                    table.GetForeignKey(fkey, out foreignKey, out primaryTable, out primaryKey);

                    var fColumns = foreignKey.Split(new[] { ';' });
                    var pColumns = primaryKey.Split(new[] { ';' });

                    for (var i = 0; i < fColumns.GetLength(0); i++)
                        metaData.Rows.Add(databaseName, DBNull.Value, DBNull.Value, DBNull.Value, tableName, primaryTable, 0, fkey, "PKEY",
                            pColumns[i], fColumns[i]);
                }

                table.Close();
                db.Close();
            }
        }
        catch
        {
        }

        return metaData;
    }

    public DataTable LoadIndexes(string cn, string databaseName, string tableName)
    {
        var metaData = new DataTable();
        metaData.Columns.Add("TABLE_CATALOG", Types.StringType);
        metaData.Columns.Add("TABLE_NAME", Types.StringType);
        metaData.Columns.Add("INDEX_CATALOG", Types.StringType);
        metaData.Columns.Add("INDEX_NAME", Types.StringType);
        metaData.Columns.Add("UNIQUE", Types.BoolType);
        metaData.Columns.Add("COLLATION", Types.Int16Type);
        metaData.Columns.Add("COLUMN_NAME", Types.StringType);

        try
        {
            var db = OpenDatabase(cn);
            db.Connect();

            var table = new Provider.VistaDB.VistaDBTable(db, tableName);
            table.Open();

            var indexes = new string[0];
            table.EnumIndexes(out indexes);

            bool active;
            int orderIndex;
            bool unique;
            bool primary;
            bool descending;
            string keyExp;

            if (indexes is not null)
                foreach (var index in indexes)
                {
                    table.GetIndex(index, out active, out orderIndex, out unique, out primary, out descending, out keyExp);

                    if (orderIndex != 0) // && keyExp != "PrimaryKey" && keyExp != "PRIMARY_KEY")
                        if (keyExp is not null && keyExp != string.Empty)
                        {
                            var columns = keyExp.Split(new[] { ';' });

                            foreach (var colName in columns)
                                metaData.Rows.Add(databaseName, tableName, databaseName, index, unique, descending ? 2 : 1, colName);
                        }
                }

            table.Close();
            db.Close();
        }
        catch
        {
        }

        return metaData;
    }

    public IDbConnection GetConnection(string cn)
    {
        var conn = new VistaDBConnection(cn);
        return conn;
    }

    public string LoadDatabases(string cn)
    {
        var dbName = "";

        try
        {
            var conn = new VistaDBConnection(cn);
            conn.Open();
            dbName = conn.Database;
            conn.Close();

            try
            {
                var index = dbName.LastIndexOfAny(new[] { '\\' });
                if (index >= 0) dbName = dbName.Substring(index + 1);
            }
            catch
            {
            }
        }
        catch
        {
        }

        return dbName;
    }

    private Provider.VistaDB.VistaDBDatabase OpenDatabase(string cn)
    {
        var conn = new VistaDBConnection();
        conn.ConnectionString = cn;
        conn.ReadOnly = true;
        conn.Open();
        var dbName = conn.Database;
        conn.Close();

        return new Provider.VistaDB.VistaDBDatabase(dbName, false, true);
    }
}