using System.Runtime.InteropServices;

namespace MyMeta.VistaDB;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IDomains))]
#endif
public class VistaDBDomains : Domains
{
}