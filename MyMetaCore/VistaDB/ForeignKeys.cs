using System.Runtime.InteropServices;

namespace MyMeta.VistaDB;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IForeignKeys))]
#endif
public class VistaDBForeignKeys : ForeignKeys
{
    internal override void LoadAll()
    {
        try
        {
            var db = (VistaDBDatabase)Table.Database;

            if (!db._FKsInLoad)
            {
                db._FKsInLoad = true;

                VistaDBForeignKeys fks = null;

                foreach (Table table in Table.Tables) fks = table.ForeignKeys as VistaDBForeignKeys;

                var metaData = db._mh.LoadForeignKeys(DbRoot.ConnectionString, Table.Database.Name, Table.Name);

                PopulateArray(metaData);

                ITables tables = Table.Tables;
                for (var i = 0; i < tables.Count; i++)
                {
                    var table = tables[i];
                    fks = table.ForeignKeys as VistaDBForeignKeys;
                    fks.AddTheOtherHalf();
                }

                db._FKsInLoad = false;
            }
            else
            {
                var metaData = db._mh.LoadForeignKeys(DbRoot.ConnectionString, Table.Database.Name, Table.Name);

                PopulateArray(metaData);
            }
        }
        catch
        {
        }
    }

    internal void AddTheOtherHalf()
    {
        var myName = Table.Name;

        foreach (Table table in Table.Tables)
            if (table.Name != myName)
                foreach (VistaDBForeignKey fkey in table.ForeignKeys)
                    if (fkey.ForeignTable.Name == myName || fkey.PrimaryTable.Name == myName)
                        AddForeignKey(fkey);
    }

    internal void AddForeignKey(VistaDBForeignKey fkey)
    {
        if (!_array.Contains(fkey)) _array.Add(fkey);
    }
}