using System.Data;
using System.Runtime.InteropServices;
using Provider.VistaDB;

namespace MyMeta.VistaDB;

#if ENTERPRISE

[ComVisible(false)]
#endif
public class ClassFactory : IClassFactory
{
    public ITables CreateTables()
    {
        return new VistaDBTables();
    }

    public ITable CreateTable()
    {
        return new VistaDBTable();
    }

    public IColumn CreateColumn()
    {
        return new VistaDBColumn();
    }

    public IColumns CreateColumns()
    {
        return new VistaDBColumns();
    }

    public IDatabase CreateDatabase()
    {
        return new VistaDBDatabase();
    }

    public IDatabases CreateDatabases()
    {
        return new VistaDBDatabases();
    }

    public IProcedure CreateProcedure()
    {
        return new VistaDBProcedure();
    }

    public IProcedures CreateProcedures()
    {
        return new VistaDBProcedures();
    }

    public IView CreateView()
    {
        return new VistaDBView();
    }

    public IViews CreateViews()
    {
        return new VistaDBViews();
    }

    public IParameter CreateParameter()
    {
        return new VistaDBParameter();
    }

    public IParameters CreateParameters()
    {
        return new VistaDBParameters();
    }

    public IForeignKey CreateForeignKey()
    {
        return new VistaDBForeignKey();
    }

    public IForeignKeys CreateForeignKeys()
    {
        return new VistaDBForeignKeys();
    }

    public IIndex CreateIndex()
    {
        return new VistaDBIndex();
    }

    public IIndexes CreateIndexes()
    {
        return new VistaDBIndexes();
    }

    public IDomain CreateDomain()
    {
        return new VistaDBDomain();
    }

    public IDomains CreateDomains()
    {
        return new VistaDBDomains();
    }

    public IResultColumn CreateResultColumn()
    {
        return new VistaDBResultColumn();
    }

    public IResultColumns CreateResultColumns()
    {
        return new VistaDBResultColumns();
    }


    public IProviderType CreateProviderType()
    {
        return new ProviderType();
    }

    public IProviderTypes CreateProviderTypes()
    {
        return new ProviderTypes();
    }

    #region IClassFactory Members

    public IDbConnection CreateConnection()
    {
        return new VistaDBConnection();
    }

    #endregion

    public static void Register()
    {
        InternalDriver.Register("VISTADB",
            new FileDbDriver
            (typeof(ClassFactory)
                , @"DataSource=", @"C:\Program Files\VistaDB 2.0\Data\Northwind.vdb", @";Cypher= None;Password=;Exclusive=False;Readonly=False;"
                , "VistaDB (*.vbd)|*.vbd|all files (*.*)|*.*"));
    }
}