using System;
using System.Runtime.InteropServices;
using System.Xml;

namespace MyMeta;

public class Column : Single, IColumn, INameValueItem
{
    private static readonly ForeignKeys _emptyForeignKeys = [];
    protected ForeignKeys _foreignKeys;

    internal Columns Columns;

    internal virtual Column Clone()
    {
        var c = (Column)DbRoot.ClassFactory.CreateColumn();
        c.DbRoot = DbRoot;
        c.Columns = Columns;
        c._row = _row;
        c._foreignKeys = _emptyForeignKeys;
        return c;
    }


    #region Objects

    public ITable Table
    {
        get
        {
            ITable theTable = null;

            if (Columns.Table is not null)
            {
                theTable = Columns.Table;
            }
            else if (Columns.Index is not null)
            {
                theTable = Columns.Index.Indexes.Table;
            }
            else if (Columns.ForeignKey is not null)
            {
                theTable = Columns.ForeignKey.ForeignKeys.Table;
            }

            return theTable;
        }
    }

    public IView View => Columns.View;

    public IDomain Domain => HasDomain ? Columns.GetDatabase().Domains[DomainName] : null;

    #endregion

    #region Properties

    [DispId(0)]
    public override string Alias
    {
        get
        {
            return GetXmlNode(out var node, false)
                   && GetUserData(node, "n", out var niceName)
                   && string.Empty != niceName
                ? niceName
                : Name;
        }

        set
        {
            if (GetXmlNode(out var node, true))
            {
                SetUserData(node, "n", value);
            }
        }
    }

    public override string Name => GetString(Columns.f_Name);

    public virtual Guid Guid => GetGuid(Columns.f_Guid);

    public virtual int PropID => GetInt32(Columns.f_PropID);

    public virtual int Ordinal => GetInt32(Columns.f_Ordinal);

    public virtual bool HasDefault => GetBool(Columns.f_HasDefault);

    public virtual string Default => GetString(Columns.f_Default);

    public virtual int Flags => GetInt32(Columns.f_Flags);

    public virtual bool IsNullable => GetBool(Columns.f_IsNullable);

    public virtual int DataType => GetInt32(Columns.f_DataType);

    public virtual Guid TypeGuid => GetGuid(Columns.f_TypeGuid);

    public virtual int CharacterMaxLength => GetInt32(Columns.f_MaxLength);

    public virtual int CharacterOctetLength => GetInt32(Columns.f_OctetLength);

    public virtual int NumericPrecision => GetInt32(Columns.f_NumericPrecision);

    public virtual int NumericScale => GetInt32(Columns.f_NumericScale);

    public virtual int DateTimePrecision => GetInt32(Columns.f_DatetimePrecision);

    public virtual string CharacterSetCatalog => GetString(Columns.f_CharSetCatalog);

    public virtual string CharacterSetSchema => GetString(Columns.f_CharSetSchema);

    public virtual string CharacterSetName => GetString(Columns.f_CharSetName);

    public virtual string DomainCatalog => GetString(Columns.f_DomainCatalog);

    public virtual string DomainSchema => GetString(Columns.f_DomainSchema);

    public virtual string DomainName => GetString(Columns.f_DomainName);

    public virtual string Description => GetString(Columns.f_Description);

    public virtual int LCID => GetInt32(Columns.f_LCID);

    public virtual int CompFlags => GetInt32(Columns.f_CompFlags);

    public virtual int SortID => GetInt32(Columns.f_SortID);

    public virtual byte[] TDSCollation => GetByteArray(Columns.f_TDSCollation);

    public virtual bool IsComputed => GetBool(Columns.f_IsComputed);

    public virtual bool IsInPrimaryKey => Columns.Table?.PrimaryKeys[Name]  is not null;

    public virtual bool IsAutoKey => GetBool(Columns.f_IsAutoKey);

    public virtual string DataTypeName => DbRoot.DomainOverride && HasDomain && Domain is not null ? Domain.DataTypeName : GetString(null);

    public virtual string LanguageType
    {
        get
        {
            if (DbRoot.DomainOverride && HasDomain && Domain is not null)
            {
                return Domain.LanguageType;
            }

            if (DbRoot.LanguageNode is not null)
            {
                var xPath = @"./Type[@From='" + DataTypeName + "']";

                var node = DbRoot.LanguageNode.SelectSingleNode(xPath, null);

                if (node is not null && GetUserData(node, "To", out var languageType)) return languageType;
            }

            return "Unknown";
        }
    }

    public virtual string DbTargetType
    {
        get
        {
            if (DbRoot.DomainOverride && HasDomain && Domain is not null) return Domain.DbTargetType;

            if (DbRoot.DbTargetNode is not null)
            {
                var xPath = @"./Type[@From='" + DataTypeName + "']";

                var node = DbRoot.DbTargetNode.SelectSingleNode(xPath, null);

                if (node is not null && GetUserData(node, "To", out var driverType)) return driverType;
            }

            return "Unknown";
        }
    }

    public virtual string DataTypeNameComplete => DbRoot.DomainOverride && HasDomain && Domain is not null ? Domain.DataTypeNameComplete : "Unknown";

    public virtual bool IsInForeignKey => ForeignKeys == _emptyForeignKeys || ForeignKeys.Count > 0;

    public virtual int AutoKeySeed => GetInt32(Columns.f_AutoKeySeed);

    public virtual int AutoKeyIncrement => GetInt32(Columns.f_AutoKeyIncrement);

    public virtual bool HasDomain
    {
        get
        {
            if (_row.Table.Columns.Contains("DOMAIN_NAME"))
            {
                var o = _row["DOMAIN_NAME"];

                if (o is not null && o != DBNull.Value) return true;
            }

            return false;
        }
    }

    #endregion

    #region Collections

    public IForeignKeys ForeignKeys
    {
        get
        {
            if (_foreignKeys is not null) return _foreignKeys;

            _foreignKeys = (ForeignKeys)DbRoot.ClassFactory.CreateForeignKeys();
            _foreignKeys.DbRoot = DbRoot;

            if (Columns.Table is not null)
            {
                var fk = Columns.Table.ForeignKeys;
            }

            return _foreignKeys;
        }
    }

    public virtual IPropertyCollection GlobalProperties
    {
        get
        {
            var db = Columns.GetDatabase() as Database;
            if (db._columnProperties is null)
            {
                db._columnProperties = new PropertyCollection
                {
                    Parent = this
                };

                var xPath = GlobalUserDataXPath;
                var xmlNode = DbRoot.UserData.SelectSingleNode(xPath, null);

                if (xmlNode is null)
                {
                    var parentNode = db.CreateGlobalXmlNode();

                    xmlNode = parentNode.OwnerDocument.CreateNode(XmlNodeType.Element, "Column", null);
                    parentNode.AppendChild(xmlNode);
                }

                db._columnProperties.LoadAllGlobal(xmlNode);
            }

            return db._columnProperties;
        }
    }

    public virtual IPropertyCollection AllProperties
    {
        get
        {
            if (_allProperties is not null) return _allProperties;

            _allProperties = new PropertyCollectionAll();
            _allProperties.Load(Properties, GlobalProperties);

            return _allProperties;
        }
    }

    protected internal virtual void AddForeignKey(ForeignKey fk)
    {
        if (null == _foreignKeys)
        {
            _foreignKeys = (ForeignKeys)DbRoot.ClassFactory.CreateForeignKeys();
            _foreignKeys.DbRoot = DbRoot;
        }

        _foreignKeys.AddForeignKey(fk);
    }

    internal PropertyCollectionAll _allProperties;

    #endregion

    #region XML User Data

    public override string UserDataXPath => Columns.UserDataXPath + @"/Column[@p='" + Name + "']";

    public override string GlobalUserDataXPath => ((Database)Columns.GetDatabase()).GlobalUserDataXPath + "/Column";

    internal override bool GetXmlNode(out XmlNode node, bool forceCreate)
    {
        node = null;
        var success = false;

        if (null == _xmlNode)
        {
            // Get the parent node
            if (Columns.GetXmlNode(out var parentNode, forceCreate))
            {
                // See if our user data already exists
                var xPath = @"./Column[@p='" + Name + "']";
                if (!GetUserData(xPath, parentNode, out _xmlNode) && forceCreate)
                {
                    // Create it, and try again
                    CreateUserMetaData(parentNode);
                    GetUserData(xPath, parentNode, out _xmlNode);
                }
            }
        }

        if (null != _xmlNode)
        {
            node = _xmlNode;
            success = true;
        }

        return success;
    }

    public override void CreateUserMetaData(XmlNode parentNode)
    {
        var myNode = parentNode.OwnerDocument.CreateNode(XmlNodeType.Element, "Column", null);
        parentNode.AppendChild(myNode);

        var attr = parentNode.OwnerDocument.CreateAttribute("p");
        attr.Value = Name;
        myNode.Attributes.Append(attr);

        attr = parentNode.OwnerDocument.CreateAttribute("n");
        attr.Value = string.Empty;
        myNode.Attributes.Append(attr);
    }

    #endregion

    #region INameValueCollection Members

    public string ItemName => Name;

    public string ItemValue => Name;

    #endregion
}