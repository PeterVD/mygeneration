using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Runtime.InteropServices;
using System.Xml;

namespace MyMeta;

#if ENTERPRISE

[ComVisible(false)]
[ClassInterface(ClassInterfaceType.AutoDual)]
#endif
public class Views : Collection<IView>, IViews, IEnumerable<IView>, ICollection
{
    internal Database Database;

    internal DataColumn f_Catalog;
    internal DataColumn f_CheckOption;
    internal DataColumn f_DateCreated;
    internal DataColumn f_DateModified;
    internal DataColumn f_Description;
    internal DataColumn f_Guid;
    internal DataColumn f_IsUpdateable;
    internal DataColumn f_Name;
    internal DataColumn f_PropID;
    internal DataColumn f_Schema;
    internal DataColumn f_Type;
    internal DataColumn f_ViewDefinition;
    public override string UserDataXPath => Database.UserDataXPath + @"/Views";

    #region IList Members

    object IList.this[int index]
    {
        get => this[index];
        set { }
    }

    #endregion


    private void BindToColumns(DataTable metaData)
    {
        if (_fieldsBound) return;

        if (metaData.Columns.Contains("TABLE_CATALOG")) f_Catalog = metaData.Columns["TABLE_CATALOG"];
        if (metaData.Columns.Contains("TABLE_SCHEMA")) f_Schema = metaData.Columns["TABLE_SCHEMA"];
        if (metaData.Columns.Contains("TABLE_NAME")) f_Name = metaData.Columns["TABLE_NAME"];
        if (metaData.Columns.Contains("TABLE_TYPE")) f_Type = metaData.Columns["TABLE_TYPE"];
        if (metaData.Columns.Contains("VIEW_DEFINITION")) f_ViewDefinition = metaData.Columns["VIEW_DEFINITION"];
        if (metaData.Columns.Contains("CHECK_OPTION")) f_CheckOption = metaData.Columns["CHECK_OPTION"];
        if (metaData.Columns.Contains("IS_UPDATABLE")) f_IsUpdateable = metaData.Columns["IS_UPDATABLE"];
        if (metaData.Columns.Contains("TABLE_GUID")) f_Guid = metaData.Columns["TABLE_GUID"];
        if (metaData.Columns.Contains("DESCRIPTION")) f_Description = metaData.Columns["DESCRIPTION"];
        if (metaData.Columns.Contains("TABLE_PROPID")) f_PropID = metaData.Columns["TABLE_PROPID"];
        if (metaData.Columns.Contains("DATE_CREATED")) f_DateCreated = metaData.Columns["DATE_CREATED"];
        if (metaData.Columns.Contains("DATE_MODIFIED")) f_DateModified = metaData.Columns["DATE_MODIFIED"];
    }

    internal virtual void LoadAll()
    {
    }

    internal virtual void PopulateArray(DataTable metaData)
    {
        BindToColumns(metaData);

        var count = metaData.Rows.Count;
        for (var i = 0; i < count; i++)
        {
            var view = (View)DbRoot.ClassFactory.CreateView();
            view.DbRoot = DbRoot;
            view.Views = this;
            view.Row = metaData.Rows[i];
            _array.Add(view);
        }
    }

    internal void AddView(View view)
    {
        _array.Add(view);
    }

    #region indexers

    public virtual IView this[object index]
    {
        get
        {
            if (index.GetType() == Types.StringType) return GetByPhysicalName(index as string);

            var idx = Convert.ToInt32(index);
            return _array[idx] as View;
        }
    }

    public View GetByName(string name)
    {
        View obj = null;

        var count = _array.Count;
        for (var i = 0; i < count; i++)
        {
            var tmp = _array[i] as View;

            if (CompareStrings(name, tmp.Name))
            {
                obj = tmp;
                break;
            }
        }

        return obj;
    }

    internal View GetByPhysicalName(string name)
    {
        View obj = null;

        var count = _array.Count;
        for (var i = 0; i < count; i++)
        {
            var tmp = _array[i] as View;

            if (CompareStrings(name, tmp.Name))
            {
                obj = tmp;
                break;
            }
        }

        return obj;
    }

    #endregion

    #region IEnumerable Members

    IEnumerator<IView> IEnumerable<IView>.GetEnumerator()
    {
        return _array.GetEnumerator();
    }

    public IEnumerator GetEnumerator()
    {
        return _array.GetEnumerator();
    }

    #endregion

    #region XML User Data

#if ENTERPRISE
    [ComVisible(false)]
#endif
    internal override bool GetXmlNode(out XmlNode node, bool forceCreate)
    {
        const string xPath = @"./Views";

        node = null;
        var success = false;

        if (null == _xmlNode)
        {
            // Get the parent node
            if (Database.GetXmlNode(out var parentNode, forceCreate))
            {
                // See if our user data already exists
                if (!GetUserData(xPath, parentNode, out _xmlNode) && forceCreate)
                {
                    // Create it, and try again
                    CreateUserMetaData(parentNode);
                    GetUserData(xPath, parentNode, out _xmlNode);
                }
            }
        }

        if (null != _xmlNode)
        {
            node = _xmlNode;
            success = true;
        }

        return success;
    }

    public override void CreateUserMetaData(XmlNode parentNode)
    {
        var myNode = parentNode.OwnerDocument.CreateNode(XmlNodeType.Element, "Views", null);
        parentNode.AppendChild(myNode);
    }

    #endregion
}