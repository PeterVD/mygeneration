using System.Runtime.InteropServices;

namespace MyMeta.Pervasive;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IIndexes))]
#endif
public class PervasiveIndexes : Indexes
{
    internal override void LoadAll()
    {
        try
        {
            var metaData = LoadData(OleDbSchemaGuid.Indexes,
                new object[] { null, null, null, null, Table.Name });

            PopulateArray(metaData);
        }
        catch
        {
        }
    }
}