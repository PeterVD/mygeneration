using System.Runtime.InteropServices;

namespace MyMeta.Pervasive;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IViews))]
#endif
public class PervasiveViews : Views
{
    internal override void LoadAll()
    {
        try
        {
            var type = DbRoot.ShowSystemData ? "SYSTEM VIEW" : "VIEW";
            var metaData = LoadData(OleDbSchemaGuid.Views, new object[] { Database.Name, null, null, "TABLE" }); //type});

            PopulateArray(metaData);
        }
        catch
        {
        }
    }
}