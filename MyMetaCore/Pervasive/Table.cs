using System.Data;
using System.Runtime.InteropServices;

namespace MyMeta.Pervasive;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(ITable))]
#endif
public class PervasiveTable : Table
{
    public override IColumns PrimaryKeys
    {
        get
        {
            if (null == _primaryKeys)
            {
                _primaryKeys = (Columns)DbRoot.ClassFactory.CreateColumns();
                _primaryKeys.Table = this;
                _primaryKeys.DbRoot = DbRoot;

                try
                {
                    var select = "SELECT Xe$Name AS COLUMN_NAME FROM X$File,X$Index,X$Field " +
                                 "WHERE Xf$Id=Xi$File and Xi$Field=Xe$Id and Xf$Name = '" + Name + "' AND Xi$Flags > 16384 " +
                                 "ORDER BY Xi$Number,Xi$Part";

                    OleDbDataAdapter adapter = new OleDbDataAdapter(select, DbRoot.ConnectionString);
                    var dataTable = new DataTable();

                    adapter.Fill(dataTable);

                    var colName = "";

                    var count = dataTable.Rows.Count;
                    for (var i = 0; i < count; i++)
                    {
                        colName = dataTable.Rows[i]["COLUMN_NAME"] as string;
                        _primaryKeys.AddColumn((Column)Columns[colName.Trim()]);
                    }
                }
                catch
                {
                }
            }

            return _primaryKeys;
        }
    }
}