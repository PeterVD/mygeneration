using System;
using System.Data;
using System.Runtime.InteropServices;
using ADODB;
using ADOX;

namespace MyMeta.Pervasive;

public class PervasiveParameters : Parameters
{
    private DataTable CreateDataTable()
    {
        var dt = new DataTable();
        dt.Columns.Add("PROCEDURE_CATALOG", Types.StringType);
        dt.Columns.Add("PROCEDURE_SCHEMA", Types.StringType);
        dt.Columns.Add("PROCEDURE_NAME", Types.StringType);
        dt.Columns.Add("PARAMETER_NAME", Types.StringType);
        dt.Columns.Add("ORDINAL_POSITION", Types.Int32Type);
        dt.Columns.Add("PARAMETER_TYPE", Types.Int32Type);
        dt.Columns.Add("PARAMETER_HASDEFAULT", Types.BoolType);
        dt.Columns.Add("PARAMETER_DEFAULT", Types.StringType);
        dt.Columns.Add("IS_NULLABLE", Types.BoolType);
        dt.Columns.Add("DATA_TYPE", Types.Int32Type);
        dt.Columns.Add("CHARACTER_MAXIMUM_LENGTH", Types.Int32Type);
        dt.Columns.Add("CHARACTER_OCTET_LENGTH", Types.Int32Type);
        dt.Columns.Add("NUMERIC_PRECISION", Types.Int32Type);
        dt.Columns.Add("NUMERIC_SCALE", Types.Int16Type);
        dt.Columns.Add("DESCRIPTION", Types.StringType);
        dt.Columns.Add("TYPE_NAME", Types.StringType);
        dt.Columns.Add("LOCAL_TYPE_NAME", Types.StringType);
        return dt;
    }

    internal override void LoadAll()
    {
        var metaData = CreateDataTable();

        var cnn = new Connection();
        var cat = new Catalog();

        // Open the Connection
        cnn.Open(DbRoot.ConnectionString, null, null, 0);
        cat.ActiveConnection = cnn;

        var proc = cat.Procedures[Procedure.Name];

        var cmd = proc.Command as Command;

        // Retrieve Parameter information
        cmd.Parameters.Refresh();

        if (cmd.Parameters.Count > 0)
        {
            var ordinal = 0;

            foreach (ADODB.Parameter param in cmd.Parameters)
            {
                var row = metaData.NewRow();

                var hyperlink = "False";

                try
                {
                    hyperlink = param.Properties["Jet OLEDB:Hyperlink"].Value.ToString();
                }
                catch
                {
                }

                row["TYPE_NAME"] = hyperlink == "False" ? param.Type.ToString() : "Hyperlink";

                row["PROCEDURE_CATALOG"] = Procedure.Database;
                row["PROCEDURE_SCHEMA"] = null;
                row["PROCEDURE_NAME"] = Procedure.Name;
                row["PARAMETER_NAME"] = param.Name;
                row["ORDINAL_POSITION"] = ordinal++;
                row["PARAMETER_TYPE"] = param.Type; //.ToString();
                row["PARAMETER_HASDEFAULT"] = false;
                row["PARAMETER_DEFAULT"] = null;
                row["IS_NULLABLE"] = false;
                row["DATA_TYPE"] = param.Type; //.ToString();
                row["CHARACTER_MAXIMUM_LENGTH"] = 0;
                row["CHARACTER_OCTET_LENGTH"] = 0;
                row["NUMERIC_PRECISION"] = param.Precision;
                row["NUMERIC_SCALE"] = param.NumericScale;
                row["DESCRIPTION"] = "";
                //	row["TYPE_NAME"]				= "";
                row["LOCAL_TYPE_NAME"] = "";

                metaData.Rows.Add(row);
            }
        }

        cnn.Close();

        PopulateArray(metaData);
    }
}