using System.Runtime.InteropServices;

namespace MyMeta.Pervasive;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(ITables))]
#endif
public class PervasiveTables : Tables
{
    internal override void LoadAll()
    {
        try
        {
            var type = DbRoot.ShowSystemData ? "SYSTEM TABLE" : "TABLE";
            var metaData = LoadData(OleDbSchemaGuid.Tables, new object[] { null, null, null, type });

            PopulateArray(metaData);
        }
        catch
        {
        }
    }
}