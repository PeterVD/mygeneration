using System.Runtime.InteropServices;

namespace MyMeta.Pervasive;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IForeignKey))]
#endif
public class PervasiveForeignKey : ForeignKey
{
    public override ITable ForeignTable
    {
        get
        {
            var catalog = ForeignKeys.Table.Database.Name;
            var schema = GetString(ForeignKeys.f_FKTableSchema);

            return DbRoot.Databases[0].Tables[GetString(ForeignKeys.f_FKTableName)];
        }
    }

    internal override void AddForeignColumn(string catalog, string schema,
        string physicalTableName, string physicalColumnName, bool primary)
    {
        var column = ForeignKeys.Table.Tables[physicalTableName].Columns[physicalColumnName] as Column;
        var c = column.Clone();

        if (primary)
        {
            if (null == _primaryColumns)
            {
                _primaryColumns = (Columns)DbRoot.ClassFactory.CreateColumns();
                _primaryColumns.ForeignKey = this;
            }

            _primaryColumns.AddColumn(c);
        }
        else
        {
            if (null == _foreignColumns)
            {
                _foreignColumns = (Columns)DbRoot.ClassFactory.CreateColumns();
                _foreignColumns.ForeignKey = this;
            }

            _foreignColumns.AddColumn(c);
        }

        column.AddForeignKey(this);
    }
}