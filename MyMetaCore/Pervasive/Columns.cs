using System;
using System.Data;
using System.Runtime.InteropServices;
using ADODB;
using ADOX;

namespace MyMeta.Pervasive;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IColumns))]
#endif
public class PervasiveColumns : Columns
{
    internal DataColumn f_AutoKey;
    internal DataColumn f_TypeName;

    internal override void LoadForTable()
    {
        var metaData = LoadData(OleDbSchemaGuid.Columns, new object[] { null, null, Table.Name });

        metaData.DefaultView.Sort = "ORDINAL_POSITION";

        PopulateArray(metaData);

        LoadExtraDataForTable();
    }

    internal override void LoadForView()
    {
        var metaData = LoadData(OleDbSchemaGuid.Columns, new object[] { null, null, View.Name });

        metaData.DefaultView.Sort = "ORDINAL_POSITION";

        PopulateArray(metaData);

        LoadExtraDataForView();
    }

    private void LoadExtraDataForTable()
    {
        try
        {
            var select =
                "SELECT \"X$Field\".Xe$Name AS Name, \"X$Field\".Xe$DataType as Type, \"X$Field\".Xe$Size AS Size, \"X$Field\".Xe$Flags AS Flags " +
                "FROM X$File,X$Field WHERE Xf$Id=Xe$File AND Xf$Name = '" + Table.Name + "' " +
                "AND \"X$Field\".Xe$DataType < 27 " +
                "ORDER BY Xe$Offset";

            OleDbDataAdapter adapter = new OleDbDataAdapter(select, DbRoot.ConnectionString);
            var dataTable = new DataTable();

            adapter.Fill(dataTable);

            if (_array.Count > 0)
            {
                var col = _array[0] as Column;

                f_TypeName = new DataColumn("TYPE_NAME", typeof(string));
                col._row.Table.Columns.Add(f_TypeName);

                f_AutoKey = new DataColumn("AUTO_INCREMENT", typeof(bool));
                col._row.Table.Columns.Add(f_AutoKey);

                var typeName = "";
                var rows = dataTable.Rows;

                var type = 0;
                var size = 0;
                var flags = 0;

                var index = 0;
                foreach (Column c in this)
                {
                    var row = rows[index++];

                    type = Convert.ToInt32(row["Type"]);
                    size = Convert.ToInt32(row["Size"]);
                    flags = Convert.ToInt32(row["Flags"]);


                    switch (type)
                    {
                        case 0:

                            if (flags == 4100)
                                typeName = "BINARY";
                            else
                                typeName = "CHAR";
                            break;


                        case 1:

                            switch (size)
                            {
                                case 1:
                                    typeName = "TINYINT";
                                    break;
                                case 2:
                                    typeName = "SMALLINT";
                                    break;
                                case 4:
                                    typeName = "INTEGER";
                                    break;
                                case 8:
                                    typeName = "BIGINT";
                                    break;
                            }

                            break;

                        case 2:

                            switch (size)
                            {
                                case 4:
                                    typeName = "REAL";
                                    break;
                                case 8:
                                    typeName = "DOUBLE";
                                    break;
                            }

                            break;

                        case 3:

                            typeName = "DATE";
                            break;

                        case 4:

                            typeName = "TIME";
                            break;

                        case 5:

                            typeName = "DECIMAL";
                            break;

                        case 6:

                            typeName = "MONEY";
                            break;

                        case 8:

                            typeName = "NUMERIC";
                            break;

                        case 9:

                            switch (size)
                            {
                                case 4:
                                    typeName = "BFLOAT4";
                                    break;
                                case 8:
                                    typeName = "BFLOAT8";
                                    break;
                            }

                            break;

                        case 11:

                            typeName = "VARCHAR";
                            break;

                        case 14:

                            switch (size)
                            {
                                case 1:
                                    typeName = "UTINYINT";
                                    break;
                                case 2:
                                    typeName = "USMALLINT";
                                    break;
                                case 4:
                                    typeName = "UINTEGER";
                                    break;
                                case 8:
                                    typeName = "UBIGINT";
                                    break;
                            }

                            break;

                        case 15:

                            switch (size)
                            {
                                case 2:
                                    typeName = "SMALLIDENTITY";
                                    c._row["AUTO_INCREMENT"] = true;
                                    break;
                                case 4:
                                    typeName = "IDENTITY";
                                    c._row["AUTO_INCREMENT"] = true;
                                    break;
                            }

                            break;

                        case 16:

                            typeName = "BIT";
                            break;

                        case 17:

                            typeName = "NUMERICSTS";
                            break;

                        case 18:

                            typeName = "NUMERICSA";
                            break;

                        case 19:

                            typeName = "CURRENCY";
                            break;

                        case 20:

                            typeName = "TIMESTAMP";
                            break;

                        case 21:

                            if (flags == 4100)
                                typeName = "LONGVARBINARY";
                            else
                                typeName = "LONGVARCHAR";
                            break;

                        case 22:

                            typeName = "LONGVARBINARY";
                            break;

                        case 25:

                            typeName = "WSTRING";
                            break;

                        case 26:

                            typeName = "WSZSTRING";
                            break;
                    }

                    c._row["TYPE_NAME"] = typeName;
                }
            }
        }
        catch
        {
        }
    }

    private void LoadExtraDataForView()
    {
        try
        {
            if (_array.Count > 0)
            {
                var cnn = new Connection();
                var rs = new Recordset();
                var cat = new Catalog();

                // Open the Connection
                cnn.Open(DbRoot.ConnectionString, null, null, 0);
                cat.ActiveConnection = cnn;

                rs.Source = cat.Views[View.Name].Command;
                rs.Fields.Refresh();
                var flds = rs.Fields;

                var col = _array[0] as Column;

                f_TypeName = new DataColumn("TYPE_NAME", typeof(string));
                col._row.Table.Columns.Add(f_TypeName);

                f_AutoKey = new DataColumn("AUTO_INCREMENT", typeof(bool));
                col._row.Table.Columns.Add(f_AutoKey);

                Column c = null;
                Field fld = null;

                var count = _array.Count;
                for (var index = 0; index < count; index++)
                {
                    fld = flds[index];
                    c = (Column)this[fld.Name];

                    c._row["TYPE_NAME"] = fld.Type.ToString();
                    c._row["AUTO_INCREMENT"] = false;
                }

                rs.Close();
                cnn.Close();
            }
        }
        catch
        {
        }
    }
}