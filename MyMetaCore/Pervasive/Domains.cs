using System.Runtime.InteropServices;

namespace MyMeta.Pervasive;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IDomains))]
#endif
public class PervasiveDomains : Domains
{
}