using System.Runtime.InteropServices;
using ADODB;
using ADOX;

namespace MyMeta.Pervasive;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IResultColumns))]
#endif
public class PervasiveResultColumns : ResultColumns
{
    internal override void LoadAll()
    {
        var cnn = new Connection();
        var rs = new Recordset();
        var cat = new Catalog();

        // Open the Connection
        cnn.Open(DbRoot.ConnectionString, null, null, 0);
        cat.ActiveConnection = cnn;

        var proc = cat.Procedures[Procedure.Name];

        // Retrieve Parameter information
        rs.Source = proc.Command as Command;
        rs.Fields.Refresh();

        PervasiveResultColumn resultColumn;

        if (rs.Fields.Count > 0)
        {
            var ordinal = 0;

            foreach (Field field in rs.Fields)
            {
                resultColumn = DbRoot.ClassFactory.CreateResultColumn() as PervasiveResultColumn;
                resultColumn.DbRoot = DbRoot;
                resultColumn.ResultColumns = this;

                resultColumn.name = field.Name;
                resultColumn.ordinal = ordinal++;
                resultColumn.typeName = field.Type.ToString();

                _array.Add(resultColumn);
            }
        }

        cnn.Close();
    }
}