using System.Runtime.InteropServices;

namespace MyMeta.Pervasive;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IDatabases))]
#endif
public class PervasiveDatabases : Databases
{
    internal override void LoadAll()
    {
        try
        {
            OleDbConnection cn = new OleDbConnection(DbRoot.ConnectionString);

            string dbName = cn.DataSource;
            int index = cn.DataSource.LastIndexOfAny(new[] { '\\' });

            if (index >= 0) dbName = cn.DataSource.Substring(index + 1);

            // We add our one and only Database
            var database = (PervasiveDatabase)DbRoot.ClassFactory.CreateDatabase();
            database._name = dbName;
            database.DbRoot = DbRoot;
            database.Databases = this;
            _array.Add(database);
        }
        catch
        {
        }
    }
}