using System.Runtime.InteropServices;

namespace MyMeta.Oracle;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IIndexes))]
#endif
public class OracleIndexes : Indexes
{
    internal override void LoadAll()
    {
        try
        {
            var metaData = LoadData(OleDbSchemaGuid.Indexes,
                new object[] { null, Table.Database.Name, null, null, Table.Name });

            PopulateArray(metaData);
        }
        catch
        {
        }
    }
}