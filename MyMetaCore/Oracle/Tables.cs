using System.Data;
using System.Runtime.InteropServices;

namespace MyMeta.Oracle;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(ITables))]
#endif
public class OracleTables : Tables
{
    internal override void LoadAll()
    {
        try
        {
            var type = DbRoot.ShowSystemData ? "SYSTEM TABLE" : "TABLE";
            var metaData = LoadData(OleDbSchemaGuid.Tables, new object[] { null, Database.Name, null, null, type });

            // Oracle returns VIEWS in Addition to when you ask for TABLES, however, if you just ask for VIEWS that works fine
            metaData.DefaultView.RowFilter = "TABLE_TYPE = '" + type + "'";

            PopulateArray(metaData);

            LoadExtraData(Database.SchemaName);
        }
        catch
        {
        }
    }

    private void LoadExtraData(string schema)
    {
        try
        {
            var select = "SELECT DISTINCT C.TABLE_NAME, C.COMMENTS AS DESCRIPTION FROM SYS.ALL_TAB_COMMENTS C, SYS.ALL_TABLES T " +
                         "WHERE T.OWNER = '" + schema + "' AND T.OWNER = C.OWNER	AND T.TABLE_NAME = C.TABLE_NAME	AND C.COMMENTS  is not null";

            OleDbDataAdapter adapter = new OleDbDataAdapter(select, DbRoot.ConnectionString);
            var dataTable = new DataTable();

            adapter.Fill(dataTable);

            var rows = dataTable.Rows;

            if (rows.Count > 0)
            {
                Table t;
                foreach (DataRow row in rows)
                {
                    t = this[row["TABLE_NAME"]] as Table;
                    t._row["DESCRIPTION"] = row["DESCRIPTION"];
                }
            }
        }
        catch
        {
        }
    }
}