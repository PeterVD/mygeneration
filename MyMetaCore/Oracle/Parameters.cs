using System.Runtime.InteropServices;

namespace MyMeta.Oracle;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IParameters))]
#endif
public class OracleParameters : Parameters
{
    internal override void LoadAll()
    {
        try
        {
            var metaData = LoadData(OleDbSchemaGuid.Procedure_Parameters,
                new object[] { null, Procedure.Database.Name, Procedure.Name });

            PopulateArray(metaData);
        }
        catch
        {
        }
    }
}