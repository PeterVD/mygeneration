using System;
using System.Runtime.InteropServices;
using ADODB;

namespace MyMeta.Oracle;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IDatabase))]
#endif
public class OracleDatabase : Database
{
    public override string Name
    {
        get
        {
            var o = _row["SCHEMA_NAME"];

            if (DBNull.Value == o) return string.Empty;

            return (string)o;
        }
    }

    public override Recordset ExecuteSql(string sql)
    {
        OleDbConnection cn = new OleDbConnection(DbRoot.ConnectionString);
        cn.Open();
        //cn.ChangeDatabase(this.Name);

        return this.ExecuteIntoRecordset(sql, cn);
    }
}