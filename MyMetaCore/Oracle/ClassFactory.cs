namespace MyMeta.Oracle;

public class ClassFactory : IClassFactory
{
    public ITables CreateTables()
    {
        return new OracleTables();
    }

    public ITable CreateTable()
    {
        return new OracleTable();
    }

    public IColumn CreateColumn()
    {
        return new OracleColumn();
    }

    public IColumns CreateColumns()
    {
        return new OracleColumns();
    }

    public IDatabase CreateDatabase()
    {
        return new OracleDatabase();
    }

    public IDatabases CreateDatabases()
    {
        return new OracleDatabases();
    }

    public IProcedure CreateProcedure()
    {
        return new OracleProcedure();
    }

    public IProcedures CreateProcedures()
    {
        return new OracleProcedures();
    }

    public IView CreateView()
    {
        return new OracleView();
    }

    public IViews CreateViews()
    {
        return new OracleViews();
    }

    public IParameter CreateParameter()
    {
        return new OracleParameter();
    }

    public IParameters CreateParameters()
    {
        return new OracleParameters();
    }

    public IForeignKey CreateForeignKey()
    {
        return new OracleForeignKey();
    }

    public IForeignKeys CreateForeignKeys()
    {
        return new OracleForeignKeys();
    }

    public IIndex CreateIndex()
    {
        return new OracleIndex();
    }

    public IIndexes CreateIndexes()
    {
        return new OracleIndexes();
    }

    public IResultColumn CreateResultColumn()
    {
        return new OracleResultColumn();
    }

    public IResultColumns CreateResultColumns()
    {
        return new OracleResultColumns();
    }

    public IDomain CreateDomain()
    {
        return new OracleDomain();
    }

    public IDomains CreateDomains()
    {
        return new OracleDomains();
    }

    public IProviderType CreateProviderType()
    {
        return new ProviderType();
    }

    public IProviderTypes CreateProviderTypes()
    {
        return new ProviderTypes();
    }

    public IDbConnection CreateConnection()
    {
        return new System.Data.OleDb.OleDbConnection();
    }

    public static void Register()
    {
        var drv = new InternalDriver
        (typeof(ClassFactory)
            , "Provider=OraOLEDB.Oracle.1;Password=myPassword;Persist Security Info=True;User ID=myID;Data Source=myDataSource"
            , true);
        drv.RequiredDatabaseName = true;
        InternalDriver.Register("ORACLE", drv);
    }
}