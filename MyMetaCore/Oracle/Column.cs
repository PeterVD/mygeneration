using System.Runtime.InteropServices;

namespace MyMeta.Oracle;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IColumn))]
#endif
public class OracleColumn : Column
{
    public override string DataTypeName
    {
        get
        {
            var cols = Columns as OracleColumns;
            return GetString(cols.f_TypeName);
        }
    }

//		override public string DataTypeName
//		{
//			get
//			{
//				MySqlColumns cols = Columns as MySqlColumns;
//				string type = this.GetString(cols.f_TypeName);
//				int index = type.IndexOf("(");
//				if(-1 != index)
//				{
//					type = type.Substring(0, index);
//				}
//				return type;
//			}
//		}

    public override string DataTypeNameComplete
    {
        get
        {
            var cols = Columns as OracleColumns;
            return GetString(cols.f_TypeName);
        }
    }

    internal override Column Clone()
    {
        var c = base.Clone();

        return c;
    }
}