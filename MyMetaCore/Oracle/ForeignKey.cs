using System.Runtime.InteropServices;

namespace MyMeta.Oracle;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IForeignKey))]
#endif
public class OracleForeignKey : ForeignKey
{
    public override ITable ForeignTable
    {
        get
        {
            var catalog = ForeignKeys.Table.Database.Name;
            var schema = GetString(ForeignKeys.f_FKTableSchema);

            return DbRoot.Databases[schema].Tables[GetString(ForeignKeys.f_FKTableName)];
        }
    }
}