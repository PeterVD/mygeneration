using System.Runtime.InteropServices;

namespace MyMeta.Oracle;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(ITable))]
#endif
public class OracleTable : Table
{
    public override IColumns PrimaryKeys
    {
        get
        {
            if (null == _primaryKeys)
            {
                var metaData = LoadData(OleDbSchemaGuid.Primary_Keys,
                    new object[] { null, Tables.Database.Name, Name });

                _primaryKeys = (Columns)DbRoot.ClassFactory.CreateColumns();
                _primaryKeys.Table = this;
                _primaryKeys.DbRoot = DbRoot;

                var cols = (Columns)Columns;

                var colName = "";

                var count = metaData.Rows.Count;
                for (var i = 0; i < count; i++)
                {
                    colName = metaData.Rows[i]["COLUMN_NAME"] as string;
                    _primaryKeys.AddColumn(cols.GetByPhysicalName(colName));
                }
            }

            return _primaryKeys;
        }
    }
}