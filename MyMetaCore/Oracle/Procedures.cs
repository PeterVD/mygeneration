using System.Runtime.InteropServices;

namespace MyMeta.Oracle;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IProcedures))]
#endif
public class OracleProcedures : Procedures
{
    internal override void LoadAll()
    {
        try
        {
            var metaData = LoadData(OleDbSchemaGuid.Procedures,
                new object[] { null, Database.Name });

            PopulateArray(metaData);
        }
        catch
        {
        }
    }
}