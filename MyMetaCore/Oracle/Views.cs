using System.Data;
using System.Runtime.InteropServices;

namespace MyMeta.Oracle;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IViews))]
#endif
public class OracleViews : Views
{
    internal override void LoadAll()
    {
        try
        {
            var type = DbRoot.ShowSystemData ? "SYSTEM VIEW" : "VIEW";
            var metaData = LoadData(OleDbSchemaGuid.Views, new object[] { null, Database.Name, null, null, type });

            base.PopulateArray(metaData);

            LoadExtraData(Database.SchemaName);
        }
        catch
        {
        }
    }

    private void LoadExtraData(string schema)
    {
        try
        {
            var select = "SELECT DISTINCT C.TABLE_NAME, C.COMMENTS AS DESCRIPTION FROM SYS.ALL_TAB_COMMENTS C, SYS.ALL_VIEWS V " +
                         "WHERE V.OWNER = '" + schema + "' AND V.OWNER = C.OWNER AND V.VIEW_NAME = C.TABLE_NAME	AND C.COMMENTS  is not null";

            OleDbDataAdapter adapter = new OleDbDataAdapter(select, DbRoot.ConnectionString);
            var dataTable = new DataTable();

            adapter.Fill(dataTable);

            var rows = dataTable.Rows;

            if (rows.Count > 0)
            {
                View v;
                foreach (DataRow row in rows)
                {
                    v = this[row["TABLE_NAME"]] as View;
                    v._row["DESCRIPTION"] = row["DESCRIPTION"];
                }
            }
        }
        catch
        {
        }
    }
}