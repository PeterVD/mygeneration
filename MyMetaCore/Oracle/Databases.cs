namespace MyMeta.Oracle;

public class OracleDatabases : Databases
{
    internal override void LoadAll()
    {
        var metaData = LoadData(OleDbSchemaGuid.Schemata, new object[] { null });

        PopulateArray(metaData);
    }

    internal override void PopulateSchemaData()
    {
        // we do nothing here
    }
}