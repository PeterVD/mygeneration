using System;
using System.Runtime.InteropServices;

namespace MyMeta;

public class ProviderType : Single, IProviderType, INameValueItem
{
    internal ProviderTypes ProviderTypes;

    #region Properties

    public virtual string Type => GetString(ProviderTypes.f_Type);

    public virtual int DataType => GetInt32(ProviderTypes.f_DataType);

    public virtual int ColumnSize => GetInt32(ProviderTypes.f_ColumnSize);

    public virtual string LiteralPrefix => GetString(ProviderTypes.f_LiteralPrefix);

    public virtual string LiteralSuffix => GetString(ProviderTypes.f_LiteralSuffix);

    public virtual string CreateParams => GetString(ProviderTypes.f_CreateParams);

    public virtual bool IsNullable => GetBool(ProviderTypes.f_IsNullable);

    public virtual bool IsCaseSensitive => GetBool(ProviderTypes.f_IsCaseSensitive);

    public virtual string Searchable => GetString(ProviderTypes.f_Searchable);

    public virtual bool IsUnsigned => GetBool(ProviderTypes.f_IsUnsigned);

    public virtual bool HasFixedPrecScale => GetBool(ProviderTypes.f_HasFixedPrecScale);

    public virtual bool CanBeAutoIncrement => GetBool(ProviderTypes.f_CanBeAutoIncrement);

    public virtual string LocalType => GetString(ProviderTypes.f_LocalType);

    public virtual int MinimumScale => GetInt32(ProviderTypes.f_MinimumScale);

    public virtual int MaximumScale => GetInt32(ProviderTypes.f_MaximumScale);

    public virtual Guid TypeGuid => GetGuid(ProviderTypes.f_TypeGuid);

    public virtual string TypeLib => GetString(ProviderTypes.f_TypeLib);

    public virtual string Version => GetString(ProviderTypes.f_Version);

    public virtual bool IsLong => GetBool(ProviderTypes.f_IsLong);

    public virtual bool BestMatch => GetBool(ProviderTypes.f_BestMatch);

    public virtual bool IsFixedLength => GetBool(ProviderTypes.f_IsFixedLength);

    #endregion

    #region INameValueCollection Members

    public string ItemName => Type;

    public string ItemValue => Type;

    #endregion
}