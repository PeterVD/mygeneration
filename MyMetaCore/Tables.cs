using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

namespace MyMeta;

public class Tables : Collection<ITable>, ITables, IEnumerable, ICollection
{
    internal Database Database;
    internal DataColumn f_Catalog;
    internal DataColumn f_DateCreated;
    internal DataColumn f_DateModified;
    internal DataColumn f_Description;
    internal DataColumn f_Guid;
    internal DataColumn f_Name;
    internal DataColumn f_PropID;
    internal DataColumn f_Schema;
    internal DataColumn f_Type;

    #region IList Members

    object IList.this[int index]
    {
        get => this[index];
        set { }
    }

    #endregion


    private void BindToColumns(DataTable metaData)
    {
        if (_fieldsBound) return;

        if (metaData.Columns.Contains("TABLE_CATALOG")) f_Catalog = metaData.Columns["TABLE_CATALOG"];
        if (metaData.Columns.Contains("TABLE_SCHEMA")) f_Schema = metaData.Columns["TABLE_SCHEMA"];
        if (metaData.Columns.Contains("TABLE_NAME")) f_Name = metaData.Columns["TABLE_NAME"];
        if (metaData.Columns.Contains("TABLE_TYPE")) f_Type = metaData.Columns["TABLE_TYPE"];
        if (metaData.Columns.Contains("TABLE_GUID")) f_Guid = metaData.Columns["TABLE_GUID"];
        if (metaData.Columns.Contains("DESCRIPTION")) f_Description = metaData.Columns["DESCRIPTION"];
        if (metaData.Columns.Contains("TABLE_PROPID")) f_PropID = metaData.Columns["TABLE_PROPID"];
        if (metaData.Columns.Contains("DATE_CREATED")) f_DateCreated = metaData.Columns["DATE_CREATED"];
        if (metaData.Columns.Contains("DATE_MODIFIED")) f_DateModified = metaData.Columns["DATE_MODIFIED"];
    }

    internal virtual void LoadAll()
    {
    }

    protected void PopulateArray(DataTable metaData)
    {
        BindToColumns(metaData);

        if (metaData.DefaultView.Count <= 0) return;

        var enumerator = metaData.DefaultView.GetEnumerator();
        while (enumerator.MoveNext())
        {
            var rowView = enumerator.Current as DataRowView;

            var table = (Table)DbRoot.ClassFactory.CreateTable();
            table.DbRoot = DbRoot;
            table.Tables = this;
            table.Row = rowView.Row;
            _array.Add(table);
        }
    }

    internal void AddTable(Table table)
    {
        _array.Add(table);
    }

    #region indexers

    public ITable this[object index]
    {
        get
        {
            if (index.GetType() == Types.StringType) return GetByPhysicalName(index as string);

            var idx = Convert.ToInt32(index);
            return _array[idx] as Table;
        }
    }

    public Table GetByName(string name)
    {
        Table table = null;

        var count = _array.Count;
        for (var i = 0; i < count; i++)
        {
            var tmp = _array[i] as Table;

            if (CompareStrings(name, tmp.Name))
            {
                table = tmp;
                break;
            }
        }

        return table;
    }

    internal Table GetByPhysicalName(string name)
    {
        Table table = null;

        var count = _array.Count;
        for (var i = 0; i < count; i++)
        {
            var tmp = _array[i] as Table;

            if (CompareStrings(name, tmp.Name))
            {
                table = tmp;
                break;
            }
        }

        return table;
    }

    #endregion

    #region IEnumerable Members

    IEnumerator<ITable> IEnumerable<ITable>.GetEnumerator()
    {
        return _array.GetEnumerator();
    }

    public IEnumerator GetEnumerator()
    {
        return _array.GetEnumerator();
    }
    #endregion

    #region XML User Data

    public override string UserDataXPath => $@"{Database.UserDataXPath}/Tables";

    internal override bool GetXmlNode(out XmlNode node, bool forceCreate)
    {
        node = null;
        var success = false;

        if (null == _xmlNode)
        {
            // Get the parent node
            if (Database.GetXmlNode(out var parentNode, forceCreate))
            {
                // See if our user data already exists
                const string xPath = @"./Tables";
                if (!GetUserData(xPath, parentNode, out _xmlNode) && forceCreate)
                {
                    // Create it, and try again
                    CreateUserMetaData(parentNode);
                    GetUserData(xPath, parentNode, out _xmlNode);
                }
            }
        }

        if (null != _xmlNode)
        {
            node = _xmlNode;
            success = true;
        }

        return success;
    }

    public override void CreateUserMetaData(XmlNode parentNode)
    {
        var myNode = parentNode.OwnerDocument.CreateNode(XmlNodeType.Element, "Tables", null);
        parentNode.AppendChild(myNode);
    }

    #endregion
}