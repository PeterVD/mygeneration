using System.Data;
using System.Runtime.InteropServices;

namespace MyMeta.Plugin;

#if ENTERPRISE

[ComVisible(false)]
#endif
public class ClassFactory : IClassFactory
{
    private IMyMetaPlugin plugin;

    public ClassFactory(IMyMetaPlugin plugin)
    {
        this.plugin = plugin;
    }

    public ITables CreateTables()
    {
        return new PluginTables(plugin);
    }

    public ITable CreateTable()
    {
        return new PluginTable(plugin);
    }

    public IColumn CreateColumn()
    {
        return new PluginColumn(plugin);
    }

    public IColumns CreateColumns()
    {
        return new PluginColumns(plugin);
    }

    public IDatabase CreateDatabase()
    {
        return new PluginDatabase(plugin);
    }

    public IDatabases CreateDatabases()
    {
        return new PluginDatabases(plugin);
    }

    public IProcedure CreateProcedure()
    {
        return new PluginProcedure(plugin);
    }

    public IProcedures CreateProcedures()
    {
        return new PluginProcedures(plugin);
    }

    public IView CreateView()
    {
        return new PluginView(plugin);
    }

    public IViews CreateViews()
    {
        return new PluginViews(plugin);
    }

    public IParameter CreateParameter()
    {
        return new PluginParameter(plugin);
    }

    public IParameters CreateParameters()
    {
        return new PluginParameters(plugin);
    }

    public IForeignKey CreateForeignKey()
    {
        return new PluginForeignKey(plugin);
    }

    public IForeignKeys CreateForeignKeys()
    {
        return new PluginForeignKeys(plugin);
    }

    public IIndex CreateIndex()
    {
        return new PluginIndex(plugin);
    }

    public IIndexes CreateIndexes()
    {
        return new PluginIndexes(plugin);
    }

    public IResultColumn CreateResultColumn()
    {
        return new PluginResultColumn(plugin);
    }

    public IResultColumns CreateResultColumns()
    {
        return new PluginResultColumns(plugin);
    }

    public IDomain CreateDomain()
    {
        return new PluginDomain(plugin);
    }

    public IDomains CreateDomains()
    {
        return new PluginDomains(plugin);
    }

    public IProviderType CreateProviderType()
    {
        return new ProviderType();
    }

    public IProviderTypes CreateProviderTypes()
    {
        return new ProviderTypes();
    }

    public IDbConnection CreateConnection()
    {
        return plugin.NewConnection;
    }
}