using System.Runtime.InteropServices;

namespace MyMeta.Plugin;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IDomains))]
#endif
public class PluginDomains : Domains
{
    private IMyMetaPlugin plugin;

    public PluginDomains(IMyMetaPlugin plugin)
    {
        this.plugin = plugin;
    }

    internal override void LoadAll()
    {
        var metaData = plugin.GetDomains(Database.Name);
        PopulateArray(metaData);
    }
}