using System.Runtime.InteropServices;

namespace MyMeta.Plugin;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(ITable))]
#endif
public class PluginTable : Table
{
    private IMyMetaPlugin plugin;

    public PluginTable(IMyMetaPlugin plugin)
    {
        this.plugin = plugin;
    }

    public override IColumns PrimaryKeys
    {
        get
        {
            if (null == _primaryKeys)
            {
                var metaData = plugin.GetPrimaryKeyColumns(Database.Name, Name);

                _primaryKeys = (Columns)DbRoot.ClassFactory.CreateColumns();
                _primaryKeys.Table = this;
                _primaryKeys.DbRoot = DbRoot;

                var colName = "";

                var count = metaData.Count;
                for (var i = 0; i < count; i++)
                {
                    colName = metaData[i];
                    _primaryKeys.AddColumn((Column)Columns[colName]);
                }
            }

            return _primaryKeys;
        }
    }

    public override object DatabaseSpecificMetaData(string key)
    {
        return plugin.GetDatabaseSpecificMetaData(this, key);
    }
}