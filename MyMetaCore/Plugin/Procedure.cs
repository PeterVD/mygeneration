using System.Runtime.InteropServices;

namespace MyMeta.Plugin;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IProcedure))]
#endif
public class PluginProcedure : Procedure
{
    private IMyMetaPlugin plugin;

    public PluginProcedure(IMyMetaPlugin plugin)
    {
        this.plugin = plugin;
    }

    public override string ProcedureText
    {
        get
        {
            var procs = Procedures as PluginProcedures;
            return GetString(procs.f_procText);
        }
    }

    public override object DatabaseSpecificMetaData(string key)
    {
        return plugin.GetDatabaseSpecificMetaData(this, key);
    }
}