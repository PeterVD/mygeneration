using System.Data;
using System.Runtime.InteropServices;

namespace MyMeta.Plugin;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IViews))]
#endif
public class PluginViews : Views
{
    private IMyMetaPlugin plugin;

    public PluginViews(IMyMetaPlugin plugin)
    {
        this.plugin = plugin;
    }

    internal override void LoadAll()
    {
        var metaData = plugin.GetViews(Database.Name);
        BindToColumns(metaData);
        PopulateArray(metaData);
    }

    #region DataColumn Binding Stuff

    // Added for 3rd party providers
    internal DataColumn f_viewText;

    private void BindToColumns(DataTable metaData)
    {
        if (false == _fieldsBound)
            if (metaData.Columns.Contains("VIEW_TEXT"))
                f_viewText = metaData.Columns["VIEW_TEXT"];
    }

    #endregion
}