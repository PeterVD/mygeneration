using System.Runtime.InteropServices;

namespace MyMeta.Plugin;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IParameters))]
#endif
public class PluginParameters : Parameters
{
    private IMyMetaPlugin plugin;

    public PluginParameters(IMyMetaPlugin plugin)
    {
        this.plugin = plugin;
    }

    internal override void LoadAll()
    {
        var metaData = plugin.GetProcedureParameters(Procedure.Database.Name, Procedure.Name);
        PopulateArray(metaData);
    }
}