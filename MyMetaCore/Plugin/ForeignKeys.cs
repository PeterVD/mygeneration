using System.Runtime.InteropServices;

namespace MyMeta.Plugin;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IForeignKeys))]
#endif
public class PluginForeignKeys : ForeignKeys
{
    private IMyMetaPlugin plugin;

    public PluginForeignKeys(IMyMetaPlugin plugin)
    {
        this.plugin = plugin;
    }

    internal override void LoadAll()
    {
        var metaData = plugin.GetForeignKeys(Table.Database.Name, Table.Name);
        PopulateArray(metaData);
    }
}