using System.Data;
using System.Runtime.InteropServices;

namespace MyMeta.Plugin;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IColumns))]
#endif
public class PluginColumns : Columns
{
    private IMyMetaPlugin plugin;

    public PluginColumns(IMyMetaPlugin plugin)
    {
        this.plugin = plugin;
    }

    internal override void LoadForTable()
    {
        var metaData = plugin.GetTableColumns(Table.Database.Name, Table.Name);
        BindToColumns(metaData);
        PopulateArray(metaData);
    }

    internal override void LoadForView()
    {
        var metaData = plugin.GetViewColumns(View.Database.Name, View.Name);
        BindToColumns(metaData);
        PopulateArray(metaData);
    }

    #region DataColumn Binding Stuff

    // Added for 3rd party providers
    internal DataColumn f_extTypeName;
    internal DataColumn f_extTypeNameComplete;

    private void BindToColumns(DataTable metaData)
    {
        if (false == _fieldsBound)
        {
            if (metaData.Columns.Contains("TYPE_NAME"))
                f_extTypeName = metaData.Columns["TYPE_NAME"];

            if (metaData.Columns.Contains("TYPE_NAME_COMPLETE"))
                f_extTypeNameComplete = metaData.Columns["TYPE_NAME_COMPLETE"];
        }
    }

    #endregion
}