using System.Data;
using System.Runtime.InteropServices;

namespace MyMeta.Plugin;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IProcedures))]
#endif
public class PluginProcedures : Procedures
{
    private IMyMetaPlugin plugin;

    public PluginProcedures(IMyMetaPlugin plugin)
    {
        this.plugin = plugin;
    }

    internal override void LoadAll()
    {
        var metaData = plugin.GetProcedures(Database.Name);
        BindToColumns(metaData);
        PopulateArray(metaData);
    }

    #region DataColumn Binding Stuff

    // Added for 3rd party providers
    internal DataColumn f_procText;

    private void BindToColumns(DataTable metaData)
    {
        if (false == _fieldsBound)
            if (metaData.Columns.Contains("PROCEDURE_TEXT"))
                f_procText = metaData.Columns["PROCEDURE_TEXT"];
    }

    #endregion
}