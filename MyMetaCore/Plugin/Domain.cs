using System.Runtime.InteropServices;

namespace MyMeta.Plugin;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IDomain))]
#endif
public class PluginDomain : Domain
{
    private IMyMetaPlugin plugin;

    public PluginDomain(IMyMetaPlugin plugin)
    {
        this.plugin = plugin;
    }

    public override object DatabaseSpecificMetaData(string key)
    {
        return plugin.GetDatabaseSpecificMetaData(this, key);
    }
}