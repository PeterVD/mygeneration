using System.Runtime.InteropServices;

namespace MyMeta.Plugin;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IResultColumn))]
#endif
public class PluginResultColumn : ResultColumn
{
    private IMyMetaPlugin plugin;

    public PluginResultColumn(IMyMetaPlugin plugin)
    {
        this.plugin = plugin;
    }

    // k3b 20070709: implemented missing Properties

    #region Properties

    public override string Name => GetString(((PluginResultColumns)ResultColumns).f_Name);

    public override int DataType => GetInt32(((PluginResultColumns)ResultColumns).f_DataType);

    public override string DataTypeName => GetString(((PluginResultColumns)ResultColumns).f_DataTypeName);

    public override string DataTypeNameComplete => GetString(((PluginResultColumns)ResultColumns).f_DataTypeNameComplete);

    public override int Ordinal => GetInt32(((PluginResultColumns)ResultColumns).f_Ordinal);

    #endregion
}