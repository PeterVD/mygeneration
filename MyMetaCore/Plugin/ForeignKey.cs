using System.Runtime.InteropServices;

namespace MyMeta.Plugin;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IForeignKey))]
#endif
public class PluginForeignKey : ForeignKey
{
    private IMyMetaPlugin plugin;

    public PluginForeignKey(IMyMetaPlugin plugin)
    {
        this.plugin = plugin;
    }

    public override object DatabaseSpecificMetaData(string key)
    {
        return plugin.GetDatabaseSpecificMetaData(this, key);
    }
}