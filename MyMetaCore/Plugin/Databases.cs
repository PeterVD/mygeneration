using System.Runtime.InteropServices;

namespace MyMeta.Plugin;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IDatabases))]
#endif
public class PluginDatabases : Databases
{
    private IMyMetaPlugin plugin;

    public PluginDatabases(IMyMetaPlugin plugin)
    {
        this.plugin = plugin;
    }

    internal override void LoadAll()
    {
        var metaData = plugin.Databases;
        PopulateArray(metaData);
    }
}