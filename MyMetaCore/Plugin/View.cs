using System.Runtime.InteropServices;

namespace MyMeta.Plugin;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IView))]
#endif
public class PluginView : View
{
    private IMyMetaPlugin plugin;

    public PluginView(IMyMetaPlugin plugin)
    {
        this.plugin = plugin;
    }

    public override string ViewText
    {
        get
        {
            var views = Views as PluginViews;
            return GetString(views.f_viewText);
        }
    }

    public override IViews SubViews
    {
        get
        {
            if (!_subViewInfoLoaded) LoadSubViewInfo();
            return _views;
        }
    }

    public override ITables SubTables
    {
        get
        {
            if (!_subViewInfoLoaded) LoadSubViewInfo();
            return _tables;
        }
    }

    private void LoadSubViewInfo()
    {
        _views = (Views)DbRoot.ClassFactory.CreateViews();
        _views.DbRoot = DbRoot;
        _views.Database = Views.Database;
        var subViews = plugin.GetViewSubViews(Database.Name, Name);
        foreach (var entity in subViews)
        {
            var view = Database.Views[entity] as View;
            if (null != view) _views.AddView(view);
        }

        _tables = (Tables)DbRoot.ClassFactory.CreateTables();
        _tables.DbRoot = DbRoot;
        _tables.Database = Views.Database;
        var subTables = plugin.GetViewSubTables(Database.Name, Name);
        foreach (var entity in subTables)
        {
            var table = Database.Tables[entity] as Table;
            if (null != table) _tables.AddTable(table);
        }
    }

    public override object DatabaseSpecificMetaData(string key)
    {
        return plugin.GetDatabaseSpecificMetaData(this, key);
    }
}