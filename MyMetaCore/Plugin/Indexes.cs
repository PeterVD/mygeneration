using System.Runtime.InteropServices;

namespace MyMeta.Plugin;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IIndexes))]
#endif
public class PluginIndexes : Indexes
{
    private IMyMetaPlugin plugin;

    public PluginIndexes(IMyMetaPlugin plugin)
    {
        this.plugin = plugin;
    }

    internal override void LoadAll()
    {
        var metaData = plugin.GetTableIndexes(Table.Database.Name, Table.Name);
        PopulateArray(metaData);
    }
}