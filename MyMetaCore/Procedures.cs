using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

namespace MyMeta;

public class Procedures : Collection<IProcedure>, IProcedures, IEnumerable, ICollection
{
    internal Database Database;

    internal DataColumn f_Catalog;
    internal DataColumn f_DateCreated;
    internal DataColumn f_DateModified;
    internal DataColumn f_Description;
    internal DataColumn f_Name;
    internal DataColumn f_ProcedureDefinition;
    internal DataColumn f_Schema;
    internal DataColumn f_Type;

    #region IList Members

    object IList.this[int index]
    {
        get => this[index];
        set { }
    }

    #endregion


    private void BindToColumns(DataTable metaData)
    {
        if (_fieldsBound) return;

        if (metaData.Columns.Contains("PROCEDURE_CATALOG")) f_Catalog = metaData.Columns["PROCEDURE_CATALOG"];
        if (metaData.Columns.Contains("PROCEDURE_SCHEMA")) f_Schema = metaData.Columns["PROCEDURE_SCHEMA"];
        if (metaData.Columns.Contains("PROCEDURE_NAME")) f_Name = metaData.Columns["PROCEDURE_NAME"];
        if (metaData.Columns.Contains("PROCEDURE_TYPE")) f_Type = metaData.Columns["PROCEDURE_TYPE"];
        if (metaData.Columns.Contains("PROCEDURE_DEFINITION")) f_ProcedureDefinition = metaData.Columns["PROCEDURE_DEFINITION"];
        if (metaData.Columns.Contains("DESCRIPTION")) f_Description = metaData.Columns["DESCRIPTION"];
        if (metaData.Columns.Contains("DATE_CREATED")) f_DateCreated = metaData.Columns["DATE_CREATED"];
        if (metaData.Columns.Contains("DATE_MODIFIED")) f_DateModified = metaData.Columns["DATE_MODIFIED"];
    }

    internal virtual void LoadAll()
    {
    }

    internal void PopulateArray(DataTable metaData)
    {
        BindToColumns(metaData);

        var count = metaData.Rows.Count;
        for (var i = 0; i < count; i++)
        {
            var procedure = (Procedure)DbRoot.ClassFactory.CreateProcedure();
            procedure.DbRoot = DbRoot;
            procedure.Procedures = this;
            procedure.Row = metaData.Rows[i];
            _array.Add(procedure);
        }
    }

    internal void AddProcedure(Procedure procedure)
    {
        _array.Add(procedure);
    }

    public override string UserDataXPath
    {
        get { return Database.UserDataXPath + @"/Procedures"; }
    }

    internal override bool GetXmlNode(out XmlNode node, bool forceCreate)
    {
        const string xPath = @"./Procedures";

        node = null;
        var success = false;

        // Get the parent node and see if our user data already exists
        if (null == _xmlNode && Database.GetXmlNode(out var parentNode, forceCreate) && !GetUserData(xPath, parentNode, out _xmlNode) && forceCreate)
        {
            // Create it, and try again
            CreateUserMetaData(parentNode);
            GetUserData(xPath, parentNode, out _xmlNode);
        }

        if (null != _xmlNode)
        {
            node = _xmlNode;
            success = true;
        }

        return success;
    }
    public override void CreateUserMetaData(XmlNode parentNode)
    {
        var myNode = parentNode.OwnerDocument.CreateNode(XmlNodeType.Element, "Procedures", null);
        parentNode.AppendChild(myNode);
    }

    #region indexers

    public virtual IProcedure this[object index]
    {
        get
        {
            if (index.GetType() == Types.StringType) return GetByPhysicalName(index as string);

            var idx = Convert.ToInt32(index);
            return _array[idx] as Procedure;
        }
    }

    public Procedure GetByName(string name)
    {
        Procedure obj = null;

        var count = _array.Count;
        for (var i = 0; i < count; i++)
        {
            var tmp = _array[i] as Procedure;

            if (CompareStrings(name, tmp.Name))
            {
                obj = tmp;
                break;
            }
        }

        return obj;
    }

    public Procedure GetByPhysicalName(string name)
    {
        Procedure obj = null;

        var count = _array.Count;
        for (var i = 0; i < count; i++)
        {
            var tmp = _array[i] as Procedure;

            if (CompareStrings(name, tmp.Name))
            {
                obj = tmp;
                break;
            }
        }

        return obj;
    }

    #endregion

    #region IEnumerable Members

    IEnumerator<IProcedure> IEnumerable<IProcedure>.GetEnumerator()
    {
        return _array.GetEnumerator();
    }

    public IEnumerator GetEnumerator()
    {
        return _array.GetEnumerator();
    }

    #endregion
}