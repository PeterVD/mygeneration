using System;
using System.Xml;

namespace MyMeta;

public class View : Single, IView, INameValueItem
{
    private Columns _columns;
    protected bool _subViewInfoLoaded;
    protected Tables _tables;
    protected Views _views;

    internal Views Views;

    public IDatabase Database => Views.Database;

    #region Collections

    public IColumns Columns
    {
        get
        {
            if (_columns is not null) return _columns;

            _columns = (Columns)DbRoot.ClassFactory.CreateColumns();
            _columns.View = this;
            _columns.DbRoot = DbRoot;
            _columns.LoadForView();

            return _columns;
        }
    }

    public virtual IViews SubViews
    {
        get
        {
            if (_views is not null) return _views;
            _views = (Views)DbRoot.ClassFactory.CreateViews();
            _views.DbRoot = DbRoot;
            _views.Database = Views.Database;

            return _views;
        }
    }

    public virtual ITables SubTables
    {
        get
        {
            if (_tables is not null) return _tables;

            _tables = (Tables)DbRoot.ClassFactory.CreateTables();
            _tables.DbRoot = DbRoot;
            _tables.Database = Views.Database;

            return _tables;
        }
    }

    public virtual IPropertyCollection GlobalProperties
    {
        get
        {
            var db = Views.Database;
            if (db._viewProperties is not null) return db._viewProperties;

            db._viewProperties = new PropertyCollection
            {
                Parent = this
            };

            var xPath = GlobalUserDataXPath;
            var xmlNode = DbRoot.UserData.SelectSingleNode(xPath, null);

            if (xmlNode is null)
            {
                var parentNode = db.CreateGlobalXmlNode();

                xmlNode = parentNode.OwnerDocument.CreateNode(XmlNodeType.Element, "View", null);
                parentNode.AppendChild(xmlNode);
            }

            db._viewProperties.LoadAllGlobal(xmlNode);

            return db._viewProperties;
        }
    }

    public virtual IPropertyCollection AllProperties
    {
        get
        {
            if (null == _allProperties)
            {
                _allProperties = new PropertyCollectionAll();
                _allProperties.Load(Properties, GlobalProperties);
            }

            return _allProperties;
        }
    }

    internal PropertyCollectionAll _allProperties;

    #endregion

    #region Properties

    public override string Alias
    {
        get
        {
            return GetXmlNode(out var node, false) && GetUserData(node, "n", out var niceName) && string.Empty != niceName ? niceName : Name;
        }
        set
        {
            if (GetXmlNode(out var node, true)) SetUserData(node, "n", value);
        }
    }

    public override string Name => GetString(Views.f_Name);

    public string Schema => GetString(Views.f_Schema);

    public virtual string ViewText => GetString(Views.f_ViewDefinition);

    public bool CheckOption => GetBool(Views.f_CheckOption);

    public bool IsUpdateable => GetBool(Views.f_IsUpdateable);

    public string Type => GetString(Views.f_Type);

    public Guid Guid => GetGuid(Views.f_Guid);

    public string Description => GetString(Views.f_Description);

    public int PropID => GetInt32(Views.f_PropID);

    public DateTime DateCreated => GetDateTime(Views.f_DateCreated);

    public DateTime DateModified => GetDateTime(Views.f_DateModified);

    #endregion

    #region XML User Data

    public override string UserDataXPath => $@"{Views.UserDataXPath}/View[@p='{Name}']";

    public override string GlobalUserDataXPath => $"{Views.Database.GlobalUserDataXPath}/View";

    internal override bool GetXmlNode(out XmlNode node, bool forceCreate)
    {
        node = null;
        var success = false;

        if (null == _xmlNode)
        {
            // Get the parent node
            if (Views.GetXmlNode(out var parentNode, forceCreate))
            {
                // See if our user data already exists
                var xPath = $@"./View[@p='{Name}']";
                if (!GetUserData(xPath, parentNode, out _xmlNode) && forceCreate)
                {
                    // Create it, and try again
                    CreateUserMetaData(parentNode);
                    GetUserData(xPath, parentNode, out _xmlNode);
                }
            }
        }

        if (null != _xmlNode)
        {
            node = _xmlNode;
            success = true;
        }

        return success;
    }

    public override void CreateUserMetaData(XmlNode parentNode)
    {
        var myNode = parentNode.OwnerDocument.CreateNode(XmlNodeType.Element, "View", null);
        parentNode.AppendChild(myNode);

        var attr = parentNode.OwnerDocument.CreateAttribute("p");
        attr.Value = Name;
        myNode.Attributes.Append(attr);

        attr = parentNode.OwnerDocument.CreateAttribute("n");
        attr.Value = "";
        myNode.Attributes.Append(attr);
    }

    #endregion

    #region INameValueCollection Members

    public string ItemName => Name;

    public string ItemValue => Name;

    #endregion
}