using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace MyMeta;

/// <summary>
/// Summary description for Collection.
/// </summary>
/// 
public class Collection<T> : MetaObject
{
    protected List<T> _array = [];
    protected bool _fieldsBound;
    public virtual int Count => _array.Count;

    public bool CompareStrings(string s1, string s2)
    {
        return 0 == string.Compare(s1, s2, DbRoot.IgnoreCase);
    }
    
    public bool IsSynchronized => false;

    public void CopyTo(Array array, int index)
    {
    }

    public object SyncRoot => null;

    public bool IsReadOnly => true;

    //		object System.Collections.IList.this[int index]
//		{
//			get	{ return this[index];}
//			set	{ }
//		}

    public void RemoveAt(int index)
    {
    }

    public void Insert(int index, object value)
    {
    }

    public void Remove(object value)
    {
    }

    public bool Contains(object value)
    {
        return false;
    }

    public void Clear()
    {
    }

    public int IndexOf(object value)
    {
        return 0;
    }

    public int Add(object value)
    {
        return 0;
    }

    public bool IsFixedSize => true;
}