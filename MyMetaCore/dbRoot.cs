// #define IGNORE_VISTA // if defined in csproj compile without VISTADB
// #define ENTERPRISE // if defined in csproj create com component

// #define PLUGINS_FROM_SUBDIRS  // if defined Plugins can also live in subdirectories of the MyMeta-bin-dir
/*
 * PLUGINS_FROM_SUBDIRS disabled because k3b found no way to use dll-s in scrips
 *  tried <%#REFERENCE subdir\myDll.dll  %> ==> script compiler error not found
 *  csc.exe /lib:plugins  ==> script compiler error not found
 */


using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SQLite;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using FirebirdSql.Data.FirebirdClient;
using MyMeta.MySql5;
using MyMeta.VistaDB;
using Npgsql;
using ClassFactory = MyMeta.Sql.ClassFactory;

namespace MyMeta;

/// <summary>
///     MyMeta is the root of the MyMeta meta-data. MyMeta is an intrinsic object available to your script and configured
///     based on the settings
///     you have entered in the Default Settings dialog. It is already connected before you script execution begins.
/// </summary>
/// <remarks>
///     MyMeta has 1 Collection:
///     <list type="table">
///         <item>
///             <term>Databases</term>
///             <description>Contains a collection of all of the databases in your system</description>
///         </item>
///     </list>
///     There is a property collection on every entity in your database, you can add key/value
///     pairs to the User Meta Data either through the user interface of MyGeneration or
///     programmatically in your scripts.  User meta data is stored in XML and never writes to your database.
///     This can be very useful, you might need more meta data than MyMeta supplies, in fact,
///     MyMeta will eventually offer extended meta data using this feature as well. The current plan
///     is that any extended data added via MyGeneration will have a key that beings with "MyMeta.Something"
///     where 'Something' equals the description.
/// </remarks>
/// <example>
///     VBScript - ****** NOTE ****** You never have to actually write this code, this is for education purposes only.
///     <code>
/// 	MyMeta.Connect "SQL", "Provider=SQLOLEDB.1;Persist Security Info=True;User ID=sa;Data Source=localhost"
/// 	
/// 	MyMeta.DbTarget	= "SqlClient"
/// 	MyMeta.DbTargetMappingFileName = "C:\Program Files\MyGeneration\Settings\DbTargets.xml"
/// 	
///  MyMeta.Language = "VB.NET"
///  MyMeta.LanguageMappingFileName = "C:\Program Files\MyGeneration\Settings\Languages.xml"
///  
///  MyMeta.UserMetaDataFileName = "C:\Program Files\MyGeneration\Settings\UserMetaData.xml"
///  </code>
///     JScript - ****** NOTE ****** You never have to actually write this code, this is for education purposes only.
///     <code>
/// 	MyMeta.Connect("SQL", "Provider=SQLOLEDB.1;Persist Security Info=True;User ID=sa;Data Source=localhost")
/// 	
/// 	MyMeta.DbTarget	= "SqlClient";
/// 	MyMeta.DbTargetMappingFileName = "C:\Program Files\MyGeneration\Settings\DbTargets.xml";
/// 	
///  MyMeta.Language = "VB.NET";
///  MyMeta.LanguageMappingFileName = "C:\Program Files\MyGeneration\Settings\Languages.xml";
///  
///  MyMeta.UserMetaDataFileName = "C:\Program Files\MyGeneration\Settings\UserMetaData.xml";
///  </code>
///     The above code is done for you long before you execute your script and the values come from the Default Settings
///     Dialog.
///     However, you can override these defaults as many of the sample scripts do. For instance, if you have a script that
///     is for SqlClient
///     only go ahead and set the MyMeta.DbTarget in your script thus overriding the Default Settings.
/// </example>

public class dbRoot : IDisposable, IAsyncDisposable
{
    public dbRoot()
    {
        Access.ClassFactory.Register();
        Advantage.ClassFactory.Register();
        DB2.ClassFactory.Register();
        Firebird.ClassFactory.Register();
        ISeries.ClassFactory.Register();
        MySql.ClassFactory.Register();
        MySql5.ClassFactory.Register();
        Oracle.ClassFactory.Register();
        Pervasive.ClassFactory.Register();
        PostgreSQL.ClassFactory.Register();
        PostgreSQL8.ClassFactory.Register();
        Sql.ClassFactory.Register();
        SQLite.ClassFactory.Register();
#if !IGNORE_VISTA
        VistaDB.ClassFactory.Register();
#endif
        Reset();
    }

    private void Reset()
    {
        UserData = null;

        IgnoreCase = true;
        requiredDatabaseName = false;
        requiresSchemaName = false;
        StripTrailingNulls = false;
        TrailingNull = ((char)0x0).ToString();

        ClassFactory = null;

        ShowSystemData = false;

        Driver = dbDriver.None;
        DriverString = "NONE";
        _databases = null;
        ConnectionString = "";
        _theConnection?.Dispose();
        _theConnection = new();
        IsConnected = false;
        _parsedConnectionString = null;
        _defaultDatabase = "";

        // Language
        _languageMappingFileName = string.Empty;
        _language = string.Empty;
        _languageDoc = null;
        LanguageNode = null;

        UserData = new XmlDocument();
        UserData.AppendChild(UserData.CreateNode(XmlNodeType.Element, "MyMeta", null));

        // DbTarget
        _dbTargetMappingFileName = string.Empty;
        _dbTarget = string.Empty;
        _dbTargetDoc = null;
        DbTargetNode = null;
    }
    
    /// <summary>
    ///     Contains all of the databases in your DBMS system.
    /// </summary>
    public IDatabases Databases
    {
        get
        {
            if (_databases == null && ClassFactory is not null)
            {
                _databases = (Databases)ClassFactory.CreateDatabases();
                _databases.DbRoot = this;
                _databases.LoadAll();
            }

            return _databases;
        }
    }

    /// <summary>
    ///     This is the default database as defined in your connection string, or if not provided your DBMS system may provide
    ///     one.
    ///     Finally, for single database systems like Microsoft Access it will be the default database.
    /// </summary>
    public IDatabase DefaultDatabase
    {
        get
        {
            IDatabase defDatabase = null;
            try
            {
                var dbases = Databases as Databases;

                if (_defaultDatabase is not null && _defaultDatabase != "")
                {
                    defDatabase = dbases.GetByPhysicalName(_defaultDatabase);
                }
                else
                {
                    if (dbases.Count == 1)
                    {
                        defDatabase = dbases[0];
                    }
                }
            }
            catch
            {
            }

            return defDatabase;
        }
    }

    public IProviderTypes ProviderTypes
    {
        get
        {
            if (_providerTypes == null)
            {
                _providerTypes = (ProviderTypes)ClassFactory.CreateProviderTypes();
                _providerTypes.DbRoot = this;
                _providerTypes.LoadAll();
            }

            return _providerTypes;
        }
    }


    public IDbConnection BuildConnection(string driver, string connectionString)
    {
        IDbConnection conn = null;
        switch (driver.ToUpper())
        {
            case MyMetaDrivers.MySql2:
                MySql5Databases.LoadAssembly();
                conn = MySql5Databases.CreateConnection(connectionString);
                break;

            case MyMetaDrivers.PostgreSQL:
            case MyMetaDrivers.PostgreSQL8:
                conn = new NpgsqlConnection(connectionString);
                break;

            case MyMetaDrivers.Firebird:
            case MyMetaDrivers.Interbase:
                conn = new FbConnection(connectionString);
                break;

            case MyMetaDrivers.SQLite:
                conn = new SQLiteConnection(connectionString);
                break;
#if !IGNORE_VISTA
            case MyMetaDrivers.VistaDB:
                try
                {
                    var mh = new MetaHelper();
                    conn = mh.GetConnection(connectionString);
                }
                catch
                {
                    throw new Exception("Invalid VistaDB connection or VistaDB not installed");
                }

                break;
#endif
            default:
                if (Plugins.ContainsKey(driver))
                    conn = GetConnectionFromPlugin(driver, connectionString);
                else
                    conn = new OleDbConnection(connectionString);
                break;
        }

        return conn;
    }

    /// <summary>
    ///     This is how you connect to your DBMS system using MyMeta. This is already called for you before your script beings
    ///     execution.
    /// </summary>
    /// <param name="driver">A string as defined in the remarks below</param>
    /// <param name="connectionString">A valid connection string for you DBMS</param>
    /// <returns>True if connected, False if not</returns>
    /// <remarks>
    ///     These are the supported "drivers".
    ///     <list type="table">
    ///         <item>
    ///             <term>"ACCESS"</term><description>Microsoft Access 97 and higher</description>
    ///         </item>
    ///         <item>
    ///             <term>"DB2"</term><description>IBM DB2</description>
    ///         </item>
    ///         <item>
    ///             <term>"MYSQL"</term>
    ///             <description>Currently limited to only MySQL running on Microsoft Operating Systems</description>
    ///         </item>
    ///         <item>
    ///             <term>"MYSQL2"</term>
    ///             <description>Uses MySQL Connector/Net, Supports 4.x schema info on Windows or Linux</description>
    ///         </item>
    ///         <item>
    ///             <term>"ORACLE"</term><description>Oracle 8i - 9</description>
    ///         </item>
    ///         <item>
    ///             <term>"SQL"</term><description>Microsoft SQL Server 2000 and higher</description>
    ///         </item>
    ///         <item>
    ///             <term>"PERVASIVE"</term><description>Pervasive 9.00+ (might work on lower but untested)</description>
    ///         </item>
    ///         <item>
    ///             <term>"POSTGRESQL"</term><description>PostgreSQL 7.3+ (might work on lower but untested)</description>
    ///         </item>
    ///         <item>
    ///             <term>"POSTGRESQL8"</term><description>PostgreSQL 8.0+</description>
    ///         </item>
    ///         <item>
    ///             <term>"FIREBIRD"</term><description>Firebird</description>
    ///         </item>
    ///         <item>
    ///             <term>"INTERBASE"</term><description>Borland's InterBase</description>
    ///         </item>
    ///         <item>
    ///             <term>"SQLITE"</term><description>SQLite</description>
    ///         </item>
    ///         <item>
    ///             <term>"VISTADB"</term><description>VistaDB Database</description>
    ///         </item>
    ///         <item>
    ///             <term>"ADVANTAGE"</term><description>Advantage Database Server</description>
    ///         </item>
    ///         <item>
    ///             <term>"ISERIES"</term><description>iSeries (AS400)</description>
    ///         </item>
    ///     </list>
    ///     Below are some sample connection strings. However, the "Data Link" dialog available on the Default Settings dialog
    ///     can help you.
    ///     <list type="table">
    ///         <item>
    ///             <term>"ACCESS"</term>
    ///             <description>Provider=Microsoft.Jet.OLEDB.4.0;Data Source=c:\access\newnorthwind.mdb;User Id=;Password=</description>
    ///         </item>
    ///         <item>
    ///             <term>"DB2"</term>
    ///             <description>Provider=IBMDADB2.1;Password=sa;User ID=DB2Admin;Data Source=MyMeta;Persist Security Info=True</description>
    ///         </item>
    ///         <item>
    ///             <term>"MYSQL"</term>
    ///             <description>Provider=MySQLProv;Persist Security Info=True;Data Source=test;UID=griffo;PWD=;PORT=3306</description>
    ///         </item>
    ///         <item>
    ///             <term>"MYSQL2"</term><description>Uses Database=Test;Data Source=Griffo;User Id=anonymous;</description>
    ///         </item>
    ///         <item>
    ///             <term>"ORACLE"</term>
    ///             <description>
    ///                 Provider=OraOLEDB.Oracle.1;Password=sa;Persist Security Info=True;User ID=GRIFFO;Data
    ///                 Source=dbMeta
    ///             </description>
    ///         </item>
    ///         <item>
    ///             <term>"SQL"</term>
    ///             <description>
    ///                 Provider=SQLOLEDB.1;Persist Security Info=False;User ID=sa;Initial Catalog=Northwind;Data
    ///                 Source=localhost
    ///             </description>
    ///         </item>
    ///         <item>
    ///             <term>"PERVASIVE"</term>
    ///             <description>Provider=PervasiveOLEDB.8.60;Data Source=demodata;Location=Griffo;Persist Security Info=False</description>
    ///         </item>
    ///         <item>
    ///             <term>"POSTGRESQL"</term>
    ///             <description>Server=www.myserver.com;Port=5432;User Id=myuser;Password=aaa;Database=mygeneration;</description>
    ///         </item>
    ///         <item>
    ///             <term>"POSTGRESQL8"</term>
    ///             <description>Server=www.myserver.com;Port=5432;User Id=myuser;Password=aaa;Database=mygeneration;</description>
    ///         </item>
    ///         <item>
    ///             <term>"FIREBIRD"</term>
    ///             <description>Database=C:\firebird\EMPLOYEE.GDB;User=SYSDBA;Password=wow;Dialect=3;Server=localhost</description>
    ///         </item>
    ///         <item>
    ///             <term>"INTERBASE"</term>
    ///             <description>Database=C:\interbase\EMPLOYEE.GDB;User=SYSDBA;Password=wow;Dialect=3;Server=localhost</description>
    ///         </item>
    ///         <item>
    ///             <term>"SQLITE"</term>
    ///             <description>Data Source=C:\SQLite\employee.db;New=False;Compress=True;Synchronous=Off;Version=3</description>
    ///         </item>
    ///         <item>
    ///             <term>"VISTADB"</term><description>DataSource=C:\Program Files\VistaDB 2.0\Data\Northwind.vdb</description>
    ///         </item>
    ///         <item>
    ///             <term>"ADVANTAGE"</term>
    ///             <description>
    ///                 Provider=Advantage.OLEDB.1;Password="";User ID=AdsSys;Data Source=C:\task1;Initial
    ///                 Catalog=aep_tutorial.add;Persist Security Info=True;Advantage Server Type=ADS_LOCAL_SERVER;Trim
    ///                 Trailing Spaces=TRUE
    ///             </description>
    ///         </item>
    ///         <item>
    ///             <term>"ISERIES"</term>
    ///             <description>
    ///                 PROVIDER=IBMDA400; DATA SOURCE=MY_SYSTEM_NAME;USER ID=myUserName;PASSWORD=myPwd;DEFAULT
    ///                 COLLECTION=MY_LIBRARY;
    ///             </description>
    ///         </item>
    ///     </list>
    /// </remarks>
    public bool Connect(string driverIn, string connectionString)
    {
        var driver = driverIn.ToUpper();
        switch (driver)
        {
            case MyMetaDrivers.None:
                return true;
#if !IGNORE_VISTA
            case MyMetaDrivers.VistaDB:
#endif
            case MyMetaDrivers.SQL:
            case MyMetaDrivers.Oracle:
            case MyMetaDrivers.Access:
            case MyMetaDrivers.MySql:
            case MyMetaDrivers.MySql2:
            case MyMetaDrivers.DB2:
            case MyMetaDrivers.ISeries:
            case MyMetaDrivers.Pervasive:
            case MyMetaDrivers.PostgreSQL:
            case MyMetaDrivers.PostgreSQL8:
            case MyMetaDrivers.Firebird:
            case MyMetaDrivers.Interbase:
            case MyMetaDrivers.SQLite:
            case MyMetaDrivers.Advantage:
                return Connect(
                    MyMetaDrivers.GetDbDriverFromName(driver),
                    driver,
                    connectionString);
            default:
                return Plugins.ContainsKey(driver) && Connect(dbDriver.Plugin, driver, connectionString);
        }
    }

    /// <summary>
    ///     Same as <see cref="Connect(string, string)" />(string, string) only this uses an enumeration.
    /// </summary>
    /// <param name="driver">The driver enumeration for you DBMS system</param>
    /// <param name="connectionString">A valid connection string for you DBMS</param>
    /// <returns></returns>
    public bool Connect(dbDriver driver, string connectionString)
    {
        return Connect(driver, string.Empty, connectionString);
    }

    /// <summary>
    ///     Same as <see cref="Connect(string, string)" />(string, string) only this uses an enumeration.
    /// </summary>
    /// <param name="driver">The driver enumeration for you DBMS system</param>
    /// <param name="pluginName">The name of the plugin</param>
    /// <param name="connectionString">A valid connection string for you DBMS</param>
    /// <returns></returns>
    public bool Connect(dbDriver driver, string pluginName, string connectionString)
    {
        Reset();

        string dbName;
        int index;

        ConnectionString = connectionString.Replace("\"", "");
        Driver = driver;

        #region not fully implemented yet

/*
            InternalDriver drv = InternalDriver.Get(settings.DbDriver);
            if (drv is not null)
            {
                this._driverString = drv.DriverId;
                this.StripTrailingNulls = drv.StripTrailingNulls;
                this.requiredDatabaseName = drv.RequiredDatabaseName;

                IDbConnection con = null;
                try
                {
                    ClassFactory = drv.CreateBuildInClass();
                    if (ClassFactory is not null)
                        con = ClassFactory.CreateConnection();
                    else
                    {
                        IMyMetaPlugin plugin = drv.CreateMyMetaPluginClass();
                        if (plugin is not null)
                        {
                            MyMetaPluginContext pluginContext = new MyMetaPluginContext(drv.DriverId, this._connectionString);
                            plugin.Initialize(pluginContext);
                            con = plugin.NewConnection;
                        }
                    }
                    if (con is not null)
                    {
                        con.ConnectionString = this._connectionString;
                        // cn.Open();
                    }
                    this._defaultDatabase = drv.GetDataBaseName(cn);
                }
                catch(Exception Ex)
                {
                    throw Ex;
                } finally {
                    if (con is not null)
                        cn.Close();
                }
            }
            else
            {
                // Error
            }
*/

        #endregion not fully implemented yet

        switch (Driver)
        {
            case dbDriver.SQL:

                ConnectUsingOleDb(Driver, ConnectionString);
                DriverString = MyMetaDrivers.SQL;
                StripTrailingNulls = false;
                requiredDatabaseName = true;
                ClassFactory = new ClassFactory();
                break;

            case dbDriver.Oracle:

                ConnectUsingOleDb(Driver, ConnectionString);
                DriverString = MyMetaDrivers.Oracle;
                StripTrailingNulls = false;
                requiredDatabaseName = true;
                ClassFactory = new Oracle.ClassFactory();
                break;

            case dbDriver.Access:

                ConnectUsingOleDb(Driver, ConnectionString);
                DriverString = MyMetaDrivers.Access;
                StripTrailingNulls = false;
                requiredDatabaseName = false;
                ClassFactory = new Access.ClassFactory();
                break;

            case dbDriver.MySql:

                ConnectUsingOleDb(Driver, ConnectionString);
                DriverString = MyMetaDrivers.MySql;
                StripTrailingNulls = true;
                requiredDatabaseName = true;
                ClassFactory = new MySql.ClassFactory();
                break;

            case dbDriver.MySql2:

                try
                {
                    MySql5Databases.LoadAssembly();
                    var conn = MySql5Databases.CreateConnection(ConnectionString);
                    conn.Open();
                    _defaultDatabase = conn.Database;
                    conn.Close();
                    conn.Dispose();

                    DriverString = MyMetaDrivers.MySql2;
                    StripTrailingNulls = true;
                    requiredDatabaseName = true;
                    ClassFactory = new MySql5.ClassFactory();
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                break;

            case dbDriver.DB2:

                ConnectUsingOleDb(Driver, ConnectionString);
                DriverString = MyMetaDrivers.DB2;
                StripTrailingNulls = false;
                requiredDatabaseName = false;
                ClassFactory = new DB2.ClassFactory();
                break;

            case dbDriver.ISeries:

                ConnectUsingOleDb(Driver, ConnectionString);
                DriverString = MyMetaDrivers.ISeries;
                StripTrailingNulls = false;
                requiredDatabaseName = false;
                ClassFactory = new ISeries.ClassFactory();
                break;

            case dbDriver.Pervasive:

                ConnectUsingOleDb(Driver, ConnectionString);
                DriverString = MyMetaDrivers.Pervasive;
                StripTrailingNulls = false;
                requiredDatabaseName = false;
                ClassFactory = new Pervasive.ClassFactory();
                break;

            case dbDriver.PostgreSQL:
            {
                using var cn = new NpgsqlConnection(ConnectionString);
                cn.Open();
                _defaultDatabase = cn.Database;
                cn.Close();

                DriverString = MyMetaDrivers.PostgreSQL;
                StripTrailingNulls = false;
                requiredDatabaseName = false;
                ClassFactory = new PostgreSQL.ClassFactory();
                break;
            }
            case dbDriver.PostgreSQL8:
            {
                using var cn8 = new NpgsqlConnection(ConnectionString);
                cn8.Open();
                _defaultDatabase = cn8.Database;
                cn8.Close();

                DriverString = MyMetaDrivers.PostgreSQL8;
                StripTrailingNulls = false;
                requiredDatabaseName = false;
                ClassFactory = new PostgreSQL8.ClassFactory();
                break;
            }
            case dbDriver.Firebird:
            {
                using var cn1 = new FbConnection(ConnectionString);
                cn1.Open();
                dbName = cn1.Database;
                cn1.Close();

                try
                {
                    index = dbName.LastIndexOfAny(['\\']);
                    if (index >= 0) _defaultDatabase = dbName[(index + 1)..];
                }
                catch
                {
                }

                DriverString = MyMetaDrivers.Firebird;
                StripTrailingNulls = false;
                requiredDatabaseName = false;
                ClassFactory = new Firebird.ClassFactory();
                break;
            }
            case dbDriver.Interbase:
            {
                using var cn2 = new FbConnection(ConnectionString);
                cn2.Open();
                _defaultDatabase = cn2.Database;
                cn2.Close();

                DriverString = MyMetaDrivers.Interbase;
                StripTrailingNulls = false;
                requiredDatabaseName = false;
                ClassFactory = new Firebird.ClassFactory();
                break;
            }
            case dbDriver.SQLite:
            {
                using var sqliteConn = new SQLiteConnection(ConnectionString);
                sqliteConn.Open();
                dbName = sqliteConn.Database;
                sqliteConn.Close();
                DriverString = MyMetaDrivers.SQLite;
                StripTrailingNulls = false;
                requiredDatabaseName = false;
                ClassFactory = new SQLite.ClassFactory();
                break;
            }
#if !IGNORE_VISTA
            case dbDriver.VistaDB:

                try
                {
                    var mh = new MetaHelper();
                    dbName = mh.LoadDatabases(ConnectionString);

                    if (dbName == "") return false;

                    _defaultDatabase = dbName;

                    DriverString = MyMetaDrivers.VistaDB;
                    StripTrailingNulls = false;
                    requiredDatabaseName = false;
                    ClassFactory = new VistaDB.ClassFactory();
                }
                catch
                {
                    throw new Exception("Invalid VistaDB connection or VistaDB not installed");
                }

                break;
#endif
            case dbDriver.Advantage:

                ConnectUsingOleDb(Driver, ConnectionString);
                DriverString = MyMetaDrivers.Advantage;
                StripTrailingNulls = false;
                requiredDatabaseName = false;
                ClassFactory = new Advantage.ClassFactory();
                var s = _defaultDatabase.Split('.');
                _defaultDatabase = s[0];
                break;

            case dbDriver.Plugin:

                IMyMetaPlugin plugin;
                var connection = GetConnectionFromPlugin(pluginName, ConnectionString, out plugin);
                if (connection is not null)
                    connection.Open();
                dbName = connection.Database;
                if (connection is not null)
                    connection.Close();
                DriverString = pluginName;
                //this.StripTrailingNulls = plugin.StripTrailingNulls;
                //this.requiredDatabaseName = plugin.RequiredDatabaseName;
                ClassFactory = new Plugin.ClassFactory(plugin);
                break;

            case dbDriver.None:

                DriverString = MyMetaDrivers.None;
                break;
        }

        IsConnected = true;
        return true;
    }

    private void ConnectUsingOleDb(dbDriver driver, string connectionString)
    {
        try
        {
            using OleDbConnection cn = new(connectionString.Replace("\"", ""));
            cn.Open();
            _defaultDatabase = GetDefaultDatabase(cn, driver);
            cn.Close();
        }
        catch (OleDbException)
        {
            throw;
        }
    }


    internal OleDbConnection TheConnection
    {
        get
        {
            if (_theConnection.State != ConnectionState.Open)
            {
                _theConnection.ConnectionString = ConnectionString;
                _theConnection.Open();
            }

            return _theConnection;
        }
    }

    private static string GetDefaultDatabase(OleDbConnection cn, dbDriver driver)
    {
        var databaseName = string.Empty;

        switch (driver)
        {
            case dbDriver.Access:

                var i = cn.DataSource.LastIndexOf(@"\");

                databaseName = i == -1 ? cn.DataSource : cn.DataSource[++i..];

                break;

            default:

                databaseName = cn.Database;
                break;
        }

        return databaseName;
    }

    /// <summary>
    ///     True if MyMeta has been successfully connected to your DBMS, False if not.
    /// </summary>
    public bool IsConnected { get; private set; }

    /// <summary>
    ///     Returns MyMeta's current dbDriver enumeration value as defined by its current connection.
    /// </summary>
    public dbDriver Driver { get; private set; } = dbDriver.None;

    /// <summary>
    ///     Returns MyMeta's current DriverString as defined by its current connection.
    /// </summary>
    /// <remarks>
    ///     These are the current possible values.
    ///     <list type="table">
    ///         <item>
    ///             <term>"ACCESS"</term><description>Microsoft Access 97 and higher</description>
    ///         </item>
    ///         <item>
    ///             <term>"DB2"</term><description>IBM DB2</description>
    ///         </item>
    ///         <item>
    ///             <term>"MYSQL"</term>
    ///             <description>Currently limited to only MySQL running on Microsoft Operating Systems</description>
    ///         </item>
    ///         <item>
    ///             <term>"ORACLE"</term><description>Oracle 8i - 9</description>
    ///         </item>
    ///         <item>
    ///             <term>"SQL"</term><description>Microsoft SQL Server 2000 and higher</description>
    ///         </item>
    ///         <item>
    ///             <term>"PostgreSQL"</term><description>PostgreSQL</description>
    ///         </item>
    ///         ///
    ///     </list>
    /// </remarks>
    public string DriverString { get; private set; } = "NONE";

    /// <summary>
    ///     Returns the current connection string. ** WARNING ** Currently the password is returned, the password will be
    ///     stripped from this
    ///     property in the very near future.
    /// </summary>
    public string ConnectionString { get; private set; } = "";

    internal Dictionary<string, string> ParsedConnectionString
    {
        get
        {
            //Provider=SQLOLEDB.1;Persist Security Info=False;User ID=sa;Initial Catalog=Northwind;Data Source=localhost
            if (null != _parsedConnectionString) return _parsedConnectionString;

            var first = ConnectionString.Split([';']);

            _parsedConnectionString = new Dictionary<string ,string>(first.GetUpperBound(0));

            for (var i = 0; i < first.GetUpperBound(0); i++)
            {
                var kv = first[i].Split(['=']);

                _parsedConnectionString.Add(kv[0].ToUpper(), 1 == kv.GetUpperBound(0) ? kv[1] : string.Empty);
            }

            return _parsedConnectionString;
        }
    }

    #region Settings

    /// <summary>
    ///     Determines whether system tables and views and alike are shown, the default is False. If True, ONLY system data is
    ///     shown.
    /// </summary>
    public bool ShowSystemData { get; set; }

    /// <summary>
    ///     If this is true then four IColumn properties are actually supplied by the Domain, if the Column has an IDomain.
    ///     The four properties are DataTypeName, DataTypeNameComplete, LanguageType, and DbTargetType.
    /// </summary>
    public bool DomainOverride { get; set; } = true;

    #endregion

    #region Plugin Members

    /// <summary>
    ///     A Plugin ConnectionString is a special feature for external assemblies.
    /// </summary>
    /// <param name="connectionString">
    ///     Sample: PluginName;Provider=SQLOLEDB.1;Persist Security Info=True;User ID=sa;Data
    ///     Source=localhost
    /// </param>
    /// <returns></returns>
    private static IDbConnection GetConnectionFromPlugin(string providerName, string pluginConnectionString)
    {
        IMyMetaPlugin plugin;

        return GetConnectionFromPlugin(providerName, pluginConnectionString, out plugin);
    }

    /// <summary>
    ///     A Plugin ConnectionString is a special feature for external assemblies.
    /// </summary>
    /// <param name="pluginConnectionString">
    ///     Sample: PluginName;Provider=SQLOLEDB.1;Persist Security Info=True;User ID=sa;Data
    ///     Source=localhost
    /// </param>
    /// <param name="plugin">Returns the plugin object.</param>
    /// <returns></returns>
    private static IDbConnection GetConnectionFromPlugin(string providerName, string pluginConnectionString, out IMyMetaPlugin plugin)
    {
        var pluginContext = new MyMetaPluginContext(providerName, pluginConnectionString);

        if (!Plugins.ContainsKey(providerName)) throw new Exception($"MyMeta Plugin \"{providerName}\" not registered.");

        plugin = Plugins[providerName] as IMyMetaPlugin;
        plugin.Initialize(pluginContext);

        return plugin.NewConnection;
    }

    private static Hashtable plugins;

    public static Hashtable Plugins
    {
        get
        {
            if (plugins is not null) return plugins;

            plugins = new Hashtable();
            var info = new FileInfo(Assembly.GetCallingAssembly().Location);
            if (info.Exists)
            {
                var fileNames = new StringBuilder();
                Exception err = null;

#if PLUGINS_FROM_SUBDIRS
                        // k3b allow plugins to be in its own directory
                        foreach (FileInfo dllFile in info.Directory.GetFiles("MyMeta.Plugins.*.dll",SearchOption.AllDirectories))
#else
                foreach (var dllFile in info.Directory.GetFiles("MyMeta.Plugins.*.dll"))
#endif
                    try
                    {
                        loadPlugin(dllFile.FullName, plugins);
                    }
                    catch (Exception ex)
                    {
                        // Fix K3b 2007-06-27 if the current plugin cannot be loaded ignore it.
                        //			i got the exception when loading a plugin that was linked against an old Interface
                        //			the chatch ensures that the rest of the application-initialisation continues ...
                        fileNames.AppendLine(dllFile.FullName);
                        err = ex;
                    }

                //TODO How to tell the caller that something is not ok. A Exception would result in a incomplete initialisation
//                        if (err is not null)
//                        	throw new ApplicationException("Cannot load Plugin(s) " + fileNames.ToString(), err);
            }

            return plugins;
        }
    }

    private static void loadPlugin(string filename, Hashtable plugins)
    {
#if PLUGINS_FROM_SUBDIRS
            if (System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) != System.IO.Path.GetDirectoryName(filename))
                AppDomain.CurrentDomain.AppendPrivatePath(System.IO.Path.GetDirectoryName(filename));
#endif

        var assembly = Assembly.LoadFile(filename);

        foreach (var type in assembly.GetTypes())
        {
            var interfaces = type.GetInterfaces();
            foreach (var iface in interfaces)
            {
                if (iface == typeof(IMyMetaPlugin))
                {
                    try
                    {
                        var constructors = type.GetConstructors();
                        var constructor = constructors[0];

                        var plugin = constructor.Invoke(BindingFlags.CreateInstance, null, [], null) as IMyMetaPlugin;
                        InternalDriver.Register(plugin.ProviderUniqueKey,
                            new PluginDriver(plugin));

                        plugins[plugin.ProviderUniqueKey] = plugin; // after register because if exception in register, donot remeber plugin
                    }
                    catch (Exception ex)
                    {
                        Trace.WriteLine("Cannot load plugin " + filename);
                        while (ex is not null)
                        {
                            Trace.WriteLine(ex.Message);
                            Trace.WriteLine(ex.StackTrace);
                            ex = ex.InnerException;
                        }
                    }
                }
            }
        }
    }
    
    #endregion

    #region XML User Data

    public const string UserDataXPath = @"//MyMeta";

    internal bool GetXmlNode(out XmlNode node, bool forceCreate)
    {
        node = null;
        var success = false;

        if (null == _xmlNode)
        {
            if (!UserData.HasChildNodes)
            {
                _xmlNode = UserData.CreateNode(XmlNodeType.Element, "MyMeta", null);
                UserData.AppendChild(_xmlNode);
            }
            else
            {
                _xmlNode = UserData.SelectSingleNode("./MyMeta");
            }
        }

        if (null != _xmlNode)
        {
            node = _xmlNode;
            success = true;
        }

        return success;
    }

    /// <summary>
    ///     The full path of the XML file that contains the user defined meta data. See IPropertyCollection
    /// </summary>
    public string UserMetaDataFileName
    {
        get => _userMetaDataFileName;
        set
        {
            _userMetaDataFileName = value;

            try
            {
                UserData = new XmlDocument();
                UserData.Load(_userMetaDataFileName);
            }
            catch
            {
                UserData = new XmlDocument();
            }
        }
    }

    /// <summary>
    ///     Call this method to save any user defined meta data that you may have modified. See
    ///     <see cref="UserMetaDataFileName" />
    /// </summary>
    /// <returns>True if saved, False if not</returns>
    public bool SaveUserMetaData()
    {
        if (UserData != null && !string.IsNullOrWhiteSpace(_userMetaDataFileName))
        {
            var f = new FileInfo(_userMetaDataFileName);
            if (!f.Exists && !f.Directory.Exists)
            {
                f.Directory.Create();
            }

            UserData.Save(_userMetaDataFileName);
            return true;
        }

        return false;
    }

    private string _userMetaDataFileName = "";

    #endregion

    #region XML Language Mapping

    /// <summary>
    ///     The full path of the XML file that contains the language mappings. The data in this file plus the value you provide
    ///     to <see cref="Language" /> determine the value of IColumn.Language.
    /// </summary>
    public string LanguageMappingFileName
    {
        get => _languageMappingFileName;
        set
        {
            try
            {
                _languageMappingFileName = value;

                _languageDoc = new XmlDocument();
                _languageDoc.Load(_languageMappingFileName);
                _language = string.Empty;
                ;
                LanguageNode = null;
            }
            catch
            {
            }
        }
    }

    /// <summary>
    ///     Returns all of the languages currently configured for the DBMS set when Connect was called.
    /// </summary>
    /// <returns>An array with all of the possible languages.</returns>
    public string[] GetLanguageMappings()
    {
        return GetLanguageMappings(DriverString);
    }

    /// <summary>
    ///     Returns all of the languages for a given driver, regardless of MyMeta's current connection
    /// </summary>
    /// <returns>An array with all of the possible languages.</returns>
    public string[] GetLanguageMappings(string driverString)
    {
        string[] mappings = null;

        if (null != _languageDoc && driverString is not null)
        {
            driverString = driverString.ToUpper();
            var xPath = @"//Languages/Language[@From='" + driverString + "']";
            var nodes = _languageDoc.SelectNodes(xPath, null);

            if (null != nodes && nodes.Count > 0)
            {
                var nodeCount = nodes.Count;
                mappings = new string[nodeCount];

                for (var i = 0; i < nodeCount; i++) mappings[i] = nodes[i].Attributes["To"].Value;
            }
        }

        return mappings;
    }

    /// <summary>
    ///     Use this to choose your Language, for example, "C#". See <see cref="LanguageMappingFileName" /> for more
    ///     information
    /// </summary>
    public string Language
    {
        get => _language;

        set
        {
            if (null != _languageDoc)
            {
                _language = value;
                var xPath = @"//Languages/Language[@From='" + DriverString + "' and @To='" + _language + "']";
                LanguageNode = _languageDoc.SelectSingleNode(xPath, null);
            }
        }
    }

    private string _languageMappingFileName = string.Empty;
    private string _language = string.Empty;
    private XmlDocument _languageDoc;
    internal XmlNode LanguageNode;

    #endregion

    #region XML DbTarget Mapping

    /// <summary>
    ///     The full path of the XML file that contains the DbTarget mappings. The data in this file plus the value you provide
    ///     to <see cref="DbTarget" /> determine the value of IColumn.DbTarget.
    /// </summary>
    public string DbTargetMappingFileName
    {
        get => _dbTargetMappingFileName;
        set
        {
            try
            {
                _dbTargetMappingFileName = value;

                _dbTargetDoc = new XmlDocument();
                _dbTargetDoc.Load(_dbTargetMappingFileName);
                _dbTarget = string.Empty;
                ;
                DbTargetNode = null;
            }
            catch
            {
            }
        }
    }

    /// <summary>
    ///     Returns all of the dbTargets currently configured for the DBMS set when Connect was called.
    /// </summary>
    /// <returns>An array with all of the possible dbTargets.</returns>
    public string[] GetDbTargetMappings()
    {
        return GetDbTargetMappings(DriverString);
    }

    /// <summary>
    ///     Returns all of the dbTargets for a given driver, regardless of MyMeta's current connection
    /// </summary>
    /// <returns>An array with all of the possible dbTargets.</returns>
    public string[] GetDbTargetMappings(string driverString)
    {
        string[] mappings = null;

        if (null != _dbTargetDoc && driverString is not null)
        {
            driverString = driverString.ToUpper();
            var xPath = @"//DbTargets/DbTarget[@From='" + driverString + "']";
            var nodes = _dbTargetDoc.SelectNodes(xPath, null);

            if (null != nodes && nodes.Count > 0)
            {
                var nodeCount = nodes.Count;
                mappings = new string[nodeCount];

                for (var i = 0; i < nodeCount; i++) mappings[i] = nodes[i].Attributes["To"].Value;
            }
        }

        return mappings;
    }

    /// <summary>
    ///     Use this to choose your DbTarget, for example, "SqlClient". See <see cref="DbTargetMappingFileName" />  for more
    ///     information
    /// </summary>
    public string DbTarget
    {
        get => _dbTarget;

        set
        {
            if (null != _dbTargetDoc)
            {
                _dbTarget = value;
                var xPath = @"//DbTargets/DbTarget[@From='" + DriverString + "' and @To='" + _dbTarget + "']";
                DbTargetNode = _dbTargetDoc.SelectSingleNode(xPath, null);
            }
        }
    }

    private string _dbTargetMappingFileName = string.Empty;
    private string _dbTarget = string.Empty;
    private XmlDocument _dbTargetDoc;
    internal XmlNode DbTargetNode;

    #endregion

    internal XmlDocument UserData = new();

    internal bool IgnoreCase = true;
    internal bool requiredDatabaseName;
    internal bool requiresSchemaName;
    internal bool StripTrailingNulls;

    internal string TrailingNull;

    internal IClassFactory ClassFactory;

    private string _defaultDatabase = string.Empty;
    private Databases _databases;
    private ProviderTypes _providerTypes;
    private Dictionary<string ,string> _parsedConnectionString;

    private XmlNode _xmlNode;

    private OleDbConnection _theConnection = new();

    /// <inheritdoc />
    public void Dispose()
    {
        _theConnection?.Dispose();
        GC.SuppressFinalize(this);
    }

    /// <inheritdoc />
    public async ValueTask DisposeAsync()
    {
        if (_theConnection is not null) await _theConnection.DisposeAsync();
        GC.SuppressFinalize(this);
    }
}