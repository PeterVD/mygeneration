using System;
using System.Runtime.InteropServices;

namespace MyMeta;

[GuidAttribute("8afcc8f1-756b-4ab1-a81c-5f921cc88e4f")]
[InterfaceType(ComInterfaceType.InterfaceIsDual)]
public interface IProviderType
{
    string Type { get; }
    int DataType { get; }
    int ColumnSize { get; }
    string LiteralPrefix { get; }
    string LiteralSuffix { get; }
    string CreateParams { get; }
    bool IsNullable { get; }
    bool IsCaseSensitive { get; }
    string Searchable { get; } // convert
    bool IsUnsigned { get; }
    bool HasFixedPrecScale { get; }
    bool CanBeAutoIncrement { get; }
    string LocalType { get; }
    int MinimumScale { get; }
    int MaximumScale { get; }
    Guid TypeGuid { get; }
    string TypeLib { get; }
    string Version { get; }
    bool IsLong { get; }
    bool BestMatch { get; }
    bool IsFixedLength { get; }
}