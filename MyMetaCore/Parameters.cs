using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

namespace MyMeta;

public class Parameters : Collection<IParameter>, IParameters, IEnumerable, ICollection
{
    internal DataColumn f_Catalog;
    internal DataColumn f_CharMaxLength;
    internal DataColumn f_CharOctetLength;
    internal DataColumn f_DataType;
    internal DataColumn f_Default;
    internal DataColumn f_Description;
    internal DataColumn f_FullTypeName;
    internal DataColumn f_HasDefault;
    internal DataColumn f_IsNullable;
    internal DataColumn f_LocalTypeName;
    internal DataColumn f_NumericPrecision;
    internal DataColumn f_NumericScale;
    internal DataColumn f_Ordinal;
    internal DataColumn f_ParameterName;
    internal DataColumn f_ProcedureName;
    internal DataColumn f_Schema;
    internal DataColumn f_Type;
    internal DataColumn f_TypeName;

    internal Procedure Procedure;

    #region IList Members

    object IList.this[int index]
    {
        get => this[index];
        set { }
    }

    #endregion


    private void BindToColumns(DataTable metaData)
    {
        if (false == _fieldsBound)
        {
            if (metaData.Columns.Contains("PROCEDURE_CATALOG")) f_Catalog = metaData.Columns["PROCEDURE_CATALOG"];
            if (metaData.Columns.Contains("PROCEDURE_SCHEMA")) f_Schema = metaData.Columns["PROCEDURE_SCHEMA"];
            if (metaData.Columns.Contains("PROCEDURE_NAME")) f_ProcedureName = metaData.Columns["PROCEDURE_NAME"];
            if (metaData.Columns.Contains("PARAMETER_NAME")) f_ParameterName = metaData.Columns["PARAMETER_NAME"];
            if (metaData.Columns.Contains("ORDINAL_POSITION")) f_Ordinal = metaData.Columns["ORDINAL_POSITION"];
            if (metaData.Columns.Contains("PARAMETER_TYPE")) f_Type = metaData.Columns["PARAMETER_TYPE"];
            if (metaData.Columns.Contains("PARAMETER_HASDEFAULT")) f_HasDefault = metaData.Columns["PARAMETER_HASDEFAULT"];
            if (metaData.Columns.Contains("PARAMETER_DEFAULT")) f_Default = metaData.Columns["PARAMETER_DEFAULT"];
            if (metaData.Columns.Contains("IS_NULLABLE")) f_IsNullable = metaData.Columns["IS_NULLABLE"];
            if (metaData.Columns.Contains("DATA_TYPE")) f_DataType = metaData.Columns["DATA_TYPE"];
            if (metaData.Columns.Contains("CHARACTER_MAXIMUM_LENGTH")) f_CharMaxLength = metaData.Columns["CHARACTER_MAXIMUM_LENGTH"];
            if (metaData.Columns.Contains("CHARACTER_OCTET_LENGTH")) f_CharOctetLength = metaData.Columns["CHARACTER_OCTET_LENGTH"];
            if (metaData.Columns.Contains("NUMERIC_PRECISION")) f_NumericPrecision = metaData.Columns["NUMERIC_PRECISION"];
            if (metaData.Columns.Contains("NUMERIC_SCALE")) f_NumericScale = metaData.Columns["NUMERIC_SCALE"];
            if (metaData.Columns.Contains("DESCRIPTION")) f_Description = metaData.Columns["DESCRIPTION"];
            if (metaData.Columns.Contains("FULL_TYPE_NAME")) f_FullTypeName = metaData.Columns["FULL_TYPE_NAME"];
            if (metaData.Columns.Contains("TYPE_NAME")) f_TypeName = metaData.Columns["TYPE_NAME"];
            if (metaData.Columns.Contains("LOCAL_TYPE_NAME")) f_LocalTypeName = metaData.Columns["LOCAL_TYPE_NAME"];
        }
    }

    internal virtual void LoadAll()
    {
    }

    internal void PopulateArray(DataTable metaData)
    {
        BindToColumns(metaData);

        var count = metaData.Rows.Count;
        for (var i = 0; i < count; i++)
        {
            var param = (Parameter)DbRoot.ClassFactory.CreateParameter();
            param.DbRoot = DbRoot;
            param.Parameters = this;
            param.Row = metaData.Rows[i];
            _array.Add(param);
        }
    }

    internal void AddTable(Parameter param)
    {
        _array.Add(param);
    }

    #region indexers

    public virtual IParameter this[object index]
    {
        get
        {
            if (index.GetType() == Types.StringType) return GetByPhysicalName(index as string);

            var idx = Convert.ToInt32(index);
            return _array[idx] as Parameter;
        }
    }

    public Parameter GetByName(string name)
    {
        Parameter obj = null;

        var count = _array.Count;
        for (var i = 0; i < count; i++)
        {
            var tmp = _array[i] as Parameter;

            if (CompareStrings(name, tmp.Name))
            {
                obj = tmp;
                break;
            }
        }

        return obj;
    }

    public Parameter GetByPhysicalName(string name)
    {
        Parameter obj = null;

        var count = _array.Count;
        for (var i = 0; i < count; i++)
        {
            var tmp = _array[i] as Parameter;

            if (CompareStrings(name, tmp.Name))
            {
                obj = tmp;
                break;
            }
        }

        return obj;
    }

    #endregion

    #region IEnumerable Members

    IEnumerator<IParameter> IEnumerable<IParameter>.GetEnumerator()
    {
        return _array.GetEnumerator();
    }

    public IEnumerator GetEnumerator()
    {
        return _array.GetEnumerator();
    }

    #endregion

    #region XML User Data

    public override string UserDataXPath => $@"{Procedure.UserDataXPath}/Parameters";

    internal override bool GetXmlNode(out XmlNode node, bool forceCreate)
    {
        const string xPath = @"./Parameters";

        node = null;
        var success = false;

        // Get the parent node and see if our user data already exists
        if (null == _xmlNode && Procedure.GetXmlNode(out var parentNode, forceCreate) && !GetUserData(xPath, parentNode, out _xmlNode) && forceCreate)
        {
            // Create it, and try again
            CreateUserMetaData(parentNode);
            GetUserData(xPath, parentNode, out _xmlNode);
        }

        if (null != _xmlNode)
        {
            node = _xmlNode;
            success = true;
        }

        return success;
    }

    public override void CreateUserMetaData(XmlNode parentNode)
    {
        var myNode = parentNode.OwnerDocument.CreateNode(XmlNodeType.Element, "Parameters", null);
        parentNode.AppendChild(myNode);
    }

    #endregion
}