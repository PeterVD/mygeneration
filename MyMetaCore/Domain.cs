using System.Runtime.InteropServices;
using System.Xml;

namespace MyMeta;

public class Domain : Single, IDomain, INameValueItem
{
    internal Domains Domains;

    #region Properties

    public override string Alias
    {
        get {
            return GetXmlNode(out var node, false) && GetUserData(node, "n", out var niceName) && !string.IsNullOrWhiteSpace(niceName)
                ? niceName
                : Name;
        }

        set
        {
            if (GetXmlNode(out var node, true)) SetUserData(node, "n", value);
        }
    }

    public override string Name => GetString(Domains.f_DomainName);

    public virtual bool HasDefault => Default.Length > 0;

    public virtual string Default => GetString(Domains.f_Default);

    public virtual bool IsNullable => GetBool(Domains.f_IsNullable);

    public virtual int CharacterMaxLength => GetInt32(Domains.f_MaxLength);

    public virtual int CharacterOctetLength => GetInt32(Domains.f_OctetLength);

    public virtual int NumericPrecision => GetInt32(Domains.f_NumericPrecision);

    public virtual int NumericScale => GetInt32(Domains.f_NumericScale);

    public virtual int DateTimePrecision => GetInt32(Domains.f_DatetimePrecision);

    public virtual string CharacterSetCatalog => GetString(Domains.f_CharSetCatalog);

    public virtual string CharacterSetSchema => GetString(Domains.f_CharSetSchema);

    public virtual string CharacterSetName => GetString(Domains.f_CharSetName);

    public virtual string DomainCatalog => GetString(Domains.f_DomainCatalog);

    public virtual string DomainSchema => GetString(Domains.f_DomainSchema);

    public virtual string DomainName => GetString(Domains.f_DomainName);

    public virtual string DataTypeName => GetString(Domains.f_DataType);

    public virtual string LanguageType
    {
        get
        {
            if (DbRoot.LanguageNode is null) return "Unknown";

            var xPath = $@"./Type[@From='{DataTypeName}']";

            var node = DbRoot.LanguageNode.SelectSingleNode(xPath, null);

            return node is not null && GetUserData(node, "To", out var languageType) ? languageType : "Unknown";
        }
    }

    public virtual string DbTargetType
    {
        get
        {
            if (DbRoot.DbTargetNode is null) return "Unknown";

            var xPath = $@"./Type[@From='{DataTypeName}']";
            var node = DbRoot.DbTargetNode.SelectSingleNode(xPath, null);

            return node is not null && GetUserData(node, "To", out var driverType) ? driverType : "Unknown";
        }
    }

    public virtual string DataTypeNameComplete => "Unknown";

    #endregion

    #region Collections

    public virtual IPropertyCollection GlobalProperties
    {
        get
        {
            var db = Domains.Database;
            if (db._domainProperties is not null) return db._domainProperties;

            db._domainProperties = new PropertyCollection
            {
                Parent = this
            };

            var xPath = GlobalUserDataXPath;
            var xmlNode = DbRoot.UserData.SelectSingleNode(xPath, null);

            if (xmlNode is null)
            {
                var parentNode = db.CreateGlobalXmlNode();

                xmlNode = parentNode.OwnerDocument.CreateNode(XmlNodeType.Element, "Domain", null);
                parentNode.AppendChild(xmlNode);
            }

            db._domainProperties.LoadAllGlobal(xmlNode);

            return db._domainProperties;
        }
    }

    public virtual IPropertyCollection AllProperties
    {
        get
        {
            if (_allProperties is not null) return _allProperties;

            _allProperties = new PropertyCollectionAll();
            _allProperties.Load(Properties, GlobalProperties);
            return _allProperties;
        }
    }

    internal PropertyCollectionAll _allProperties;

    #endregion

    #region XML User Data

    public override string UserDataXPath => Domains.UserDataXPath + @"/Domain[@p='" + Name + "']";

    public override string GlobalUserDataXPath => Domains.UserDataXPath + @"/Domain[@p='" + Name + "']";

    internal override bool GetXmlNode(out XmlNode node, bool forceCreate)
    {
        node = null;
        var success = false;

        if (null == _xmlNode)
        {
            // Get the parent node
            if (Domains.GetXmlNode(out var parentNode, forceCreate))
            {
                // See if our user data already exists
                var xPath = $@"./Domain[@p='{Name}']";
                if (!GetUserData(xPath, parentNode, out _xmlNode) && forceCreate)
                {
                    // Create it, and try again
                    CreateUserMetaData(parentNode);
                    GetUserData(xPath, parentNode, out _xmlNode);
                }
            }
        }

        if (null != _xmlNode)
        {
            node = _xmlNode;
            success = true;
        }

        return success;
    }

    public override void CreateUserMetaData(XmlNode parentNode)
    {
        var myNode = parentNode.OwnerDocument.CreateNode(XmlNodeType.Element, "Column", null);
        parentNode.AppendChild(myNode);

        var attr = parentNode.OwnerDocument.CreateAttribute("p");
        attr.Value = Name;
        myNode.Attributes.Append(attr);

        attr = parentNode.OwnerDocument.CreateAttribute("n");
        attr.Value = "";
        myNode.Attributes.Append(attr);
    }

    #endregion

    #region INameValueCollection Members

    public string ItemName => Name;

    public string ItemValue => Name;

    #endregion
}