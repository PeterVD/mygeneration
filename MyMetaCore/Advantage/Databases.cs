using System.Runtime.InteropServices;

namespace MyMeta.Advantage;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IDatabases))]
#endif
public class AdvantageDatabases : Databases
{
    internal override void LoadAll()
    {
        var metaData = LoadData(OleDbSchemaGuid.Catalogs, null);

        PopulateArray(metaData);
    }
}