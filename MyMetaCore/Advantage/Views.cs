using System.Data;
using System.Runtime.InteropServices;

namespace MyMeta.Advantage;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IViews))]
#endif
public class AdvantageViews : Views
{
    internal override void LoadAll()
    {
        try
        {
            var type = DbRoot.ShowSystemData ? "SYSTEM VIEW" : "VIEW";
            var metaData = LoadData(OleDbSchemaGuid.Views, new object[] { null });

            PopulateArray(metaData);

            LoadDescriptions();
        }
        catch
        {
        }
    }

    private void LoadDescriptions()
    {
        try
        {
            var select = @"SELECT objName, value FROM ::fn_listextendedproperty ('MS_Description', 'user', 'dbo', 'view', null, null, null)";

            OleDbConnection cn = new OleDbConnection(DbRoot.ConnectionString);
            cn.Open();
            cn.ChangeDatabase("[" + Database.Name + "]");

            OleDbDataAdapter adapter = new OleDbDataAdapter(select, cn);
            var dataTable = new DataTable();

            adapter.Fill(dataTable);

            cn.Close();

            View v;

            foreach (DataRow row in dataTable.Rows)
            {
                v = this[row["objName"] as string] as View;

                if (null != v) v._row["DESCRIPTION"] = row["value"] as string;
            }
        }
        catch
        {
        }
    }
}