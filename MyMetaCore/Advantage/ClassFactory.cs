using System;
using System.Data;
using System.Runtime.InteropServices;

namespace MyMeta.Advantage;

#if ENTERPRISE

[ComVisible(false)]
#endif
public class ClassFactory : IClassFactory
{
    public ITables CreateTables()
    {
        return new AdvantageTables();
    }

    public ITable CreateTable()
    {
        return new AdvantageTable();
    }

    public IColumn CreateColumn()
    {
        return new AdvantageColumn();
    }

    public IColumns CreateColumns()
    {
        return new AdvantageColumns();
    }

    public IDatabase CreateDatabase()
    {
        return new AdvantageDatabase();
    }

    public IDatabases CreateDatabases()
    {
        return new AdvantageDatabases();
    }

    public IProcedure CreateProcedure()
    {
        return new AdvantageProcedure();
    }

    public IProcedures CreateProcedures()
    {
        return new AdvantageProcedures();
    }

    public IView CreateView()
    {
        return new AdvantageView();
    }

    public IViews CreateViews()
    {
        return new AdvantageViews();
    }

    public IParameter CreateParameter()
    {
        return new AdvantageParameter();
    }

    public IParameters CreateParameters()
    {
        return new AdvantageParameters();
    }

    public IForeignKey CreateForeignKey()
    {
        return new AdvantageForeignKey();
    }

    public IForeignKeys CreateForeignKeys()
    {
        return new AdvantageForeignKeys();
    }

    public IIndex CreateIndex()
    {
        return new AdvantageIndex();
    }

    public IIndexes CreateIndexes()
    {
        return new AdvantageIndexes();
    }

    public IResultColumn CreateResultColumn()
    {
        return new AdvantageResultColumn();
    }

    public IResultColumns CreateResultColumns()
    {
        return new AdvantageResultColumns();
    }

    public IDomain CreateDomain()
    {
        return new AdvantageDomain();
    }

    public IDomains CreateDomains()
    {
        return new AdvantageDomains();
    }

    public IProviderType CreateProviderType()
    {
        return new ProviderType();
    }

    public IProviderTypes CreateProviderTypes()
    {
        return new ProviderTypes();
    }

    public IDbConnection CreateConnection()
    {
        return null;
    }

    public static void Register()
    {
        InternalDriver.Register("ADVANTAGE",
            new MyInternalDriver
            (typeof(ClassFactory)
                , @"Provider=Advantage.OLEDB.1;Password="";User ID=AdsSys;Data Source=C:\Program Files\Extended Systems\Advantage\Help\examples\aep_tutorial\task1;Initial Catalog=aep_tutorial.add;Persist Security Info=True;Advantage Server Type=ADS_LOCAL_SERVER;Trim Trailing Spaces=TRUE"
                , true));
    }

    internal class MyInternalDriver : InternalDriver
    {
        internal MyInternalDriver(Type factory, string connString, bool isOleDB)
            : base(factory, connString, isOleDB)
        {
        }

        public override string GetDataBaseName(IDbConnection con)
        {
            var result = GetDataBaseName(con).Split('.')[0];
            return result;
        }
    }
}