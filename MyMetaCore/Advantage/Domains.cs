using System.Data;
using System.Runtime.InteropServices;

namespace MyMeta.Advantage;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IDomains))]
#endif
public class AdvantageDomains : Domains
{
    internal override void LoadAll()
    {
        try
        {
            var select = "SELECT * FROM INFORMATION_SCHEMA.DOMAINS";

            OleDbConnection cn = new OleDbConnection(DbRoot.ConnectionString);
            cn.Open();
            cn.ChangeDatabase("[" + Database.Name + "]");

            OleDbDataAdapter adapter = new OleDbDataAdapter(select, cn);
            var metaData = new DataTable();

            adapter.Fill(metaData);
            cn.Close();

            PopulateArray(metaData);
        }
        catch
        {
        }
    }
}