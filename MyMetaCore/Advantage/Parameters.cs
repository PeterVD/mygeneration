using System.Runtime.InteropServices;

namespace MyMeta.Advantage;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IParameters))]
#endif
public class AdvantageParameters : Parameters
{
    internal override void LoadAll()
    {
        try
        {
            var metaData = LoadData(OleDbSchemaGuid.Procedure_Parameters,
                new object[] { null, null, Procedure.Name });

            PopulateArray(metaData);
        }
        catch
        {
        }
    }
}