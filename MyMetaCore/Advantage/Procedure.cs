using System.Runtime.InteropServices;

namespace MyMeta.Advantage;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IProcedure))]
#endif
public class AdvantageProcedure : Procedure
{
    public override string Alias
    {
        get
        {
            var name = base.Name.Split(';');

            return name[0];
        }
    }

    public override string Name
    {
        get
        {
            var name = base.Name.Split(';');

            return name[0];
        }
    }
}