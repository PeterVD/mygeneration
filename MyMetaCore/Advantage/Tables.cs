using System.Data;
using System.Runtime.InteropServices;

namespace MyMeta.Advantage;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(ITables))]
#endif
public class AdvantageTables : Tables
{
    internal override void LoadAll()
    {
        try
        {
            var type = DbRoot.ShowSystemData ? "SYSTEM TABLE" : "TABLE";
            var metaData = LoadData(OleDbSchemaGuid.Tables, new object[] { null, null, null, type });

            PopulateArray(metaData);

            LoadDescriptions();
        }
        catch
        {
        }
    }

    private void LoadDescriptions()
    {
        try
        {
            var select = @"SELECT objName, value FROM ::fn_listextendedproperty ('MS_Description', 'user', 'dbo', 'table', null, null, null)";

            OleDbConnection cn = new OleDbConnection(DbRoot.ConnectionString);
            cn.Open();
            cn.ChangeDatabase("[" + Database.Name + "]");

            OleDbDataAdapter adapter = new OleDbDataAdapter(select, cn);
            var dataTable = new DataTable();

            adapter.Fill(dataTable);

            cn.Close();

            Table t;

            foreach (DataRow row in dataTable.Rows)
            {
                t = this[row["objName"] as string] as Table;

                if (null != t) t._row["DESCRIPTION"] = row["value"] as string;
            }
        }
        catch
        {
        }
    }
}