using System.Runtime.InteropServices;

namespace MyMeta.Advantage;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(ITable))]
#endif
public class AdvantageTable : Table
{
    public override IColumns PrimaryKeys
    {
        get
        {
            if (null == _primaryKeys)
            {
                var metaData = LoadData(OleDbSchemaGuid.Primary_Keys,
                    new object[] { null, null, Name });

                _primaryKeys = (Columns)DbRoot.ClassFactory.CreateColumns();
                _primaryKeys.Table = this;
                _primaryKeys.DbRoot = DbRoot;

                var data = "";

                var count = metaData.Rows.Count;
                if (count > 0)
                {
                    data = metaData.Rows[0]["COLUMN_NAME"] as string;
                    var pks = data.Split(';');
                    foreach (var colName in pks) _primaryKeys.AddColumn((Column)Columns[colName]);
                }
            }

            return _primaryKeys;
        }
    }
}