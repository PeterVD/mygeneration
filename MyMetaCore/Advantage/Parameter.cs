using System.Runtime.InteropServices;

namespace MyMeta.Advantage;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IParameter))]
#endif
public class AdvantageParameter : Parameter
{
    public override string DataTypeNameComplete
    {
        get
        {
            switch (TypeName)
            {
                case "binary":
                case "char":
                case "nchar":
                case "nvarchar":
                case "varchar":
                case "varbinary":

                    return TypeName + "(" + CharacterMaxLength + ")";

                case "decimal":
                case "numeric":

                    return TypeName + "(" + NumericPrecision + "," + NumericScale + ")";

                default:

                    return TypeName;
            }
        }
    }
}