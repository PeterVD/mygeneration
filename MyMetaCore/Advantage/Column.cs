using System.Runtime.InteropServices;

namespace MyMeta.Advantage;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IColumn))]
#endif
public class AdvantageColumn : Column
{
    public override bool IsAutoKey => DataTypeName == "AutoInc" ? true : false;

    public override bool IsComputed
    {
        get
        {
            if (DataTypeName == "timestamp") return true;

            return GetBool(Columns.f_IsComputed);
        }
    }


    public override string DataTypeName
    {
        get
        {
            if (DbRoot.DomainOverride)
                if (HasDomain)
                    if (Domain is not null)
                        return Domain.DataTypeName;

            var cols = Columns as AdvantageColumns;
            return GetString(cols.f_TypeName);
        }
    }

    public override string DataTypeNameComplete
    {
        get
        {
            if (DbRoot.DomainOverride)
                if (HasDomain)
                    if (Domain is not null)
                        return Domain.DataTypeNameComplete;

            return DataTypeName;
        }
    }

    internal override Column Clone()
    {
        var c = base.Clone();

        return c;
    }
}