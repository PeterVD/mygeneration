using System.Data;
using System.Runtime.InteropServices;

namespace MyMeta.Advantage;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IColumns))]
#endif
public class AdvantageColumns : Columns
{
    internal DataColumn f_AutoKey;
    internal DataColumn f_TypeName;

    internal override void LoadForTable()
    {
        var metaData = LoadData(OleDbSchemaGuid.Columns, new object[] { null, null, Table.Name });
        PopulateArray(metaData);
        LoadExtraData(Table.Name, "T");
    }

    internal override void LoadForView()
    {
        var metaData = LoadData(OleDbSchemaGuid.Columns, new object[] { null, null, View.Name });
        PopulateArray(metaData);
        LoadExtraData(View.Name, "V");
    }

    private void LoadExtraData(string name, string type)
    {
        try
        {
            var select = "SELECT Name, Field_Type FROM system.columns WHERE Parent = '" + name + "'";

            OleDbConnection cn = new OleDbConnection(DbRoot.ConnectionString);
            cn.Open();

            OleDbDataAdapter adapter = new OleDbDataAdapter(select, cn);
            var dataTable = new DataTable();

            adapter.Fill(dataTable);
            cn.Close();

            if (_array.Count > 0)
            {
                var col = _array[0] as Column;

                f_TypeName = new DataColumn("TYPE_NAME", typeof(string));
                col._row.Table.Columns.Add(f_TypeName);

                var typeName = "";
                var rows = dataTable.Rows;

                var count = _array.Count;
                Column c = null;

                for (var index = 0; index < count; index++)
                {
                    c = GetByPhysicalName((string)rows[index]["Name"]);

                    switch ((short)rows[index]["Field_Type"])
                    {
                        case 1: typeName = "Logical"; break;
                        case 2: typeName = "Numeric"; break;
                        case 3: typeName = "Date"; break;
                        case 4: typeName = "String"; break;
                        case 5: typeName = "Memo"; break;
                        case 6: typeName = "Binary"; break;
                        case 7: typeName = "Image"; break;
                        case 8: typeName = "Varchar"; break;
                        case 9: typeName = "Compactdate"; break;
                        case 10: typeName = "Double"; break;
                        case 11: typeName = "Integer"; break;
                        case 12: typeName = "ShortInt"; break;
                        case 13: typeName = "Time"; break;
                        case 14: typeName = "TimeStamp"; break;
                        case 15: typeName = "AutoInc"; break;
                        case 16: typeName = "Raw"; break;
                        case 17: typeName = "CurDouble"; break;
                        case 18: typeName = "Money"; break;
                        case 19: typeName = "LongLong"; break;
                        case 20: typeName = "CIString"; break;
                        default: typeName = "Uknown"; break;
                    }

                    c._row["TYPE_NAME"] = typeName;
                }
            }
        }
        catch
        {
        }
    }
}