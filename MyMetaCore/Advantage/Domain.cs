using System.Runtime.InteropServices;

namespace MyMeta.Advantage;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IDomain))]
#endif
public class AdvantageDomain : Domain
{
    public override string DataTypeNameComplete
    {
        get
        {
            switch (DataTypeName)
            {
                case "binary":
                case "char":
                case "nchar":
                case "nvarchar":
                case "varchar":
                case "varbinary":

                    return DataTypeName + "(" + CharacterMaxLength + ")";

                case "decimal":
                case "numeric":

                    return DataTypeName + "(" + NumericPrecision + "," + NumericScale + ")";

                default:

                    return DataTypeName;
            }
        }
    }
}