using System.Runtime.InteropServices;

namespace MyMeta.Advantage;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IDatabase))]
#endif
public class AdvantageDatabase : Database
{
}