using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

namespace MyMeta;

public class KeyValuePair : IProperty
{
    public KeyValuePair(string key, string val)
    {
        Key = key;
        Value = val;
    }

    public string Key { get; } = string.Empty;

    public string Value { get; set; } = string.Empty;

    public bool IsGlobal => true;
}

/// <summary>
///     Summary description for KeyValueCollection.
/// </summary>
public class KeyValueCollection : IPropertyCollection, IEnumerable, ICollection, IEnumerator
{
    private IEnumerator enumerator;
    private Hashtable hash = new();

    public IProperty this[string key] => hash[key] as IProperty;

    public IProperty AddKeyValue(string key, string value)
    {
        var pair = new KeyValuePair(key, value);
        hash[key] = pair;
        return pair;
    }

    public void RemoveKey(string key)
    {
        hash.Remove(key);
    }

    public bool ContainsKey(string key)
    {
        return hash.ContainsKey(key);
    }

    public void Clear()
    {
        hash.Clear();
    }

    public void Reset()
    {
        enumerator = null;
    }

    public object Current
    {
        get
        {
            enumerator ??= GetEnumerator();
            var entry = (IProperty)enumerator.Current;
            return entry;
        }
    }

    public bool MoveNext()
    {
        enumerator ??= GetEnumerator();
        return enumerator.MoveNext();
    }

    public bool IsSynchronized => hash.Values.IsSynchronized;

    public int Count => hash.Count;

    public void CopyTo(Array array, int index)
    {
        hash.Values.CopyTo(array, index);
    }

    public object SyncRoot => hash.Values.SyncRoot;

    IEnumerator<IProperty> IEnumerable<IProperty>.GetEnumerator()
    {
        return hash.Values.OfType<IProperty>().GetEnumerator();
    }

    public IEnumerator GetEnumerator()
    {
        return hash.Values.GetEnumerator();
    }
}