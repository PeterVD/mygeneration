using System.Runtime.InteropServices;
using System.Xml;

namespace MyMeta;

/// <summary>
/// Summary description for Collection.
/// </summary>
/// 
public class Property : Single, IProperty
{
    private PropertyCollection _collection;

    private string _value;

    public PropertyCollection Parent
    {
        set => _collection = value;
    }

    public string Key { get; private set; }

    public string Value
    {
        get => _value;

        set
        {
            if (GetXmlNode(out var node, true)) SetUserData(node, "v", value);
            _value = value;
        }
    }

    public bool IsGlobal { get; private set; }

    public void QuickCreate(string key, string value, bool isGlobal)
    {
        Key = key;
        _value = value;
        IsGlobal = isGlobal;
    }

    internal void SetKey(string key)
    {
        if (GetXmlNode(out var node, true)) SetUserData(node, "k", key);

        Key = key;
    }

    #region XML User Data

    public override string UserDataXPath => $@"{_collection.UserDataXPath}/Property[@k='{Key}']";


    internal override bool GetXmlNode(out XmlNode node, bool forceCreate)
    {
        node = null;
        var success = false;

        if (null == _xmlNode)
        {
            // Get the parent node
            if (_collection.GetXmlNode(out var parentNode, forceCreate))
            {
                // See if our user data already exists
                var xPath = $@"./Property[@k='{Key}']";
                if (!GetUserData(xPath, parentNode, out _xmlNode) && forceCreate)
                {
                    // Create it, and try again
                    CreateUserMetaData(parentNode);
                    GetUserData(xPath, parentNode, out _xmlNode);
                }
            }
        }

        if (null != _xmlNode)
        {
            node = _xmlNode;
            success = true;
        }

        return success;
    }

    public override void CreateUserMetaData(XmlNode parentNode)
    {
        var myNode = parentNode.OwnerDocument.CreateNode(XmlNodeType.Element, "Property", null);
        parentNode.AppendChild(myNode);

        var attr = parentNode.OwnerDocument.CreateAttribute("k");
        attr.Value = Key;
        myNode.Attributes.Append(attr);

        attr = parentNode.OwnerDocument.CreateAttribute("v");
        attr.Value = "";
        myNode.Attributes.Append(attr);
    }

    #endregion
}