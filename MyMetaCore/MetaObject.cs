using System;
using System.Data;
using System.Data.OleDb;
using System.Runtime.InteropServices;
using System.Xml;

namespace MyMeta;

public class MetaObject
{
    protected XmlNode _xmlNode;

    internal dbRoot DbRoot { get; set; }

    protected virtual DataTable LoadData(Guid oleDbSchemaGuid, object[] filter)
    {
        try
        {
            var cn = DbRoot.TheConnection;
            return cn.GetOleDbSchemaTable(oleDbSchemaGuid, filter);
        }
        catch (Exception)
        {
            return new DataTable();
        }
    }
    
    internal virtual bool GetXmlNode(out XmlNode node, bool forceCreate)
    {
        node = null;
        return false;
    }

    public virtual string UserDataXPath => string.Empty;

    public virtual string GlobalUserDataXPath => string.Empty;

    public virtual void CreateUserMetaData(XmlNode parentNode)
    {
    }
    public virtual void CreateUserMetaData()
    {
    }

    protected bool GetUserData(string xPath, XmlNode startingNode, out XmlNodeList data)
    {
        data = null;
        var success = false;

        var nodeList = null == startingNode 
            ? DbRoot.UserData.SelectNodes(xPath, null) 
            : startingNode.SelectNodes(xPath, null);

        if (null != nodeList)
        {
            data = nodeList;
            success = true;
        }

        return success;
    }

    protected bool GetUserData(string xPath, XmlNode startingNode, out XmlNode data)
    {
        data = null;
        var success = false;

        var node = null == startingNode 
            ? DbRoot.UserData.SelectSingleNode(xPath, null) 
            : startingNode.SelectSingleNode(xPath, null);

        if (null != node)
        {
            data = node;
            success = true;
        }

        return success;
    }

    protected bool GetUserData(string xPath, XmlNode startingNode, string attribute, out string data)
    {
        data = string.Empty;

        var node = null == startingNode 
            ? DbRoot.UserData.SelectSingleNode(xPath, null) 
            : startingNode.SelectSingleNode(xPath, null);

        var coll = node?.Attributes;
        if (coll?[attribute] is null) return false;
        data = coll[attribute].Value;

        return true;
    }

    protected static bool GetUserData(XmlNode startingNode, string attribute, out string data)
    {
        data = string.Empty;

        var coll = startingNode?.Attributes;
        if (coll?[attribute] is null) return false;

        data = coll[attribute].Value;
        return true;
    }

    protected static void SetUserData(XmlNode node, string attribute, string data)
    {
        if (node  is not null)
        {
            node.Attributes[attribute].Value = data;
        }
    }
}