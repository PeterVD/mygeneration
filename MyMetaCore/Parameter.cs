using System;
using System.Runtime.InteropServices;
using System.Xml;

namespace MyMeta;

public class Parameter : Single, IParameter, INameValueItem
{
    internal Parameters Parameters;

    #region Collections

    public virtual IPropertyCollection GlobalProperties
    {
        get
        {
            var db = Parameters.Procedure.Procedures.Database;
            if (null != db._parameterProperties) return db._parameterProperties;

            db._parameterProperties = new PropertyCollection
            {
                Parent = this
            };

            var xPath = GlobalUserDataXPath;
            var xmlNode = DbRoot.UserData.SelectSingleNode(xPath, null);

            if (xmlNode is null)
            {
                var parentNode = db.CreateGlobalXmlNode();

                xmlNode = parentNode.OwnerDocument.CreateNode(XmlNodeType.Element, "Parameter", null);
                parentNode.AppendChild(xmlNode);
            }

            db._parameterProperties.LoadAllGlobal(xmlNode);

            return db._parameterProperties;
        }
    }

    public virtual IPropertyCollection AllProperties
    {
        get
        {
            if (null == _allProperties)
            {
                _allProperties = new PropertyCollectionAll();
                _allProperties.Load(Properties, GlobalProperties);
            }

            return _allProperties;
        }
    }

    internal PropertyCollectionAll _allProperties;

    #endregion

    #region Properties

#if ENTERPRISE
    [DispId(0)]
#endif
    public override string Alias
    {
        get => GetXmlNode(out var node, false) && GetUserData(node, "n", out var niceName) && string.Empty != niceName ? niceName : Name;

        set
        {
            if (GetXmlNode(out var node, true))
            {
                SetUserData(node, "n", value);
            }
        }
    }

    public override string Name => GetString(Parameters.f_ParameterName);

    public virtual int Ordinal => GetInt32(Parameters.f_Ordinal);

    public virtual int ParameterType => GetInt32(Parameters.f_Type);

    public virtual bool HasDefault => GetBool(Parameters.f_HasDefault);

    public virtual string Default
    {
        get
        {
            if (HasDefault && null != Parameters.f_Default)
            {
                var o = _row[Parameters.f_Default];

                if (o == DBNull.Value) return "<null>";
            }

            return GetString(Parameters.f_Default);
        }
    }

    public virtual bool IsNullable => GetBool(Parameters.f_IsNullable);

    public virtual int DataType => GetInt32(Parameters.f_DataType);

    public virtual int CharacterMaxLength => GetInt32(Parameters.f_CharMaxLength);

    public virtual int CharacterOctetLength => GetInt32(Parameters.f_CharOctetLength);

    public virtual int NumericPrecision => GetInt32(Parameters.f_NumericPrecision);

    public virtual int NumericScale => GetInt16(Parameters.f_NumericScale);

    public virtual string Description => GetString(Parameters.f_Description);

    public virtual string TypeName => GetString(Parameters.f_TypeName);

    public virtual string LocalTypeName => GetString(Parameters.f_LocalTypeName);

    public virtual ParamDirection Direction
    {
        get
        {
            var dir = ParameterType;

            return dir switch
            {
                1 => ParamDirection.Input,
                2 => ParamDirection.InputOutput,
                3 => ParamDirection.Output,
                4 => ParamDirection.ReturnValue,
                _ => ParamDirection.Unknown
            };
        }
    }

    public virtual string LanguageType
    {
        get
        {
            if (DbRoot.LanguageNode is null) return "Unknown";

            var xPath = $@"./Type[@From='{TypeName}']";
            var node = DbRoot.LanguageNode.SelectSingleNode(xPath, null);
            if (node is null) return "Unknown";

            return GetUserData(node, "To", out var languageType) ? languageType : "Unknown";
        }
    }

    public virtual string DbTargetType
    {
        get
        {
            if (DbRoot.DbTargetNode is null) return "Unknown";
            
            var xPath = $@"./Type[@From='{TypeName}']";
            var node = DbRoot.DbTargetNode.SelectSingleNode(xPath, null);
            if (node is null) return "Unknown";

            return GetUserData(node, "To", out var driverType) ? driverType : "Unknown";
        }
    }

    public virtual string DataTypeNameComplete => GetString(Parameters.f_FullTypeName);

    #endregion

    #region XML User Data

    public override string UserDataXPath
    {
        get { return Parameters.UserDataXPath + @"/Parameter[@p='" + Name + "']"; }
    }

    public override string GlobalUserDataXPath
    {
        get { return Parameters.Procedure.Procedures.Database.GlobalUserDataXPath + "/Parameter"; }
    }

    internal override bool GetXmlNode(out XmlNode node, bool forceCreate)
    {
        node = null;
        var success = false;

        // Get the parent node
        if (null == _xmlNode && Parameters.GetXmlNode(out var parentNode, forceCreate))
        {
            // See if our user data already exists
            var xPath = $@"./Parameter[@p='{Name}']";
            if (!GetUserData(xPath, parentNode, out _xmlNode) && forceCreate)
            {
                // Create it, and try again
                CreateUserMetaData(parentNode);
                GetUserData(xPath, parentNode, out _xmlNode);
            }
        }

        if (null != _xmlNode)
        {
            node = _xmlNode;
            success = true;
        }

        return success;
    }

    public override void CreateUserMetaData(XmlNode parentNode)
    {
        var myNode = parentNode.OwnerDocument.CreateNode(XmlNodeType.Element, "Parameter", null);
        parentNode.AppendChild(myNode);

        var attr = parentNode.OwnerDocument.CreateAttribute("p");
        attr.Value = Name;
        myNode.Attributes.Append(attr);

        attr = parentNode.OwnerDocument.CreateAttribute("n");
        attr.Value = "";
        myNode.Attributes.Append(attr);
    }

    #endregion

    #region INameValueCollection Members

    public string ItemName => Name;

    public string ItemValue => Name;

    #endregion
}