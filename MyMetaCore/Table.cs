using System;
using System.Runtime.InteropServices;
using System.Xml;

namespace MyMeta;

#if ENTERPRISE

[ComVisible(false)]
[ClassInterface(ClassInterfaceType.AutoDual)]
#endif
public class Table : Single, ITable, INameValueItem
{
    protected Columns _columns;
    protected ForeignKeys _foreignKeys;
    protected Indexes _indexes;
    protected ForeignKeys _indirectForeignKeys;
    protected Columns _primaryKeys;

    internal Tables Tables;

    public IDatabase Database => Tables.Database;

    #region Collections

    public IColumns Columns
    {
        get
        {
            if (_columns is not null) return _columns;

            _columns = (Columns)DbRoot.ClassFactory.CreateColumns();
            _columns.Table = this;
            _columns.DbRoot = DbRoot;
            _columns.LoadForTable();

            return _columns;
        }
    }

    public IForeignKeys ForeignKeys
    {
        get
        {
            if (_foreignKeys is not null) return _foreignKeys;

            _foreignKeys = (ForeignKeys)DbRoot.ClassFactory.CreateForeignKeys();
            _foreignKeys.Table = this;
            _foreignKeys.DbRoot = DbRoot;
            _foreignKeys.LoadAll();

            return _foreignKeys;
        }
    }

    public IForeignKeys IndirectForeignKeys
    {
        get
        {
            if (_indirectForeignKeys is not null) return _indirectForeignKeys;
            _indirectForeignKeys = (ForeignKeys)DbRoot.ClassFactory.CreateForeignKeys();
            _indirectForeignKeys.Table = this;
            _indirectForeignKeys.DbRoot = DbRoot;
            _indirectForeignKeys.LoadAllIndirect();

            return _indirectForeignKeys;
        }
    }

    public IIndexes Indexes
    {
        get
        {
            if (_indexes is not null) return _indexes;

            _indexes = (Indexes)DbRoot.ClassFactory.CreateIndexes();
            _indexes.Table = this;
            _indexes.DbRoot = DbRoot;
            _indexes.LoadAll();

            return _indexes;
        }
    }

    public virtual IPropertyCollection GlobalProperties
    {
        get
        {
            var db = Tables.Database;
            if (db._tableProperties is not null) return db._tableProperties;

            db._tableProperties = new PropertyCollection();
            db._tableProperties.Parent = this;

            var xPath = GlobalUserDataXPath;
            var xmlNode = DbRoot.UserData.SelectSingleNode(xPath, null);

            if (xmlNode is null)
            {
                var parentNode = db.CreateGlobalXmlNode();

                xmlNode = parentNode.OwnerDocument.CreateNode(XmlNodeType.Element, "Table", null);
                parentNode.AppendChild(xmlNode);
            }

            db._tableProperties.LoadAllGlobal(xmlNode);

            return db._tableProperties;
        }
    }

    public virtual IPropertyCollection AllProperties
    {
        get
        {
            if (null == _allProperties)
            {
                _allProperties = new PropertyCollectionAll();
                _allProperties.Load(Properties, GlobalProperties);
            }

            return _allProperties;
        }
    }

    internal PropertyCollectionAll _allProperties;

    public virtual IColumns PrimaryKeys => null;

    #endregion

    #region Properties

#if ENTERPRISE
    [DispId(0)]
#endif
    public override string Alias
    {
        get
        {
            return GetXmlNode(out var node, false) && GetUserData(node, "n", out var niceName) && string.Empty != niceName ? niceName : Name;
        }

        set
        {
            if (GetXmlNode(out var node, true))
            {
                SetUserData(node, "n", value);
            }
        }
    }

    public override string Name => GetString(Tables.f_Name);

    public string Schema => GetString(Tables.f_Schema);

    public string Type => GetString(Tables.f_Type);

    public Guid Guid => GetGuid(Tables.f_Guid);

    public string Description => GetString(Tables.f_Description);

    public int PropID => GetInt32(Tables.f_PropID);

    public DateTime DateCreated => GetDateTime(Tables.f_DateCreated);

    public DateTime DateModified => GetDateTime(Tables.f_DateModified);

    #endregion

    #region XML User Data

    public override string UserDataXPath => $@"{Tables.UserDataXPath}/Table[@p='{Name}']";

    public override string GlobalUserDataXPath => $"{Tables.Database.GlobalUserDataXPath}/Table";

    internal override bool GetXmlNode(out XmlNode node, bool forceCreate)
    {
        node = null;
        var success = false;

        if (null == _xmlNode)
        {
            // Get the parent node
            if (Tables.GetXmlNode(out var parentNode, forceCreate))
            {
                // See if our user data already exists
                var xPath = $@"./Table[@p='{Name}']";
                if (!GetUserData(xPath, parentNode, out _xmlNode) && forceCreate)
                {
                    // Create it, and try again
                    CreateUserMetaData(parentNode);
                    GetUserData(xPath, parentNode, out _xmlNode);
                }
            }
        }

        if (null != _xmlNode)
        {
            node = _xmlNode;
            success = true;
        }

        return success;
    }

    public override void CreateUserMetaData(XmlNode parentNode)
    {
        var myNode = parentNode.OwnerDocument.CreateNode(XmlNodeType.Element, "Table", null);
        parentNode.AppendChild(myNode);

        var attr = parentNode.OwnerDocument.CreateAttribute("p");
        attr.Value = Name;
        myNode.Attributes.Append(attr);

        attr = parentNode.OwnerDocument.CreateAttribute("n");
        attr.Value = "";
        myNode.Attributes.Append(attr);
    }

    #endregion

    #region INameValueCollection Members

    public string ItemName => Name;

    public string ItemValue => Name;

    #endregion
}