using System;

namespace MyMeta;

public class Single : MetaObject
{
    protected PropertyCollection _properties;
    internal DataRow _row;

    internal DataRow Row
    {
        set => _row = value;
    }

    public virtual IPropertyCollection Properties
    {
        get
        {
            if (_properties != null) return _properties;

            _properties = new PropertyCollection();
            _properties.Parent = this;
            _properties.LoadAll();

            return _properties;
        }
    }

    public virtual object DatabaseSpecificMetaData(string key)
    {
        return null;
    }


    public virtual string Alias
    {
        get => string.Empty;

        set { }
    }

    public virtual string Name => string.Empty;

    protected string GetString(DataColumn col)
    {
        if (col == null) return string.Empty;

        var o = _row[col];

        if (DBNull.Value == o) return string.Empty;

        var s = (string)o;
        if (DbRoot.StripTrailingNulls && s.EndsWith(DbRoot.TrailingNull))
        {
            s = s.Remove(s.Length - 1, 1);
        }
        return s;

    }

    protected Guid GetGuid(DataColumn col)
    {
        if (null == col) return Guid.Empty;

        var o = _row[col];

        return DBNull.Value == o ? Guid.Empty : (Guid)o;
    }

    private static readonly DateTime NullDateTime = new(1, 1, 1, 1, 1, 1, 1);

    protected DateTime GetDateTime(DataColumn col)
    {
        if (null == col)
        {
            return NullDateTime;
        }
        var o = _row[col];

        return DBNull.Value == o ? NullDateTime : (DateTime)o;
    }

    protected bool GetBool(DataColumn col)
    {
        if (null == col) return false;

        var o = _row[col];

        if (DBNull.Value == o) return false;

        if (o is bool b)
            return b;

        var i = Convert.ToInt32(o);
        return i != 0;
    }

    protected short GetInt16(DataColumn col)
    {
        if (null == col) return 0;

        var o = _row[col];

        return DBNull.Value == o ? (short)0 : Convert.ToInt16(o);
    }

    protected int GetInt32(DataColumn col)
    {
        if (null == col) return 0;

        var o = _row[col];

        return DBNull.Value == o ? 0 : Convert.ToInt32(o);
    }

    protected decimal GetDecimal(DataColumn col)
    {
        if (col == null) return 0;
        var o = _row[col];

        return DBNull.Value == o ? 0 : Convert.ToDecimal(o);
    }

    protected byte[] GetByteArray(DataColumn col)
    {
        if (col == null) return null;
        var o = _row[col];

        return DBNull.Value == o ? null : (byte[])o;
    }
}