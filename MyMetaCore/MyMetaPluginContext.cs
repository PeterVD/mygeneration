using System;
using System.Data;

namespace MyMeta;

public class MyMetaPluginContext : IMyMetaPluginContext
{
    public MyMetaPluginContext(string providerName, string connectionString)
    {
        ProviderName = providerName;
        ConnectionString = connectionString;
    }

    public bool IncludeSystemEntities { get; set; }

    public string ProviderName { get; set; } = string.Empty;

    public string ConnectionString { get; set; } = string.Empty;

    public DataTable CreateDatabasesDataTable()
    {
        var table = new DataTable();
        table.Columns.Add("CATALOG_NAME", Types.StringType);
        table.Columns.Add("DESCRIPTION", Types.StringType);
        table.Columns.Add("SCHEMA_NAME", Types.StringType);
        table.Columns.Add("SCHEMA_OWNER", Types.StringType);
        table.Columns.Add("DEFAULT_CHARACTER_SET_CATALOG", Types.StringType);
        table.Columns.Add("DEFAULT_CHARACTER_SET_SCHEMA", Types.StringType);
        table.Columns.Add("DEFAULT_CHARACTER_SET_NAME", Types.StringType);
        return table;
    }

    public DataTable CreateForeignKeysDataTable()
    {
        var metaData = new DataTable();
        metaData.Columns.Add("PK_TABLE_CATALOG", Types.StringType);
        metaData.Columns.Add("PK_TABLE_SCHEMA", Types.StringType);
        metaData.Columns.Add("PK_TABLE_NAME", Types.StringType);
        metaData.Columns.Add("PK_COLUMN_NAME", Types.StringType);
        metaData.Columns.Add("PK_COLUMN_GUID", Types.GuidType);
        metaData.Columns.Add("PK_COLUMN_PROPID", Types.Int64Type);
        metaData.Columns.Add("FK_TABLE_CATALOG", Types.StringType);
        metaData.Columns.Add("FK_TABLE_SCHEMA", Types.StringType);
        metaData.Columns.Add("FK_TABLE_NAME", Types.StringType);
        metaData.Columns.Add("FK_COLUMN_NAME", Types.StringType);
        metaData.Columns.Add("FK_COLUMN_GUID", Types.GuidType);
        metaData.Columns.Add("FK_COLUMN_PROPID", Types.Int64Type);
        metaData.Columns.Add("ORDINAL", Types.Int64Type);
        metaData.Columns.Add("UPDATE_RULE", Types.StringType);
        metaData.Columns.Add("DELETE_RULE", Types.StringType);
        metaData.Columns.Add("PK_NAME", Types.StringType);
        metaData.Columns.Add("FK_NAME", Types.StringType);
        metaData.Columns.Add("DEFERRABILITY", Types.Int16Type);
        return metaData;
    }

    public DataTable CreateTablesDataTable()
    {
        var metaData = new DataTable();
        metaData.Columns.Add("TABLE_CATALOG", Types.StringType);
        metaData.Columns.Add("TABLE_SCHEMA", Types.StringType);
        metaData.Columns.Add("TABLE_NAME", Types.StringType);
        metaData.Columns.Add("TABLE_TYPE", Types.StringType);
        metaData.Columns.Add("TABLE_GUID", Types.GuidType);
        metaData.Columns.Add("DESCRIPTION", Types.StringType);
        metaData.Columns.Add("TABLE_PROPID", Types.Int64Type);
        metaData.Columns.Add("DATE_CREATED", Types.DateTimeType);
        metaData.Columns.Add("DATE_MODIFIED", Types.DateTimeType);
        return metaData;
    }

    public DataTable CreateViewsDataTable()
    {
        var metaData = new DataTable();
        metaData.Columns.Add("TABLE_CATALOG", Types.StringType);
        metaData.Columns.Add("TABLE_SCHEMA", Types.StringType);
        metaData.Columns.Add("TABLE_NAME", Types.StringType);
        metaData.Columns.Add("TABLE_TYPE", Types.StringType);
        metaData.Columns.Add("TABLE_GUID", Types.GuidType);
        metaData.Columns.Add("DESCRIPTION", Types.StringType);
        metaData.Columns.Add("VIEW_TEXT", Types.StringType);
        metaData.Columns.Add("IS_UPDATABLE", Types.BoolType);
        metaData.Columns.Add("TABLE_PROPID", Types.Int64Type);
        metaData.Columns.Add("DATE_CREATED", Types.DateTimeType);
        metaData.Columns.Add("DATE_MODIFIED", Types.DateTimeType);
        return metaData;
    }

    public DataTable CreateColumnsDataTable()
    {
        var metaData = new DataTable();
        metaData.Columns.Add("TABLE_CATALOG", Types.StringType);
        metaData.Columns.Add("TABLE_SCHEMA", Types.StringType);
        metaData.Columns.Add("TABLE_NAME", Types.StringType);
        metaData.Columns.Add("COLUMN_NAME", Types.StringType);
        metaData.Columns.Add("COLUMN_GUID", Types.GuidType);
        metaData.Columns.Add("COLUMN_PROPID", Types.Int64Type);
        metaData.Columns.Add("ORDINAL_POSITION", Types.Int64Type);
        metaData.Columns.Add("COLUMN_HASDEFAULT", Types.BoolType);
        metaData.Columns.Add("COLUMN_DEFAULT", Types.StringType);
        metaData.Columns.Add("COLUMN_FLAGS", Types.Int64Type);
        metaData.Columns.Add("IS_NULLABLE", Types.BoolType);
        metaData.Columns.Add("DATA_TYPE", Types.Int32Type);
        metaData.Columns.Add("TYPE_NAME", Types.StringType);
        metaData.Columns.Add("TYPE_NAME_COMPLETE", Types.StringType);
        metaData.Columns.Add("TYPE_GUID", Types.GuidType);
        metaData.Columns.Add("CHARACTER_MAXIMUM_LENGTH", Types.Int64Type);
        metaData.Columns.Add("CHARACTER_OCTET_LENGTH", Types.Int64Type);
        metaData.Columns.Add("NUMERIC_PRECISION", Types.Int32Type);
        metaData.Columns.Add("NUMERIC_SCALE", Types.Int16Type);
        metaData.Columns.Add("DATETIME_PRECISION", Types.Int64Type);
        metaData.Columns.Add("CHARACTER_SET_CATALOG", Types.StringType);
        metaData.Columns.Add("CHARACTER_SET_SCHEMA", Types.StringType);
        metaData.Columns.Add("CHARACTER_SET_NAME", Types.StringType);
        metaData.Columns.Add("COLLATION_CATALOG", Types.StringType);
        metaData.Columns.Add("COLLATION_SCHEMA", Types.StringType);
        metaData.Columns.Add("COLLATION_NAME", Types.StringType);
        metaData.Columns.Add("DOMAIN_CATALOG", Types.StringType);
        metaData.Columns.Add("DOMAIN_SCHEMA", Types.StringType);
        metaData.Columns.Add("DOMAIN_NAME", Types.StringType);
        metaData.Columns.Add("DESCRIPTION", Types.StringType);
        metaData.Columns.Add("COLUMN_LCID", Types.Int32Type);
        metaData.Columns.Add("COLUMN_COMPFLAGS", Types.Int32Type);
        metaData.Columns.Add("COLUMN_SORTID", Types.Int32Type);
        metaData.Columns.Add("IS_COMPUTED", Types.BoolType);
        metaData.Columns.Add("IS_AUTO_KEY", Types.BoolType);
        metaData.Columns.Add("AUTO_KEY_SEED", Types.Int32Type);
        metaData.Columns.Add("AUTO_KEY_INCREMENT", Types.Int32Type);
        return metaData;
    }

    public DataTable CreateIndexesDataTable()
    {
        var metaData = new DataTable();
        metaData.Columns.Add("TABLE_CATALOG", Types.StringType);
        metaData.Columns.Add("TABLE_SCHEMA", Types.StringType);
        metaData.Columns.Add("TABLE_NAME", Types.StringType);
        metaData.Columns.Add("INDEX_CATALOG", Types.StringType);
        metaData.Columns.Add("INDEX_SCHEMA", Types.StringType);
        metaData.Columns.Add("INDEX_NAME", Types.StringType);
        metaData.Columns.Add("PRIMARY_KEY", Types.BoolType);
        metaData.Columns.Add("UNIQUE", Types.BoolType);
        metaData.Columns.Add("CLUSTERED", Types.BoolType);
        metaData.Columns.Add("TYPE", Types.Int32Type);
        metaData.Columns.Add("FILL_FACTOR", Types.Int32Type);
        metaData.Columns.Add("INITIAL_SIZE", Types.Int32Type);
        metaData.Columns.Add("NULLS", Types.Int32Type);
        metaData.Columns.Add("SORT_BOOKMARKS", Types.BoolType);
        metaData.Columns.Add("AUTO_UPDATE", Types.BoolType);
        metaData.Columns.Add("NULL_COLLATION", Types.Int32Type);
        metaData.Columns.Add("ORDINAL_POSITION", Types.Int64Type);
        metaData.Columns.Add("COLUMN_NAME", Types.StringType);
        metaData.Columns.Add("COLUMN_GUID", Types.GuidType);
        metaData.Columns.Add("COLUMN_PROPID", Types.Int64Type);
        metaData.Columns.Add("COLLATION", Types.Int16Type);
        metaData.Columns.Add("CARDINALITY", Types.DecimalType);
        metaData.Columns.Add("PAGES", Types.Int32Type);
        metaData.Columns.Add("FILTER_CONDITION", Types.StringType);
        metaData.Columns.Add("INTEGRATED", Types.BoolType);
        return metaData;
    }

    public DataTable CreateProceduresDataTable()
    {
        var metaData = new DataTable();
        metaData.Columns.Add("PROCEDURE_CATALOG", Types.StringType);
        metaData.Columns.Add("PROCEDURE_SCHEMA", Types.StringType);
        metaData.Columns.Add("PROCEDURE_NAME", Types.StringType);
        metaData.Columns.Add("PROCEDURE_TYPE", Types.Int16Type);
        metaData.Columns.Add("PROCEDURE_DEFINITION", Types.StringType);
        metaData.Columns.Add("DESCRIPTION", Types.StringType);
        metaData.Columns.Add("DATE_CREATED", Types.DateTimeType);
        metaData.Columns.Add("DATE_MODIFIED", Types.DateTimeType);
        return metaData;
    }

    public DataTable CreateParametersDataTable()
    {
        var metaData = new DataTable();
        metaData.Columns.Add("PROCEDURE_CATALOG", Types.StringType);
        metaData.Columns.Add("PROCEDURE_SCHEMA", Types.StringType);
        metaData.Columns.Add("PROCEDURE_NAME", Types.StringType);
        metaData.Columns.Add("PARAMETER_NAME", Types.StringType);
        metaData.Columns.Add("ORDINAL_POSITION", Types.Int32Type);
        metaData.Columns.Add("PARAMETER_TYPE", Types.Int32Type);
        metaData.Columns.Add("PARAMETER_HASDEFAULT", Types.BoolType);
        metaData.Columns.Add("PARAMETER_DEFAULT", Types.StringType);
        metaData.Columns.Add("IS_NULLABLE", Types.BoolType);
        metaData.Columns.Add("DATA_TYPE", Types.Int32Type);
        metaData.Columns.Add("CHARACTER_MAXIMUM_LENGTH", Types.Int64Type);
        metaData.Columns.Add("CHARACTER_OCTET_LENGTH", Types.Int64Type);
        metaData.Columns.Add("NUMERIC_PRECISION", Types.Int32Type);
        metaData.Columns.Add("NUMERIC_SCALE", Types.Int16Type);
        metaData.Columns.Add("DESCRIPTION", Types.StringType);
        metaData.Columns.Add("TYPE_NAME", Types.StringType);
        metaData.Columns.Add("LOCAL_TYPE_NAME", Types.StringType);
        return metaData;
    }

    public DataTable CreateResultColumnsDataTable()
    {
        var metaData = new DataTable();
        // Fix k3b 20070709: PluginResultColumns.BindToColumns and 
        //      MyMetaPluginContext.CreateResultColumnsDataTable
        //      ColumnNames were different
        metaData.Columns.Add("COLUMN_NAME", Types.StringType);
        // metaData.Columns.Add("Alias", Types.StringType);
        metaData.Columns.Add("ORDINAL_POSITION", Types.Int64Type);
        metaData.Columns.Add("TYPE_NAME", Types.StringType);
        metaData.Columns.Add("TYPE_NAME_COMPLETE", Types.StringType);
        // metaData.Columns.Add("LanguageType", Types.StringType);
        // metaData.Columns.Add("DbTargetType", Types.StringType);
        return metaData;
    }

    public DataTable CreateDomainsDataTable()
    {
        var metaData = new DataTable();
        metaData.Columns.Add("DOMAIN_CATALOG", Types.StringType);
        metaData.Columns.Add("DOMAIN_SCHEMA", Types.StringType);
        metaData.Columns.Add("DOMAIN_NAME", Types.StringType);
        metaData.Columns.Add("DATA_TYPE", Types.StringType);
        metaData.Columns.Add("CHARACTER_MAXIMUM_LENGTH", Types.Int32Type);
        metaData.Columns.Add("CHARACTER_OCTET_LENGTH", Types.Int32Type);
        metaData.Columns.Add("COLLATION_CATALOG", Types.StringType);
        metaData.Columns.Add("COLLATION_SCHEMA", Types.StringType);
        metaData.Columns.Add("COLLATION_NAME", Types.StringType);
        metaData.Columns.Add("CHARACTER_SET_CATALOG", Types.StringType);
        metaData.Columns.Add("CHARACTER_SET_SCHEMA", Types.StringType);
        metaData.Columns.Add("CHARACTER_SET_NAME", Types.StringType);
        metaData.Columns.Add("NUMERIC_PRECISION", Types.ByteType);
        metaData.Columns.Add("NUMERIC_PRECISION_RADIX", Types.Int16Type);
        metaData.Columns.Add("NUMERIC_SCALE", Types.Int32Type);
        metaData.Columns.Add("DATETIME_PRECISION", Types.Int16Type);
        metaData.Columns.Add("DOMAIN_DEFAULT", Types.StringType);
        return metaData;
    }
}