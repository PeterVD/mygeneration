using System;
using System.Runtime.InteropServices;

namespace MyMeta.PostgreSQL;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IColumn))]
#endif
public class PostgreSQLColumn : Column
{
    public override bool IsNullable
    {
        get
        {
            var s = GetString(Columns.f_IsNullable);

            if (s == "YES")
                return true;
            return false;
        }
    }

    public override bool HasDefault
    {
        get
        {
            var o = _row[Columns.f_Default];

            if (o == DBNull.Value) return false;

            return true;
        }
    }


    public override bool IsAutoKey
    {
        get
        {
            var cols = Columns as PostgreSQLColumns;
            return GetBool(cols.f_AutoKey);
        }
    }

    public override bool IsComputed => GetBool(Columns.f_IsComputed);


    public override string DataTypeName
    {
        get
        {
            var cols = Columns as PostgreSQLColumns;
            return GetString(cols.f_TypeName);
        }
    }

    public override string DataTypeNameComplete
    {
        get
        {
            var cols = Columns as PostgreSQLColumns;
            return GetString(cols.f_TypeNameComplete);
        }
    }

    internal override Column Clone()
    {
        var c = base.Clone();

        return c;
    }
}