using System.Data;
using System.Runtime.InteropServices;
using Npgsql;

namespace MyMeta.PostgreSQL;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(ITable))]
#endif
public class PostgreSQLTable : Table
{
    public override IColumns PrimaryKeys
    {
        get
        {
            if (null == _primaryKeys)
            {
                var query =
                    "select a.attname as COLUMN_NAME, c.conname as constraint, d.relname as table " +
                    "from pg_attribute a, pg_index b, pg_constraint c, pg_class d, pg_namespace n " +
                    "WHERE n.nspname = '" + Schema + "' AND d.relname = '" + Name + "' AND a.attrelid = b.indexrelid " +
                    "and b.indrelid = c.conrelid and b.indrelid = d.relfilenode and c.contype = 'p' AND b.indisprimary";

                var cn = ConnectionHelper.CreateConnection(DbRoot, Database.Name);

                var metaData = new DataTable();
                var adapter = new NpgsqlDataAdapter(query, cn);

                adapter.Fill(metaData);
                cn.Close();

                _primaryKeys = (Columns)DbRoot.ClassFactory.CreateColumns();
                _primaryKeys.Table = this;
                _primaryKeys.DbRoot = DbRoot;

                var colName = "";

                var count = metaData.Rows.Count;
                for (var i = 0; i < count; i++)
                {
                    colName = metaData.Rows[i]["COLUMN_NAME"] as string;
                    _primaryKeys.AddColumn((Column)Columns[colName]);
                }
            }

            return _primaryKeys;
        }
    }
}