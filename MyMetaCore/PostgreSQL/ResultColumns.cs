using System.Data;
using System.Runtime.InteropServices;

namespace MyMeta.PostgreSQL;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IResultColumns))]
#endif
public class PostgreSQLResultColumns : ResultColumns
{
    internal override void LoadAll()
    {
        try
        {
            var schema = "";

            if (-1 == Procedure.Schema.IndexOf(".")) schema = Procedure.Schema + ".";

            var select = "SET FMTONLY ON EXEC " + Procedure.Database.Name + "." + schema +
                         Procedure.Name + " ";

            var paramCount = Procedure.Parameters.Count;

            if (paramCount > 0)
            {
                var parameters = Procedure.Parameters;
                IParameter param = null;

                var c = parameters.Count;

                for (var i = 0; i < c; i++)
                {
                    param = parameters[i];

                    if (param.Direction == ParamDirection.ReturnValue) paramCount--;
                }
            }

            for (var i = 0; i < paramCount; i++)
            {
                if (i > 0) select += ",";

                select += "null";
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter(select, DbRoot.ConnectionString);
            var metaData = new DataTable();

            adapter.Fill(metaData);

            PostgreSQLResultColumn resultColumn = null;

            var count = metaData.Columns.Count;
            for (var i = 0; i < count; i++)
            {
                resultColumn = DbRoot.ClassFactory.CreateResultColumn() as PostgreSQLResultColumn;
                resultColumn.DbRoot = DbRoot;
                resultColumn.ResultColumns = this;
                resultColumn._column = metaData.Columns[i];
                _array.Add(resultColumn);
            }
        }
        catch
        {
        }
    }
}