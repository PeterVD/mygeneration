using System.Runtime.InteropServices;

namespace MyMeta.PostgreSQL;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IIndex))]
#endif
public class PostgreSQLIndex : Index
{
    public override string Type
    {
        get
        {
            var type = GetString(Indexes.f_Type);
            return type.ToUpper();
        }
    }
}