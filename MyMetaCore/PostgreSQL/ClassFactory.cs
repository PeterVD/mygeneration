using System.Data;
using System.Runtime.InteropServices;
using Npgsql;

namespace MyMeta.PostgreSQL;

#if ENTERPRISE

[ComVisible(false)]
#endif
public class ClassFactory : IClassFactory
{
    public ITables CreateTables()
    {
        return new PostgreSQLTables();
    }

    public ITable CreateTable()
    {
        return new PostgreSQLTable();
    }

    public IColumn CreateColumn()
    {
        return new PostgreSQLColumn();
    }

    public IColumns CreateColumns()
    {
        return new PostgreSQLColumns();
    }

    public IDatabase CreateDatabase()
    {
        return new PostgreSQLDatabase();
    }

    public IDatabases CreateDatabases()
    {
        return new PostgreSQLDatabases();
    }

    public IProcedure CreateProcedure()
    {
        return new PostgreSQLProcedure();
    }

    public IProcedures CreateProcedures()
    {
        return new PostgreSQLProcedures();
    }

    public IView CreateView()
    {
        return new PostgreSQLView();
    }

    public IViews CreateViews()
    {
        return new PostgreSQLViews();
    }

    public IParameter CreateParameter()
    {
        return new PostgreSQLParameter();
    }

    public IParameters CreateParameters()
    {
        return new PostgreSQLParameters();
    }

    public IForeignKey CreateForeignKey()
    {
        return new PostgreSQLForeignKey();
    }

    public IForeignKeys CreateForeignKeys()
    {
        return new PostgreSQLForeignKeys();
    }

    public IIndex CreateIndex()
    {
        return new PostgreSQLIndex();
    }

    public IIndexes CreateIndexes()
    {
        return new PostgreSQLIndexes();
    }

    public IResultColumn CreateResultColumn()
    {
        return new PostgreSQLResultColumn();
    }

    public IResultColumns CreateResultColumns()
    {
        return new PostgreSQLResultColumns();
    }

    public IDomain CreateDomain()
    {
        return new PostgreSQLDomain();
    }

    public IDomains CreateDomains()
    {
        return new PostgreSQLDomains();
    }

    public IProviderType CreateProviderType()
    {
        return new ProviderType();
    }

    public IProviderTypes CreateProviderTypes()
    {
        return new ProviderTypes();
    }

    public IDbConnection CreateConnection()
    {
        return new NpgsqlConnection();
    }

    public static void Register()
    {
        InternalDriver.Register("POSTGRESQL",
            new InternalDriver
            (typeof(ClassFactory)
                , "Server=127.0.0.1;Port=5432;User Id=myUser;Password=myPasswordt;Database=test;"
                , false));
    }
}