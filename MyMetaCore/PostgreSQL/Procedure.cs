using System.Runtime.InteropServices;

namespace MyMeta.PostgreSQL;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IProcedure))]
#endif
public class PostgreSQLProcedure : Procedure
{
    private Parameters _parameters;
    internal string param_types = "";

    public override string Alias
    {
        get
        {
            var name = base.Name.Split(';');

            return name[0];
        }
    }

    public override string Name
    {
        get
        {
            var name = base.Name.Split(';');

            return name[0];
        }
    }

    public override IParameters Parameters
    {
        get
        {
            if (null == _parameters)
            {
                _parameters = (Parameters)DbRoot.ClassFactory.CreateParameters();
                _parameters.Procedure = this;
                _parameters.DbRoot = DbRoot;

//					if(this.param_types.Length > 0)
//					{
//						string query = "SELECT typname::text FROM pg_type WHERE oid IN(";
//						query += this.param_types.Replace(" ", ",");
//						query += ");";
//					}
            }

            return _parameters;
        }
    }
}