using System.Data;
using System.Runtime.InteropServices;
using Npgsql;

namespace MyMeta.PostgreSQL;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(ITables))]
#endif
public class PostgreSQLTables : Tables
{
    internal override void LoadAll()
    {
        try
        {
            var query =
                "SELECT CAST(current_database() AS character varying) AS table_catalog, " +
                "CAST(nc.nspname AS character varying) AS table_schema, " +
                "CAST(c.relname AS character varying) AS table_name, " +
                "CAST('BASE TABLE' AS character varying) AS table_type, d.description " +
                "FROM pg_namespace nc, pg_user u, pg_class c LEFT OUTER JOIN pg_description d ON d.objoid = c.oid AND d.objsubid = 0  " +
                "WHERE c.relnamespace = nc.oid AND u.usesysid = c.relowner AND c.relkind= 'r'";

            if (!DbRoot.ShowSystemData) query += " AND nc.nspname <> 'pg_catalog'";

            query += " ORDER BY c.relname";

            var cn = ConnectionHelper.CreateConnection(DbRoot, Database.Name);

            var metaData = new DataTable();
            var adapter = new NpgsqlDataAdapter(query, cn);

            adapter.Fill(metaData);
            cn.Close();

            PopulateArray(metaData);
        }
        catch
        {
        }
    }
}