using System;
using System.Data;
using System.Runtime.InteropServices;
using Npgsql;

namespace MyMeta.PostgreSQL;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IIndexes))]
#endif
public class PostgreSQLIndexes : Indexes
{
    internal override void LoadAll()
    {
        try
        {
            var select = @"SELECT current_database() as table_catalog, tab.relname AS table_name, " +
                         "n.nspname as TABLE_NAMESPACE, cls.relname as INDEX_NAME, idx.indisunique as UNIQUE, " +
                         "idx.indisclustered as CLUSTERED, a.amname as TYPE, indkey AS columns FROM pg_index idx " +
                         "JOIN pg_class cls ON cls.oid=indexrelid " +
                         "JOIN pg_class tab ON tab.oid=indrelid AND tab.relname = '" + Table.Name + "' " +
                         "JOIN pg_namespace n ON n.oid=tab.relnamespace AND n.nspname = '" + Table.Schema + "' " +
                         "JOIN pg_am a ON a.oid = cls.relam " +
                         "LEFT JOIN pg_depend dep ON (dep.classid = cls.tableoid AND dep.objid = cls.oid AND dep.refobjsubid = '0') " +
                         "LEFT OUTER JOIN pg_constraint con ON (con.tableoid = dep.refclassid AND con.oid = dep.refobjid) " +
                         "WHERE con.conname IS NULL ORDER BY cls.relname;";

            var cn = new NpgsqlConnection(DbRoot.ConnectionString);

            var adapter = new NpgsqlDataAdapter(select, cn);
            cn.Open();
            cn.ChangeDatabase(Table.Tables.Database.Name);
            var metaData = new DataTable();

            adapter.Fill(metaData);
            cn.Close();

            PopulateArrayNoHookup(metaData);

            for (var i = 0; i < Count; i++)
            {
                var index = this[i] as Index;

                if (null != index)
                {
                    var s = index._row["columns"] as string;
                    var colIndexes = s.Split(' ');

                    foreach (var colIndex in colIndexes)
                        if (colIndex != "0")
                        {
                            var id = Convert.ToInt32(colIndex);

                            var column = Table.Columns[id - 1] as Column;
                            index.AddColumn(column.Name);
                        }
                }
            }
        }
        catch
        {
        }
    }
}