using System.Runtime.InteropServices;

namespace MyMeta.PostgreSQL;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IDomain))]
#endif
public class PostgreSQLDomain : Domain
{
}