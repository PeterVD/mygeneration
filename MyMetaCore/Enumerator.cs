using System.Collections;

namespace MyMeta;

/// <summary>
///     Summary description for Enumerator.
/// </summary>
public class Enumerator : IEnumerator
{
    protected ArrayList _array;

    public Enumerator(ArrayList array)
    {
        _array = array;
    }

    #region IEnumerator Members

    public void Reset()
    {
        _index = -1;
    }

    public object Current
    {
        get
        {
            object obj = null;

            if (_index >= 0 && _index < _array.Count) return _array[_index];

            return obj;
        }
    }

    public bool MoveNext()
    {
        if (++_index < _array.Count)
            return true;
        return false;
    }

    private int _index = -1;

    #endregion
}