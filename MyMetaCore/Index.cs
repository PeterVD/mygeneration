using System.Xml;

namespace MyMeta;

public class Index : Single, IIndex
{
    private Columns _columns;
    internal Indexes Indexes;

    #region Objects

    public ITable Table => Indexes.Table;

    #endregion

    public virtual IColumns Columns => _columns;

    internal void AddColumn(string physicalColumnName)
    {
        if (null == _columns)
        {
            _columns = (Columns)DbRoot.ClassFactory.CreateColumns();
            _columns.DbRoot = DbRoot;
            _columns.Index = this;
        }

        var column = Indexes.Table.Columns[physicalColumnName] as Column;
        _columns.AddColumn(column);
    }

    #region Collections

    public virtual IPropertyCollection GlobalProperties
    {
        get
        {
            var db = Indexes.Table.Tables.Database;
            if (db._indexProperties is not null) return db._indexProperties;

            db._indexProperties = new PropertyCollection
            {
                Parent = this
            };

            var xPath = GlobalUserDataXPath;
            var xmlNode = DbRoot.UserData.SelectSingleNode(xPath, null);

            if (xmlNode is null)
            {
                var parentNode = db.CreateGlobalXmlNode();

                xmlNode = parentNode.OwnerDocument.CreateNode(XmlNodeType.Element, "Index", null);
                parentNode.AppendChild(xmlNode);
            }

            db._indexProperties.LoadAllGlobal(xmlNode);

            return db._indexProperties;
        }
    }

    public virtual IPropertyCollection AllProperties
    {
        get
        {
            if (null == _allProperties)
            {
                _allProperties = new PropertyCollectionAll();
                _allProperties.Load(Properties, GlobalProperties);
            }

            return _allProperties;
        }
    }

    internal PropertyCollectionAll _allProperties;

    #endregion

    #region Properties

    public override string Alias
    {
        get { return GetXmlNode(out var node, false) && GetUserData(node, "n", out var niceName) && string.Empty != niceName ? niceName : Name; }

        set
        {
            if (GetXmlNode(out var node, true)) SetUserData(node, "n", value);
        }
    }

    public override string Name => GetString(Indexes.f_IndexName);

    public virtual string Schema => GetString(Indexes.f_IndexSchema);

    public virtual bool Unique => GetBool(Indexes.f_Unique);

    public virtual bool Clustered => GetBool(Indexes.f_Clustered);

    public virtual string Type
    {
        get
        {
            var i = GetInt32(Indexes.f_Type);

            return i switch
            {
                1 => "BTREE",
                2 => "HASH",
                3 => "CONTENT",
                4 => "OTHER",
                _ => "OTHER"
            };
        }
    }

    public virtual int FillFactor => GetInt32(Indexes.f_FillFactor);

    public virtual int InitialSize => GetInt32(Indexes.f_InitializeSize);

    public virtual bool SortBookmarks => GetBool(Indexes.f_SortBookmarks);

    public virtual bool AutoUpdate => GetBool(Indexes.f_AutoUpdate);

    public virtual string NullCollation
    {
        get
        {
            var i = GetInt32(Indexes.f_NullCollation);

            return i switch
            {
                1 => "END",
                2 => "HIGH",
                4 => "LOW",
                8 => "START",
                _ => "UNKNOWN"
            };
        }
    }

    public virtual string Collation
    {
        get
        {
            int i = GetInt16(Indexes.f_Collation);

            return i switch
            {
                1 => "ASCENDING",
                2 => "DESCENDING",
                _ => "UNKNOWN"
            };
        }
    }

    public virtual decimal Cardinality => GetDecimal(Indexes.f_Cardinality);

    public virtual int Pages => GetInt32(Indexes.f_Pages);

    public virtual string FilterCondition => GetString(Indexes.f_FilterCondition);

    public virtual bool Integrated => GetBool(Indexes.f_Integrated);

    #endregion

    #region XML User Data

    public override string UserDataXPath => $@"{Indexes.UserDataXPath}/Index[@p='{Name}']";

    public override string GlobalUserDataXPath => $"{Indexes.Table.Tables.Database.GlobalUserDataXPath}/Index";

    internal override bool GetXmlNode(out XmlNode node, bool forceCreate)
    {
        node = null;
        var success = false;

        if (null == _xmlNode)
        {
            // Get the parent node
            if (Indexes.GetXmlNode(out var parentNode, forceCreate))
            {
                // See if our user data already exists
                var xPath = $@"./Index[@p='{Name}']";
                if (!GetUserData(xPath, parentNode, out _xmlNode) && forceCreate)
                {
                    // Create it, and try again
                    CreateUserMetaData(parentNode);
                    GetUserData(xPath, parentNode, out _xmlNode);
                }
            }
        }

        if (null != _xmlNode)
        {
            node = _xmlNode;
            success = true;
        }

        return success;
    }

    public override void CreateUserMetaData(XmlNode parentNode)
    {
        var myNode = parentNode.OwnerDocument.CreateNode(XmlNodeType.Element, "Index", null);
        parentNode.AppendChild(myNode);

        var attr = parentNode.OwnerDocument.CreateAttribute("p");
        attr.Value = Name;
        myNode.Attributes.Append(attr);

        attr = parentNode.OwnerDocument.CreateAttribute("n");
        attr.Value = "";
        myNode.Attributes.Append(attr);
    }

    #endregion

    #region INameValueCollection Members

    public string ItemName => Name;

    public string ItemValue => Name;

    #endregion
}