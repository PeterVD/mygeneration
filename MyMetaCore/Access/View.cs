using System.Runtime.InteropServices;

namespace MyMeta.Access;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IView))]
#endif
public class AccessView : View
{
}