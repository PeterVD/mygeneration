using System.Runtime.InteropServices;

namespace MyMeta.Access;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IResultColumn))]
#endif
public class AccessResultColumn : ResultColumn
{
    #region Properties

    public override string Name => name;

    public override string DataTypeName
    {
        get
        {
            switch (typeName)
            {
                case "adVarWChar":
                    return "Text";
                case "adLongVarWChar":
                    return "Memo";
                case "adUnsignedTinyInt":
                    return "Byte";
                case "adCurrency":
                    return "Currency";
                case "adDate":
                    return "DateTime";
                case "adBoolean":
                    return @"Yes/No";
                case "adLongVarBinary":
                    return "OLE Object";
                case "adInteger":
                    return "Long";
                case "adDouble":
                    return "Double";
                case "adGUID":
                    return "Replication ID";
                case "adSingle":
                    return "Single";
                case "adNumeric":
                    return "Decimal";
                case "adSmallInt":
                    return "Integer";
                case "adVarBinary":
                    return "Binary";
                case "Hyperlink":
                    return "Hyperlink";
                default:
                    return typeName;
            }
        }
    }

    public override string DataTypeNameComplete
    {
        get
        {
            switch (typeName)
            {
                case "adVarWChar":
                    return "Text";
                case "adLongVarWChar":
                    return "Memo";
                case "adUnsignedTinyInt":
                    return "Byte";
                case "adCurrency":
                    return "Currency";
                case "adDate":
                    return "DateTime";
                case "adBoolean":
                    //return @"Yes/No";
                    return "Bit";
                case "adLongVarBinary":
                    //return "OLE Object";
                    return "LongBinary";
                case "adInteger":
                    return "Long";
                case "adDouble":
                    return "IEEEDouble";
                case "adGUID":
                    //return "Replication ID";
                    return "Guid";
                case "adSingle":
                    return "IEEESingle";
                case "adNumeric":
                    return "Decimal";
                case "adSmallInt":
                    return "Integer";
                case "adVarBinary":
                    return "Binary";
                case "Hyperlink":
                    return "Text (255)";
                default:
                    return typeName;
            }
        }
    }

    public override int Ordinal => ordinal;

    internal string name;
    internal string typeName;
    internal int ordinal;

    #endregion
}