using System.Data;
using System.Runtime.InteropServices;

namespace MyMeta.Access;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(ITable))]
#endif
public class AccessTable : Table
{
    public override IColumns PrimaryKeys
    {
        get
        {
            if (null == _primaryKeys)
            {
                var metaData = LoadData(OleDbSchemaGuid.Primary_Keys,
                    new object[] { null, null, Name });

                _primaryKeys = (Columns)DbRoot.ClassFactory.CreateColumns();
                _primaryKeys.Table = this;
                _primaryKeys.DbRoot = DbRoot;

                var colName = "";

                metaData.DefaultView.Sort = "ORDINAL";

                foreach (DataRowView rv in metaData.DefaultView)
                {
                    colName = rv.Row["COLUMN_NAME"] as string;
                    _primaryKeys.AddColumn((Column)Columns[colName]);
                }

                //int count = metaData.Rows.Count;
                //for(int i = 0; i < count; i++)
                //{
                //    colName = metaData.Rows[i]["COLUMN_NAME"] as string;
                //    _primaryKeys.AddColumn((Column)this.Columns[colName]);
                //}
            }

            return _primaryKeys;
        }
    }
}