using System.Data;
using System.Runtime.InteropServices;

namespace MyMeta.Access;

public class ClassFactory : IClassFactory
{
    public ITables CreateTables()
    {
        return new AccessTables();
    }

    public ITable CreateTable()
    {
        return new AccessTable();
    }

    public IColumn CreateColumn()
    {
        return new AccessColumn();
    }

    public IColumns CreateColumns()
    {
        return new AccessColumns();
    }

    public IDatabase CreateDatabase()
    {
        return new AccessDatabase();
    }

    public IDatabases CreateDatabases()
    {
        return new AccessDatabases();
    }

    public IProcedure CreateProcedure()
    {
        return new AccessProcedure();
    }

    public IProcedures CreateProcedures()
    {
        return new AccessProcedures();
    }

    public IView CreateView()
    {
        return new AccessView();
    }

    public IViews CreateViews()
    {
        return new AccessViews();
    }

    public IParameter CreateParameter()
    {
        return new AccessParameter();
    }

    public IParameters CreateParameters()
    {
        return new AccessParameters();
    }

    public IForeignKey CreateForeignKey()
    {
        return new AccessForeignKey();
    }

    public IForeignKeys CreateForeignKeys()
    {
        return new AccessForeignKeys();
    }

    public IIndex CreateIndex()
    {
        return new AccessIndex();
    }

    public IIndexes CreateIndexes()
    {
        return new AccessIndexes();
    }

    public IResultColumn CreateResultColumn()
    {
        return new AccessResultColumn();
    }

    public IResultColumns CreateResultColumns()
    {
        return new AccessResultColumns();
    }

    public IDomain CreateDomain()
    {
        return new AccessDomain();
    }

    public IDomains CreateDomains()
    {
        return new AccessDomains();
        ;
    }

    public IProviderType CreateProviderType()
    {
        return new ProviderType();
    }

    public IProviderTypes CreateProviderTypes()
    {
        return new ProviderTypes();
    }

    public IDbConnection CreateConnection()
    {
        return new System.Data.OleDb.OleDbConnection();
    }

    public static void Register()
    {
        InternalDriver.Register("ACCESS",
            new InternalDriver
            (typeof(ClassFactory)
                , @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=c:\access\Northwind.mdb;User Id=;Password="
                , true));
    }
}