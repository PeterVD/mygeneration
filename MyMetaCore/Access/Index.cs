using System.Runtime.InteropServices;

namespace MyMeta.Access;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IIndex))]
#endif
public class AccessIndex : Index
{
}