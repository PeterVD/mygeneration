using System.Runtime.InteropServices;

namespace MyMeta.Access;
public class AccessColumn : Column
{
    public override bool IsInPrimaryKey => Columns.Table?.PrimaryKeys[Name]  is not null;

    public override bool IsAutoKey
    {
        get
        {
            var cols = Columns as AccessColumns;
            return cols is not null && GetBool(cols.f_AutoKey);
        }
    }

    public override int AutoKeyIncrement
    {
        get
        {
            var cols = Columns as AccessColumns;
            return GetInt32(cols?.f_AutoKeyIncrement);
        }
    }

    public override int AutoKeySeed
    {
        get
        {
            var cols = Columns as AccessColumns;
            return GetInt32(cols?.f_AutoKeySeed);
        }
    }

    public override string DataTypeName
    {
        get
        {
            var cols = Columns as AccessColumns;
            var type = GetString(cols?.f_TypeName);

            return type switch
            {
                "adWChar" or "adVarWChar" => "Text",
                "adLongVarWChar" => "Memo",
                "adUnsignedTinyInt" => "Byte",
                "adCurrency" => "Currency",
                "adDate" => "DateTime",
                "adBoolean" => @"Yes/No",
                "adLongVarBinary" => "OLE Object",
                "adInteger" => "Long",
                "adDouble" => "Double",
                "adGUID" => "Replication ID",
                "adSingle" => "Single",
                "adNumeric" => "Decimal",
                "adSmallInt" => "Integer",
                "adVarBinary" => "Binary",
                "Hyperlink" => "Hyperlink",
                _ => type
            };
        }
    }

    public override string DataTypeNameComplete
    {
        get
        {
            var cols = Columns as AccessColumns;
            var type = GetString(cols.f_TypeName);

            return type switch
            {
                "adWChar" or "adVarWChar" => "Text",
                "adLongVarWChar" => "Memo",
                "adUnsignedTinyInt" => "Byte",
                "adCurrency" => "Currency",
                "adDate" => "DateTime",
                "adBoolean" => "Bit",
                "adLongVarBinary" => "LongBinary",
                "adInteger" => "Long",
                "adDouble" => "IEEEDouble",
                "adGUID" => "Guid",
                "adSingle" => "IEEESingle",
                "adNumeric" => "Decimal",
                "adSmallInt" => "Integer",
                "adVarBinary" => "Binary",
                "Hyperlink" => "Text (255)",
                _ => type
            };
        }
    }
}