using System;
using System.Data;
using System.Data.OleDb;
using System.Runtime.InteropServices;
using ADODB;
using ADOX;

namespace MyMeta.Access;

public class AccessColumns : Columns
{
    internal DataColumn f_AutoKey;
    internal DataColumn f_TypeName;

    internal override void LoadForTable()
    {
        var metaData = LoadData(OleDbSchemaGuid.Columns, [null, null, Table.Name]);

        metaData.DefaultView.Sort = "ORDINAL_POSITION";

        PopulateArray(metaData);

        LoadExtraDataForTable();
    }

    internal override void LoadForView()
    {
        var metaData = LoadData(OleDbSchemaGuid.Columns, [null, null, View.Name]);

        metaData.DefaultView.Sort = "ORDINAL_POSITION";

        PopulateArray(metaData);

        LoadExtraDataForView();
    }

    private void LoadExtraDataForTable()
    {
        try
        {
            if (_array.Count <= 0) return;

            var cnn = new Connection();
            var cat = new Catalog();

            // Open the Connection
            cnn.Open(DbRoot.ConnectionString, null, null, 0);
            cat.ActiveConnection = cnn;

            ADOX.Columns cols = null;
            cols = cat.Tables[Table.Name].Columns;

            var col = _array[0] as Column;

            f_TypeName = new DataColumn("TYPE_NAME", typeof(string));
            col._row.Table.Columns.Add(f_TypeName);

            f_AutoKey = new DataColumn("AUTO_INCREMENT", typeof(bool));
            col._row.Table.Columns.Add(f_AutoKey);

            f_AutoKeySeed = new DataColumn("AUTO_KEY_SEED", typeof(int));
            col._row.Table.Columns.Add(f_AutoKeySeed);

            f_AutoKeyIncrement = new DataColumn("AUTO_KEY_INCREMENT", typeof(int));
            col._row.Table.Columns.Add(f_AutoKeyIncrement);

            var count = _array.Count;
            Column c = null;
            ADOX.Column cx = null;

            for (var index = 0; index < count; index++)
            {
                cx = cols[index];
                c = (Column)this[cx.Name];

                var hyperlink = "False";

                try
                {
                    hyperlink = cx.Properties["Jet OLEDB:Hyperlink"].Value.ToString();
                }
                catch
                {
                }

                var name = cx.Name;

                Console.WriteLine("-----------------------------------------");
                foreach (ADOX.Property prop in cx.Properties)
                {
                    Console.WriteLine(prop.Attributes.ToString());
                    Console.WriteLine(prop.Name);
                    if (null != prop.Value)
                        Console.WriteLine(prop.Value.ToString());
                }

                c._row["TYPE_NAME"] = hyperlink == "False" ? cx.Type.ToString() : "Hyperlink";

                try
                {
                    if (c.Default == "GenGUID()")
                    {
                        c._row["AUTO_INCREMENT"] = Convert.ToBoolean(cx.Properties["Jet OLEDB:AutoGenerate"].Value);
                    }
                    else
                    {
                        c._row["AUTO_INCREMENT"] = Convert.ToBoolean(cx.Properties["Autoincrement"].Value);
                        c._row["AUTO_KEY_SEED"] = Convert.ToInt32(cx.Properties["Seed"].Value);
                        c._row["AUTO_KEY_INCREMENT"] = Convert.ToInt32(cx.Properties["Increment"].Value);
                    }
                }
                catch
                {
                }
            }

            cnn.Close();
        }
        catch
        {
        }
    }

    private void LoadExtraDataForView()
    {
        try
        {
            if (_array.Count > 0)
            {
                var cnn = new Connection();
                var rs = new Recordset();
                var cat = new Catalog();

                // Open the Connection
                cnn.Open(DbRoot.ConnectionString, null, null, 0);
                cat.ActiveConnection = cnn;

                rs.Source = cat.Views[View.Name].Command;
                rs.Fields.Refresh();
                var flds = rs.Fields;

                var col = _array[0] as Column;

                f_TypeName = new DataColumn("TYPE_NAME", typeof(string));
                col._row.Table.Columns.Add(f_TypeName);

                f_AutoKey = new DataColumn("AUTO_INCREMENT", typeof(bool));
                col._row.Table.Columns.Add(f_AutoKey);

                Column c = null;
                Field fld = null;

                var count = _array.Count;
                for (var index = 0; index < count; index++)
                {
                    fld = flds[index];
                    c = (Column)this[fld.Name];

                    c._row["TYPE_NAME"] = fld.Type.ToString();
                    c._row["AUTO_INCREMENT"] = false;
                }

                rs.Close();
                cnn.Close();
            }
        }
        catch
        {
        }
    }
}