using System.Runtime.InteropServices;

namespace MyMeta.Access;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IViews))]
#endif
public class AccessViews : Views
{
    internal override void LoadAll()
    {
        try
        {
            var type = DbRoot.ShowSystemData ? "SYSTEM VIEW" : "VIEW";
            var metaData = LoadData(OleDbSchemaGuid.Tables, new object[] { null, null, null, type });

//				DataTable metaData = this.LoadData(OleDbSchemaGuid.Views, null, null, null, type);


            PopulateArray(metaData);
        }
        catch
        {
        }
    }
}