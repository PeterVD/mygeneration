using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace MyMeta;

/// <summary>
/// Summary description for Collection.
/// </summary>
/// 
public class PropertyCollectionAll : Collection<IProperty>, IPropertyCollection, IEnumerable, IEnumerator, ICollection
{
    private IPropertyCollection _global;
    private IEnumerator _globalEnumerator;

    private IPropertyCollection _local;
    private IEnumerator _localEnumerator;

    private bool _useLocalEnum = true;
    private bool _wereDone;

    internal void Load(IPropertyCollection local, IPropertyCollection global)
    {
        _local = local;
        _global = global;
    }

    #region IPropertyCollection

    public IProperty this[string key]
    {
        get
        {
            if (_local.ContainsKey(key)) return _local[key];

            if (_global.ContainsKey(key)) return _global[key];

            return null;
        }
    }

    /// <summary>
    ///     This method will either add or update a key value pair.  If the key already exists in the collection the value will
    ///     be updated.
    ///     If this key doesn't exist the key/value pair will be added.  If only want to update, and not add new items, use
    ///     <see cref="ContainsKey" /> to determine if the key already exists.
    /// </summary>
    /// <param name="key"></param>
    /// <param name="value"></param>
    /// <returns></returns>
    public IProperty AddKeyValue(string key, string value)
    {
        throw new NotImplementedException("Cannot call AddKeyValue on this collection");
    }

    /// <summary>
    ///     Removes a key/value pair from the collection, no error is thrown if the key doesn't exist.
    /// </summary>
    /// <param name="key">The key of the desired key/value pair</param>
    public void RemoveKey(string key)
    {
        throw new NotImplementedException("Cannot call AddKeyValue on this collection");
    }

    /// <summary>
    ///     Use ContainsKey to determine if a key exists in the collection.
    /// </summary>
    /// <param name="key">The key of the desired key/value pair</param>
    /// <returns>True if the key exists, False if not</returns>
    public bool ContainsKey(string key)
    {
        if (_local.ContainsKey(key)) return true;

        if (_global.ContainsKey(key)) return true;

        return false;
    }

    /// <summary>
    ///     Removes all key/value pairs from the collection.
    /// </summary>
    public new void Clear()
    {
        throw new NotImplementedException("Cannot call AddKeyValue on this collection");
    }

    #endregion

    #region IEnumerable Members

    IEnumerator<IProperty> IEnumerable<IProperty>.GetEnumerator()
    {
        return _array.GetEnumerator();
    }

    public IEnumerator GetEnumerator()
    {
        Reset();
        return this;
    }

    #endregion

    #region IEnumerator Members

    public void Reset()
    {
        _useLocalEnum = true;
        _wereDone = false;
        _localEnumerator = _local.GetEnumerator();
        _globalEnumerator = _global.GetEnumerator();
    }

    public object Current => _useLocalEnum ? (IProperty)_localEnumerator.Current : (IProperty)_globalEnumerator.Current;

    public bool MoveNext()
    {
        if (_useLocalEnum && _localEnumerator.MoveNext()) return true;

        if (!_wereDone)
        {
            _useLocalEnum = false;

            while (true)
            {
                if (_globalEnumerator.MoveNext())
                {
                    var prop = (IProperty)_globalEnumerator.Current;

                    if (!_local.ContainsKey(prop.Key)) return true;
                }
                else
                {
                    break;
                }
            }

            _wereDone = true;
        }

        return false;
    }

    #endregion

    #region ICollection Members

    public new bool IsSynchronized =>
        // TODO:  Add Databases.IsSynchronized getter implementation
        false;

    public new void CopyTo(Array array, int index)
    {
        // TODO:  Add Databases.CopyTo implementation
    }

    public new object SyncRoot =>
        // TODO:  Add Databases.SyncRoot getter implementation
        null;

    #endregion
}