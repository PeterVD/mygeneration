using System;
using System.Collections.Generic;
using System.Data;

namespace MyMeta;

public delegate string ShowOleDbDialogHandler(string connstring);

public class InternalDriver
{
    public ShowOleDbDialogHandler ShowOLEDBDialog;

    internal InternalDriver(Type factory, string connString, bool isOleDB)
    {
        this.factory = factory;
        IsOleDB = isOleDB;
        ConnectString = connString;
    }

    public string OnShowOLEDBDialog(string connstring)
    {
        if (ShowOLEDBDialog is not null) return ShowOLEDBDialog(connstring);
        return connstring;
    }

    public virtual string BrowseConnectionString(string connstr)
    {
        return IsOleDB ? OnShowOLEDBDialog(connstr) : null;
    }

    #region driver properties

    private readonly Type factory;

    public bool RequiredDatabaseName { get; set; }


    public bool StripTrailingNulls { get; set; }


    public IClassFactory CreateBuildInClass()
    {
        if (factory.IsSubclassOf(typeof(IClassFactory)))
            return factory.Assembly.CreateInstance(factory.Name) as IClassFactory;
        return null;
    }

    public IMyMetaPlugin CreateMyMetaPluginClass()
    {
        if (factory.IsSubclassOf(typeof(IMyMetaPlugin)))
            return factory.Assembly.CreateInstance(factory.Name) as IMyMetaPlugin;
        return null;
    }

    public string DriverId { get; private set; }

    public virtual string ConnectString { get; set; }

    public bool IsOleDB { get; protected set; }

    public virtual string GetDataBaseName(IDbConnection con)
    {
        return con.Database;
    }

    #endregion

    #region driver mapping

    public static InternalDriver Register(string driverId, InternalDriver driver)
    {
        internalDrivers[driverId] = driver;
        driver.DriverId = driverId;
        return driver;
    }

    private static readonly Dictionary<string, InternalDriver> internalDrivers = new();

    public static InternalDriver Get(string driverId)
    {
        InternalDriver result;
        if (internalDrivers.TryGetValue(driverId, out result))
            return result;
        return null;
    }

    #endregion
}