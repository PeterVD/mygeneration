using System;
using System.Runtime.InteropServices;

namespace MyMeta.SQLite;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IColumn))]
#endif
public class SQLiteColumn : Column
{
    public override bool IsAutoKey
    {
        get
        {
            if (IsInPrimaryKey && (DataTypeName == "INTEGER" || DataTypeName == "INT"))
                return true;
            return false;
        }
    }


    public override bool IsNullable => Convert.ToBoolean(GetString(Columns.f_IsNullable));

    public override bool HasDefault => Convert.ToBoolean(GetString(Columns.f_HasDefault));


    public override string DataTypeName
    {
        get
        {
            var cols = Columns as SQLiteColumns;
            return GetString(cols.f_TypeName);
        }
    }

    public override string DataTypeNameComplete
    {
        get
        {
            var cols = Columns as SQLiteColumns;
            return GetString(cols.f_TypeNameComplete);
        }
    }

    internal override Column Clone()
    {
        var c = base.Clone();

        return c;
    }
}