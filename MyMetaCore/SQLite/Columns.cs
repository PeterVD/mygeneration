using System.Data;
using System.Runtime.InteropServices;

namespace MyMeta.SQLite;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IColumns))]
#endif
public class SQLiteColumns : Columns
{
    private MetaDataHelper _helper;

    internal DataColumn f_TypeName;
    internal DataColumn f_TypeNameComplete;

    private MetaDataHelper Helper
    {
        get
        {
            if (_helper is null)
            {
                var connection = ConnectionHelper.CreateConnection(DbRoot);

                _helper = new MetaDataHelper(connection);
                connection.Close();
            }

            return _helper;
        }
    }

    internal override void LoadForTable()
    {
        var metaData = Helper.LoadTableColumns(Table.Name);

        PopulateArray(metaData);

        if (metaData.Columns.Contains(MetaDataHelper.DATA_TYPE_NAME))
            f_TypeName = metaData.Columns[MetaDataHelper.DATA_TYPE_NAME];
        if (metaData.Columns.Contains(MetaDataHelper.FULL_DATA_TYPE))
            f_TypeNameComplete = metaData.Columns[MetaDataHelper.FULL_DATA_TYPE];
    }

    internal override void LoadForView()
    {
        var metaData = Helper.LoadViewColumns(View.Name);

        PopulateArray(metaData);

        if (metaData.Columns.Contains(MetaDataHelper.DATA_TYPE_NAME))
            f_TypeName = metaData.Columns[MetaDataHelper.DATA_TYPE_NAME];
        if (metaData.Columns.Contains(MetaDataHelper.FULL_DATA_TYPE))
            f_TypeNameComplete = metaData.Columns[MetaDataHelper.FULL_DATA_TYPE];
    }
}