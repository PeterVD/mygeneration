using System;
using System.Data;
using System.Runtime.InteropServices;

namespace MyMeta.SQLite;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(ITable))]
#endif
public class SQLiteTable : Table
{
    private MetaDataHelper _helper;

    private MetaDataHelper Helper
    {
        get
        {
            if (_helper is null)
            {
                var connection = ConnectionHelper.CreateConnection(DbRoot);
                _helper = new MetaDataHelper(connection);
                connection.Close();
            }

            return _helper;
        }
    }

    public override IColumns PrimaryKeys
    {
        get
        {
            if (null == _primaryKeys)
            {
                var metaData = Helper.LoadTableColumns(Name);

                _primaryKeys = (Columns)DbRoot.ClassFactory.CreateColumns();
                _primaryKeys.Table = this;
                _primaryKeys.DbRoot = DbRoot;

                var colName = "";
                var isKey = false;

                foreach (DataRow row in metaData.Rows)
                    if (!row.IsNull(MetaDataHelper.COLUMN_IS_KEY))
                    {
                        isKey = Convert.ToBoolean(row[MetaDataHelper.COLUMN_IS_KEY]);
                        if (isKey)
                        {
                            colName = row["COLUMN_NAME"] as string;
                            _primaryKeys.AddColumn((Column)Columns[colName]);
                        }
                    }
            }

            return _primaryKeys;
        }
    }
}