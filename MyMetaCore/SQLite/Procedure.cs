using System.Runtime.InteropServices;

namespace MyMeta.SQLite;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IProcedure))]
#endif
public class SQLiteProcedure : Procedure
{
}