using System.Runtime.InteropServices;

namespace MyMeta.SQLite;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IForeignKeys))]
#endif
public class SQLiteForeignKeys : ForeignKeys
{
    private MetaDataHelper _helper;

    private MetaDataHelper Helper
    {
        get
        {
            if (_helper is null)
            {
                var connection = ConnectionHelper.CreateConnection(DbRoot);
                _helper = new MetaDataHelper(connection);
                connection.Close();
            }

            return _helper;
        }
    }

    internal override void LoadAll()
    {
        var metaData = Helper.LoadFKColumns(Table.Name);

        if (metaData is not null) PopulateArray(metaData);
    }
}