using System.Data.SQLite;

namespace MyMeta.SQLite;

/// <summary>
///     Summary description for ConnectionHelper.
/// </summary>
public class ConnectionHelper
{
    //
    public static SQLiteConnection CreateConnection(dbRoot dbRoot)
    {
        var cn = new SQLiteConnection(dbRoot.ConnectionString);
        cn.Open();
        //cn.ChangeDatabase(database);
        return cn;
    }
}