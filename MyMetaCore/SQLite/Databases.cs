using System.Data.SQLite;
using System.Runtime.InteropServices;

namespace MyMeta.SQLite;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IDatabases))]
#endif
public class SQLiteDatabases : Databases
{
    internal override void LoadAll()
    {
        try
        {
            var cn = new SQLiteConnection(DbRoot.ConnectionString);

            // We add our one and only Database
            var database = (SQLiteDatabase)DbRoot.ClassFactory.CreateDatabase();
            database.DbRoot = DbRoot;
            database.Databases = this;
            database._name = cn.Database.Length == 0 ? "main" : cn.Database;
            _array.Add(database);
        }
        catch
        {
        }
    }
}