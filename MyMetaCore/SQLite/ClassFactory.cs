using System.Data;
using System.Data.SQLite;
using System.Runtime.InteropServices;

namespace MyMeta.SQLite;

#if ENTERPRISE

[ComVisible(false)]
#endif
public class ClassFactory : IClassFactory
{
    public ITables CreateTables()
    {
        return new SQLiteTables();
    }

    public ITable CreateTable()
    {
        return new SQLiteTable();
    }

    public IColumn CreateColumn()
    {
        return new SQLiteColumn();
    }

    public IColumns CreateColumns()
    {
        return new SQLiteColumns();
    }

    public IDatabase CreateDatabase()
    {
        return new SQLiteDatabase();
    }

    public IDatabases CreateDatabases()
    {
        return new SQLiteDatabases();
    }

    public IProcedure CreateProcedure()
    {
        return new SQLiteProcedure();
    }

    public IProcedures CreateProcedures()
    {
        return new SQLiteProcedures();
    }

    public IView CreateView()
    {
        return new SQLiteView();
    }

    public IViews CreateViews()
    {
        return new SQLiteViews();
    }

    public IParameter CreateParameter()
    {
        return new SQLiteParameter();
    }

    public IParameters CreateParameters()
    {
        return new SQLiteParameters();
    }

    public IForeignKey CreateForeignKey()
    {
        return new SQLiteForeignKey();
    }

    public IForeignKeys CreateForeignKeys()
    {
        return new SQLiteForeignKeys();
    }

    public IIndex CreateIndex()
    {
        return new SQLiteIndex();
    }

    public IIndexes CreateIndexes()
    {
        return new SQLiteIndexes();
    }

    public IResultColumn CreateResultColumn()
    {
        return new SQLiteResultColumn();
    }

    public IResultColumns CreateResultColumns()
    {
        return new SQLiteResultColumns();
    }

    public IDomain CreateDomain()
    {
        return new SQLiteDomain();
    }

    public IDomains CreateDomains()
    {
        return new SQLiteDomains();
    }

    public IProviderType CreateProviderType()
    {
        return new ProviderType();
    }

    public IProviderTypes CreateProviderTypes()
    {
        return new ProviderTypes();
    }

    public IDbConnection CreateConnection()
    {
        return new SQLiteConnection();
    }

    public static void Register()
    {
        InternalDriver driver = new FileDbDriver
        (typeof(ClassFactory)
            , "Data Source=", "database.db", ";New=False;Compress=True;Synchronous=Off"
            , "SqlLiteDB (*.db)|*.db|all files (*.*)|*.*");
        InternalDriver.Register("SQLITE",
            driver);
    }
}