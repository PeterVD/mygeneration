using System.Runtime.InteropServices;

namespace MyMeta.SQLite;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IParameter))]
#endif
public class SQLiteParameter : Parameter
{
    public override string DataTypeNameComplete =>
        /*switch(this.TypeName)
            {
                case "binary":
                case "char":
                case "nchar":
                case "nvarchar":
                case "varchar":
                case "varbinary":

                    return this.TypeName + "(" + this.CharacterMaxLength + ")";

                case "decimal":
                case "numeric":

                    return this.TypeName + "(" + this.NumericPrecision + "," + this.NumericScale + ")";

                default:*/
        TypeName;
    //}
}