using System.Runtime.InteropServices;
using ADODB;

namespace MyMeta.SQLite;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IDatabase))]
#endif
public class SQLiteDatabase : Database
{
    internal string _name = "";

    public override string Name => _name;

    public override string Alias => _name;

    public override Recordset ExecuteSql(string sql)
    {
        var cn = ConnectionHelper.CreateConnection(DbRoot);

        return ExecuteIntoRecordset(sql, cn);
    }
}