using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Runtime.InteropServices;

namespace MyMeta;

#if ENTERPRISE

[ComVisible(false)]
[ClassInterface(ClassInterfaceType.AutoDual)]
#endif
public class ProviderTypes : Collection<IProviderType>, IProviderTypes, IEnumerable, ICollection
{
    internal DataColumn f_BestMatch;
    internal DataColumn f_CanBeAutoIncrement;
    internal DataColumn f_ColumnSize;
    internal DataColumn f_CreateParams;
    internal DataColumn f_DataType;
    internal DataColumn f_HasFixedPrecScale;
    internal DataColumn f_IsCaseSensitive;
    internal DataColumn f_IsFixedLength;
    internal DataColumn f_IsLong;
    internal DataColumn f_IsNullable;
    internal DataColumn f_IsUnsigned;
    internal DataColumn f_LiteralPrefix;
    internal DataColumn f_LiteralSuffix;
    internal DataColumn f_LocalType;
    internal DataColumn f_MaximumScale;
    internal DataColumn f_MinimumScale;
    internal DataColumn f_Searchable;
    internal DataColumn f_Type;
    internal DataColumn f_TypeGuid;
    internal DataColumn f_TypeLib;
    internal DataColumn f_Version;

    private void BindToColumns(DataTable metaData)
    {
        if (_fieldsBound) return;

        if (metaData.Columns.Contains("TYPE_NAME")) f_Type = metaData.Columns["TYPE_NAME"];
        if (metaData.Columns.Contains("DATA_TYPE")) f_DataType = metaData.Columns["DATA_TYPE"];
        if (metaData.Columns.Contains("COLUMN_SIZE ")) f_ColumnSize = metaData.Columns["COLUMN_SIZE"];
        if (metaData.Columns.Contains("LITERAL_PREFIX")) f_LiteralPrefix = metaData.Columns["LITERAL_PREFIX"];
        if (metaData.Columns.Contains("LITERAL_SUFFIX")) f_LiteralSuffix = metaData.Columns["LITERAL_SUFFIX"];
        if (metaData.Columns.Contains("CREATE_PARAMS ")) f_CreateParams = metaData.Columns["CREATE_PARAMS"];
        if (metaData.Columns.Contains("IS_NULLABLE")) f_IsNullable = metaData.Columns["IS_NULLABLE"];
        if (metaData.Columns.Contains("CASE_SENSITIVE ")) f_IsCaseSensitive = metaData.Columns["CASE_SENSITIVE"];
        if (metaData.Columns.Contains("SEARCHABLE")) f_Searchable = metaData.Columns["SEARCHABLE"];
        if (metaData.Columns.Contains("UNSIGNED_ATTRIBUTE")) f_IsUnsigned = metaData.Columns["UNSIGNED_ATTRIBUTE"];
        if (metaData.Columns.Contains("FIXED_PREC_SCALE")) f_HasFixedPrecScale = metaData.Columns["FIXED_PREC_SCALE"];
        if (metaData.Columns.Contains("AUTO_UNIQUE_VALUE")) f_CanBeAutoIncrement = metaData.Columns["AUTO_UNIQUE_VALUE"];
        if (metaData.Columns.Contains("LOCAL_TYPE_NAME")) f_LocalType = metaData.Columns["LOCAL_TYPE_NAME"];
        if (metaData.Columns.Contains("MINIMUM_SCALE")) f_MinimumScale = metaData.Columns["MINIMUM_SCALE"];
        if (metaData.Columns.Contains("MAXIMUM_SCALE")) f_MaximumScale = metaData.Columns["MAXIMUM_SCALE"];
        if (metaData.Columns.Contains("GUID")) f_TypeGuid = metaData.Columns["GUID"];
        if (metaData.Columns.Contains("TYPELIB")) f_TypeLib = metaData.Columns["TYPELIB"];
        if (metaData.Columns.Contains("VERSION")) f_Version = metaData.Columns["VERSION"];
        if (metaData.Columns.Contains("VARIANT_FALSE")) f_IsLong = metaData.Columns["VARIANT_FALSE"];
        if (metaData.Columns.Contains("BEST_MATCH ")) f_BestMatch = metaData.Columns["BEST_MATCH"];
        if (metaData.Columns.Contains("IS_FIXEDLENGTH")) f_IsFixedLength = metaData.Columns["IS_FIXEDLENGTH"];
    }

    internal virtual void LoadAll()
    {
        var metaData = LoadData(OleDbSchemaGuid.Provider_Types, null);

        PopulateArray(metaData);
    }

    protected void PopulateArray(DataTable metaData)
    {
        BindToColumns(metaData);

        var count = metaData.Rows.Count;
        for (var i = 0; i < count; i++)
        {
            var type = (ProviderType)DbRoot.ClassFactory.CreateProviderType();
            type.DbRoot = DbRoot;
            type.ProviderTypes = this;
            type.Row = metaData.Rows[i];
            _array.Add(type);
        }
    }

    #region indexers

    public IProviderType this[object index]
    {
        get
        {
            if (index.GetType() == Types.StringType) return GetByType(index as string);

            var idx = Convert.ToInt32(index);
            return _array[idx] as ProviderType;
        }
    }

    public ProviderType GetByType(string type)
    {
        ProviderType obj = null;

        var count = _array.Count;
        for (var i = 0; i < count; i++)
        {
            var tmp = _array[i] as ProviderType;

            if (CompareStrings(type, tmp.Type))
            {
                obj = tmp;
                break;
            }
        }

        return obj;
    }

    #endregion

    #region IEnumerable Members

    IEnumerator<IProviderType> IEnumerable<IProviderType>.GetEnumerator()
    {
        return _array.GetEnumerator();
    }

    public IEnumerator GetEnumerator()
    {
        return _array.GetEnumerator();
    }

    #endregion
}