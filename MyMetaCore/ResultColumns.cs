using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Xml;

namespace MyMeta;

public class ResultColumns : Collection<IResultColumn>, IResultColumns, IEnumerable, ICollection
{
    internal Procedure Procedure;

    public IResultColumn this[object index]
    {
        get
        {
            if (index is string s) return GetByPhysicalName(s);

            var idx = Convert.ToInt32(index);
            return _array[idx];
        }
    }

    #region IList Members

    object IList.this[int index]
    {
        get => this[index];
        set { }
    }

    #endregion

    internal virtual void LoadAll()
    {
    }

    internal ResultColumn GetByPhysicalName(string name)
    {
        ResultColumn obj = null;

        var count = _array.Count;
        for (var i = 0; i < count; i++)
        {
            var tmp = _array[i] as ResultColumn;

            if (CompareStrings(name, tmp.Name))
            {
                obj = tmp;
                break;
            }
        }

        return obj;
    }

    #region XML User Data

    public override string UserDataXPath => $@"{Procedure.UserDataXPath}/ResultColumns";

    internal override bool GetXmlNode(out XmlNode node, bool forceCreate)
    {
        node = null;
        var success = false;

        if (null == _xmlNode)
        {
            // Get the parent node
            if (Procedure.GetXmlNode(out var parentNode, forceCreate))
            {
                // See if our user data already exists
                const string xPath = @"./ResultColumns";
                if (!GetUserData(xPath, parentNode, out _xmlNode) && forceCreate)
                {
                    // Create it, and try again
                    CreateUserMetaData(parentNode);
                    GetUserData(xPath, parentNode, out _xmlNode);
                }
            }
        }

        if (null != _xmlNode)
        {
            node = _xmlNode;
            success = true;
        }

        return success;
    }

    public override void CreateUserMetaData(XmlNode parentNode)
    {
        var myNode = parentNode.OwnerDocument.CreateNode(XmlNodeType.Element, "ResultColumns", null);
        parentNode.AppendChild(myNode);
    }

    #endregion

    #region IEnumerable Members

    IEnumerator<IResultColumn> IEnumerable<IResultColumn>.GetEnumerator()
    {
        return _array.GetEnumerator();
    }

    public IEnumerator GetEnumerator()
    {
        return _array.GetEnumerator();
    }

    #endregion
}