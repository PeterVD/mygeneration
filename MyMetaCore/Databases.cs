using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Runtime.InteropServices;
using System.Xml;

namespace MyMeta;

#if ENTERPRISE

[ComVisible(false)]
[ClassInterface(ClassInterfaceType.AutoDual)]
#endif
public class Databases : Collection<IDatabase>, IDatabases, IEnumerable, ICollection
{
    internal DataColumn f_Catalog;
    internal DataColumn f_DefCharSetCat;
    internal DataColumn f_DefCharSetName;
    internal DataColumn f_DefCharSetSchema;
    internal DataColumn f_Description;
    internal DataColumn f_SchemaName;
    internal DataColumn f_SchemaOwner;

    object IList.this[int index]
    {
        get => this[index];
        set { }
    }

    private void BindToColumns(DataTable metaData)
    {
        if (_fieldsBound) return;

        if (metaData.Columns.Contains("CATALOG_NAME")) f_Catalog = metaData.Columns["CATALOG_NAME"];
        if (metaData.Columns.Contains("DESCRIPTION")) f_Description = metaData.Columns["DESCRIPTION"];
        if (metaData.Columns.Contains("SCHEMA_NAME")) f_SchemaName = metaData.Columns["SCHEMA_NAME"];
        if (metaData.Columns.Contains("SCHEMA_OWNER")) f_SchemaOwner = metaData.Columns["SCHEMA_OWNER"];
        if (metaData.Columns.Contains("DEFAULT_CHARACTER_SET_CATALOG")) f_DefCharSetCat = metaData.Columns["DEFAULT_CHARACTER_SET_CATALOG"];
        if (metaData.Columns.Contains("DEFAULT_CHARACTER_SET_SCHEMA")) f_DefCharSetSchema = metaData.Columns["DEFAULT_CHARACTER_SET_SCHEMA"];
        if (metaData.Columns.Contains("DEFAULT_CHARACTER_SET_NAME")) f_DefCharSetName = metaData.Columns["DEFAULT_CHARACTER_SET_NAME"];

        f_SchemaName ??= metaData.Columns.Add("SCHEMA_NAME", Types.StringType);
        f_SchemaOwner ??= metaData.Columns.Add("SCHEMA_OWNER", Types.StringType);
        f_DefCharSetCat ??= metaData.Columns.Add("DEFAULT_CHARACTER_SET_CATALOG", Types.StringType);
        f_DefCharSetSchema ??= metaData.Columns.Add("DEFAULT_CHARACTER_SET_SCHEMA", Types.StringType);
        f_DefCharSetName ??= metaData.Columns.Add("DEFAULT_CHARACTER_SET_NAME", Types.StringType);
    }

    internal virtual void LoadAll()
    {
    }

    internal void PopulateArray(DataTable metaData)
    {
        BindToColumns(metaData);

        var count = metaData.Rows.Count;
        for (var i = 0; i < count; i++)
        {
            var database = (Database)DbRoot.ClassFactory.CreateDatabase();
            database.DbRoot = DbRoot;
            database.Databases = this;
            database.Row = metaData.Rows[i];
            _array.Add(database);
        }

        PopulateSchemaData();
    }

    internal virtual void PopulateSchemaData()
    {
        for (var i = 0; i < Count; i++)
        {
            var db = this[i] as Database;
            var dt = LoadData(OleDbSchemaGuid.Schemata, [db.Name]);

            if (dt.Rows.Count == 1)
            {
                db._row[f_SchemaName] = dt.Rows[0]["SCHEMA_NAME"];
                db._row[f_SchemaOwner] = dt.Rows[0]["SCHEMA_OWNER"];
                db._row[f_DefCharSetCat] = dt.Rows[0]["DEFAULT_CHARACTER_SET_CATALOG"];
                db._row[f_DefCharSetSchema] = dt.Rows[0]["DEFAULT_CHARACTER_SET_SCHEMA"];
                db._row[f_DefCharSetName] = dt.Rows[0]["DEFAULT_CHARACTER_SET_NAME"];
            }
        }
    }

    internal void AddDatabase(Database database)
    {
        _array.Add(database);
    }

    public override string UserDataXPath => $@"{dbRoot.UserDataXPath}/Databases";

    internal override bool GetXmlNode(out XmlNode node, bool forceCreate)
    {
        node = null;
        var success = false;

        if (null == _xmlNode)
        {
            // Get the parent node
            if (DbRoot.GetXmlNode(out var parentNode, forceCreate))
            {
                // See if our user data already exists
                const string xPath = @"./Databases";
                if (!GetUserData(xPath, parentNode, out _xmlNode) && forceCreate)
                {
                    // Create it, and try again
                    CreateUserMetaData(parentNode);
                    GetUserData(xPath, parentNode, out _xmlNode);
                }
            }
        }

        if (null != _xmlNode)
        {
            node = _xmlNode;
            success = true;
        }

        return success;
    }

    public override void CreateUserMetaData(XmlNode parentNode)
    {
        var myNode = parentNode.OwnerDocument.CreateNode(XmlNodeType.Element, "Databases", null);
        parentNode.AppendChild(myNode);
    }


    public virtual IDatabase this[object index]
    {
        get
        {
            if (index.GetType() == Types.StringType) return GetByPhysicalName(index as string);

            var idx = Convert.ToInt32(index);
            return _array[idx];
        }
    }

    public virtual Database GetByName(string name)
    {
        Database obj = null;

        var count = _array.Count;
        for (var i = 0; i < count; i++)
        {
            var tmp = _array[i] as Database;
            if (CompareStrings(name, tmp.Name))
            {
                obj = tmp;
                break;
            }
        }

        return obj;
    }

    internal Database GetByPhysicalName(string name)
    {
        Database obj = null;

        var count = _array.Count;
        for (var i = 0; i < count; i++)
        {
            var tmp = _array[i] as Database;
            if (CompareStrings(name, tmp.Name))
            {
                obj = tmp;
                break;
            }
        }

        return obj;
    }

    IEnumerator<IDatabase> IEnumerable<IDatabase>.GetEnumerator()
    {
        return _array.GetEnumerator();
    }

    public IEnumerator GetEnumerator()
    {
        return _array.GetEnumerator();
    }
}