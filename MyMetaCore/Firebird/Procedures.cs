using System;
using System.Runtime.InteropServices;
using FirebirdSql.Data.FirebirdClient;

namespace MyMeta.Firebird;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IProcedures))]
#endif
public class FirebirdProcedures : Procedures
{
    public override IProcedure this[object name] => base[name];

    internal override void LoadAll()
    {
        try
        {
            var cn = new FbConnection(DbRoot.ConnectionString);
            cn.Open();
            var metaData = cn.GetSchema("Procedures", new[] { Database.Name });
            cn.Close();

            PopulateArray(metaData);
        }
        catch (Exception ex)
        {
            var m = ex.Message;
        }
    }
}