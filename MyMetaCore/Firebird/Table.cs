using System;
using System.Data;
using System.Runtime.InteropServices;
using FirebirdSql.Data.FirebirdClient;

namespace MyMeta.Firebird;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(ITable))]
#endif
public class FirebirdTable : Table
{
    public override IColumns PrimaryKeys
    {
        get
        {
            if (null == _primaryKeys)
            {
                _primaryKeys = (Columns)DbRoot.ClassFactory.CreateColumns();
                _primaryKeys.Table = this;
                _primaryKeys.DbRoot = DbRoot;

                try
                {
                    var cn = new FbConnection(DbRoot.ConnectionString);
                    cn.Open();
                    var metaData = cn.GetSchema("PrimaryKeys", new[] { null, null, Name });
                    cn.Close();

                    string colName;
                    Column c;
                    foreach (DataRow row in metaData.Rows)
                    {
                        colName = row["COLUMN_NAME"] as string;

                        c = Columns[colName] as Column;

                        _primaryKeys.AddColumn(c);
                    }
                }
                catch (Exception ex)
                {
                    var m = ex.Message;
                }
            }

            return _primaryKeys;
        }
    }
}