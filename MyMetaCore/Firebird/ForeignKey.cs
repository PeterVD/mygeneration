using System.Runtime.InteropServices;

namespace MyMeta.Firebird;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IForeignKey))]
#endif
public class FirebirdForeignKey : ForeignKey
{
    public override ITable ForeignTable
    {
        get
        {
            var catalog = ForeignKeys.Table.Database.Name;
            var schema = GetString(ForeignKeys.f_FKTableSchema);

            return DbRoot.Databases[0].Tables[GetString(ForeignKeys.f_FKTableName)];
        }
    }
}