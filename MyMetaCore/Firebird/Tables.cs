using System;
using System.Runtime.InteropServices;
using FirebirdSql.Data.FirebirdClient;

namespace MyMeta.Firebird;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(ITables))]
#endif
public class FirebirdTables : Tables
{
    internal override void LoadAll()
    {
        try
        {
            var type = DbRoot.ShowSystemData ? "SYSTEM TABLE" : "TABLE";

            var cn = new FbConnection(DbRoot.ConnectionString);
            cn.Open();
            var metaData = cn.GetSchema("Tables", new[] { null, null, null, type });
            cn.Close();

            PopulateArray(metaData);
        }
        catch (Exception ex)
        {
            var m = ex.Message;
        }
    }
}