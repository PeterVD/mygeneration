using FirebirdSql.Data.FirebirdClient;

namespace MyMeta.Firebird;

public class FirebirdDatabases : Databases
{
    internal override void LoadAll()
    {
        try
        {
            using var cn = new FbConnection(DbRoot.ConnectionString);
            cn.Open();
            var dbName = cn.Database;
            cn.Close();

            var index = dbName.LastIndexOfAny(['\\']);

            if (index >= 0) dbName = dbName[(index + 1)..];

            // We add our one and only Database
            var database = (FirebirdDatabase)DbRoot.ClassFactory.CreateDatabase();
            database._name = dbName;
            database.DbRoot = DbRoot;
            database.Databases = this;
            _array.Add(database);
        }
        catch
        {
        }
    }
}