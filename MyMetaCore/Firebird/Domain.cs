using System.Runtime.InteropServices;

namespace MyMeta.Firebird;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IDomain))]
#endif
public class FirebirdDomain : Domain
{
    public override string DataTypeNameComplete
    {
        get
        {
            var domains = Domains as FirebirdDomains;
            return GetString(domains.f_TypeNameComplete);
        }
    }

    public override int CharacterMaxLength
    {
        get
        {
            switch (DataTypeName)
            {
                case "VARCHAR":
                case "CHAR":
                    return (int)_row["DOMAIN_SIZE"];

                default:
                    return GetInt32(Domains.f_MaxLength);
            }
        }
    }

    public override int NumericPrecision
    {
        get
        {
            switch (DataTypeName)
            {
                case "VARCHAR":
                case "CHAR":
                    return 0;

                default:
                    return base.NumericPrecision;
            }
        }
    }
}