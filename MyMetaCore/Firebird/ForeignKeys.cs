using System;
using System.Data;
using System.Runtime.InteropServices;
using FirebirdSql.Data.FirebirdClient;

namespace MyMeta.Firebird;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IForeignKeys))]
#endif
public class FirebirdForeignKeys : ForeignKeys
{
    internal override void LoadAll()
    {
        try
        {
            var cn = new FbConnection(DbRoot.ConnectionString);
            cn.Open();
            var metaData1 = cn.GetSchema("ForeignKeys", new[] { null, null, Table.Name });
            var metaData2 = cn.GetSchema("ForeignKeys", new[] { null, null, null, null, null, Table.Name });
            cn.Close();

            var rows = metaData2.Rows;
            var count = rows.Count;
            for (var i = 0; i < count; i++) metaData1.ImportRow(rows[i]);

            PopulateArrayNoHookup(metaData1);

            ForeignKey key = null;
            var keyName = "";

            foreach (DataRow row in metaData1.Rows)
            {
                keyName = row["FK_NAME"] as string;

                key = GetByName(keyName);

                key.AddForeignColumn(null, null, (string)row["PK_TABLE_NAME"], (string)row["PK_COLUMN_NAME"], true);
                key.AddForeignColumn(null, null, (string)row["FK_TABLE_NAME"], (string)row["FK_COLUMN_NAME"], false);
            }
        }
        catch (Exception ex)
        {
            var m = ex.Message;
        }
    }
}