using System;

namespace MyMeta;

internal static class Types
{
    public static Type StringType = typeof(string);
    public static Type GuidType = typeof(Guid);
    public static Type ByteType = typeof(byte);
    public static Type DateTimeType = typeof(DateTime);
    public static Type Int64Type = typeof(long);
    public static Type Int32Type = typeof(int);
    public static Type Int16Type = typeof(short);
    public static Type BoolType = typeof(bool);
    public static Type DecimalType = typeof(decimal);
}