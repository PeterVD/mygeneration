using System;
using System.Xml;

namespace MyMeta;

public class ForeignKey : Single, IForeignKey
{
    protected Columns _foreignColumns;
    protected Columns _primaryColumns;
    internal ForeignKeys ForeignKeys;

    public virtual IColumns PrimaryColumns => _primaryColumns;

    public virtual IColumns ForeignColumns => _foreignColumns;

    internal virtual void AddForeignColumn(string catalog, string schema,
        string physicalTableName, string physicalColumnName, bool primary)
    {
        Tables tables = null;

        if (catalog is { Length: > 0 } || schema is { Length: > 0 })
        {
            var catSchema = catalog != string.Empty ? catalog : schema;
            tables = DbRoot.Databases[catSchema].Tables as Tables;
        }
        else
        {
            // This DBMS is a one horse database
            tables = (Tables)ForeignKeys.Table.Database.Tables;
        }

        var column = tables[physicalTableName].Columns[physicalColumnName] as Column;
        var c = column.Clone();

        if (primary)
        {
            if (null == _primaryColumns)
            {
                _primaryColumns = (Columns)DbRoot.ClassFactory.CreateColumns();
                _primaryColumns.ForeignKey = this;
            }

            _primaryColumns.AddColumn(c);
        }
        else
        {
            if (null == _foreignColumns)
            {
                _foreignColumns = (Columns)DbRoot.ClassFactory.CreateColumns();
                _foreignColumns.ForeignKey = this;
            }

            _foreignColumns.AddColumn(c);
        }

        column.AddForeignKey(this);
    }

    #region Objects

    public virtual ITable PrimaryTable
    {
        get
        {
            var catSchema = "";

            try
            {
                var catalog = DBNull.Value == _row["PK_TABLE_CATALOG"] ? string.Empty : _row["PK_TABLE_CATALOG"] as string;
                var schema = DBNull.Value == _row["PK_TABLE_SCHEMA"] ? string.Empty : _row["PK_TABLE_SCHEMA"] as string;

                catSchema = catalog is { Length: > 0 } || schema is { Length: > 0 }
                    ? !string.IsNullOrWhiteSpace(catalog) ? catalog : schema
                    : ForeignKeys.Table.Database.Name;
            }
            catch
            {
                catSchema = ForeignKeys.Table.Database.Name;
            }

            return DbRoot.Databases[catSchema].Tables[GetString(ForeignKeys.f_PKTableName)];
        }
    }

    public virtual ITable ForeignTable
    {
        get
        {
            var catSchema = "";

            try
            {
                var catalog = DBNull.Value == _row["FK_TABLE_CATALOG"] ? string.Empty : _row["FK_TABLE_CATALOG"] as string;
                var schema = DBNull.Value == _row["FK_TABLE_SCHEMA"] ? string.Empty : _row["FK_TABLE_SCHEMA"] as string;

                catSchema = catalog is { Length: > 0 } || schema is { Length: > 0 }
                    ? !string.IsNullOrWhiteSpace(catalog) ? catalog : schema
                    : ForeignKeys.Table.Database.Name;
            }
            catch
            {
                catSchema = ForeignKeys.Table.Database.Name;
            }

            return DbRoot.Databases[catSchema].Tables[GetString(ForeignKeys.f_FKTableName)];
        }
    }

    #endregion

    #region Collections

    public virtual IPropertyCollection GlobalProperties
    {
        get
        {
            var db = ForeignKeys.Table.Tables.Database;
            if (null != db._foreignkeyProperties) return db._foreignkeyProperties;
            db._foreignkeyProperties = new PropertyCollection
            {
                Parent = this
            };

            var xPath = GlobalUserDataXPath;
            var xmlNode = DbRoot.UserData.SelectSingleNode(xPath, null);

            if (xmlNode is null)
            {
                var parentNode = db.CreateGlobalXmlNode();

                xmlNode = parentNode.OwnerDocument.CreateNode(XmlNodeType.Element, "ForeignKey", null);
                parentNode.AppendChild(xmlNode);
            }

            db._foreignkeyProperties.LoadAllGlobal(xmlNode);

            return db._foreignkeyProperties;
        }
    }

    public virtual IPropertyCollection AllProperties
    {
        get
        {
            if (null != _allProperties) return _allProperties;

            _allProperties = new PropertyCollectionAll();
            _allProperties.Load(Properties, GlobalProperties);

            return _allProperties;
        }
    }

    internal PropertyCollectionAll _allProperties;

    #endregion

    #region Properties

    public override string Alias
    {
        get { return GetXmlNode(out var node, false) && GetUserData(node, "n", out var niceName) && string.Empty != niceName ? niceName : Name; }

        set
        {
            if (GetXmlNode(out var node, true))
            {
                SetUserData(node, "n", value);
            }
        }
    }

    public override string Name => GetString(ForeignKeys.f_FKName);

    public virtual int Ordinal => GetInt32(ForeignKeys.f_Ordinal);

    public virtual string UpdateRule => GetString(ForeignKeys.f_UpdateRule);

    public virtual string DeleteRule => GetString(ForeignKeys.f_DeleteRule);

    public virtual string PrimaryKeyName => GetString(ForeignKeys.f_PKName);

    public virtual string Deferrability
    {
        get
        {
            var i = GetInt16(ForeignKeys.f_Deferrability);

            return i switch
            {
                1 => "INITIALLY_DEFERRED",
                2 => "INITIALLY_IMMEDIATE",
                3 => "NOT_DEFERRABLE",
                _ => "UNKNOWN"
            };
        }
    }

    #endregion

    #region XML User Data

    public override string UserDataXPath => ForeignKeys.UserDataXPath + @"/ForeignKey[@p='" + Name + "']";

    public override string GlobalUserDataXPath => ForeignKeys.Table.Tables.Database.GlobalUserDataXPath + "/ForeignKey";

    internal override bool GetXmlNode(out XmlNode node, bool forceCreate)
    {
        node = null;
        var success = false;

        if (null == _xmlNode)
        {
            // Get the parent node
            if (ForeignKeys.GetXmlNode(out var parentNode, forceCreate))
            {
                // See if our user data already exists
                var xPath = $@"./ForeignKey[@p='{Name}']";
                if (!GetUserData(xPath, parentNode, out _xmlNode) && forceCreate)
                {
                    // Create it, and try again
                    CreateUserMetaData(parentNode);
                    GetUserData(xPath, parentNode, out _xmlNode);
                }
            }
        }

        if (null != _xmlNode)
        {
            node = _xmlNode;
            success = true;
        }

        return success;
    }

    public override void CreateUserMetaData(XmlNode parentNode)
    {
        var myNode = parentNode.OwnerDocument.CreateNode(XmlNodeType.Element, "ForeignKey", null);
        parentNode.AppendChild(myNode);

        var attr = parentNode.OwnerDocument.CreateAttribute("p");
        attr.Value = Name;
        myNode.Attributes.Append(attr);

        attr = parentNode.OwnerDocument.CreateAttribute("n");
        attr.Value = "";
        myNode.Attributes.Append(attr);
    }

    #endregion

    #region INameValueCollection Members

    public string ItemName => Name;

    public string ItemValue => Name;

    #endregion
}