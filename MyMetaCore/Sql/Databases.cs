namespace MyMeta.Sql;

public class SqlDatabases : Databases
{
    internal override void LoadAll()
    {
        var metaData = LoadData(OleDbSchemaGuid.Catalogs, null);

        PopulateArray(metaData);
    }
}