using System.Runtime.InteropServices;

namespace MyMeta.Sql;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(ITable))]
#endif
public class SqlTable : Table
{
    public override IColumns PrimaryKeys
    {
        get
        {
            if (null == _primaryKeys)
            {
                var metaData = LoadData(OleDbSchemaGuid.Primary_Keys,
                    new object[] { Tables.Database.Name, Schema, Name });

                _primaryKeys = (Columns)DbRoot.ClassFactory.CreateColumns();
                _primaryKeys.Table = this;
                _primaryKeys.DbRoot = DbRoot;

                var colName = "";

                var count = metaData.Rows.Count;
                for (var i = 0; i < count; i++)
                {
                    colName = metaData.Rows[i]["COLUMN_NAME"] as string;
                    _primaryKeys.AddColumn((Column)Columns[colName]);
                }
            }

            return _primaryKeys;
        }
    }

    public override object DatabaseSpecificMetaData(string key)
    {
        return SqlDatabase.DBSpecific(key, this);
    }
}