using System.Runtime.InteropServices;

namespace MyMeta.Sql;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IForeignKeys))]
#endif
public class SqlForeignKeys : ForeignKeys
{
    internal override void LoadAll()
    {
        try
        {
            var metaData1 = LoadData(OleDbSchemaGuid.Foreign_Keys,
                new object[] { Table.Database.Name, Table.Schema, Table.Name });

            var metaData2 = LoadData(OleDbSchemaGuid.Foreign_Keys,
                new object[] { null, null, null, Table.Database.Name, Table.Schema, Table.Name });

            var rows = metaData2.Rows;
            var count = rows.Count;
            for (var i = 0; i < count; i++) metaData1.ImportRow(rows[i]);

            PopulateArray(metaData1);
        }
        catch
        {
        }
    }
}