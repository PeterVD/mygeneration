using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Data.SqlClient;

namespace MyMeta.Sql;

public class SqlResultColumns : ResultColumns
{
    internal override void LoadAll()
    {
        try
        {
            var schema = Procedure.Schema.IndexOf(".") == -1 ? $"{Procedure.Schema}." : string.Empty;
            
            //SET FMTONLY ON 
            var select = $"EXEC [{Procedure.Database.Name}].{schema}[{Procedure.Name}] ";

            var paramCount = Procedure.Parameters.Count;

            if (paramCount > 0)
            {
                var parameters = Procedure.Parameters;
                var c = parameters.Count;
                for (var i = 0; i < c; i++)
                {
                    if (parameters[i].Direction == ParamDirection.ReturnValue) paramCount--;
                }
            }

            for (var i = 0; i < paramCount; i++)
            {
                if (i > 0) select += ",";

                select += "null";
            }

            var metaData = new DataTable();

            try
            {
                //Provider=SQLOLEDB.1;Persist Security Info=False;User ID=sa;Initial Catalog=Northwind;Data Source=localhost
                //Data Source=myServerAddress;Initial Catalog=myDataBase;User Id=myUsername;Password=myPassword;
                //Provider=SQLNCLI;Server=myServerAddress;Database=myDataBase;Uid=myUsername;Pwd=myPassword;
                //Data Source=myServerAddress;Initial Catalog=myDataBase;User Id=myUsername;Password=myPassword;

                var parts = DbRoot.ConnectionString.Split(';');
                var conn = new Dictionary<string, string>(parts.Length);
                foreach (var part in parts)
                {
                    var idx = part.IndexOf('=');
                    if (idx > 0)
                    {
                        var name = part[..idx];
                        var val = part[(idx + 1)..];
                        conn[name.Trim()] = val.Trim();
                    }
                }

                StringBuilder cn = new();
                foreach (var keyValue in conn)
                {
                    switch (keyValue.Key.ToLower())
                    {
                        case "provider":
                        case "extended properties":
                            break;
                        case "server":
                        case "data source":
                            cn.Append($"Data Source={keyValue.Value};");
                            break;
                        case "user id":
                        case "uid":
                            cn.Append($"User ID={keyValue.Value};");
                            break;
                        case "password":
                        case "pwd":
                            cn.Append($"Password={keyValue.Value};");
                            break;
                        case "initial catalog":
                        case "database":
                            cn.Append($"Initial Catalog={keyValue.Value};");
                            break;
                        case "marsconn":
                            if (string.Equals(keyValue.Value, "yes", StringComparison.InvariantCultureIgnoreCase))
                            {
                                cn.Append("MultipleActiveResultSets=true;");
                            }
                            break;
                        default:
                            cn.Append($"{keyValue.Key}={keyValue.Value};");
                            break;
                    }
                }

                using var sqlconn = new SqlConnection(cn.ToString());
                sqlconn.Open();
                using var sqlcmd = sqlconn.CreateCommand();
                sqlcmd.CommandText = select;
                sqlcmd.CommandType = CommandType.Text;
                using var reader = sqlcmd.ExecuteReader(CommandBehavior.SchemaOnly);

                metaData = reader.GetSchemaTable();
                foreach (DataRow row in metaData.Rows)
                {
                    var resultColumn = DbRoot.ClassFactory.CreateResultColumn() as SqlResultColumn;
                    resultColumn.DbRoot = DbRoot;
                    resultColumn.ResultColumns = this;
                    resultColumn._row = row;
                    _array.Add(resultColumn);
                }
            }
            catch
            {
                //
            }
        }
        catch
        {
        }
    }
}