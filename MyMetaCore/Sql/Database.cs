namespace MyMeta.Sql;

public class SqlDatabase : Database
{
    internal static object DBSpecific(string key, Single single)
    {
        if (key != DatabaseSpecific.EXTENDED_PROPERTIES) return null;

        object retVal = null;
        var dext = new DatabaseSpecific();

        return single switch
        {
            IColumn column => dext.ExtendedProperties(column),
            ITable table => dext.ExtendedProperties(table),
            IProcedure procedure => dext.ExtendedProperties(procedure),
            IView view => dext.ExtendedProperties(view),
            _ => retVal
        };
    }
}