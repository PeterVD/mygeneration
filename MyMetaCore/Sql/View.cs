using System.Data;
using System.Runtime.InteropServices;

namespace MyMeta.Sql;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IView))]
#endif
public class SqlView : View
{
    public override string ViewText
    {
        get
        {
            var tmp = base.ViewText;
            if (tmp.Length == 0) tmp = LoadViewSource();
            return tmp;
        }
    }

    public override IViews SubViews
    {
        get
        {
            if (!_subViewInfoLoaded) LoadSubViewInfo();
            return _views;
        }
    }

    public override ITables SubTables
    {
        get
        {
            if (!_subViewInfoLoaded) LoadSubViewInfo();
            return _tables;
        }
    }

    private string LoadViewSource()
    {
        var text = string.Empty;
        OleDbConnection cn = null;
        OleDbDataReader reader = null;
        try
        {
            var select = string.Format(@"SELECT CASE WHEN encrypted = 1 THEN NULL ELSE com.text END as Source FROM sysobjects o, syscomments com 
WHERE o.id = object_id(N'[{0}].[{1}]')
and com.id=o.id 
and com.status=2 
order by colid;", Schema, Name);
            cn = new OleDbConnection(DbRoot.ConnectionString);
            cn.Open();
            cn.ChangeDatabase(Database.Name);

            OleDbCommand cmd = cn.CreateCommand();
            cmd.CommandText = select;

            try
            {
                reader = cmd.ExecuteReader();

                while (reader.Read()) text += reader[0].ToString();

                reader.Close();
            }
            catch
            {
                if (reader is not null)
                    reader.Close();
            }

            if (text == string.Empty)
            {
                select = string.Format(@"SELECT CASE WHEN encrypted = 1 THEN NULL ELSE com.text END as Source FROM sysobjects o, syscomments com 
WHERE o.id = object_id(N'[{0}].[{1}]')
and com.id=o.id 
order by colid;", Schema, Name);

                cmd = cn.CreateCommand();
                cmd.CommandText = select;
                reader = cmd.ExecuteReader();

                while (reader.Read()) text += reader[0].ToString();

                reader.Close();
                cn.Close();
            }

            cn.Close();

            text = text.TrimStart(' ', '\r', '\n', '\t');
        }
        catch
        {
            if (reader is not null)
                reader.Close();
            if (cn is not null && cn.State != ConnectionState.Closed && cn.State != ConnectionState.Broken)
                cn.Close();
        }

        return text;
    }

    private void LoadSubViewInfo()
    {
        _views = (Views)DbRoot.ClassFactory.CreateViews();
        _views.DbRoot = DbRoot;
        _views.Database = Views.Database;

        _tables = (Tables)DbRoot.ClassFactory.CreateTables();
        _tables.DbRoot = DbRoot;
        _tables.Database = Views.Database;

        try
        {
            var select = "SELECT * FROM INFORMATION_SCHEMA.VIEW_TABLE_USAGE WHERE VIEW_NAME = '"
                         + Name + "' AND VIEW_SCHEMA = '" + Schema + "';";

            OleDbConnection cn = new OleDbConnection(DbRoot.ConnectionString);
            cn.Open();
            cn.ChangeDatabase(Database.Name);

            OleDbDataAdapter adapter = new OleDbDataAdapter(select, cn);
            var dataTable = new DataTable();

            adapter.Fill(dataTable);

            cn.Close();

            var entity = "";

            Table table;
            View view;

            foreach (DataRow row in dataTable.Rows)
            {
                entity = row["TABLE_NAME"] as string;

                // It might be a table or a view
                table = Database.Tables[entity] as Table;

                if (null != table)
                {
                    // It's a table
                    _tables.AddTable(table);
                }
                else
                {
                    // Check for View
                    view = Database.Views[entity] as View;

                    if (null != view)
                        // It's a table
                        _views.AddView(view);
                }
            }
        }
        catch
        {
        }
    }

    public override object DatabaseSpecificMetaData(string key)
    {
        return SqlDatabase.DBSpecific(key, this);
    }
}