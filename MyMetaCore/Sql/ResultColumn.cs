using System;
using System.Data;
using System.Runtime.InteropServices;

namespace MyMeta.Sql;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IResultColumn))]
#endif
public class SqlResultColumn : ResultColumn
{
    internal DataColumn _column;

    internal DataRow _row;

    #region Properties

    //ColumnName ColumnOrdinal ColumnSize NumericPrecision NumericScale IsUnique IsKey BaseServerName BaseCatalogName BaseColumnName BaseSchemaName BaseTableName DataType AllowDBNull ProviderType IsAliased IsExpression IsIdentity IsAutoIncrement IsRowVersion IsHidden IsLong IsReadOnly ProviderSpecificDataType DataTypeName XmlSchemaCollectionDatabase XmlSchemaCollectionOwningSchema XmlSchemaCollectionName UdtAssemblyQualifiedName NonVersionedProviderType 
    public override string Name
    {
        get
        {
            if (_row is not null) return _row["ColumnName"].ToString();

            return _column.ColumnName;
        }
    }

    public override string DataTypeName
    {
        get
        {
            if (_row is not null) return _row["DataTypeName"].ToString();

            return _column.DataType.ToString();
        }
    }

    public override string DataTypeNameComplete => SqlColumn.GetFullDataTypeName(DataTypeName, CharacterMaxLength, NumericPrecision, NumericScale);

    public override int Ordinal
    {
        get
        {
            if (_row is not null) return Convert.ToInt32(_row["ColumnOrdinal"]);

            return _column.Ordinal;
        }
    }

    public int CharacterMaxLength
    {
        get
        {
            try
            {
                return Convert.ToInt32(_row["ColumnSize"]);
            }
            catch
            {
            }

            return 0;
        }
    }

    public int CharacterOctetLength
    {
        get
        {
            if (DataTypeName.StartsWith("n", StringComparison.CurrentCultureIgnoreCase)) return CharacterMaxLength * 2;

            return CharacterMaxLength;
        }
    }

    public int NumericPrecision
    {
        get
        {
            try
            {
                var i = Convert.ToInt32(_row["NumericPrecision"]);
                if (i < 255) return i;
            }
            catch
            {
            }

            return 0;
        }
    }

    public int NumericScale
    {
        get
        {
            try
            {
                var i = Convert.ToInt32(_row["NumericScale"]);
                if (i < 255) return i;
            }
            catch
            {
            }

            return 0;
        }
    }

    #endregion
}