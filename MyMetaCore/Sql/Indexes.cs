using System.Runtime.InteropServices;

namespace MyMeta.Sql;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IIndexes))]
#endif
public class SqlIndexes : Indexes
{
    internal override void LoadAll()
    {
        try
        {
            var metaData = LoadData(OleDbSchemaGuid.Indexes,
                new object[] { Table.Database.Name, Table.Schema, null, null, Table.Name });

            PopulateArray(metaData);
        }
        catch
        {
        }
    }
}