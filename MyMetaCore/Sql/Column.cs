namespace MyMeta.Sql;

public class SqlColumn : Column
{
    public override bool IsAutoKey
    {
        get
        {
            var cols = Columns as SqlColumns;
            return GetBool(cols.f_AutoKey);
        }
    }

    public override bool IsComputed
    {
        get
        {
            if (DataTypeName == "timestamp") return true;

            return GetBool(Columns.f_IsComputed);
        }
    }


    public override string DataTypeName
    {
        get
        {
            if (DbRoot.DomainOverride && HasDomain && Domain is not null) return Domain.DataTypeName;

            var cols = Columns as SqlColumns;
            return GetString(cols.f_TypeName);
        }
    }

    public override string DataTypeNameComplete
    {
        get
        {
            if (DbRoot.DomainOverride && HasDomain && Domain is not null) return Domain.DataTypeNameComplete;

            var dtnf = GetFullDataTypeName(DataTypeName, CharacterMaxLength, NumericPrecision, NumericScale);
            /*switch(this.DataTypeName)
            {
                case "varchar":
                case "nvarchar":
                case "varbinary":
                    if (this.CharacterMaxLength > 1000000)
                        dtnf = this.DataTypeName + "(MAX)";
                    else
                        dtnf = this.DataTypeName + "(" + this.CharacterMaxLength + ")";
                    break;
                case "binary":
                case "char":
                case "nchar":

                    dtnf = this.DataTypeName + "(" + this.CharacterMaxLength + ")";
                    break;

                case "decimal":
                case "numeric":

                    dtnf = this.DataTypeName + "(" + this.NumericPrecision + "," + this.NumericScale + ")";
                    break;

                default:

                    dtnf = this.DataTypeName;
                    break;
            }*/

            return dtnf;
        }
    }
    
    public override object DatabaseSpecificMetaData(string key)
    {
        return SqlDatabase.DBSpecific(key, this);
    }

    internal static string GetFullDataTypeName(string name, int charMaxLen, int precision, int scale)
    {
        string dtnf = name switch
        {
            "varchar" or "nvarchar" or "varbinary" => charMaxLen > 1000000 ? $"{name}(MAX)" : $"{name}({charMaxLen})",
            "binary" or "char" or "nchar" => $"{name}({charMaxLen})",
            "decimal" or "numeric" => $"{name}({precision},{scale})",
            _ => name
        };

        return dtnf;
    }
}