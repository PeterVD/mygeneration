using System.Data;
using System.Runtime.InteropServices;

namespace MyMeta.Sql;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IProcedure))]
#endif
public class SqlProcedure : Procedure
{
    public override string Alias
    {
        get
        {
            var name = base.Name.Split(';');

            return name[0];
        }
    }

    public override string Name
    {
        get
        {
            var name = base.Name.Split(';');

            return name[0];
        }
    }

    public override string ProcedureText
    {
        get
        {
            var tmp = base.ProcedureText;
            if (tmp.Length == 0) tmp = LoadProcedureSource();
            return tmp;
        }
    }


    private string LoadProcedureSource()
    {
        var text = string.Empty;
        OleDbConnection cn = null;
        OleDbDataReader reader = null;
        try
        {
            var select = string.Format(@"SELECT CASE WHEN encrypted = 1 THEN NULL ELSE com.text END as Source FROM sysobjects o, syscomments com 
WHERE o.id = object_id(N'[{0}].[{1}]')
and com.id=o.id 
and com.status=2 
order by colid;", Schema, Name);
            cn = new OleDbConnection(DbRoot.ConnectionString);
            cn.Open();
            cn.ChangeDatabase(Database.Name);

            OleDbCommand cmd = cn.CreateCommand();
            cmd.CommandText = select;
            try
            {
                reader = cmd.ExecuteReader();

                while (reader.Read()) text += reader[0].ToString();

                reader.Close();
            }
            catch
            {
                if (reader is not null)
                    reader.Close();
            }

            if (text == string.Empty)
            {
                select = string.Format(@"SELECT CASE WHEN encrypted = 1 THEN NULL ELSE com.text END as Source FROM sysobjects o, syscomments com 
WHERE o.id = object_id(N'[{0}].[{1}]')
and com.id=o.id 
order by colid;", Schema, Name);

                cmd = cn.CreateCommand();
                cmd.CommandText = select;
                reader = cmd.ExecuteReader();

                while (reader.Read()) text += reader[0].ToString();

                reader.Close();
            }

            cn.Close();
            text = text.TrimStart(' ', '\r', '\n', '\t');
        }
        catch
        {
            if (reader is not null)
                reader.Close();
            if (cn is not null && cn.State != ConnectionState.Closed && cn.State != ConnectionState.Broken)
                cn.Close();
        }

        return text;
    }

    public override object DatabaseSpecificMetaData(string key)
    {
        return SqlDatabase.DBSpecific(key, this);
    }
}