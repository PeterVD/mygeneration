using System.Data;
using System.Runtime.InteropServices;

namespace MyMeta.Sql;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IParameters))]
#endif
public class SqlParameters : Parameters
{
    internal override void LoadAll()
    {
        try
        {
            var metaData = LoadData(OleDbSchemaGuid.Procedure_Parameters,
                new object[] { Procedure.Database.Name, Procedure.Schema, Procedure.Name });

            PopulateArray(metaData);

            LoadDescriptions();
        }
        catch
        {
        }
    }

    private void LoadDescriptions()
    {
        try
        {
            var select = @"SELECT objName, value FROM ::fn_listextendedproperty (default, 'user', 'dbo', 'procedure', '" + Procedure.Name +
                         "', 'parameter', default)";

            OleDbConnection cn = new OleDbConnection(DbRoot.ConnectionString);
            cn.Open();
            cn.ChangeDatabase("[" + Procedure.Database.Name + "]");

            OleDbDataAdapter adapter = new OleDbDataAdapter(select, cn);
            var dataTable = new DataTable();

            adapter.Fill(dataTable);

            cn.Close();

            Parameter p;

            foreach (DataRow row in dataTable.Rows)
            {
                p = this[row["objName"] as string] as Parameter;

                if (null != p) p._row["DESCRIPTION"] = row["value"] as string;
            }
        }
        catch
        {
        }
    }
}