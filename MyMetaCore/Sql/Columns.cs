using System;

namespace MyMeta.Sql;

public class SqlColumns : Columns
{
    internal DataColumn f_AutoKey;
    internal DataColumn f_TypeName;

    internal override void LoadForTable()
    {
        var metaData = LoadData(OleDbSchemaGuid.Columns, [Table.Database.Name, Table.Schema, Table.Name]);
        PopulateArray(metaData);

        LoadExtraData(Table.Name, "T");
        LoadAutoKeyInfo();
        LoadDescriptions();
    }

    internal override void LoadForView()
    {
        var metaData = LoadData(OleDbSchemaGuid.Columns, [View.Database.Name, View.Schema, View.Name]);
        PopulateArray(metaData);

        LoadExtraData(View.Name, "V");
    }

    private void LoadExtraData(string name, string type)
    {
        try
        {
            var dbName = type == "T" ? Table.Database.Name : View.Database.Name;
            var schema = type == "T" ? Table.Schema : View.Schema;
            var select = $"EXEC [{dbName}].dbo.sp_columns '{name}', '{schema}'";

            using var cn = new OleDbConnection(DbRoot.ConnectionString);
            cn.Open();
            cn.ChangeDatabase($"[{dbName}]");

            using var adapter = new OleDbDataAdapter(select, cn);
            var dataTable = new DataTable();

            adapter.Fill(dataTable);

            if (_array.Count > 0)
            {
                var col = _array[0] as Column;

                f_TypeName = new DataColumn("TYPE_NAME", Types.StringType);
                col._row.Table.Columns.Add(f_TypeName);

                var rows = dataTable.Rows;

                var count = _array.Count;

                for (var index = 0; index < count; index++)
                {
                    var c = (Column)this[index];

                    var typeName = rows[index]["TYPE_NAME"] as string;
                    if (typeName.EndsWith(" identity"))
                    {
                        typeName = typeName.Replace(" identity", "");
                        typeName = typeName.Replace("()", "");
                    }
                    c._row["TYPE_NAME"] = typeName;
                }
            }

            select = $"""
                      select COLUMN_NAME, DATA_TYPE from 
                      INFORMATION_SCHEMA.COLUMNS 
                      where table_schema = '{schema}' 
                      and table_catalog='{dbName}' and table_name='{name}' 
                      and DATA_TYPE in ('nvarchar', 'varchar', 'varbinary') 
                      and character_maximum_length=-1 and character_octet_length=-1;
                      """;

            using var columnAdapter = new OleDbDataAdapter(select, cn);
            using var columnDataTable = new DataTable();

            adapter.Fill(columnDataTable);

            foreach (DataRow row in columnDataTable.Rows)
            {
                if (this[row["COLUMN_NAME"] as string] is Column colz)
                {
                    colz._row["TYPE_NAME"] = row["DATA_TYPE"] as string;
                }
            }

            cn.Close();
        }
        catch
        {
        }
    }

    private void LoadDescriptions()
    {
        try
        {
            var select = $"""
                          SELECT objName, value 
                          FROM ::fn_listextendedproperty ('MS_Description', 'user', '{Table.Schema}', 'table', '{Table.Name}', 'column', null)
                          UNION
                          SELECT objName, value 
                          FROM ::fn_listextendedproperty ('MS_Description', 'schema', '{Table.Schema}', 'table', '{Table.Name}', 'column', null)
                          """;

            using var cn = new OleDbConnection(DbRoot.ConnectionString);
            cn.Open();
            cn.ChangeDatabase("[" + Table.Database.Name + "]");

            using var adapter = new OleDbDataAdapter(select, cn);
            using var dataTable = new DataTable();

            adapter.Fill(dataTable);
            cn.Close();

            foreach (DataRow row in dataTable.Rows)
            {
                var c = this[row["objName"] as string] as Column;

                if (null != c) c._row["DESCRIPTION"] = row["value"] as string;
            }
        }
        catch (Exception ex)
        {
            var s = ex.Message;
        }
    }

    private void LoadAutoKeyInfo()
    {
        try
        {
            var select =
                $"""
                 SELECT TABLE_NAME, COLUMN_NAME, IDENT_SEED('[{Table.Name}]') AS AUTO_KEY_SEED, IDENT_INCR('[{Table.Name}]') AS AUTO_KEY_INCREMENT 
                 FROM INFORMATION_SCHEMA.COLUMNS 
                 WHERE TABLE_NAME = '{Table.Name}' AND TABLE_SCHEMA = '{Table.Schema}' AND columnproperty(object_id('[{Table.Schema}].[{Table.Name}]'), COLUMN_NAME, 'IsIdentity') = 1
                 """;

            using var cn = new OleDbConnection(DbRoot.ConnectionString);
            cn.Open();
            cn.ChangeDatabase("[" + Table.Database.Name + "]");

            using var adapter = new OleDbDataAdapter(select, cn);
            using var dataTable = new DataTable();

            adapter.Fill(dataTable);
            cn.Close();

            if (dataTable.Rows.Count > 0)
            {
                f_AutoKeySeed = new DataColumn("AUTO_KEY_SEED", Types.Int32Type);
                f_AutoKeyIncrement = new DataColumn("AUTO_KEY_INCREMENT", Types.Int32Type);
                f_AutoKey = new DataColumn("AUTO_INCREMENT", Types.BoolType);

                var col = _array[0] as Column;
                col._row.Table.Columns.Add(f_AutoKeySeed);
                col._row.Table.Columns.Add(f_AutoKeyIncrement);
                col._row.Table.Columns.Add(f_AutoKey);

                var rows = dataTable.Rows;

                for (var i = 0; i < rows.Count; i++)
                {
                    var row = rows[i];
                    col = this[row["COLUMN_NAME"]] as Column;
                    col._row["AUTO_KEY_SEED"] = row["AUTO_KEY_SEED"];
                    col._row["AUTO_KEY_INCREMENT"] = row["AUTO_KEY_INCREMENT"];
                    col._row["AUTO_INCREMENT"] = true;
                }
            }
        }
        catch (Exception ex)
        {
            var s = ex.Message;
        }
    }
}