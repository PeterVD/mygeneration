using System.Runtime.InteropServices;

namespace MyMeta.Sql;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IParameter))]
#endif
public class SqlParameter : Parameter
{
    public override string DataTypeNameComplete
    {
        get
        {
            switch (TypeName)
            {
                case "binary":
                case "char":
                case "nchar":
                case "nvarchar":
                case "varchar":
                case "varbinary":

                    return TypeName + "(" + CharacterMaxLength + ")";

                case "decimal":
                case "numeric":

                    return TypeName + "(" + NumericPrecision + "," + NumericScale + ")";

                default:

                    return TypeName;
            }
        }
    }
}