using System.Runtime.InteropServices;

namespace MyMeta.Sql;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IForeignKey))]
#endif
public class SqlForeignKey : ForeignKey
{
}