using System.Runtime.InteropServices;

namespace MyMeta.DB2;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IDatabases))]
#endif
public class DB2Databases : Databases
{
    internal override void LoadAll()
    {
        try
        {
            OleDbConnection cn = new OleDbConnection(DbRoot.ConnectionString);

            // We add our one and only Database
            var database = (DB2Database)DbRoot.ClassFactory.CreateDatabase();
            database._name = cn.DataSource;
            database.DbRoot = DbRoot;
            database.Databases = this;
            _array.Add(database);
        }
        catch
        {
        }
    }
}