using System.Data;
using System.Runtime.InteropServices;

namespace MyMeta.DB2;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IParameters))]
#endif
public class DB2Parameters : Parameters
{
    internal override void LoadAll()
    {
        try
        {
            var metaData = LoadData(OleDbSchemaGuid.Procedure_Parameters, new object[] { null, null, Procedure.Name });

            PopulateArray(metaData);

            LoadExtraData();
        }
        catch
        {
        }
    }

    private void LoadExtraData()
    {
        try
        {
            var select = "SELECT PARMNAME, TYPENAME, CODEPAGE FROM SYSCAT.PROCPARMS WHERE PROCNAME = '" + Procedure.Name + "' ORDER BY ORDINAL";

            OleDbDataAdapter adapter = new OleDbDataAdapter(select, DbRoot.ConnectionString);
            var dataTable = new DataTable();

            adapter.Fill(dataTable);

            if (_array.Count > 0)
            {
                var rows = dataTable.Rows;
                var paramName = "";

                var count = _array.Count;
                Parameter p = null;

                foreach (DataRow row in rows)
                {
                    paramName = row["PARMNAME"] as string;
                    p = this[paramName] as Parameter;

                    p._row["TYPE_NAME"] = (row["TYPENAME"] as string).Trim();

                    var codepage = -1;
                    try
                    {
                        codepage = (short)row["CODEPAGE"];
                    }
                    catch
                    {
                    }

                    if (codepage == 0)
                        // Check for "bit data"
                        switch (p.TypeName)
                        {
                            case "CHARACTER":
                            case "VARCHAR":
                            case "LONG VARCHAR":

                                p._row["TYPE_NAME"] = p.TypeName + " FOR BIT DATA";
                                break;
                        }
                }
            }
        }
        catch
        {
        }
    }
}