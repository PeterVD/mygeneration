using System.Runtime.InteropServices;

namespace MyMeta.DB2;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IProcedures))]
#endif
public class DB2Procedures : Procedures
{
    internal override void LoadAll()
    {
        try
        {
            var metaData = LoadData(OleDbSchemaGuid.Procedures, null);

            PopulateArray(metaData);
        }
        catch
        {
        }
    }
}