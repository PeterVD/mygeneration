using System.Data;
using System.Runtime.InteropServices;

namespace MyMeta.DB2;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IColumns))]
#endif
public class DB2Columns : Columns
{
    internal DataColumn f_AutoKey;
    internal DataColumn f_InPrimaryKey;
    internal DataColumn f_TypeName;


    internal override void LoadForTable()
    {
        var metaData = LoadData(OleDbSchemaGuid.Columns, new object[] { null, null, Table.Name });

        PopulateArray(metaData);

        LoadExtraData("T");
    }

    internal override void LoadForView()
    {
        var metaData = LoadData(OleDbSchemaGuid.Columns, new object[] { null, null, View.Name });

        PopulateArray(metaData);

        LoadExtraData("V");
    }

    private void LoadExtraData(string type)
    {
        try
        {
            var name = "T" == type ? Table.Name : View.Name;
            var select = "SELECT COLNAME, TYPENAME, CODEPAGE, IDENTITY FROM SYSCAT.COLUMNS WHERE TABNAME = '" + name + "' ORDER BY COLNO";

            OleDbDataAdapter adapter = new OleDbDataAdapter(select, DbRoot.ConnectionString);
            var dataTable = new DataTable();

            adapter.Fill(dataTable);

            if (_array.Count > 0)
            {
                var col = _array[0] as Column;

                f_TypeName = new DataColumn("TYPE_NAME", typeof(string));
                col._row.Table.Columns.Add(f_TypeName);

                f_AutoKey = new DataColumn("AUTO_INCREMENT", typeof(bool));
                col._row.Table.Columns.Add(f_AutoKey);

                var rows = dataTable.Rows;
                var identity = "";
                var colName = "";

                var count = _array.Count;
                Column c = null;

                foreach (DataRow row in rows)
                {
                    colName = row["COLNAME"] as string;

                    c = this[colName] as Column;

                    identity = row["IDENTITY"] as string;
                    c._row["AUTO_INCREMENT"] = identity == "Y" ? true : false;
                    c._row["TYPE_NAME"] = (row["TYPENAME"] as string).Trim();

                    var codepage = -1;
                    try
                    {
                        codepage = (short)row["CODEPAGE"];
                    }
                    catch
                    {
                    }

                    if (codepage == 0)
                        // Check for "bit data"
                        switch (c.DataTypeName)
                        {
                            case "CHARACTER":
                            case "VARCHAR":
                            case "LONG VARCHAR":

                                c._row["TYPE_NAME"] = c.DataTypeName + " FOR BIT DATA";
                                break;
                        }
                }
            }
        }
        catch
        {
        }
    }
}