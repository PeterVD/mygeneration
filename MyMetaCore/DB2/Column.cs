using System.Runtime.InteropServices;

namespace MyMeta.DB2;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IColumn))]
#endif
public class DB2Column : Column
{
    public override bool IsInPrimaryKey
    {
        get
        {
            if (null == Columns.Table) return false;

            var cols = Columns.Table.PrimaryKeys as Columns;
            var col = cols.GetByPhysicalName(Name);

            return null == col ? false : true;
        }
    }

    public override bool IsAutoKey
    {
        get
        {
            var cols = Columns as DB2Columns;
            return GetBool(cols.f_AutoKey);
        }
    }

    public override string DataTypeName
    {
        get
        {
            var cols = Columns as DB2Columns;
            return GetString(cols.f_TypeName);
        }
    }

    public override string DataTypeNameComplete => "Unknown";

    internal override Column Clone()
    {
        var c = base.Clone();

        return c;
    }
    //				DB2Columns cols = Columns as DB2Columns;
    //				return this.GetString(cols.f_TypeName);
}