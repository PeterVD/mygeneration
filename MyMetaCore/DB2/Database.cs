using System.Runtime.InteropServices;

namespace MyMeta.DB2;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IDatabase))]
#endif
public class DB2Database : Database
{
    internal string _desc = "";

    internal string _name = "";
    public override string Alias => _name;

    public override string Name => _name;

    public override string Description => _desc;
}