using System.Runtime.InteropServices;

namespace MyMeta.DB2;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IIndexes))]
#endif
public class DB2Indexes : Indexes
{
    internal override void LoadAll()
    {
        try
        {
            var metaData = LoadData(OleDbSchemaGuid.Indexes,
                new object[] { null, null, null, null, Table.Name });

            PopulateArray(metaData);
        }
        catch
        {
        }
    }
}