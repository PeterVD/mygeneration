using System;
using System.Runtime.InteropServices;
using System.Xml;

namespace MyMeta;

public class Procedure : Single, IProcedure, INameValueItem
{
    private Parameters _parameters;
    private ResultColumns _resultColumns;

    internal Procedures Procedures;

    public IDatabase Database => Procedures.Database;

    #region Collections

    public virtual IResultColumns ResultColumns
    {
        get
        {
            if (_resultColumns is not null) return _resultColumns;

            _resultColumns = (ResultColumns)DbRoot.ClassFactory.CreateResultColumns();
            _resultColumns.Procedure = this;
            _resultColumns.DbRoot = DbRoot;
            _resultColumns.LoadAll();

            return _resultColumns;
        }
    }

    public virtual IParameters Parameters
    {
        get
        {
            if (_parameters is not null) return _parameters;

            _parameters = (Parameters)DbRoot.ClassFactory.CreateParameters();
            _parameters.Procedure = this;
            _parameters.DbRoot = DbRoot;
            _parameters.LoadAll();

            return _parameters;
        }
    }

    public virtual IPropertyCollection GlobalProperties
    {
        get
        {
            var db = Procedures.Database;
            if (db._procedureProperties is not null) return db._procedureProperties;

            db._procedureProperties = new PropertyCollection
            {
                Parent = this
            };

            var xPath = GlobalUserDataXPath;
            var xmlNode = DbRoot.UserData.SelectSingleNode(xPath, null);

            if (xmlNode is null)
            {
                var parentNode = db.CreateGlobalXmlNode();

                xmlNode = parentNode.OwnerDocument.CreateNode(XmlNodeType.Element, "Procedure", null);
                parentNode.AppendChild(xmlNode);
            }

            db._procedureProperties.LoadAllGlobal(xmlNode);

            return db._procedureProperties;
        }
    }

    public virtual IPropertyCollection AllProperties
    {
        get
        {
            if (null == _allProperties)
            {
                _allProperties = new PropertyCollectionAll();
                _allProperties.Load(Properties, GlobalProperties);
            }

            return _allProperties;
        }
    }

    internal PropertyCollectionAll _allProperties;

    #endregion

    #region Properties

    public override string Alias
    {
        get
        {
            return GetXmlNode(out var node, false) && GetUserData(node, "n", out var niceName) && string.Empty != niceName ? niceName : Name;
        }

        set
        {
            if (GetXmlNode(out var node, true))
            {
                SetUserData(node, "n", value);
            }
        }
    }

    public override string Name => GetString(Procedures.f_Name);

    public string Schema => GetString(Procedures.f_Schema);

    public virtual short Type => GetInt16(Procedures.f_Type);

    public virtual string ProcedureText => GetString(Procedures.f_ProcedureDefinition);

    public virtual string Description => GetString(Procedures.f_Description);

    public virtual DateTime DateCreated => GetDateTime(Procedures.f_DateCreated);

    public virtual DateTime DateModified => GetDateTime(Procedures.f_DateModified);

    #endregion

    #region XML User Data

    public override string UserDataXPath => $@"{Procedures.UserDataXPath}/Procedure[@p='{Name}']";

    public override string GlobalUserDataXPath => $"{Procedures.Database.GlobalUserDataXPath}/Procedure";

    internal override bool GetXmlNode(out XmlNode node, bool forceCreate)
    {
        node = null;
        var success = false;

        if (null == _xmlNode)
        {
            // Get the parent node
            if (Procedures.GetXmlNode(out var parentNode, forceCreate))
            {
                // See if our user data already exists
                var xPath = $@"./Procedure[@p='{Name}']";
                if (!GetUserData(xPath, parentNode, out _xmlNode) && forceCreate)
                {
                    // Create it, and try again
                    CreateUserMetaData(parentNode);
                    GetUserData(xPath, parentNode, out _xmlNode);
                }
            }
        }

        if (null != _xmlNode)
        {
            node = _xmlNode;
            success = true;
        }

        return success;
    }

    public override void CreateUserMetaData(XmlNode parentNode)
    {
        var myNode = parentNode.OwnerDocument.CreateNode(XmlNodeType.Element, "Procedure", null);
        parentNode.AppendChild(myNode);

        var attr = parentNode.OwnerDocument.CreateAttribute("p");
        attr.Value = Name;
        myNode.Attributes.Append(attr);

        attr = parentNode.OwnerDocument.CreateAttribute("n");
        attr.Value = "";
        myNode.Attributes.Append(attr);
    }

    #endregion

    #region INameValueCollection Members

    public string ItemName => Name;

    public string ItemValue => Name;

    #endregion
}