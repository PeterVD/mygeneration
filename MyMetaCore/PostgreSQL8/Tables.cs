using System.Data;
using System.Runtime.InteropServices;
using Npgsql;

namespace MyMeta.PostgreSQL8;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(ITables))]
#endif
public class PostgreSQL8Tables : Tables
{
    internal override void LoadAll()
    {
        try
        {
            // New PostgreSQL sorted query from Michael McKean
            var query = "select * from information_schema.tables where table_type = 'BASE TABLE' " +
                        " and table_schema = '" + Database.SchemaName + "' ORDER BY table_name;";
            /*string query = "select * from information_schema.tables where table_type = 'BASE TABLE' " +
                " and table_schema = '" + this.Database.SchemaName + "'";*/

            var cn = ConnectionHelper.CreateConnection(DbRoot, Database.Name);

            var metaData = new DataTable();
            var adapter = new NpgsqlDataAdapter(query, cn);

            adapter.Fill(metaData);
            cn.Close();

            PopulateArray(metaData);
        }
        catch
        {
        }
    }
}