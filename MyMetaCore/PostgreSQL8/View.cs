using System.Data;
using System.Runtime.InteropServices;

namespace MyMeta.PostgreSQL8;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IView))]
#endif
public class PostgreSQL8View : View
{
    public override IViews SubViews
    {
        get
        {
            if (!_subViewInfoLoaded) LoadSubViewInfo();
            return _views;
        }
    }

    public override ITables SubTables
    {
        get
        {
            if (!_subViewInfoLoaded) LoadSubViewInfo();
            return _tables;
        }
    }

    private void LoadSubViewInfo()
    {
        _views = (Views)DbRoot.ClassFactory.CreateViews();
        _tables = (Tables)DbRoot.ClassFactory.CreateTables();

        try
        {
            var select = "SELECT * FROM INFORMATION_SCHEMA.VIEW_TABLE_USAGE WHERE VIEW_NAME = '"
                         + Name + "' AND VIEW_SCHEMA = '" + Schema + "';";

            OleDbConnection cn = new OleDbConnection(DbRoot.ConnectionString);
            cn.Open();
            cn.ChangeDatabase(Database.Name);

            OleDbDataAdapter adapter = new OleDbDataAdapter(select, cn);
            var dataTable = new DataTable();

            adapter.Fill(dataTable);

            cn.Close();

            var entity = "";

            Table table;
            View view;

            foreach (DataRow row in dataTable.Rows)
            {
                entity = row["TABLE_NAME"] as string;

                // It might be a table or a view
                table = Database.Tables[entity] as Table;

                if (null != table)
                {
                    // It's a table
                    _tables.AddTable(table);
                }
                else
                {
                    // Check for View
                    view = Database.Views[entity] as View;

                    if (null != view)
                        // It's a table
                        _views.AddView(view);
                }
            }
        }
        catch
        {
        }
    }
}