using System.Data;
using System.Runtime.InteropServices;
using Npgsql;

namespace MyMeta.PostgreSQL8;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IProcedure))]
#endif
public class PostgreSQL8Procedure : Procedure
{
    private PostgreSQL8Parameters _parameters;
    internal string _specific_name = "";

    public override IParameters Parameters
    {
        get
        {
            if (null == _parameters)
            {
                _parameters = (PostgreSQL8Parameters)DbRoot.ClassFactory.CreateParameters();
                _parameters.Procedure = this;
                _parameters.DbRoot = DbRoot;

                var query = "select * from information_schema.parameters where specific_schema = '" +
                            Database.SchemaName + "' and specific_name = '" + (string)_row["specific_name"] + "'";

                var cn = ConnectionHelper.CreateConnection(DbRoot, Database.Name);

                var metaData = new DataTable();
                var adapter = new NpgsqlDataAdapter(query, cn);

                adapter.Fill(metaData);
                cn.Close();

                metaData.Columns["udt_name"].ColumnName = "TYPE_NAME";
                metaData.Columns["data_type"].ColumnName = "TYPE_NAMECOMPLETE";

                if (metaData.Columns.Contains("TYPE_NAME"))
                    _parameters.f_TypeName = metaData.Columns["TYPE_NAME"];

                if (metaData.Columns.Contains("TYPE_NAMECOMPLETE"))
                    _parameters.f_TypeNameComplete = metaData.Columns["TYPE_NAMECOMPLETE"];

                _parameters.PopulateArray(metaData);
            }

            return _parameters;
        }
    }
}