using System.Runtime.InteropServices;

namespace MyMeta.PostgreSQL8;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IForeignKey))]
#endif
public class PostgreSQL8ForeignKey : ForeignKey
{
}