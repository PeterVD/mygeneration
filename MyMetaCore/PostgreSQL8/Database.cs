using System.Runtime.InteropServices;
using ADODB;
using Npgsql;

namespace MyMeta.PostgreSQL8;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IDatabase))]
#endif
public class PostgreSQL8Database : Database
{
    public override Recordset ExecuteSql(string sql)
    {
        var cn = new NpgsqlConnection(DbRoot.ConnectionString);
        cn.Open();
        cn.ChangeDatabase(Name);

        return ExecuteIntoRecordset(sql, cn);
    }
}