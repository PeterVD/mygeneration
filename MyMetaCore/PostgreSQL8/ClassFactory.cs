using System.Data;
using System.Runtime.InteropServices;
using Npgsql;

namespace MyMeta.PostgreSQL8;

#if ENTERPRISE

[ComVisible(false)]
#endif
public class ClassFactory : IClassFactory
{
    public ITables CreateTables()
    {
        return new PostgreSQL8Tables();
    }

    public ITable CreateTable()
    {
        return new PostgreSQL8Table();
    }

    public IColumn CreateColumn()
    {
        return new PostgreSQL8Column();
    }

    public IColumns CreateColumns()
    {
        return new PostgreSQL8Columns();
    }

    public IDatabase CreateDatabase()
    {
        return new PostgreSQL8Database();
    }

    public IDatabases CreateDatabases()
    {
        return new PostgreSQL8Databases();
    }

    public IProcedure CreateProcedure()
    {
        return new PostgreSQL8Procedure();
    }

    public IProcedures CreateProcedures()
    {
        return new PostgreSQL8Procedures();
    }

    public IView CreateView()
    {
        return new PostgreSQL8View();
    }

    public IViews CreateViews()
    {
        return new PostgreSQL8Views();
    }

    public IParameter CreateParameter()
    {
        return new PostgreSQL8Parameter();
    }

    public IParameters CreateParameters()
    {
        return new PostgreSQL8Parameters();
    }

    public IForeignKey CreateForeignKey()
    {
        return new PostgreSQL8ForeignKey();
    }

    public IForeignKeys CreateForeignKeys()
    {
        return new PostgreSQL8ForeignKeys();
    }

    public IIndex CreateIndex()
    {
        return new PostgreSQL8Index();
    }

    public IIndexes CreateIndexes()
    {
        return new PostgreSQL8Indexes();
    }

    public IResultColumn CreateResultColumn()
    {
        return new PostgreSQL8ResultColumn();
    }

    public IResultColumns CreateResultColumns()
    {
        return new PostgreSQL8ResultColumns();
    }

    public IDomain CreateDomain()
    {
        return new PostgreSQL8Domain();
    }

    public IDomains CreateDomains()
    {
        return new PostgreSQL8Domains();
    }

    public IProviderType CreateProviderType()
    {
        return new ProviderType();
    }

    public IProviderTypes CreateProviderTypes()
    {
        return new ProviderTypes();
    }

    public IDbConnection CreateConnection()
    {
        return new NpgsqlConnection();
    }

    public static void Register()
    {
        InternalDriver.Register("POSTGRESQL8",
            new InternalDriver
            (typeof(ClassFactory)
                , "Server=127.0.0.1;Port=5432;User Id=myUser;Password=myPasswordt;Database=test;"
                , false));
    }
}