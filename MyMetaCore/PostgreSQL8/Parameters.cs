using System.Data;
using System.Runtime.InteropServices;

namespace MyMeta.PostgreSQL8;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IParameters))]
#endif
public class PostgreSQL8Parameters : Parameters
{
    internal DataColumn f_TypeNameComplete;

    internal override void LoadAll()
    {
        try
        {
        }
        catch
        {
        }
    }
}