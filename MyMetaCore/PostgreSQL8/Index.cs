using System.Runtime.InteropServices;

namespace MyMeta.PostgreSQL8;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IIndex))]
#endif
public class PostgreSQL8Index : Index
{
    public override string Type
    {
        get
        {
            var type = GetString(Indexes.f_Type);
            return type.ToUpper();
        }
    }
}