using System.Runtime.InteropServices;

namespace MyMeta.PostgreSQL8;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IDomain))]
#endif
public class PostgreSQL8Domain : Domain
{
    public override string DataTypeNameComplete
    {
        get
        {
            var domains = Domains as PostgreSQL8Domains;
            return GetString(domains.f_TypeNameComplete);
        }
    }
}