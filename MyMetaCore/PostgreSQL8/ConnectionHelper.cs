using Npgsql;

namespace MyMeta.PostgreSQL8;

/// <summary>
///     Summary description for ConnectionHelper.
/// </summary>
public class ConnectionHelper
{
    //
//		static public NpgsqlConnection CreateConnection(MyMeta.dbRoot dbRoot, string database)
//		{
//			string cnstr = dbRoot.ConnectionString + "Database=" + database + ";";
//			NpgsqlConnection cn = new Npgsql.NpgsqlConnection(cnstr);
//			return cn;
//		}

    public static NpgsqlConnection CreateConnection(dbRoot dbRoot, string database)
    {
        var cn = new NpgsqlConnection(dbRoot.ConnectionString);
        cn.Open();
        cn.ChangeDatabase(database);
        return cn;
    }
}