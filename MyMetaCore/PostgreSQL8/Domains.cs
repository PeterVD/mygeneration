using System.Data;
using System.Runtime.InteropServices;
using Npgsql;

namespace MyMeta.PostgreSQL8;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IDomains))]
#endif
public class PostgreSQL8Domains : Domains
{
    internal DataColumn f_TypeNameComplete;

    internal override void LoadAll()
    {
        var query = "select * from information_schema.domains where domain_catalog = '" + Database.Name +
                    "' and domain_schema = '" + Database.SchemaName + "'";

        var cn = ConnectionHelper.CreateConnection(DbRoot, Database.Name);

        var metaData = new DataTable();
        var adapter = new NpgsqlDataAdapter(query, cn);

        adapter.Fill(metaData);
        cn.Close();

        metaData.Columns["udt_name"].ColumnName = "DATA_TYPE";
        metaData.Columns["data_type"].ColumnName = "TYPE_NAMECOMPLETE";

        if (metaData.Columns.Contains("TYPE_NAMECOMPLETE"))
            f_TypeNameComplete = metaData.Columns["TYPE_NAMECOMPLETE"];

        PopulateArray(metaData);
    }
}