using System.Data;
using System.Runtime.InteropServices;
using Npgsql;

namespace MyMeta.PostgreSQL8;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(ITable))]
#endif
public class PostgreSQL8Table : Table
{
    public override IColumns PrimaryKeys
    {
        get
        {
            if (null == _primaryKeys)
            {
                // New PostgreSQL primary key query from Michael McKean
                var query = "SELECT  c.column_name " +
                            "FROM   information_schema.key_column_usage c " +
                            "INNER JOIN  information_schema.table_constraints t " +
                            "ON  c.constraint_name = t.constraint_name " +
                            "WHERE  c.table_name = '" + Name + "' " +
                            " AND c.table_schema = '" + Schema + "' " +
                            " AND  t.constraint_type = 'PRIMARY KEY' ";
                /*string query = "select c.column_name from information_schema.table_constraints t, information_schema.constraint_column_usage c " +
                    "where t.table_name = '" + this.Name + "' and t.table_schema = '" + this.Schema +
                    "' and c.constraint_name = t.constraint_name and t.constraint_type = 'PRIMARY KEY'";*/

                var cn = ConnectionHelper.CreateConnection(DbRoot, Database.Name);

                var metaData = new DataTable();
                var adapter = new NpgsqlDataAdapter(query, cn);

                adapter.Fill(metaData);
                cn.Close();

                _primaryKeys = (Columns)DbRoot.ClassFactory.CreateColumns();
                _primaryKeys.Table = this;
                _primaryKeys.DbRoot = DbRoot;

                var colName = "";

                var count = metaData.Rows.Count;
                for (var i = 0; i < count; i++)
                {
                    colName = metaData.Rows[i]["COLUMN_NAME"] as string;
                    _primaryKeys.AddColumn((Column)Columns[colName]);
                }
            }

            return _primaryKeys;
        }
    }
}