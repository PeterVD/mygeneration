using System.Runtime.InteropServices;

namespace MyMeta.PostgreSQL8;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IParameter))]
#endif
public class PostgreSQL8Parameter : Parameter
{
    public override string Name
    {
        get
        {
            var n = base.Name;

            if (n == string.Empty) n = "[" + Parameters.Procedure.Name + ":" + Ordinal + "]";

            return n;
        }
    }

    public override ParamDirection Direction => ParamDirection.Input;


    public override string DataTypeNameComplete
    {
        get
        {
            var parameters = Parameters as PostgreSQL8Parameters;
            return GetString(parameters.f_TypeNameComplete);
        }
    }
}