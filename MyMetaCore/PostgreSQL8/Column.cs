using System;
using System.Runtime.InteropServices;

namespace MyMeta.PostgreSQL8;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IColumn))]
#endif
public class PostgreSQL8Column : Column
{
    internal int _autoInc;
    internal int _autoSeed;
    internal bool _isAutoKey;

    public override bool IsNullable
    {
        get
        {
            var s = GetString(Columns.f_IsNullable);

            if (s == "YES")
                return true;
            return false;
        }
    }

    public override bool HasDefault
    {
        get
        {
            var o = _row[Columns.f_Default];

            if (o == DBNull.Value) return false;

            return true;
        }
    }


    public override bool IsAutoKey => _isAutoKey;

    public override int AutoKeyIncrement => _autoInc;

    public override int AutoKeySeed => _autoSeed;

    public override bool IsComputed => GetBool(Columns.f_IsComputed);


    public override string DataTypeName
    {
        get
        {
            var cols = Columns as PostgreSQL8Columns;
            return GetString(cols.f_TypeName);
        }
    }

    public override string DataTypeNameComplete
    {
        get
        {
            var cols = Columns as PostgreSQL8Columns;
            return GetString(cols.f_TypeNameComplete);
        }
    }

    internal override Column Clone()
    {
        var c = base.Clone();

        return c;
    }
}