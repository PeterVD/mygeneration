using System.Data;
using System.Runtime.InteropServices;
using Npgsql;

namespace MyMeta.PostgreSQL8;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IDatabases))]
#endif
public class PostgreSQL8Databases : Databases
{
    internal override void LoadAll()
    {
        var query =
            "select datname as CATALOG_NAME, s.usename as SCHEMA_OWNER, current_schema() as SCHEMA_NAME from pg_database d " +
            "INNER JOIN pg_user s on d.datdba = s.usesysid where datistemplate = 'f' ORDER BY datname";

        var cn = new NpgsqlConnection(DbRoot.ConnectionString);

        var adapter = new NpgsqlDataAdapter(query, cn);
        var metaData = new DataTable();

        adapter.Fill(metaData);

        PopulateArray(metaData);
    }
}