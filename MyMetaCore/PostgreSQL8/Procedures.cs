using System.Data;
using System.Runtime.InteropServices;
using Npgsql;

namespace MyMeta.PostgreSQL8;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IProcedures))]
#endif
public class PostgreSQL8Procedures : Procedures
{
    internal string _specific_name = "";

    public override IProcedure this[object name] => base[name];

    internal override void LoadAll()
    {
        try
        {
            var query = "SELECT routine_definition as PROCEDURE_DEFINITION, specific_name, " +
                        "routine_name as PROCEDURE_NAME, routine_schema as PROCEDURE_SCHEMA, routine_catalog as PROCEDURE_CATALOG " +
                        "from information_schema.routines where routine_schema = '" + Database.SchemaName +
                        "' and routine_catalog = '" + Database.Name + "'";

            var cn = ConnectionHelper.CreateConnection(DbRoot, Database.Name);

            var metaData = new DataTable();
            var adapter = new NpgsqlDataAdapter(query, cn);

            adapter.Fill(metaData);
            cn.Close();

            PopulateArray(metaData);
        }
        catch
        {
        }
    }
}