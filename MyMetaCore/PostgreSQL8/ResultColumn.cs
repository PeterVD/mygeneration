using System.Data;
using System.Runtime.InteropServices;

namespace MyMeta.PostgreSQL8;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IResultColumn))]
#endif
public class PostgreSQL8ResultColumn : ResultColumn
{
    internal DataColumn _column;

    #region Properties

    public override string Name => _column.ColumnName;

    public override string DataTypeName => _column.DataType.ToString();

    public override int Ordinal => _column.Ordinal;

    #endregion
}