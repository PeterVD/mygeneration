using System.Data;
using System.Runtime.InteropServices;
using Npgsql;

namespace MyMeta.PostgreSQL8;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IViews))]
#endif
public class PostgreSQL8Views : Views
{
    internal override void LoadAll()
    {
        try
        {
            var query = "select * from information_schema.views where table_schema = '" +
                        Database.SchemaName + "'";

            var cn = ConnectionHelper.CreateConnection(DbRoot, Database.Name);

            var metaData = new DataTable();
            var adapter = new NpgsqlDataAdapter(query, cn);

            adapter.Fill(metaData);
            cn.Close();

            PopulateArray(metaData);
        }
        catch
        {
        }
    }
}