using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

namespace MyMeta;

/// <summary>
/// Summary description for Collection.
/// </summary>
/// 
public class PropertyCollection : Collection<IProperty>, IPropertyCollection, IEnumerable, IEnumerator, ICollection
{
    //private SortedList _collection = new();
    private Dictionary<string, Property> _collection = new();

    private IEnumerator<KeyValuePair<string, Property>> _enumerator;
    private MetaObject _parent;

    internal MetaObject Parent
    {
        set => _parent = value;
    }

    public IProperty this[string key] => _collection[key];

    public IProperty AddKeyValue(string key, string value)
    {
        Property prop = null;

        if (!GetXmlNode(out _, true)) return prop;

        if (_collection.TryGetValue(key, out prop))
        {
            // Modify
            prop.Value = value;
        }
        else
        {
            // Add
            prop = new Property
            {
                Parent = this
            };

            prop.SetKey(key);
            prop.Value = value;

            _collection.Add(key, prop);
        }

        return prop;
    }

    public void RemoveKey(string key)
    {
        if (!GetXmlNode(out var node, true) || !_collection.ContainsKey(key)) return;

        var prop = this[key] as Property;
        
        var propNode = node.SelectSingleNode($@"./Property[@k='{prop.Key}']");

        if (null != propNode) node.RemoveChild(propNode);

        _collection.Remove(key);
    }

    public new void Clear()
    {
        if (GetXmlNode(out var node, true))
        {
            node.RemoveAll();
        }

        _collection.Clear();
    }

    public new int Count => _collection.Count;

    public bool ContainsKey(string key)
    {
        return _collection.ContainsKey(key);
    }

    internal virtual void LoadAll()
    {
        //-----------------------------------------
        // Local User Meta Data
        //-----------------------------------------
        if (_xmlNode is not null) return;

        const string xPath = @"./Properties";

        // Get the parent node and See if our user data already exists
        if (_parent.GetXmlNode(out var parentNode, false) && GetUserData(xPath, parentNode, out _xmlNode))
        {
            foreach (XmlNode propNode in _xmlNode.ChildNodes)
            {
                var attrs = propNode.Attributes;
                QuickCreateProperty(attrs["k"].Value, attrs["v"].Value, false);
            }
        }
    }

    internal virtual void LoadAllGlobal(XmlNode node)
    {
        const string xPath = @"./Properties";

        if (node is null) return;

        //-----------------------------------------
        // Global User Meta Data
        //-----------------------------------------
        if (!GetUserData(xPath, node, out _xmlNode))
        {
            CreateUserMetaData(node);
            GetUserData(xPath, node, out _xmlNode);
        }

        foreach (XmlNode propNode in _xmlNode.ChildNodes)
        {
            var attrs = propNode.Attributes;
            if (attrs.Count >= 2) QuickCreateProperty(attrs["k"].Value, attrs["v"].Value, true);
        }
    }

    private void QuickCreateProperty(string key, string value, bool isGlobal)
    {
        var prop = new Property
        {
            Parent = this
        };
        prop.QuickCreate(key, value, isGlobal);
        _collection.Add(key, prop);
    }

    #region XML User Data

    public override string UserDataXPath
    {
        get { return _parent.UserDataXPath + @"/Properties"; }
    }

    internal override bool GetXmlNode(out XmlNode node, bool forceCreate)
    {
        const string xPath = @"./Properties";

        node = null;
        var success = false;

        if (null == _xmlNode)
        {
            // Get the parent node and see if our user data already exists
            if (_parent.GetXmlNode(out var parentNode, forceCreate) && !GetUserData(xPath, parentNode, out _xmlNode) && forceCreate)
            {
                // Create it, and try again
                CreateUserMetaData(parentNode);
                GetUserData(xPath, parentNode, out _xmlNode);
            }
        }

        if (null != _xmlNode)
        {
            node = _xmlNode;
            success = true;
        }

        return success;
    }

    public override void CreateUserMetaData(XmlNode parentNode)
    {
        var myNode = parentNode.OwnerDocument.CreateNode(XmlNodeType.Element, "Properties", null);
        parentNode.AppendChild(myNode);
    }

    #endregion


    IEnumerator<IProperty> IEnumerable<IProperty>.GetEnumerator()
    {
        return _array.GetEnumerator();
    }

    public IEnumerator GetEnumerator()
    {
        Reset();
        return this;
    }
    
    public void Reset()
    {
        _enumerator = _collection.GetEnumerator();
    }

    public object Current => _enumerator.Current.Value;

    public bool MoveNext()
    {
        return _enumerator.MoveNext();
    }

    public new bool IsSynchronized =>
        // TODO:  Add Databases.IsSynchronized getter implementation
        false;

    public new void CopyTo(Array array, int index)
    {
        // TODO:  Add Databases.CopyTo implementation
    }

    public new object SyncRoot =>
        // TODO:  Add Databases.SyncRoot getter implementation
        null;
}