using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Xml;
using ADODB;

namespace MyMeta;

public class Database : Single, IDatabase, INameValueItem
{
    // Global properties are per Database
    internal PropertyCollection _columnProperties;
    internal PropertyCollection _databaseProperties;
    internal PropertyCollection _domainProperties;
    protected Domains _domains;
    internal PropertyCollection _foreignkeyProperties;
    internal PropertyCollection _indexProperties;
    internal PropertyCollection _parameterProperties;
    internal PropertyCollection _procedureProperties;
    protected Procedures _procedures;
    internal PropertyCollection _resultColumnProperties;
    internal PropertyCollection _tableProperties;
    protected Tables _tables;
    internal PropertyCollection _viewProperties;
    protected Views _views;

    internal Databases Databases;

    public virtual Recordset ExecuteSql(string sql)
    {
        var oRS = new Recordset();
        
        using var cn = new OleDbConnection(DbRoot.ConnectionString);
        cn.Open();
        try
        {
            cn.ChangeDatabase(Name);
        }
        catch
        {
        } // some databases don't have the concept of catalogs. Catch this and throw it out

        using var command = new OleDbCommand(sql, cn);
        command.CommandType = CommandType.Text;

        using var reader = command.ExecuteReader();
        var firstTime = true;

        while (reader.Read())
        {
            if (firstTime)
            {
                var schema = reader.GetSchemaTable();

                foreach (DataRow row in schema.Rows)
                {
                    var fieldName = row["ColumnName"].ToString();
                    var dataType = row["DataType"].ToString();
                    var length = Convert.ToInt32(row["ColumnSize"]);

                    oRS.Fields.Append(fieldName, GetADOType(dataType), length,
                        FieldAttributeEnum.adFldIsNullable, Missing.Value);
                }

                oRS.Open(Missing.Value, Missing.Value,
                    CursorTypeEnum.adOpenStatic, LockTypeEnum.adLockOptimistic, 1);

                firstTime = false;
            }

            oRS.AddNew(Missing.Value, Missing.Value);

            for (var i = 0; i < reader.FieldCount; i++)
            {
                oRS.Fields[i].Value = reader[i] is Guid ? $"{{{reader[i]}}}" : reader[i];
            }
        }

        cn.Close();

        //Move to the first record
        if (!firstTime)
        {
            oRS.MoveFirst();
        }
        else
        {
            oRS = null;
        }
        
        return oRS;
    }


    public virtual ITables Tables
    {
        get
        {
            if (_tables is not null) return _tables;

            _tables = (Tables)DbRoot.ClassFactory.CreateTables();
            _tables.DbRoot = DbRoot;
            _tables.Database = this;
            _tables.LoadAll();

            return _tables;
        }
    }

    public virtual IViews Views
    {
        get
        {
            if (_views is not null) return _views;

            _views = (Views)DbRoot.ClassFactory.CreateViews();
            _views.DbRoot = DbRoot;
            _views.Database = this;
            _views.LoadAll();

            return _views;
        }
    }

    public virtual IProcedures Procedures
    {
        get
        {
            if (_procedures is not null) return _procedures;

            _procedures = (Procedures)DbRoot.ClassFactory.CreateProcedures();
            _procedures.DbRoot = DbRoot;
            _procedures.Database = this;
            _procedures.LoadAll();

            return _procedures;
        }
    }

    public virtual IDomains Domains
    {
        get
        {
            if (_domains is not null) return _domains;

            _domains = (Domains)DbRoot.ClassFactory.CreateDomains();
            _domains.DbRoot = DbRoot;
            _domains.Database = this;
            _domains.LoadAll();

            return _domains;
        }
    }

//		virtual public IPropertyCollection GlobalProperties 
//		{ 
//			get
//			{
//				Database db = this as Database;
//				if(null == db._columnProperties)
//				{
//					db._columnProperties = new PropertyCollection();
//					db._columnProperties.Parent = this;
//
//					string xPath    = this.GlobalUserDataXPath;
//					XmlNode xmlNode = this.dbRoot.UserData.SelectSingleNode(xPath, null);
//
//					if(xmlNode is null)
//					{
//						XmlNode parentNode = db.CreateGlobalXmlNode();
//
//						xmlNode = parentNode.OwnerDocument.CreateNode(XmlNodeType.Element, "Database", null);
//						parentNode.AppendChild(xmlNode);
//					}
//
//					db._columnProperties.LoadAllGlobal(xmlNode);
//				}
//
//				return db._columnProperties;
//			}
//		}


    public override string Alias
    {
        get {
            return GetXmlNode(out var node, false) && GetUserData(node, "n", out var niceName) && !string.IsNullOrWhiteSpace(niceName)
                ? niceName
                : Name;
        }

        set
        {
            if (GetXmlNode(out var node, true))
            {
                SetUserData(node, "n", value);
            }
        }
    }

    public override string Name => GetString(Databases.f_Catalog);

    public virtual string Description => GetString(Databases.f_Description);

    public virtual string SchemaName => GetString(Databases.f_SchemaName);

    public virtual string SchemaOwner => GetString(Databases.f_SchemaOwner);

    public virtual string DefaultCharSetCatalog => GetString(Databases.f_DefCharSetCat);

    public virtual string DefaultCharSetSchema => GetString(Databases.f_DefCharSetSchema);

    public virtual string DefaultCharSetName => GetString(Databases.f_DefCharSetName);

    public virtual dbRoot Root => DbRoot;

    protected Recordset ExecuteIntoRecordset(string sql, IDbConnection cn)
    {
        var oRS = new Recordset();

        using var command = cn.CreateCommand();
        command.CommandText = sql;
        command.CommandType = CommandType.Text;

        using var reader = command.ExecuteReader();

        var firstTime = true;

        // Skip columns contains the index of any columns that we cannot handle, array types and such ...
        HashSet<int> skipColumns = [];

        while (reader.Read())
        {
            if (firstTime)
            {
                var schema = reader.GetSchemaTable();

                var colID = 0;
                foreach (DataRow row in schema.Rows)
                {
                    var fieldName = row["ColumnName"].ToString();
                    var dataType = row["DataType"].ToString();
                    var length = Convert.ToInt32(row["ColumnSize"]);

                    try
                    {
                        oRS.Fields.Append(fieldName, GetADOType(dataType), length,
                            FieldAttributeEnum.adFldIsNullable, Missing.Value);
                    }
                    catch
                    {
                        // We can't handle this column type, ie, Firebird array types
                        skipColumns.Add(colID);
                    }
                    colID++;
                }

                oRS.Open(Missing.Value, Missing.Value,
                    CursorTypeEnum.adOpenStatic, LockTypeEnum.adLockOptimistic, 1);

                firstTime = false;
            }

            oRS.AddNew(Missing.Value, Missing.Value);

            for (int i = 0, j = 0; i < reader.FieldCount; i++)
                // Skip columns that we cannot handle
            {
                if (!skipColumns.Contains(i))
                {
                    if (reader[j] is Guid)
                    {
                        oRS.Fields[j].Value = $"{{{reader[j]}}}";
                    }
                    else
                    {
                        try
                        {
                            oRS.Fields[j].Value = reader[j];
                        }
                        catch
                        {
                            // For some reason it wouldn't accept this value?
                            oRS.Fields[j].Value = DBNull.Value;
                        }
                    }
                    j++;
                }
            }
        }

        cn.Close();

        //Move to the first record
        if (!firstTime)
        {
            oRS.MoveFirst();
        }
        else
        {
            oRS = null;
        }
        

        return oRS;
    }

    protected DataTypeEnum GetADOType(string sType)
    {
        return sType switch
        {
            null => DataTypeEnum.adEmpty,
            "System.Byte" => DataTypeEnum.adUnsignedTinyInt,
            "System.SByte" => DataTypeEnum.adTinyInt,
            "System.Boolean" => DataTypeEnum.adBoolean,
            "System.Int16" => DataTypeEnum.adSmallInt,
            "System.Int32" => DataTypeEnum.adInteger,
            "System.Int64" => DataTypeEnum.adBigInt,
            "System.Single" => DataTypeEnum.adSingle,
            "System.Double" => DataTypeEnum.adDouble,
            "System.Decimal" => DataTypeEnum.adDecimal,
            "System.DateTime" => DataTypeEnum.adDate,
            "System.Guid" => DataTypeEnum.adGUID,
            "System.String" => DataTypeEnum.adBSTR //.adChar;
            ,
            "System.Byte[]" => DataTypeEnum.adBinary,
            "System.Array" => DataTypeEnum.adArray,
            "System.Object" => DataTypeEnum.adVariant,
            _ => 0
        };
    }
    
    public override string UserDataXPath => Databases.UserDataXPath + @"/Database[@p='" + Name + "']";

    public override string GlobalUserDataXPath => @"//MyMeta/Global/Databases/Database[@p='" + Name + "']";

    internal override bool GetXmlNode(out XmlNode node, bool forceCreate)
    {
        node = null;
        var success = false;

        if (null == _xmlNode)
        {
            // Get the parent node
            if (Databases.GetXmlNode(out var parentNode, forceCreate))
            {
                // See if our user data already exists
                var xPath = $@"./Database[@p='{Name}']";
                if (!GetUserData(xPath, parentNode, out _xmlNode) && forceCreate)
                {
                    // Create it, and try again
                    CreateUserMetaData(parentNode);
                    GetUserData(xPath, parentNode, out _xmlNode);
                }
            }
        }

        if (null != _xmlNode)
        {
            node = _xmlNode;
            success = true;
        }

        return success;
    }

    internal XmlNode CreateGlobalXmlNode()
    {
        XmlNode node = null;

        DbRoot.GetXmlNode(out var parentNode, true);

        node = parentNode.SelectSingleNode(@"./Global");
        if (node is null)
        {
            node = parentNode.OwnerDocument.CreateNode(XmlNodeType.Element, "Global", null);
            parentNode.AppendChild(node);
        }

        parentNode = node;

        node = parentNode.SelectSingleNode(@"./Databases");
        if (node is null)
        {
            node = parentNode.OwnerDocument.CreateNode(XmlNodeType.Element, "Databases", null);
            parentNode.AppendChild(node);
        }

        parentNode = node;

        node = parentNode.SelectSingleNode(@"./Database[@p='" + Name + "']");
        if (node is null)
        {
            node = parentNode.OwnerDocument.CreateNode(XmlNodeType.Element, "Database", null);
            parentNode.AppendChild(node);

            XmlAttribute attr;

            attr = node.OwnerDocument.CreateAttribute("p");
            attr.Value = Name;
            node.Attributes.Append(attr);
        }

        return node;
    }

    public override void CreateUserMetaData(XmlNode parentNode)
    {
        var myNode = parentNode.OwnerDocument.CreateNode(XmlNodeType.Element, "Database", null);
        parentNode.AppendChild(myNode);

        var attr = parentNode.OwnerDocument.CreateAttribute("p");
        attr.Value = Name;
        myNode.Attributes.Append(attr);

        attr = parentNode.OwnerDocument.CreateAttribute("n");
        attr.Value = "";
        myNode.Attributes.Append(attr);
    }


    public string ItemName => Name;

    public string ItemValue => Name;

}