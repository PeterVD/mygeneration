using System;
using System.Runtime.InteropServices;

namespace MyMeta.ISeries;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IDatabases))]
#endif
public class ISeriesDatabases : Databases
{
    internal override void LoadAll()
    {
        try
        {
            var database = (ISeriesDatabase)DbRoot.ClassFactory.CreateDatabase();
            //database._name = cn.DataSource;

            OleDbConnection cn = new OleDbConnection(DbRoot.ConnectionString);
            cn.Open();
            OleDbCommand cmd = cn.CreateCommand();
            cmd.CommandText = "SELECT c.CATALOG_NAME, t.TABLE_SCHEMA FROM SYSTABLES t, QSYS2.SYSCATALOGS c";
            OleDbDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                //database. = reader["CATALOG_NAME"].ToString();
                database._name = reader["TABLE_SCHEMA"].ToString();
                break;
            }

            reader.Close();
            cn.Close();

            database.DbRoot = DbRoot;
            database.Databases = this;
            _array.Add(database);
        }
        catch (Exception ex)
        {
            var message = ex.Message;
        }
    }
}