using System;
using System.Data;
using System.Runtime.InteropServices;

namespace MyMeta.ISeries;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IForeignKeys))]
#endif
public class ISeriesForeignKeys : ForeignKeys
{
    internal override void LoadAll()
    {
        try
        {
            var sql1 =
                @"SELECT c.CONSTRAINT_SCHEMA, c.CONSTRAINT_NAME, cpk.CONSTRAINT_NAME as PK_CONSTRAINT_NAME, c.CONSTRAINT_TYPE, c.TABLE_SCHEMA, c.TABLE_NAME, col.COLUMN_NAME
FROM SYSCST c, SYSCST cpk, QSYS2.SYSREFCST rf, SYSCSTCOL col
WHERE c.CONSTRAINT_SCHEMA = rf.CONSTRAINT_SCHEMA
AND c.CONSTRAINT_NAME = rf.CONSTRAINT_NAME
AND rf.UNIQUE_CONSTRAINT_SCHEMA = cpk.CONSTRAINT_SCHEMA
AND rf.UNIQUE_CONSTRAINT_NAME = cpk.CONSTRAINT_NAME
AND col.CONSTRAINT_SCHEMA = c.CONSTRAINT_SCHEMA
AND col.CONSTRAINT_NAME = c.CONSTRAINT_NAME
AND c.CONSTRAINT_TYPE = 'FOREIGN KEY'
AND c.TABLE_SCHEMA = '" + Table.Schema + @"' 
AND c.TABLE_NAME = '" + Table.Name + @"'
ORDER BY c.CONSTRAINT_SCHEMA, c.CONSTRAINT_NAME";

            var sql2 =
                @"SELECT c.CONSTRAINT_SCHEMA, c.CONSTRAINT_NAME, cpk.CONSTRAINT_NAME as PK_CONSTRAINT_NAME, c.CONSTRAINT_TYPE, cpk.TABLE_SCHEMA, cpk.TABLE_NAME, col.COLUMN_NAME
FROM SYSCST c, SYSCST cpk, QSYS2.SYSREFCST rf, SYSCSTCOL col
WHERE c.CONSTRAINT_SCHEMA = rf.CONSTRAINT_SCHEMA
AND c.CONSTRAINT_NAME = rf.CONSTRAINT_NAME
AND rf.UNIQUE_CONSTRAINT_SCHEMA = cpk.CONSTRAINT_SCHEMA
AND rf.UNIQUE_CONSTRAINT_NAME = cpk.CONSTRAINT_NAME
AND col.CONSTRAINT_SCHEMA = cpk.CONSTRAINT_SCHEMA
AND col.CONSTRAINT_NAME = cpk.CONSTRAINT_NAME
AND c.CONSTRAINT_TYPE = 'FOREIGN KEY'
AND c.TABLE_SCHEMA = '" + Table.Schema + @"' 
AND c.TABLE_NAME = '" + Table.Name + @"'
ORDER BY c.CONSTRAINT_SCHEMA, c.CONSTRAINT_NAME";

            OleDbDataAdapter adapter1 = new OleDbDataAdapter(sql1, DbRoot.ConnectionString);
            var dataTableFk = new DataTable();
            adapter1.Fill(dataTableFk);

            OleDbDataAdapter adapter2 = new OleDbDataAdapter(sql2, DbRoot.ConnectionString);
            var dataTablePk = new DataTable();
            adapter2.Fill(dataTablePk);

            PopulateArray(dataTableFk, dataTablePk);
        }
        catch
        {
        }
    }

    internal void PopulateArray(DataTable dataTableFk, DataTable dataTablePk)
    {
        var metaData = new DataTable();
        metaData.Columns.AddRange(
            new[]
            {
                new DataColumn("PK_TABLE_CATALOG"),
                new DataColumn("PK_TABLE_SCHEMA"),
                new DataColumn("PK_TABLE_NAME"),
                new DataColumn("FK_TABLE_CATALOG"),
                new DataColumn("FK_TABLE_SCHEMA"),
                new DataColumn("FK_TABLE_NAME"),
                new DataColumn("ORDINAL"),
                new DataColumn("PK_NAME"),
                new DataColumn("FK_NAME"),
                new DataColumn("UPDATE_RULE"),
                new DataColumn("DELETE_RULE"),
                new DataColumn("DEFERRABILITY")
            }
        );
        BindToColumns(metaData);

        ForeignKey key = null;

        var j = 0;
        for (var i = 0; i < dataTableFk.Rows.Count; i++)
        {
            var rowPk = dataTablePk.Rows[i];
            var rowFk = dataTableFk.Rows[i];
            try
            {
                var pkschema = rowPk["TABLE_SCHEMA"].ToString();
                var pktable = rowPk["TABLE_NAME"].ToString();
                var pkcolumn = rowPk["COLUMN_NAME"].ToString();
                var fkschema = rowPk["TABLE_SCHEMA"].ToString();
                var fktable = rowFk["TABLE_NAME"].ToString();
                var fkcolumn = rowFk["COLUMN_NAME"].ToString();
                var fkKeyName = rowFk["CONSTRAINT_NAME"].ToString();
                var pkKeyName = rowFk["PK_CONSTRAINT_NAME"].ToString();

                //DataRow row = rowView.Row;
                key = GetByName(fkKeyName);

                if (null == key)
                {
                    var row = metaData.NewRow();

                    row["PK_TABLE_CATALOG"] = pkschema;
                    row["PK_TABLE_SCHEMA"] = pkschema;
                    row["PK_TABLE_NAME"] = pktable;
                    row["FK_TABLE_CATALOG"] = fkschema;
                    row["FK_TABLE_SCHEMA"] = fkschema;
                    row["FK_TABLE_NAME"] = fktable;
                    row["ORDINAL"] = j++.ToString();
                    row["PK_NAME"] = pkKeyName;
                    row["FK_NAME"] = fkKeyName;
                    row["UPDATE_RULE"] = "";
                    row["DELETE_RULE"] = "";
                    row["DEFERRABILITY"] = 0;

                    metaData.Rows.Add(row);

                    key = (ForeignKey)DbRoot.ClassFactory.CreateForeignKey();
                    key.DbRoot = DbRoot;
                    key.ForeignKeys = this;
                    key.Row = row;
                    _array.Add(key);
                }

                key.AddForeignColumn(pkschema, pkschema, pktable, pkcolumn, true);
                key.AddForeignColumn(fkschema, fkschema, fktable, fkcolumn, false);
            }
            catch (Exception ex)
            {
                var tmp = ex.Message;
            }
        }
    }

    private void BindToColumns(DataTable metaData)
    {
        if (false == _fieldsBound)
        {
            if (metaData.Columns.Contains("PK_TABLE_CATALOG")) f_PKTableCatalog = metaData.Columns["PK_TABLE_CATALOG"];
            if (metaData.Columns.Contains("PK_TABLE_SCHEMA")) f_PKTableSchema = metaData.Columns["PK_TABLE_SCHEMA"];
            if (metaData.Columns.Contains("PK_TABLE_NAME")) f_PKTableName = metaData.Columns["PK_TABLE_NAME"];
            if (metaData.Columns.Contains("FK_TABLE_CATALOG")) f_FKTableCatalog = metaData.Columns["FK_TABLE_CATALOG"];
            if (metaData.Columns.Contains("FK_TABLE_SCHEMA")) f_FKTableSchema = metaData.Columns["FK_TABLE_SCHEMA"];
            if (metaData.Columns.Contains("FK_TABLE_NAME")) f_FKTableName = metaData.Columns["FK_TABLE_NAME"];
            if (metaData.Columns.Contains("ORDINAL")) f_Ordinal = metaData.Columns["ORDINAL"];
            if (metaData.Columns.Contains("UPDATE_RULE")) f_UpdateRule = metaData.Columns["UPDATE_RULE"];
            if (metaData.Columns.Contains("DELETE_RULE")) f_DeleteRule = metaData.Columns["DELETE_RULE"];
            if (metaData.Columns.Contains("PK_NAME")) f_PKName = metaData.Columns["PK_NAME"];
            if (metaData.Columns.Contains("FK_NAME")) f_FKName = metaData.Columns["FK_NAME"];
            if (metaData.Columns.Contains("DEFERRABILITY")) f_Deferrability = metaData.Columns["DEFERRABILITY"];

            _fieldsBound = true;
        }
    }
}