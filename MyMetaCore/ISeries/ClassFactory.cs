using System.Data;
using System.Runtime.InteropServices;

namespace MyMeta.ISeries;

#if ENTERPRISE

[ComVisible(false)]
#endif
public class ClassFactory : IClassFactory
{
    public ITables CreateTables()
    {
        return new ISeriesTables();
    }

    public ITable CreateTable()
    {
        return new ISeriesTable();
    }

    public IColumn CreateColumn()
    {
        return new ISeriesColumn();
    }

    public IColumns CreateColumns()
    {
        return new ISeriesColumns();
    }

    public IDatabase CreateDatabase()
    {
        return new ISeriesDatabase();
    }

    public IDatabases CreateDatabases()
    {
        return new ISeriesDatabases();
    }

    public IProcedure CreateProcedure()
    {
        return new ISeriesProcedure();
    }

    public IProcedures CreateProcedures()
    {
        return new ISeriesProcedures();
    }

    public IView CreateView()
    {
        return new ISeriesView();
    }

    public IViews CreateViews()
    {
        return new ISeriesViews();
    }

    public IParameter CreateParameter()
    {
        return new ISeriesParameter();
    }

    public IParameters CreateParameters()
    {
        return new ISeriesParameters();
    }

    public IForeignKey CreateForeignKey()
    {
        return new ISeriesForeignKey();
    }

    public IForeignKeys CreateForeignKeys()
    {
        return new ISeriesForeignKeys();
    }

    public IIndex CreateIndex()
    {
        return new ISeriesIndex();
    }

    public IIndexes CreateIndexes()
    {
        return new ISeriesIndexes();
    }

    public IDomain CreateDomain()
    {
        return new ISeriesDomain();
    }

    public IDomains CreateDomains()
    {
        return new ISeriesDomains();
    }

    public IResultColumn CreateResultColumn()
    {
        return new ISeriesResultColumn();
    }

    public IResultColumns CreateResultColumns()
    {
        return new ISeriesResultColumns();
    }


    public IProviderType CreateProviderType()
    {
        return new ProviderType();
    }

    public IProviderTypes CreateProviderTypes()
    {
        return new ProviderTypes();
    }

    public IDbConnection CreateConnection()
    {
        return new System.Data.OleDb.OleDbConnection();
    }

    public static void Register()
    {
        InternalDriver.Register("ISERIES",
            new InternalDriver
            (typeof(ClassFactory)
                , "PROVIDER=IBMDA400; DATA SOURCE=MY_SYSTEM_NAME;USER ID=myUserName;PASSWORD=myPwd;DEFAULT COLLECTION=MY_LIBRARY;"
                , true));
    }
}