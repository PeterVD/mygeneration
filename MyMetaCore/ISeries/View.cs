using System.Runtime.InteropServices;

namespace MyMeta.ISeries;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IView))]
#endif
public class ISeriesView : View
{
}