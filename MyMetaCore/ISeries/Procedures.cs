using System.Runtime.InteropServices;

namespace MyMeta.ISeries;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IProcedures))]
#endif
public class ISeriesProcedures : Procedures
{
    internal override void LoadAll()
    {
        try
        {
            var metaData = LoadData(OleDbSchemaGuid.Procedures, null);

            PopulateArray(metaData);
        }
        catch
        {
        }
    }
}