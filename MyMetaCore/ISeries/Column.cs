using System.Runtime.InteropServices;

namespace MyMeta.ISeries;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IColumn))]
#endif
public class ISeriesColumn : Column
{
    public override bool IsInPrimaryKey
    {
        get
        {
            if (null == Columns.Table) return false;

            var cols = Columns.Table.PrimaryKeys as Columns;
            var col = cols.GetByPhysicalName(Name);

            return null == col ? false : true;
        }
    }

    public override bool IsAutoKey
    {
        get
        {
            var cols = Columns as ISeriesColumns;
            return GetBool(cols.f_AutoKey);
        }
    }

    public override string DataTypeName
    {
        get
        {
            var cols = Columns as ISeriesColumns;
            return GetString(cols.f_TypeName);
        }
    }

    public override string DataTypeNameComplete
    {
        get
        {
            var cols = Columns as ISeriesColumns;
            return GetString(cols.f_FullTypeName);
        }
    }

    internal override Column Clone()
    {
        var c = base.Clone();

        return c;
    }
}