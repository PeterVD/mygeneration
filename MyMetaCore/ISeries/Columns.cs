using System.Data;
using System.Runtime.InteropServices;

namespace MyMeta.ISeries;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IColumns))]
#endif
public class ISeriesColumns : Columns
{
    internal DataColumn f_AutoKey;
    internal DataColumn f_FullTypeName;
    internal DataColumn f_InPrimaryKey;
    internal DataColumn f_TypeName;


    internal override void LoadForTable()
    {
        var metaData = LoadData(OleDbSchemaGuid.Columns, new object[] { null, null, Table.Name });

        PopulateArray(metaData);

        LoadExtraData("T");
    }

    internal override void LoadForView()
    {
        var metaData = LoadData(OleDbSchemaGuid.Columns, new object[] { null, null, View.Name });

        PopulateArray(metaData);

        LoadExtraData("V");
    }

    private void LoadExtraData(string type)
    {
        try
        {
            var name = "T" == type ? Table.Name : View.Name;
            var schema = "T" == type ? Table.Schema : View.Schema;
            //string select = "SELECT COLNAME, TYPENAME, CODEPAGE, IDENTITY FROM SYSCOLUMNS WHERE TABNAME = '" + name + "' ORDER BY COLNO";
            var select = "SELECT COLUMN_NAME, DATA_TYPE, CCSID, IS_IDENTITY FROM SYSCOLUMNS WHERE TABLE_SCHEMA = '" + schema +
                         "' AND TABLE_NAME = '" + name + "' ORDER BY ORDINAL_POSITION";

            OleDbDataAdapter adapter = new OleDbDataAdapter(select, DbRoot.ConnectionString);
            var dataTable = new DataTable();

            adapter.Fill(dataTable);

            if (_array.Count > 0)
            {
                var col = _array[0] as Column;

                if (col._row.Table.Columns.IndexOf("TYPE_NAME") == -1)
                {
                    f_TypeName = new DataColumn("TYPE_NAME", typeof(string));
                    col._row.Table.Columns.Add(f_TypeName);
                }

                if (col._row.Table.Columns.IndexOf("FULL_TYPE_NAME") == -1)
                {
                    f_FullTypeName = new DataColumn("FULL_TYPE_NAME", typeof(string));
                    col._row.Table.Columns.Add(f_FullTypeName);
                }

                if (col._row.Table.Columns.IndexOf("AUTO_INCREMENT") == -1)
                {
                    f_AutoKey = new DataColumn("AUTO_INCREMENT", typeof(bool));
                    col._row.Table.Columns.Add(f_AutoKey);
                }

                var rows = dataTable.Rows;
                var identity = "";
                var colName = "";

                var count = _array.Count;
                Column c = null;

                foreach (DataRow row in rows)
                {
                    colName = row["COLUMN_NAME"] as string;

                    c = this[colName] as Column;

                    identity = row["IS_IDENTITY"] as string;
                    c._row["AUTO_INCREMENT"] = identity[0] == 'Y' ? true : false;
                    c._row["TYPE_NAME"] = (row["DATA_TYPE"] as string).Trim();

                    var codepage = -1;
                    try
                    {
                        codepage = (int)row["CCSID"];
                    }
                    catch
                    {
                    }


                    var fulltypename = c.DataTypeName;
                    switch (fulltypename)
                    {
                        case "CHAR":
                        case "VARCHAR":
                        case "GRAPHIC":
                        case "VARGRAPHIC":
                        case "BINARY":
                        case "VARBINARY":
                        case "CLOB":
                        case "BLOB":
                        case "DBCLOB":
                        case "DATALINK":
                            var tmp = int.Parse(c._row[f_MaxLength].ToString());
                            fulltypename += "(" + tmp + ")";
                            break;
                        case "DECIMAL":
                        case "NUMERIC":
                            var tmp1 = int.Parse(c._row[f_NumericPrecision].ToString());
                            var tmp2 = int.Parse(c._row[f_NumericScale].ToString());
                            fulltypename += "(" + tmp1 + "," + tmp2 + ")";
                            break;
                    }

                    if (codepage == 65535)
                        // Check for "bit data"
                        switch (c.DataTypeName)
                        {
                            case "CHAR":
                            case "VARCHAR":
                                c._row["TYPE_NAME"] = c.DataTypeName + " FOR BIT DATA";
                                fulltypename += " FOR BIT DATA";
                                break;
                        }
                    else
                        // Check for "bit data"
                        switch (c.DataTypeName)
                        {
                            case "CHAR":
                            case "VARCHAR":
                                fulltypename += " CCSID " + codepage;
                                break;
                        }

                    c._row["FULL_TYPE_NAME"] = fulltypename;
                }
            }
        }
        catch
        {
        }
    }
}