using System.Runtime.InteropServices;

namespace MyMeta.ISeries;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IResultColumn))]
#endif
public class ISeriesResultColumn : ResultColumn
{
    #region Properties

    public override string Alias => name;

    public override string DataTypeName => typeName;

    public override int Ordinal => ordinal;

    internal string name = "";
    internal string typeName = "";
    internal int ordinal;

    #endregion
}