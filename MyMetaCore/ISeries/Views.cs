using System.Runtime.InteropServices;

namespace MyMeta.ISeries;

#if ENTERPRISE

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
[ComDefaultInterface(typeof(IViews))]
#endif
public class ISeriesViews : Views
{
    internal override void LoadAll()
    {
        try
        {
            var type = DbRoot.ShowSystemData ? "SYSTEM VIEW" : "VIEW";
            var metaData = LoadData(OleDbSchemaGuid.Tables, new object[] { null, null, null, type });

            PopulateArray(metaData);
        }
        catch
        {
        }
    }
}