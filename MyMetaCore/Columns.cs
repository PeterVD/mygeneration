using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Xml;

namespace MyMeta;

public class Columns : Collection<IColumn>, IColumns, IEnumerable, ICollection
{
    internal bool FKsLoaded;
    internal ForeignKey ForeignKey;
    internal Index Index;

    internal Table Table;
    internal View View;

    #region IList Members

    object IList.this[int index]
    {
        get => this[index];
        set { }
    }

    #endregion

    internal virtual void LoadForTable()
    {
    }

    internal virtual void LoadForView()
    {
    }

    internal void PopulateArray(DataTable metaData)
    {
        BindToColumns(metaData);

        if (metaData.DefaultView.Count > 0)
        {
            var enumerator = metaData.DefaultView.GetEnumerator();
            while (enumerator.MoveNext())
            {
                var rowView = enumerator.Current as DataRowView;

                var column = (Column)DbRoot.ClassFactory.CreateColumn();
                column.DbRoot = DbRoot;
                column.Columns = this;
                column.Row = rowView.Row;
                _array.Add(column);
            }
        }
    }

    internal void AddColumn(Column column)
    {
        var count = _array.Count;
        for (var i = 0; i < count; i++)
        {
            var c = _array[i] as Column;

            if (c.Name == column.Name) return;
        }

        _array.Add(column);
    }

    #region DataColumn Binding Stuff

    internal DataColumn f_TableCatalog;
    internal DataColumn f_TableSchema;
    internal DataColumn f_TableName;
    internal DataColumn f_Name;
    internal DataColumn f_Guid;
    internal DataColumn f_PropID;
    internal DataColumn f_Ordinal;
    internal DataColumn f_HasDefault;
    internal DataColumn f_Default;
    internal DataColumn f_Flags;
    internal DataColumn f_IsNullable;
    internal DataColumn f_DataType;
    internal DataColumn f_TypeGuid;
    internal DataColumn f_MaxLength;
    internal DataColumn f_OctetLength;
    internal DataColumn f_NumericPrecision;
    internal DataColumn f_NumericScale;
    internal DataColumn f_DatetimePrecision;
    internal DataColumn f_CharSetCatalog;
    internal DataColumn f_CharSetSchema;
    internal DataColumn f_CharSetName;
    internal DataColumn f_CollationCatalog;
    internal DataColumn f_CollationSchema;
    internal DataColumn f_CollationName;
    internal DataColumn f_DomainCatalog;
    internal DataColumn f_DomainSchema;
    internal DataColumn f_DomainName;
    internal DataColumn f_Description;
    internal DataColumn f_LCID;
    internal DataColumn f_CompFlags;
    internal DataColumn f_SortID;
    internal DataColumn f_TDSCollation;
    internal DataColumn f_IsComputed;
    internal DataColumn f_IsAutoKey;
    internal DataColumn f_AutoKeySeed;
    internal DataColumn f_AutoKeyIncrement;


    private void BindToColumns(DataTable metaData)
    {
        if (_fieldsBound) return;

        if (metaData.Columns.Contains("TABLE_CATALOG")) f_TableCatalog = metaData.Columns["TABLE_CATALOG"];
        if (metaData.Columns.Contains("TABLE_SCHEMA")) f_TableSchema = metaData.Columns["TABLE_SCHEMA"];
        if (metaData.Columns.Contains("TABLE_NAME")) f_TableName = metaData.Columns["TABLE_NAME"];
        if (metaData.Columns.Contains("COLUMN_NAME")) f_Name = metaData.Columns["COLUMN_NAME"];
        if (metaData.Columns.Contains("COLUMN_GUID")) f_Guid = metaData.Columns["COLUMN_GUID"];
        if (metaData.Columns.Contains("COLUMN_PROPID")) f_PropID = metaData.Columns["COLUMN_PROPID"];
        if (metaData.Columns.Contains("ORDINAL_POSITION")) f_Ordinal = metaData.Columns["ORDINAL_POSITION"];
        if (metaData.Columns.Contains("COLUMN_HASDEFAULT")) f_HasDefault = metaData.Columns["COLUMN_HASDEFAULT"];
        if (metaData.Columns.Contains("COLUMN_DEFAULT")) f_Default = metaData.Columns["COLUMN_DEFAULT"];
        if (metaData.Columns.Contains("COLUMN_FLAGS")) f_Flags = metaData.Columns["COLUMN_FLAGS"];
        if (metaData.Columns.Contains("IS_NULLABLE")) f_IsNullable = metaData.Columns["IS_NULLABLE"];
        if (metaData.Columns.Contains("DATA_TYPE")) f_DataType = metaData.Columns["DATA_TYPE"];
        if (metaData.Columns.Contains("TYPE_GUID")) f_TypeGuid = metaData.Columns["TYPE_GUID"];
        if (metaData.Columns.Contains("CHARACTER_MAXIMUM_LENGTH")) f_MaxLength = metaData.Columns["CHARACTER_MAXIMUM_LENGTH"];
        if (metaData.Columns.Contains("CHARACTER_OCTET_LENGTH")) f_OctetLength = metaData.Columns["CHARACTER_OCTET_LENGTH"];
        if (metaData.Columns.Contains("NUMERIC_PRECISION")) f_NumericPrecision = metaData.Columns["NUMERIC_PRECISION"];
        if (metaData.Columns.Contains("NUMERIC_SCALE")) f_NumericScale = metaData.Columns["NUMERIC_SCALE"];
        if (metaData.Columns.Contains("DATETIME_PRECISION")) f_DatetimePrecision = metaData.Columns["DATETIME_PRECISION"];
        if (metaData.Columns.Contains("CHARACTER_SET_CATALOG")) f_CharSetCatalog = metaData.Columns["CHARACTER_SET_CATALOG"];
        if (metaData.Columns.Contains("CHARACTER_SET_SCHEMA")) f_CharSetSchema = metaData.Columns["CHARACTER_SET_SCHEMA"];
        if (metaData.Columns.Contains("CHARACTER_SET_NAME")) f_CharSetName = metaData.Columns["CHARACTER_SET_NAME"];
        if (metaData.Columns.Contains("COLLATION_CATALOG")) f_CollationCatalog = metaData.Columns["COLLATION_CATALOG"];
        if (metaData.Columns.Contains("COLLATION_SCHEMA")) f_CollationSchema = metaData.Columns["COLLATION_SCHEMA"];
        if (metaData.Columns.Contains("COLLATION_NAME")) f_CollationName = metaData.Columns["COLLATION_NAME"];
        if (metaData.Columns.Contains("DOMAIN_CATALOG")) f_DomainCatalog = metaData.Columns["DOMAIN_CATALOG"];
        if (metaData.Columns.Contains("DOMAIN_SCHEMA")) f_DomainSchema = metaData.Columns["DOMAIN_SCHEMA"];
        if (metaData.Columns.Contains("DOMAIN_NAME")) f_DomainName = metaData.Columns["DOMAIN_NAME"];
        if (metaData.Columns.Contains("DESCRIPTION")) f_Description = metaData.Columns["DESCRIPTION"];
        if (metaData.Columns.Contains("COLUMN_LCID")) f_LCID = metaData.Columns["COLUMN_LCID"];
        if (metaData.Columns.Contains("COLUMN_COMPFLAGS")) f_CompFlags = metaData.Columns["COLUMN_COMPFLAGS"];
        if (metaData.Columns.Contains("COLUMN_SORTID")) f_SortID = metaData.Columns["COLUMN_SORTID"];
        if (metaData.Columns.Contains("COLUMN_TDSCOLLATION")) f_TDSCollation = metaData.Columns["COLUMN_TDSCOLLATION"];
        if (metaData.Columns.Contains("IS_COMPUTED")) f_IsComputed = metaData.Columns["IS_COMPUTED"];
        if (metaData.Columns.Contains("IS_AUTO_KEY")) f_IsAutoKey = metaData.Columns["IS_AUTO_KEY"];
        if (metaData.Columns.Contains("AUTO_KEY_SEED")) f_AutoKeySeed = metaData.Columns["AUTO_KEY_SEED"];
        if (metaData.Columns.Contains("AUTO_KEY_INCREMENT")) f_AutoKeyIncrement = metaData.Columns["AUTO_KEY_INCREMENT"];
    }

    #endregion

    #region indexers

    public IColumn this[object index]
    {
        get
        {
            if (index.GetType() == Types.StringType) return GetByPhysicalName(index as string);

            var idx = Convert.ToInt32(index);
            return _array[idx];
        }
    }

    public Column GetByName(string name)
    {
        Column obj = null;

        var count = _array.Count;
        for (var i = 0; i < count; i++)
        {
            var tmp = _array[i] as Column;

            if (CompareStrings(name, tmp.Name))
            {
                obj = tmp;
                break;
            }
        }

        return obj;
    }
    
    internal Column GetByPhysicalName(string name)
    {
        Column obj = null;

        var count = _array.Count;
        for (var i = 0; i < count; i++)
        {
            var tmp = _array[i] as Column;

            if (CompareStrings(name, tmp.Name))
            {
                obj = tmp;
                break;
            }
        }

        return obj;
    }

    #endregion

    #region IEnumerable Members

    IEnumerator<IColumn> IEnumerable<IColumn>.GetEnumerator()
    {
        return _array.GetEnumerator();
    }

    public IEnumerator GetEnumerator()
    {
        return _array.GetEnumerator();
    }

    #endregion

    #region XML User Data

    public override string UserDataXPath
    {
        get
        {
            var xPath = "";

            if (null != Table)
                xPath = Table.UserDataXPath;
            else if (null != View)
                xPath = View.UserDataXPath;
            else if (null != Index)
                xPath = Index.UserDataXPath;
            else if (null != ForeignKey) xPath = ForeignKey.UserDataXPath;

            return xPath + @"/Columns";
        }
    }

    private MetaObject GetParent()
    {
        MetaObject parent = null;

        if (null != Table)
            parent = Table;
        else if (null != View)
            parent = View;
        else if (null != Index)
            parent = Index.Indexes.Table;
        else if (null != ForeignKey) return ForeignKey.ForeignKeys.Table;

        return parent;
    }

    internal IDatabase GetDatabase()
    {
        IDatabase database = null;

        if (null != Table)
            database = Table.Database;
        else if (null != View)
            database = View.Database;
        else if (null != Index)
            database = Index.Indexes.Table.Database;
        else if (null != ForeignKey) database = ForeignKey.ForeignKeys.Table.Database;

        return database;
    }

    internal override bool GetXmlNode(out XmlNode node, bool forceCreate)
    {
        node = null;
        var success = false;

        if (null == _xmlNode)
        {
            // Get the parent node
            if (GetParent().GetXmlNode(out var parentNode, forceCreate))
            {
                // See if our user data already exists
                const string xPath = @"./Columns";
                if (!GetUserData(xPath, parentNode, out _xmlNode) && forceCreate)
                {
                    // Create it, and try again
                    CreateUserMetaData(parentNode);
                    GetUserData(xPath, parentNode, out _xmlNode);
                }
            }
        }

        if (null != _xmlNode)
        {
            node = _xmlNode;
            success = true;
        }

        return success;
    }

    public override void CreateUserMetaData(XmlNode parentNode)
    {
        var myNode = parentNode.OwnerDocument.CreateNode(XmlNodeType.Element, "Columns", null);
        parentNode.AppendChild(myNode);
    }

    #endregion
}