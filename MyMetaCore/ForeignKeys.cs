using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Runtime.InteropServices;
using System.Xml;

namespace MyMeta;

#if ENTERPRISE

[ComVisible(false)]
[ClassInterface(ClassInterfaceType.AutoDual)]
#endif
public class ForeignKeys : Collection<IForeignKey>, IForeignKeys, IEnumerable, ICollection
{
    internal DataColumn f_Deferrability;
    internal DataColumn f_DeleteRule;
    internal DataColumn f_FKName;
    internal DataColumn f_FKTableCatalog;
    internal DataColumn f_FKTableName;
    internal DataColumn f_FKTableSchema;
    internal DataColumn f_Ordinal;
    internal DataColumn f_PKName;
    internal DataColumn f_PKTableCatalog;
    internal DataColumn f_PKTableName;
    internal DataColumn f_PKTableSchema;
    internal DataColumn f_UpdateRule;

    internal Table Table;

    #region IList Members

    object IList.this[int index]
    {
        get => this[index];
        set { }
    }

    #endregion

    private void BindToColumns(DataTable metaData)
    {
        if (_fieldsBound) return;

        if (metaData.Columns.Contains("PK_TABLE_CATALOG")) f_PKTableCatalog = metaData.Columns["PK_TABLE_CATALOG"];
        if (metaData.Columns.Contains("PK_TABLE_SCHEMA")) f_PKTableSchema = metaData.Columns["PK_TABLE_SCHEMA"];
        if (metaData.Columns.Contains("PK_TABLE_NAME")) f_PKTableName = metaData.Columns["PK_TABLE_NAME"];
        if (metaData.Columns.Contains("FK_TABLE_CATALOG")) f_FKTableCatalog = metaData.Columns["FK_TABLE_CATALOG"];
        if (metaData.Columns.Contains("FK_TABLE_SCHEMA")) f_FKTableSchema = metaData.Columns["FK_TABLE_SCHEMA"];
        if (metaData.Columns.Contains("FK_TABLE_NAME")) f_FKTableName = metaData.Columns["FK_TABLE_NAME"];
        if (metaData.Columns.Contains("ORDINAL")) f_Ordinal = metaData.Columns["ORDINAL"];
        if (metaData.Columns.Contains("UPDATE_RULE")) f_UpdateRule = metaData.Columns["UPDATE_RULE"];
        if (metaData.Columns.Contains("DELETE_RULE")) f_DeleteRule = metaData.Columns["DELETE_RULE"];
        if (metaData.Columns.Contains("PK_NAME")) f_PKName = metaData.Columns["PK_NAME"];
        if (metaData.Columns.Contains("FK_NAME")) f_FKName = metaData.Columns["FK_NAME"];
        if (metaData.Columns.Contains("DEFERRABILITY")) f_Deferrability = metaData.Columns["DEFERRABILITY"];

        _fieldsBound = true;
    }

    internal virtual void LoadAll()
    {
    }

    internal virtual void LoadAllIndirect()
    {
    }

    internal void PopulateArray(DataTable metaData)
    {
        BindToColumns(metaData);

        foreach (DataRowView rowView in metaData.DefaultView)
        {
            try
            {
                var row = rowView.Row;

                var keyName = row["FK_NAME"] as string;

                var key = GetByName(keyName);

                if (null == key)
                {
                    key = (ForeignKey)DbRoot.ClassFactory.CreateForeignKey();
                    key.DbRoot = DbRoot;
                    key.ForeignKeys = this;
                    key.Row = row;
                    _array.Add(key);
                }

                var catalog = DBNull.Value == row["PK_TABLE_CATALOG"] ? string.Empty : row["PK_TABLE_CATALOG"] as string;
                var schema = DBNull.Value == row["PK_TABLE_SCHEMA"] ? string.Empty : row["PK_TABLE_SCHEMA"] as string;
                key.AddForeignColumn(catalog, schema, (string)row["PK_TABLE_NAME"], (string)row["PK_COLUMN_NAME"], true);

                catalog = DBNull.Value == row["FK_TABLE_CATALOG"] ? string.Empty : row["FK_TABLE_CATALOG"] as string;
                schema = DBNull.Value == row["FK_TABLE_SCHEMA"] ? string.Empty : row["FK_TABLE_SCHEMA"] as string;
                key.AddForeignColumn(catalog, schema, (string)row["FK_TABLE_NAME"], (string)row["FK_COLUMN_NAME"], false);
            }
            catch
            {
            }
        }
    }

    internal void PopulateArrayNoHookup(DataTable metaData)
    {
        BindToColumns(metaData);

        ForeignKey key = null;
        var keyName = "";

        foreach (DataRowView rowView in metaData.DefaultView)
        {
            var row = rowView.Row;

            keyName = row["FK_NAME"] as string;

            key = GetByName(keyName);

            if (null == key)
            {
                key = (ForeignKey)DbRoot.ClassFactory.CreateForeignKey();
                key.DbRoot = DbRoot;
                key.ForeignKeys = this;
                key.Row = row;
                _array.Add(key);
            }
        }
    }

    internal void AddForeignKey(ForeignKey fk)
    {
        var exists = this[fk.Name];

        if (null == exists) _array.Add(fk);
    }

    #region indexers

    public virtual IForeignKey this[object index]
    {
        get
        {
            if (index.GetType() == Types.StringType) return GetByPhysicalName(index as string);

            var idx = Convert.ToInt32(index);
            return _array[idx] as ForeignKey;
        }
    }

    public ForeignKey GetByName(string name)
    {
        ForeignKey obj = null;

        var count = _array.Count;
        for (var i = 0; i < count; i++)
        {
            var tmp = _array[i] as ForeignKey;

            if (CompareStrings(name, tmp.Name))
            {
                obj = tmp;
                break;
            }
        }

        return obj;
    }

    internal ForeignKey GetByPhysicalName(string name)
    {
        ForeignKey obj = null;

        var count = _array.Count;
        for (var i = 0; i < count; i++)
        {
            var tmp = _array[i] as ForeignKey;

            if (CompareStrings(name, tmp.Name))
            {
                obj = tmp;
                break;
            }
        }

        return obj;
    }

    #endregion

    #region IEnumerable Members

    IEnumerator<IForeignKey> IEnumerable<IForeignKey>.GetEnumerator()
    {
        return _array.GetEnumerator();
    }

    public IEnumerator GetEnumerator()
    {
        return _array.GetEnumerator();
    }

    #endregion

    #region XML User Data

#if ENTERPRISE
    [ComVisible(false)]
#endif
    public override string UserDataXPath
    {
        get { return Table.UserDataXPath + @"/ForeignKeys"; }
    }

#if ENTERPRISE
    [ComVisible(false)]
#endif
    internal override bool GetXmlNode(out XmlNode node, bool forceCreate)
    {
        node = null;
        var success = false;

        if (null == _xmlNode)
        {
            // Get the parent node
            XmlNode parentNode = null;
            if (Table.GetXmlNode(out parentNode, forceCreate))
            {
                // See if our user data already exists
                var xPath = @"./ForeignKeys";
                if (!GetUserData(xPath, parentNode, out _xmlNode) && forceCreate)
                {
                    // Create it, and try again
                    CreateUserMetaData(parentNode);
                    GetUserData(xPath, parentNode, out _xmlNode);
                }
            }
        }

        if (null != _xmlNode)
        {
            node = _xmlNode;
            success = true;
        }

        return success;
    }

#if ENTERPRISE
    [ComVisible(false)]
#endif
    public override void CreateUserMetaData(XmlNode parentNode)
    {
        var myNode = parentNode.OwnerDocument.CreateNode(XmlNodeType.Element, "ForeignKeys", null);
        parentNode.AppendChild(myNode);
    }

    #endregion
}