using System.Runtime.InteropServices;
using System.Xml;

namespace MyMeta;

#if ENTERPRISE

[ComVisible(false)]
[ClassInterface(ClassInterfaceType.AutoDual)]
#endif
public class ResultColumn : Single, IResultColumn, INameValueItem
{
    internal ResultColumns ResultColumns;

    #region Collections

    public virtual IPropertyCollection GlobalProperties
    {
        get
        {
            var db = ResultColumns.Procedure.Procedures.Database;
            if (null == db._resultColumnProperties)
            {
                db._resultColumnProperties = new PropertyCollection();
                db._resultColumnProperties.Parent = this;

                var xPath = GlobalUserDataXPath;
                var xmlNode = DbRoot.UserData.SelectSingleNode(xPath, null);

                if (xmlNode is null)
                {
                    var parentNode = db.CreateGlobalXmlNode();

                    xmlNode = parentNode.OwnerDocument.CreateNode(XmlNodeType.Element, "ResultColumn", null);
                    parentNode.AppendChild(xmlNode);
                }

                db._resultColumnProperties.LoadAllGlobal(xmlNode);
            }

            return db._resultColumnProperties;
        }
    }

    public virtual IPropertyCollection AllProperties
    {
        get
        {
            if (null == _allProperties)
            {
                _allProperties = new PropertyCollectionAll();
                _allProperties.Load(Properties, GlobalProperties);
            }

            return _allProperties;
        }
    }

    internal PropertyCollectionAll _allProperties;

    #endregion

    #region Properties

    public override string Alias
    {
        get
        {
            return GetXmlNode(out var node, false) && GetUserData(node, "n", out var niceName) && string.Empty != niceName ? niceName : Name;
        }

        set
        {
            if (GetXmlNode(out var node, true))
            {
                SetUserData(node, "n", value);
            }
        }
    }

    public override string Name => GetString(null);

    public virtual int DataType => 0;

    public virtual string DataTypeName => GetString(null);

    public virtual string DataTypeNameComplete => GetString(null);

    public virtual int Ordinal => GetInt32(null);

    public virtual string LanguageType
    {
        get
        {
            if (DbRoot.LanguageNode is null) return "Unknown";

            var xPath = $@"./Type[@From='{DataTypeName}']";
            var node = DbRoot.LanguageNode.SelectSingleNode(xPath, null);
            if (node is null) return "Unknown";

            return GetUserData(node, "To", out var languageType) ? languageType : "Unknown";
        }
    }

    public virtual string DbTargetType
    {
        get
        {
            if (DbRoot.DbTargetNode is null) return "Unknown";

            var xPath = $@"./Type[@From='{DataTypeName}']";
            var node = DbRoot.DbTargetNode.SelectSingleNode(xPath, null);
            if (node is null) return "Unknown";

            return GetUserData(node, "To", out var driverType) ? driverType : "Unknown";
        }
    }

    #endregion

    #region XML User Data

    public override string UserDataXPath => $@"{ResultColumns.UserDataXPath}/ResultColumn[@p='{Name}']";

    public override string GlobalUserDataXPath => $"{ResultColumns.Procedure.Procedures.Database.GlobalUserDataXPath}/ResultColumn";

    internal override bool GetXmlNode(out XmlNode node, bool forceCreate)
    {
        node = null;
        var success = false;

        if (null == _xmlNode)
        {
            // Get the parent node
            if (ResultColumns.GetXmlNode(out var parentNode, forceCreate))
            {
                // See if our user data already exists
                var xPath = $@"./ResultColumn[@p='{Name}']";
                if (!GetUserData(xPath, parentNode, out _xmlNode) && forceCreate)
                {
                    // Create it, and try again
                    CreateUserMetaData(parentNode);
                    GetUserData(xPath, parentNode, out _xmlNode);
                }
            }
        }

        if (null != _xmlNode)
        {
            node = _xmlNode;
            success = true;
        }

        return success;
    }

#if ENTERPRISE
    [ComVisible(false)]
#endif
    public override void CreateUserMetaData(XmlNode parentNode)
    {
        var myNode = parentNode.OwnerDocument.CreateNode(XmlNodeType.Element, "ResultColumn", null);
        parentNode.AppendChild(myNode);

        var attr = parentNode.OwnerDocument.CreateAttribute("p");
        attr.Value = Name;
        myNode.Attributes.Append(attr);

        attr = parentNode.OwnerDocument.CreateAttribute("n");
        attr.Value = "";
        myNode.Attributes.Append(attr);
    }

    #endregion

    #region INameValueCollection Members

    public string ItemName => Name;

    public string ItemValue => Ordinal.ToString();

    #endregion
}